package bap.jp.smartfashion.api.authentication.dto;

import javax.validation.constraints.NotEmpty;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.enums.RoleEnum.ApplicationRole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a authentication
 */
@Getter
@Setter
@Data
public class AuthenticationDTO {
	
	@ApiModelProperty(notes = "Email or phone number", required = true)
	@NotEmpty(message = "{info.email} {validation.not-empty}")
	private String userName;

	@ApiModelProperty(notes = "Password", required = true)
	@NotEmpty(message = "{info.password} {validation.not-empty}")
	private String password;

	@ApiModelProperty(notes = "Remember me")
	private boolean rememberMe;
	
	@ApiModelProperty(notes = "Token belong to device (used for MOBILE_ANDROID, MOBILE_IOS)")
	private String deviceToken;
	
	@ApiModelProperty(notes = "Platform", allowableValues = "WEB, MOBILE_ANDROID, MOBILE_IOS")
	private String platform;
	
	@ApiModelProperty(notes = "Login type"
			+ " - Type = 0: logging with admin role"
			+ " - Type = 1: logging with shop role "
			+ " - Type = 2 or don't set: logging with user role", allowableValues = "0, 1, 2")
	/**
	 * - Type = 0 then logging with admin role
	 * - Type = 1 then logging with shop role 
	 * - Type = 2 then logging with user role 
	 */
	private Integer type = 2;

	@JsonIgnore
	public Boolean isRememberMe() {
		return rememberMe;
	}
	
	@JsonIgnore
	public boolean isAdmin() {
		return this.type == 0;
	}
	
	@JsonIgnore
	public boolean isShop() {
		return this.type == 1;
	}
	
	@JsonIgnore
	public boolean isUser() {
		return this.type == 2;
	}
	
	@JsonIgnore
	public String getRole() {
		String role = StringUtils.EMPTY;
		switch (this.type) {
		case 0:
			role = ApplicationRole.ROLE_ADMIN.name();
			break;
		case 1:
			role = ApplicationRole.ROLE_SHOP.name();
			break;
		default:
			role = ApplicationRole.ROLE_USER.name();
			break;
		}
		return role;
	}
}
