package bap.jp.smartfashion.api.authentication.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *  A DTO representing a authentication SNS
 */
@Getter
@Setter
@Data
public class AuthenticationSNSDTO {
	@NotEmpty(message = "Uid is required")
	@ApiModelProperty(notes = "Uid of SNS", required = true)
	private String uid;
	
	@ApiModelProperty(notes = "Access token of SNS")
	private String accessToken;
	
	@ApiModelProperty(notes = "User name", required = true)
	private String userName;
	
	@Email(message = "Invalid email format")
	@ApiModelProperty(notes = "Email")
	private String email;
	
	@ApiModelProperty(notes = "User phone")
	private String phone;
	
	@NotEmpty(message = "User type is required")
	@ApiModelProperty(notes = "User type", allowableValues = "FACEBOOK, GOOGLE", required = true)
	private String userType;
	
	@ApiModelProperty(notes = "User avatar")
	private String avatar;
	
	@ApiModelProperty(notes = "Token belong to device (used for MOBILE_ANDROID, MOBILE_IOS)")
	private String deviceToken;
	
	@ApiModelProperty(notes = "Platform", allowableValues = "WEB, MOBILE_ANDROID, MOBILE_IOS")
	private String platform;
	
	@ApiModelProperty(hidden = true)
	private boolean registerStatus;
}
