package bap.jp.smartfashion.api.authentication;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bap.jp.smartfashion.api.authentication.dto.AuthenticationDTO;
import bap.jp.smartfashion.common.model.DeviceToken;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.repository.DeviceTokenRepository;
import bap.jp.smartfashion.common.repository.UserRepository;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;

/**
 * Service class for managing authentication.
 */
@Service
public class AuthenticationService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DeviceTokenRepository deviceTokenRepository;
	
	/**
	 * Find user by uid from SNS
	 *
	 * @param uid uid of {@link User} SNS
	 * @param userType {@link User} SNS type belong {@link UserTypeEnum}
	 * @return {@link User} type {@link Optional}
	 */
	public Optional<User> findUserLoginWithSNS(String uid, String userType) {
		return this.userRepository.findUserLoginWithSNS(uid, userType);
	};
	
	/**
	 * Find user by email or phone for local(normal user)
	 *
	 * @param emailOrPhone Email address or phone of {@link User}
	 * @return {@link User} type {@link Optional}
	 */
	public Optional<User> findUserLoginWithLocal(String emailOrPhone, String userType, String roleType) {
		return this.userRepository.findUserLoginWithLocal(emailOrPhone, userType, roleType);
	};
	
	/**
	 * Find {@link User} by userId
	 *
	 * @param user id
	 * @return {@link User} type {@link Optional}
	 */
	public Optional<User> findUserByUserId(int userId) {
		return this.userRepository.findById(userId);
	};
	
	/**
	 * Update Platform for {@link User}
	 *
	 * @param deviceToken {@link DeviceToken} deviceToken
	 * @param user {@link User}
	 * @param platform {@link User} platform
	 * @throws NotFoundException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updatePlatformLastedForUser(User user, String deviceToken, String platform) {
		platform = StringUtils.isEmpty(platform) ? UserEnum.Platform.WEB.getCode() : platform;
		DeviceToken deviceTokenRes = this.findDeviceTokenByUser_Id(user);
		if (UserEnum.Platform.WEB.getCode().equals(platform)) {
			if (deviceTokenRes.getId() != null) {
				this.deviceTokenRepository.delete(deviceTokenRes);
			}
		}else {
			deviceTokenRes.setUser(user);
			deviceTokenRes.setDeviceToken(deviceToken);
			this.deviceTokenRepository.save(deviceTokenRes);
		}
		
		user.setPlatform(platform);
		this.userRepository.save(user);
	}
	
	/**
	 * Find {@link DeviceToken} by {@link User} Id
	 * @param user {@link User}
	 * @return {@link DeviceToken}
	 */
	private DeviceToken findDeviceTokenByUser_Id(User user) {
		return this.deviceTokenRepository.findByUser_Id(user.getId()).orElse(new DeviceToken());
	}
}
