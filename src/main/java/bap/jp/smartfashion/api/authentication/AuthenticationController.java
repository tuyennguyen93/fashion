package bap.jp.smartfashion.api.authentication;

import java.util.Optional;
import javax.validation.Valid;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import bap.jp.smartfashion.api.authentication.dto.AuthenticationDTO;
import bap.jp.smartfashion.api.authentication.dto.AuthenticationSNSDTO;
import bap.jp.smartfashion.api.user.UserService;
import bap.jp.smartfashion.common.base.BaseController;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.JwtToken;
import bap.jp.smartfashion.common.vo.UserManagement;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;

/**
 * Controller to authenticate users
 * 
 */
@RestController
@RequestMapping("/api/v1/authentication")
@Api(value = "Authentication REST Endpoint", description = "Authentication API")
public class AuthenticationController extends BaseController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@Autowired 
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	private final Logger log = LoggerFactory.getLogger(AuthenticationController.class);
	
	/**
	 * Signin
	 * 
	 * @param authenticationDto {@link AuthenticationDTO} info
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws AuthenticationException
	 * @throws NotFoundException 
	 * @throws BadRequestException 
	 * @throws JsonProcessingException 
	 */
	@PostMapping("/signin")
	@ApiOperation(value = "Sign in")
	public APIResponse<JwtToken> signIn(
		@ApiParam(value = "User login information")
		@Valid
		@RequestBody AuthenticationDTO authenticationDto, Errors errors) throws AuthenticationException, NotFoundException, BadRequestException, JsonProcessingException {
		log.info("REST request to sign in: {}", authenticationDto);
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_SIGN_IN_BAD_REQUEST, messages.toString(), true);
		}
		Optional<User> userOptional = this.authenticationService.findUserLoginWithLocal(authenticationDto.getUserName(), UserEnum.UserTypeEnum.LOCAL.toString(), authenticationDto.getRole());
		if (userOptional.isPresent()) {
			User user = userOptional.get();
			if (!this.bCryptPasswordEncoder.matches(authenticationDto.getPassword(), user.getPassword())) {
				throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_INVALID_EMAIL_OR_PASSWORD, "Invalid email or password.");
			} else if (!user.isRegisterStatus()) {
				throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_USER_INACTIVE, APIConstants.USER_INACTIVE_MSG);
			} else if (user.isBlocked()) {
				throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_USER_BLOCKED, APIConstants.USER_BLOCKED_MSG);
			} else {
				if (authenticationDto.getType() == 1) {
					Shop shop = this.userService.getShopByUserId(user.getId());
					if (shop == null) {
						throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_SHOP_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG,"Shop"));
					}
					this.userService.checkShopBlockingLogin(user.getId());
				}
				Authentication authentication = this.authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(user.getId(), authenticationDto.getPassword()));// authentication username (email and phone) and password.
				SecurityContextHolder.getContext().setAuthentication(authentication);// set authentication into Security Context
				boolean isRememberMe = (authenticationDto.isRememberMe() == null) ? false : authenticationDto.isRememberMe();//set rememberMe value
				this.authenticationService.updatePlatformLastedForUser(user, authenticationDto.getDeviceToken(), authenticationDto.getPlatform());
				return APIResponse.okStatus(this.jwtForAPIResponse(Integer.toString(user.getId()), isRememberMe));
			}
		} else {
			throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_USER_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG,"User"));
		}
	}
	
	/**
	 * Signin with SNS
	 * 
	 * @param authenticationSNSDTO {@link AuthenticationSNSDTO} info
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws AuthenticationException
	 * @throws ConflictException 
	 * @throws NotFoundException 
	 * @throws JsonProcessingException 
	 * @throws BadRequestException
	 */
	@PostMapping("/signin/sns")
	@ApiOperation(value = "Sign in with SNS")
	@ApiResponses(value = {
	})
	public APIResponse<JwtToken> signInWithSNS(
		@ApiParam(value = "User SNS login information")
		@Valid
		@RequestBody AuthenticationSNSDTO authenticationSNSDto, Errors errors) throws AuthenticationException, ConflictException, NotFoundException, JsonProcessingException, BadRequestException {
		log.info("REST request to sign in SNS: {}", authenticationSNSDto);
		if (errors.hasErrors()) { // return error if invalid from hibernate
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_SIGN_IN_SNS_BAD_REQUEST, messages.toString(), true);
		}
		
		boolean isValidUserType =  EnumUtils.isValidEnum(UserEnum.UserTypeEnum.class, authenticationSNSDto.getUserType());
		if (!isValidUserType) {
			throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_INVALID_USER_TYPE, "Invalid user type");
		}
		
		Optional<User> userOptional = this.authenticationService.findUserLoginWithSNS(authenticationSNSDto.getUid(), authenticationSNSDto.getUserType());
		User user = new User();
		
		if (!userOptional.isPresent()) { //create new user if user is not existed
			user = this.userService.createUserSNS(authenticationSNSDto);
		} else {
			user = userOptional.get();
		}
		
		if (!user.isRegisterStatus()) {
			throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_USER_INACTIVE, APIConstants.USER_INACTIVE_MSG);
		} else if (user.isBlocked()) {
			throw new AuthenticationException(AuthenticationException.UNAUTHORIZED_USER_BLOCKED, APIConstants.USER_BLOCKED_MSG);
		} else {
			int userId = user.getId();
			this.authenticationService.updatePlatformLastedForUser(user, authenticationSNSDto.getDeviceToken(), authenticationSNSDto.getPlatform());
			this.userService.checkShopBlockingLogin(userId);
			return APIResponse.okStatus(this.jwtForAPIResponse(Integer.toString(userId), false));
		}
	}
	
	/**
	 * Create new users
	 * 
	 * @param userManagement {@link UserManagement} info
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws ConflictException
	 * @throws NotFoundException 
	 * @throws JsonProcessingException 
	 * @throws BadRequestException 
	 */
	@PostMapping("/signup")
	@ApiOperation(value = "Sign up")
	@ApiResponses(value = { 
	})
	public APIResponse<UserDTO> signUp(
		@ApiParam(value = "Information to create new users")
		@Valid
		@RequestBody UserManagement userManagement, Errors errors) throws ConflictException, NotFoundException, JsonProcessingException, BadRequestException {
		log.info("REST request to sign up: {}", userManagement);
		this.userService.validationEmail(errors, userManagement.getEmail());
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_SIGN_UP_BAD_REQUEST, messages.toString(), true);
		}
		User user = this.userService.createUser(userManagement);
		return APIResponse.okStatus(new UserDTO(user));
	}
}