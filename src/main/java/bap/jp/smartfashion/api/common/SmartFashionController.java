package bap.jp.smartfashion.api.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ImageUploadDTO;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PreSignedAWS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller to managing common function in application
 */
@RestController
@RequestMapping("/api/v1/")
@Api(description = "Common API for application")
public class SmartFashionController {
	
	@Autowired
	private BaseService baseService;
	
	/**
	 * Upload info to create pre-signed url in s3
	 * 
	 * @param images
	 * @return
	 */
	@PostMapping("/images/pre-signed")
	@ApiOperation(value = "Upload images with pre-signed URL")
	public APIResponse<List<PreSignedAWS>> uploadImages(
			@ApiParam(value = "List fileName", required = true) @RequestBody List<ImageUploadDTO> images)  {
		return APIResponse.okStatus(this.baseService.uploadImages(images));
	}
}