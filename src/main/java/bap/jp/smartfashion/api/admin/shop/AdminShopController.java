package bap.jp.smartfashion.api.admin.shop;

import java.util.List;

import javax.validation.Valid;

import bap.jp.smartfashion.api.admin.shop.dto.ProductInfoDTO;
import bap.jp.smartfashion.api.admin.shop.dto.ShopDetailsDTO;
import bap.jp.smartfashion.api.admin.shop.dto.ShopStatusDTO;
import bap.jp.smartfashion.common.vo.ObjectID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import bap.jp.smartfashion.api.admin.shop.dto.AdminShopDTO;
import bap.jp.smartfashion.api.admin.user.AdminUserController;
import bap.jp.smartfashion.common.dto.ShopDTO;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller to managing shops by admin
 */
@RestController
@RequestMapping("/api/v1/admin/shops")
@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
public class AdminShopController {
	
	@Autowired
	private AdminShopService adminShopService;
	
	private final Logger log = LoggerFactory.getLogger(AdminUserController.class);
	
	/**
	 * Get {@link Shop} list
	 * @param <T>
	 *
	 * @param page page number
	 * @param limit Limit on per page
	 * @return {@link APIResponse} of {@link ShopDTO}
	 */
	@GetMapping
	@ApiOperation(value = "Find shop list")
	public APIResponse<PageInfo<AdminShopDTO>> getShopList(
		@ApiParam(value = "Shop's status", required = false, allowableValues = "ALL, ACTIVE, PENDING, BLOCKED, REJECTED, SUSPENDED")
		@RequestParam(value = "status", required = false) String status,
		@ApiParam(value ="Search Shop by shop's name or shop's location or product's collection", required = false)
		@RequestParam(value = "query", required = false) String query,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",OLDEST_TO_NEWEST, NEWEST_TO_OLDEST")
		@RequestParam(value = "sort", required = false) String sort) {
		log.info("REST request to find shop list");
		return APIResponse.okStatus(this.adminShopService.getShopList(page, limit, sort, query, status));
	}
	
	/** 
	 * Update {@link Shop} by id
	 *
	 * @param shopId {@link Shop} id
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws NotFoundException if {@link Shop} not found
	 * @throws JsonProcessingException 
	 * @throws BadRequestException 
	 */
	@PutMapping(value = "/{shopId}")
	@ApiOperation(value = "Update shop by shop id")
	@ApiResponses(value = {
	})
	public APIResponse<ShopDTO> updateShop(
		@ApiParam(value = "Shop ID", required = true)
		@PathVariable(value = "shopId", required = true) int shopId,
		
		@ApiParam(value = "Information to update shop")
		@Valid
		@RequestBody ShopDTO shopDTO, Errors errors) throws NotFoundException, JsonProcessingException, BadRequestException {
		log.info("REST request to update Shop ID : {}", shopDTO);
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_SHOP_UPDATE_BAD_REQUEST, messages.toString(), true);
		}
		return APIResponse.okStatus(new ShopDTO(this.adminShopService.updateShop(shopId, shopDTO)));
	}
	
	/**
	 * Get {@link Shop} by Id
	 * 
	 * @param shopId {@link Shop} Id
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws  NotFoundException if {@link Shop} not found
	 */
	@GetMapping("/{shopId}")
	@ApiOperation(value = "Find shop by shop id")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public APIResponse<ShopDTO> findShopById(
		@ApiParam(value = "Shop ID", required =  true)
		@PathVariable(value = "shopId") int shopId) throws NotFoundException {
		log.info("REST request to find shop by shop Id: {}", shopId);
		return APIResponse.okStatus(new ShopDTO(this.adminShopService.findShopByShopId(shopId)));
	}

	/**
	 * Get shop's details
	 *
	 * @param shopId {@link Shop}
	 * @return {@link APIResponse} data is {@link ShopDetailsDTO}
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@GetMapping("/{shopId}/details")
	@ApiOperation(value = "Get shop's detail")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_BAD_REQUEST_CODE, message = APIConstants.ERROR_SHOP_IS_REJECTED)
	})
	public APIResponse<ShopDetailsDTO> getShopDetails(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId) throws NotFoundException, BadRequestException {
		return APIResponse.okStatus(this.adminShopService.getShopDetails(shopId));
	}

	/**
	 * Get List of products in shop
	 *
	 * @param shopId {@link Shop}
	 * @param page current page
	 * @param limit count of record in one page
	 * @return {@link APIResponse} data is {@link PageInfo} of {@link ProductInfo}
	 * @throws NotFoundException if shop isn't found
	 * @throws BadRequestException if Shop is rejected
	 */
	@GetMapping("/{shopId}/products")
	@ApiOperation(value = "Get list of products in shop")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_BAD_REQUEST_CODE, message = APIConstants.ERROR_SHOP_IS_REJECTED)
	})
	public APIResponse<PageInfo<ProductInfoDTO>> getProductsListOfShop(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "page", required = false)
			@RequestParam(value = "page", required = false) Integer page,
			@ApiParam(value = "limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit) throws NotFoundException, BadRequestException {
		return APIResponse.okStatus(this.adminShopService.getProductsListOfShop(shopId, page, limit));
	}

	/**
	 * Get List of shops's status
	 *
	 * @return {@link APIResponse} data is list of {@link ShopStatusDTO}
	 */
	@GetMapping("/status")
	@ApiOperation(value = "Get List of Shops's status")
	public APIResponse<List<ShopStatusDTO>> getShops_StatusList() {
		return APIResponse.okStatus(this.adminShopService.getShops_StatusList());
	}

	/**
	 * Set status for shops
	 *
	 * @param currentStatus current status of shops
	 * @param updatedStatus status is updated to shops after
	 * @param shopIdsList list of shops's ID
	 * @return Object null notice update list successfully
	 * @throws BadRequestException
	 * @throws NotFoundException
	 */
	@PutMapping("/status")
	@ApiOperation(value = "Set status for shop")
	@ApiResponses(value = {

	})
	public APIResponse<List<AdminShopDTO>> setStatusForShop(
			@ApiParam(value = "Current Status", required = true, allowableValues = "ACTIVE, PENDING, BLOCKED, REJECTED, SUSPENDED")
			@RequestParam(value = "currentStatus", required = true) String currentStatus,
			@ApiParam(value = "Updated Status", required = true, allowableValues = "BLOCK, SUSPEND, REJECT, APPROVE, REACTIVE, REOPEN")
			@RequestParam(value = "updatedStatus", required = true) String updatedStatus,
			@ApiParam(value = "List of Shop's ID")
			@RequestBody ObjectID objectID) throws BadRequestException, NotFoundException {
		return APIResponse.okStatus(this.adminShopService.setStatusForShop(currentStatus, updatedStatus, objectID));
	}
}
