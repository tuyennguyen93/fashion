package bap.jp.smartfashion.api.admin.shop;

import bap.jp.smartfashion.api.admin.shop.dto.AdminShopStatisticDTO;
import bap.jp.smartfashion.api.admin.shop.dto.AdminShopStatus;
import bap.jp.smartfashion.api.admin.shop.dto.ProductInfoDTO;
import bap.jp.smartfashion.api.admin.shop.dto.ShopDetailsDTO;
import bap.jp.smartfashion.api.admin.shop.dto.ShopStatusDTO;
import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.model.UserBankAccount;
import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.UserBankAccountRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.enums.ShopEnum;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.CodeEnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.api.admin.shop.dto.AdminShopDTO;
import bap.jp.smartfashion.api.user.shop.UserShopService;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ShopDTO;
import bap.jp.smartfashion.common.mapper.ShopMapper;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.repository.ShopRepository;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing shops by admin.
 */
@Service
public class AdminShopService extends BaseService {

	@Autowired
	private ShopRepository shopRepository;
	
	@Autowired
	private ShopMapper shopMapper;

	@Autowired
	private UserBankAccountRepository userBankAccountRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private MessageService messageService;
	
	private final Logger log = LoggerFactory.getLogger(AdminShopService.class);
	
	/** 
	 * Update {@link Shop} by id
	 *
	 * @param shopId {@link Shop} id
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws NotFoundException if {@link Shop} not found
	 */
	public Shop updateShop(int shopId, ShopDTO shopDTO) throws NotFoundException {
		Shop shop = this.findShopByShopId(shopId);
		shop = this.shopMapper.shopDTOToShop(shopDTO, shop); //set data from ShopDTO to shop

		return this.shopRepository.save(shop);// save shop
	}
	
	/**
	 * Get {@link Shop} list
	 * @param <T>
	 *
	 * @param page page number
	 * @param limit Limit on per page
	 * @return {@link PageInfo} of {@link ShopDTO}
	 */
//	@Cacheable(value = UserShopService.SHOP_LIST_CACHE)
	public PageInfo<AdminShopDTO> getShopList(Integer page, Integer limit, String sortStr, String query, String status) {
		Sort sort = Sort.by(Order.asc("id"));
		switch (sortStr) {
			case "OLDEST_TO_NEWEST":
				sort = Sort.by(Order.asc("created"));
			break;

			case "NEWEST_TO_OLDEST":
				sort = Sort.by(Order.desc("created"));
			break;
		}

		String resultStatus = CodeEnumUtils.fromCodeQuietly(ShopEnum.ShopStatus.class, status) != null ? status : StringUtils.EMPTY;
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		query = StringUtils.defaultString(query);
		Page<AdminShopDTO> data = this.shopRepository.statisticAllShops(query, resultStatus, pageRequest).map(this::convertAdminShopStatisticDTOToAdminShopDTO);
		PageInfo<AdminShopDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		
		return pageInfo;
	}
	
	/**
	 * Get {@link Shop} by Id
	 * 
	 * @param id {@link Shop} Id
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws  NotFoundException if {@link Shop} not found
	 */
	public Shop findShopByShopId(int shopId) throws NotFoundException {
		Shop shop = this.shopRepository.findById(shopId).orElseThrow(() -> 
			new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND, String.format(
					this.messageService.getMessage("error.not-found-msg"),
					this.messageService.getMessage("info.shop")), HttpStatus.NOT_FOUND));
		
		return shop;
	}

	/**
	 * Get shop's details
	 *
	 * @param shopId {@link Shop}
	 * @return {@link ShopDetailsDTO}
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	public ShopDetailsDTO getShopDetails(Integer shopId) throws NotFoundException, BadRequestException {
		Shop shop = this.shopRepository.findById(shopId).orElse(null);
		if(shop == null) {
			throw new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("info.shop")), HttpStatus.NOT_FOUND);
		} else {
			if(ShopEnum.ShopStatus.REJECTED.getCode().equals(shop.getStatus())) {
				throw new BadRequestException(SmartFashionException.ERROR_SHOP_STATUS_REJECT,
					this.messageService.getMessage("error.shop-is-rejected"), false);
			}
		}
		ShopDetailsDTO shopDetailsDTO = new ShopDetailsDTO();
		shopDetailsDTO.setShop(new ShopDTO(shop));
		shopDetailsDTO.setUser(new UserDTO(shop.getUser()));
		shopDetailsDTO.setShopBranches(shop.getShopBranches());
		shopDetailsDTO.setShopWarehouses(shop.getShopWareHouses());
		if(shop.getUser().getId() != null) {
			Optional<UserBankAccount> userBankAccount = this.userBankAccountRepository.findOneByUser_Id(shop.getUser().getId());
			if(userBankAccount.isPresent()) {
				shopDetailsDTO.setUserBankAccount(userBankAccount.get());
			}
		}

		return shopDetailsDTO;
	}

	/**
	 * Get List of products in shop
	 *
	 * @param shopId {@link Shop}
	 * @param page current page
	 * @param limit count of record in one page
	 * @return {@link APIResponse} data is {@link PageInfo} of {@link ProductInfoDTO}
	 * @throws NotFoundException if shop isn't found
	 * @throws BadRequestException if Shop is rejected
	 */
	public PageInfo<ProductInfoDTO> getProductsListOfShop(Integer shopId, Integer page, Integer limit) throws NotFoundException, BadRequestException {
		Shop shop = this.shopRepository.findById(shopId).orElse(null);
		if(shop == null) {
			throw new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("info.shop")), HttpStatus.NOT_FOUND);
		} else {
			if(ShopEnum.ShopStatus.REJECTED.getCode().equals(shop.getStatus())) {
				throw new BadRequestException(SmartFashionException.ERROR_SHOP_STATUS_REJECT,
					this.messageService.getMessage("error.shop-is-rejected"), false);
			}
		}
		Sort sort = Sort.by(Sort.Order.desc("created"));
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Page<ProductInfoDTO> convertProductInfo = this.productRepository.findAllByShop_IdAndStatus(shopId, ProductEnum.ProductStatus.ACTIVE.getCode(), pageRequest).map(this::convertProductToProductInfo);
		PageInfo<ProductInfoDTO> productInfos = SmartFashionUtils.pagingResponse(convertProductInfo);
		return productInfos;
	}

	/**
	 * Get List of Shops's status
	 *
	 * @return List of {@link ShopStatusDTO}
	 */
	public List<ShopStatusDTO> getShops_StatusList() {
		List<ShopStatusDTO> shopStatusDTOs = new ArrayList<>();
		AdminShopStatus adminShopStatus = this.shopRepository.findAllByShopStatus();
		shopStatusDTOs.add(new ShopStatusDTO(ShopEnum.ShopStatus.ACTIVE.getCode(), adminShopStatus.getActive()));
		shopStatusDTOs.add(new ShopStatusDTO(ShopEnum.ShopStatus.PENDING.getCode(), adminShopStatus.getPending()));
		shopStatusDTOs.add(new ShopStatusDTO(ShopEnum.ShopStatus.REJECTED.getCode(), adminShopStatus.getRejected()));
		shopStatusDTOs.add(new ShopStatusDTO(ShopEnum.ShopStatus.BLOCKED.getCode(), adminShopStatus.getBlocked()));
		shopStatusDTOs.add(new ShopStatusDTO(ShopEnum.ShopStatus.SUSPENDED.getCode(), adminShopStatus.getSuspended()));
		shopStatusDTOs.add(0, new ShopStatusDTO("ALL", shopStatusDTOs.stream().mapToLong(shopStatusDTO -> shopStatusDTO.getAmount()).sum()));

		return shopStatusDTOs;
	}

	/**
	 * Convert Product to ProductInfoDTO
	 *
	 * @param product {@link Product}
	 * @return {@link ProductInfoDTO}
	 */
	private ProductInfoDTO convertProductToProductInfo(Product product) {
			ProductInfoDTO productInfo = new ProductInfoDTO();
			productInfo.setName(product.getName());
			productInfo.setId(product.getId());
			String collectionIcon = StringUtils.substring(product.getProductCollection().getName(), 0, 1);
			productInfo.setCollection(collectionIcon.toUpperCase());
			productInfo.setItems(product.getProductDetails().size());

			int countPassedItems = 0;
			for(ProductDetail productDetail : product.getProductDetails()) {
				if(!StringUtils.isEmpty(productDetail.getKeyPoint())) {
					countPassedItems++;
				}
			}
			if(countPassedItems > 0 && countPassedItems == product.getProductDetails().size()) {
				productInfo.setKeyPointStatus("All Pass");
			} else if(countPassedItems > 0 && countPassedItems < product.getProductDetails().size()){
				if(String.valueOf(countPassedItems).length() == 1) {
					String count = StringUtils.leftPad(String.valueOf(countPassedItems), 2, "0");
					productInfo.setKeyPointStatus(String.format("%s Pass", count));
				} else {
					productInfo.setKeyPointStatus(String.format("%s Pass", String.valueOf(countPassedItems)));
				}
			} else {
				productInfo.setKeyPointStatus("No Pass");
			}
			Optional<ProductInteraction> foundProductInteraction = product.getProductInteractions().stream().filter(productInteraction -> productInteraction.getActionCode().equals(ProductEnum.ProductInteraction.TRY_PRODUCT.getCode())).findFirst();
			if(foundProductInteraction.isPresent()) {
				productInfo.setTried(foundProductInteraction.get().getActionCount());
			}

			productInfo.setCreated(Date.from(product.getCreated().atZone( ZoneId.systemDefault()).toInstant()));

		return productInfo;
	}

	/**
	 * Convert Shop to AdminShopDTO
	 *
	 * @param shop {@link Shop}
	 * @return {@link AdminShopDTO}
	 */
	private AdminShopDTO convertAdminShopStatisticDTOToAdminShopDTO(AdminShopStatisticDTO adminShopStatisticDTO) {
		AdminShopDTO adminShopDTO = new AdminShopDTO(adminShopStatisticDTO);

		// get list of categories from group string of collections
		if(!StringUtils.isEmpty(adminShopStatisticDTO.getCollections())) {
			String[] firstCharacterCollections =  adminShopStatisticDTO.getCollections().split(",");
			Arrays.sort(firstCharacterCollections, Comparator.reverseOrder());
			adminShopDTO.setCategories(Arrays.asList(firstCharacterCollections));
		}

		// Get list of locations
		if(!StringUtils.isEmpty(adminShopStatisticDTO.getBusinessAddress())) {
			List<String> locations =new ArrayList<>();
			String[] businessAddresses = adminShopStatisticDTO.getBusinessAddress().split(";");
			for(String address : Arrays.asList(businessAddresses)) {
				String[] data = address.split(", ");
				int indexAdd = data.length > 1 ? data.length-2 : 0;
				String city = data[indexAdd];
				locations.add(city);
			}
			adminShopDTO.setLocations(locations.stream().sorted().collect(Collectors.toList()));
		}

		return adminShopDTO;
	}

    /**
     * Set status for shops
     *
     * @param currentStatus current status of shops
     * @param updatedStatus status is updated to shops after
     * @param shopID {@link ObjectID} list of shops's ID
     * @return Object null notice update list successfully
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public List<AdminShopDTO> setStatusForShop(String currentStatus, String updatedStatus, ObjectID shopID) throws BadRequestException, NotFoundException {
        List<Shop> shops = new ArrayList<>();
        String resultCurrentStatus = CodeEnumUtils.fromCodeQuietly(ShopEnum.ShopStatus.class, currentStatus) != null ? currentStatus : "";
        String resultUpdatedStatus = "";
        if(!resultCurrentStatus.equalsIgnoreCase("") && !StringUtils.isEmpty(updatedStatus)) {
            switch (resultCurrentStatus) {
                case "ACTIVE": {
                    if(updatedStatus.equalsIgnoreCase("BLOCK")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.BLOCKED.getCode();
                    } else if(updatedStatus.equalsIgnoreCase("SUSPEND")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.SUSPENDED.getCode();
                    }
                }
                break;
                case "BLOCKED": {
                    if(updatedStatus.equalsIgnoreCase("REOPEN")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.ACTIVE.getCode();
                    }
                }
                break;
                case "PENDING": {
                    if(updatedStatus.equalsIgnoreCase("APPROVE")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.ACTIVE.getCode();
                    } else if(updatedStatus.equalsIgnoreCase("REJECT")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.REJECTED.getCode();
                    }
                }
                break;
                case "SUSPENDED": {
                    if(updatedStatus.equalsIgnoreCase("REACTIVE")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.ACTIVE.getCode();
                    } else if(updatedStatus.equalsIgnoreCase("BLOCK")) {
                        resultUpdatedStatus = ShopEnum.ShopStatus.BLOCKED.getCode();
                    }
                }
                break;
            }

            if(!StringUtils.isEmpty(resultUpdatedStatus)) {
                if(shopID.getIdsList().isEmpty()) {
                    throw new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND,
							String.format(this.messageService.getMessage("error.not-found-msg"),
									this.messageService.getMessage("list-shops-ID")), HttpStatus.NOT_FOUND);
                }
                for(Integer shopId : shopID.getIdsList()) {
                    Shop exitedShop = this.checkExitedShop(shopId);
                    if(!resultCurrentStatus.equals(exitedShop.getStatus())) {
                        throw new BadRequestException(BadRequestException.ERROR_SHOP_STATUS_BAD_REQUEST,
                                String.format(this.messageService.getMessage("error.bad-value-msg"),
										this.messageService.getMessage("status-shop-with-ID") + " :" + shopId), false);
                    }
                    exitedShop.setStatus(resultUpdatedStatus);
                    shops.add(exitedShop);
                }
                shops = this.shopRepository.saveAll(shops);
            } else {
                throw new BadRequestException(BadRequestException.ERROR_SHOP_STATUS_BAD_REQUEST,
                        String.format(this.messageService.getMessage("error.bad-value-msg"),
								this.messageService.getMessage("updated-status")), false);
            }
        } else {
            throw new BadRequestException(BadRequestException.ERROR_SHOP_STATUS_BAD_REQUEST,
                    String.format(this.messageService.getMessage("error.bad.value-msg"),
							this.messageService.getMessage("current-updated-status")), false);
        }
        return shops.stream().map(AdminShopDTO::new).collect(Collectors.toList());
    }
}
