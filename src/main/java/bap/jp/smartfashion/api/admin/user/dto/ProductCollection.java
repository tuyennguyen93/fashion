package bap.jp.smartfashion.api.admin.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ProductCollection {
    private Integer collectionId;
    private long amount;
}