package bap.jp.smartfashion.api.admin.product.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductColorDTO;
import bap.jp.smartfashion.common.model.ProductColor;
import bap.jp.smartfashion.common.repository.ProductColorRepository;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

@Service
public class ProductColorService extends BaseService {

    @Autowired
    private ProductColorRepository productColorRepository;

    public List<ProductColorDTO> getProductColorList() {
        List<ProductColorDTO> productColorDTOs = this.productColorRepository.findAll().stream().map(ProductColorDTO::new).collect(Collectors.toList());
        return productColorDTOs;
    }

    public ProductColorDTO createColor(ProductColorDTO productColorDto) throws ConflictException {
        if (this.findOneByName(productColorDto) != null) {
            throw new ConflictException(ConflictException.ERROR_PRODUCT_COLOR_EXITED,
                String.format(APIConstants.ERROR_EXISTED_MSG, "Product color"));
        }
        ProductColor productColor = new ProductColor();
        BeanUtils.copyProperties(productColorDto, productColor);
        productColorDto = new ProductColorDTO(this.productColorRepository.save(productColor));

        return productColorDto;
    }

    public ProductColorDTO updateColor(Integer productColorId, ProductColorDTO productColorDto)
            throws ConflictException, NotFoundException {

        ProductColor existproductColor = this.findOneByName(productColorDto);
        if (existproductColor != null && productColorId != existproductColor.getId()) {
            throw new ConflictException(ConflictException.ERROR_PRODUCT_COLOR_EXITED,
                String.format(APIConstants.ERROR_EXISTED_MSG, "Product color"));
        }
        ProductColor productColor = this.findProductColorById(productColorId);
        productColor.setName(productColorDto.getName());
        productColor.setColorCode(productColorDto.getColorCode());
        productColorDto = new ProductColorDTO(this.productColorRepository.save(productColor));

        return productColorDto;
    }

    public void deleteColor(Integer productColorId) throws NotFoundException {
        this.findProductColorById(productColorId);
        this.productColorRepository.deleteById(productColorId);
    }

    private ProductColor findOneByName(ProductColorDTO productColorDTO) {
        return this.productColorRepository.findOneByName(productColorDTO.getName()).orElse(null);
    }

    private ProductColor findProductColorById(Integer productColorId) throws NotFoundException {
        return this.productColorRepository.findById(productColorId).orElseThrow(() ->
                new NotFoundException(NotFoundException.ERROR_PRODUCT_COLOR_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product color"), HttpStatus.NOT_FOUND));
    }
}
