package bap.jp.smartfashion.api.admin.user_admin.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserProperty {
    private Integer userId;
}
