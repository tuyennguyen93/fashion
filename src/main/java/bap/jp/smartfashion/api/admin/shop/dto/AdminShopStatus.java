package bap.jp.smartfashion.api.admin.shop.dto;

public interface AdminShopStatus {
    long getActive();
    long getPending();
    long getRejected();
    long getBlocked();
    long getSuspended();
}
