package bap.jp.smartfashion.api.admin.product;

import bap.jp.smartfashion.api.admin.product.dto.AdminProductDTO;
import bap.jp.smartfashion.api.admin.product.dto.AdminProductDetailDTO;
import bap.jp.smartfashion.api.admin.product.dto.ProductStatusDTO;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.CodeEnumUtils;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdminProductService extends BaseService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private MessageService messageService;

    /**
     * Get List of Products's details
     *
     * @param status status of product
     * @param sortStr sort by "OLDEST_TO_NEWEST, NEWEST_TO_OLDEST"
     * @param query search by Product's name or Product code or Gender or Shop's name
     * @param page current page
     * @param limit limit in a page
     * @return {@link PageInfo} {@link AdminProductDTO}
     */
    public PageInfo<AdminProductDTO> getProductsList(String status, String sortStr, String query, Integer page, Integer limit) {
        Sort sort = Sort.by(Sort.Order.asc("id"));
        switch (sortStr) {
            case "OLDEST_TO_NEWEST":
                sort = Sort.by(Sort.Order.asc("created"));
                break;

            case "NEWEST_TO_OLDEST":
                sort = Sort.by(Sort.Order.desc("created"));
                break;
        }
        String resultStatus = CodeEnumUtils.fromCodeQuietly(ProductEnum.ProductStatus.class, status) != null ? status : "";
        String searchInfo = StringUtils.isEmpty(query) ? StringUtils.EMPTY : query;
        PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
        Page<AdminProductDTO> data = this.productRepository
                .findAllByConditionsForAdmin(searchInfo, resultStatus, pageRequest).map(this::convertProductToAdminProductDTO);

        PageInfo<AdminProductDTO> productDetailsList = SmartFashionUtils.pagingResponse(data);

        return productDetailsList;
    }

    /**
     * Convert Product to AdminProductDTO
     *
     * @param product {@link Product}
     * @return {@link AdminProductDTO}
     */
    private AdminProductDTO convertProductToAdminProductDTO(Product product) {
        AdminProductDTO productDTO = new AdminProductDTO(product);
        Optional<ProductInteraction> productInteraction = product.getProductInteractions()
                .stream().filter(interaction-> interaction.getActionCode().equals(ProductEnum.ProductInteraction.TRY_PRODUCT.getCode())).findFirst();
        if(productInteraction.isPresent()) {
            productDTO.setTriedTime(productInteraction.get().getActionCount());
        }

        int countPassedItems = 0;
        for(ProductDetail productDetail : product.getProductDetails()) {
            if(!StringUtils.isEmpty(productDetail.getKeyPoint())) {
                countPassedItems++;
            }
        }
        if(countPassedItems > 0 && countPassedItems == product.getProductDetails().size()) {
            productDTO.setItemsKeyPointStatus("All Pass");
        } else if(countPassedItems > 0 && countPassedItems < product.getProductDetails().size()){
            if(String.valueOf(countPassedItems).length() == 1) {
                String count = StringUtils.leftPad(String.valueOf(countPassedItems), 2, "0");
                productDTO.setItemsKeyPointStatus(String.format("%s Pass", count));
            } else {
                productDTO.setItemsKeyPointStatus(String.format("%s Pass", String.valueOf(countPassedItems)));
            }
        } else {
            productDTO.setItemsKeyPointStatus("No Pass");
        }
        return productDTO;
    }

    /**
     * Get List of Products's status with Amount
     *
     * @return List of {@link ProductStatusDTO}
     */
    public List<ProductStatusDTO> getProducts_StatusList() {
        List<ProductStatusDTO> productStatusList = this.productRepository.findStatusAndAmountListForAdmin();
        long allCount = 0;
        boolean activeStatus = false;
        boolean pendingStatus = false;
        for(ProductStatusDTO  productStatusDTO : productStatusList) {
            if(ProductEnum.ProductStatus.ACTIVE.getCode().equals(productStatusDTO.getStatus())) {
                activeStatus = true;
            } else if(ProductEnum.ProductStatus.PENDING.getCode().equals(productStatusDTO.getStatus())) {
                pendingStatus = true;
            }
            allCount = allCount + productStatusDTO.getAmount();
        }
        if(!pendingStatus) {
            productStatusList.add(new ProductStatusDTO(ProductEnum.ProductStatus.PENDING.getCode(),  0));
        }
        if(!activeStatus) {
            productStatusList.add(0, new ProductStatusDTO(ProductEnum.ProductStatus.ACTIVE.getCode(), 0));
        }
        productStatusList.add(0, new ProductStatusDTO("ALL", allCount));
        return productStatusList;
    }

    /**
     * Get Product's details
     *
     * @param productId {@link Product}
     * @return {@link AdminProductDetailDTO}
     * @throws NotFoundException
     */
    public AdminProductDetailDTO getProductDetails(Integer productId) throws NotFoundException {
        Product exitedProduct = this.productRepository.findByIdForAdmin(productId).orElseThrow(
                ()-> new NotFoundException(NotFoundException.ERROR_PRODUCT_DETAIL_NOT_FOUND,
                        String.format(this.messageService.getMessage("error.not-found-msg"),
                                this.messageService.getMessage("product-with-ID")+ ": " + productId), HttpStatus.NOT_FOUND));
        AdminProductDetailDTO productDetails = new AdminProductDetailDTO(exitedProduct);
        String avatar= "";
        int index = 1, passedItems = 0;
        List<AdminProductDetailDTO.PassedItems> passedItemsList = new ArrayList<>();
        for(ProductDetail productDetail : exitedProduct.getProductDetails()) {
            if(!StringUtils.isEmpty(productDetail.getKeyPoint())) {
                passedItems++;
                passedItemsList.add(new AdminProductDetailDTO().new PassedItems(index, productDetail.getProductPhotos().get(0).getUrlOriginal()));
                if(StringUtils.isEmpty(avatar)) {
                    avatar = productDetail.getProductPhotos().get(0).getUrlOriginal();
                }
            }
            index++;
        }
        productDetails.setAvatarImage(avatar);
        productDetails.setPassedItems(passedItems);
        productDetails.setPhotosList(passedItemsList);
        Optional<ProductInteraction> triedAction = exitedProduct.getProductInteractions()
                .stream().filter(interaction -> ProductEnum.ProductInteraction.TRY_PRODUCT.getCode().equals(interaction.getActionCode())).findFirst();
        if(triedAction.isPresent()) {
            productDetails.setTriedTime(triedAction.get().getActionCount());
        }
        String productTypeParentName = this.productService.getProductTypeParentName(exitedProduct.getProductType().getProductTypeParentId());
        productDetails.getProductType().setProductTypeParentName(productTypeParentName);

        return productDetails;
    }
}
