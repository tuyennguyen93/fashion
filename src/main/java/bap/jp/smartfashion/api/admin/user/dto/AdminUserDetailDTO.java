package bap.jp.smartfashion.api.admin.user.dto;

import bap.jp.smartfashion.common.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AdminUserDetailDTO {
    private Integer id;
    private String name;
    private String avatar;
    private String phone;
    private String email;
    private Integer gender;

    // followed shops amount
    private Integer followed = 0;

    // favorited products amount
    private long favorited;

    // tried products amount
    private long tried;

    public AdminUserDetailDTO(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.avatar = user.getAvatar();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.gender = user.getGender();
    }

}