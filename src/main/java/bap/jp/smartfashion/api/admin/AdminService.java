package bap.jp.smartfashion.api.admin;

import java.util.Optional;

import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.Role;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.JwtToken;
import bap.jp.smartfashion.config.UserPrinciple;
import bap.jp.smartfashion.config.jwt.JwtProvider;
import bap.jp.smartfashion.enums.RoleEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.repository.UserRepository;

/**
 * Service class for managing admin.
 */
@Service
public class AdminService extends BaseService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private MessageService messageService;

	/**
	 * Find user by email or phone for local(admin user)
	 *
	 * @param emailOrPhone Email address or phone of {@link User}
	 * @return {@link User} type {@link Optional}
	 */
	public Optional<User> findUserLoginWithLocalForAdmin(String emailOrPhone, String userType, String roleType) {
		return this.userRepository.findUserLoginWithLocal(emailOrPhone, userType, roleType);
	};

	/**
	 * Get admin info by token
	 *
	 * @param jwtToken {@link JwtToken}
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public UserDTO getAdminInfoByToken(String jwtToken) throws NotFoundException {
		String jwt = SmartFashionUtils.getJwtToken(jwtToken);
		int userId = Integer.parseInt(this.jwtProvider.getUserNameFromJwtToken(jwt)); // return userid

		UserDTO userDTO = this.userRepository.findById(userId).map(UserDTO::new).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("info.admin")), HttpStatus.NOT_FOUND));

		Shop shop = this.getShopByUserId(userId);
		userDTO.setShop(shop);
		return userDTO;
	}
}
