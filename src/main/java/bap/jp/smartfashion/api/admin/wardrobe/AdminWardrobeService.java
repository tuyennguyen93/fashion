package bap.jp.smartfashion.api.admin.wardrobe;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeDTO;
import bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeTypeDTO;
import bap.jp.smartfashion.api.user.model.UserModelService;
import bap.jp.smartfashion.api.user.product.UserProductService;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.UserProductCollectionDTO;
import bap.jp.smartfashion.common.repository.UserModelRepository;
import bap.jp.smartfashion.common.repository.UserProductRepository;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.util.SmartFashionUtils;

/**
 * Service class for managing model by admin.
 */
@Service
public class AdminWardrobeService extends BaseService {
	
	@Autowired
	private UserModelRepository userModelRepository;
	
	@Autowired
	private UserProductRepository userProductRepository;
	
	@Autowired
	private UserProductService userProductService;
	
	@Autowired
	private UserModelService userModelService;
	
	/**
	 * Get user list
	 * @param page page to get data
	 * @param limit limit each page
	 * @param sortStr sort by
	 * @param query query to search data
	 * @param collectionType
	 * @return PageInfo<AdminWardrobeDTO>
	 */
	public PageInfo<AdminWardrobeDTO> getUserModelList(Integer page,Integer limit,String sortStr, String query, Integer collectionType) {
		Sort sort = Sort.by(Order.asc("id"));
		switch (StringUtils.defaultString(sortStr)) {
			case "OLDEST_TO_NEWEST":
				sort = Sort.by(Order.asc("created"));
				break;
			case "NEWEST_TO_OLDEST":
				sort = Sort.by(Order.desc("created"));
				break;
			default:
				break;
		}
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		query = StringUtils.defaultString(query);
		Page<AdminWardrobeDTO> data = null;
		if (collectionType == null || collectionType == 0) {
			data =  this.userModelRepository.findAllByUser_NameContainingOrUser_EmailContainingAndUser_IdIsNotNull(query, query, pageRequest).map(model -> {
				return new AdminWardrobeDTO(model, true);
			});
		} else {
			data =  this.userModelRepository.findAllByUserModelCollection_IdAndUser_NameContainingOrUser_EmailContaining(collectionType, query, query, pageRequest).map(model -> {
				return new AdminWardrobeDTO(model, true);
			});
		}
		PageInfo<AdminWardrobeDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * Get product list
	 * @param page page to get data
	 * @param limit limit each page
	 * @param sortStr sort by
	 * @param query query to search data
	 * @param collectionType
	 * @return PageInfo<AdminWardrobeDTO>
	 */
	public PageInfo<AdminWardrobeDTO> getUserProductList(Integer page,Integer limit,String sortStr, String query, Integer collectionType) {
		Sort sort = Sort.by(Order.asc("id"));
		switch (StringUtils.defaultString(sortStr)) {
			case "OLDEST_TO_NEWEST":
				sort = Sort.by(Order.asc("created"));
				break;
			case "NEWEST_TO_OLDEST":
				sort = Sort.by(Order.desc("created"));
				break;
			default:
				break;
		}
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		query = StringUtils.defaultString(query);
		Page<AdminWardrobeDTO> data = null;
		if (collectionType == null || collectionType == 0) {
			data =  this.userProductRepository.findAllByUser_NameContainingOrUser_EmailContaining(query, query, pageRequest).map(product -> {
				return new AdminWardrobeDTO(product, false);
			});
		} else {
			data =  this.userProductRepository.findAllByUserProductCollection_IdAndUser_NameContainingOrUser_EmailContaining(collectionType, query, query, pageRequest).map(product -> {
				return new AdminWardrobeDTO(product, false);
			});
		}
		PageInfo<AdminWardrobeDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * Get all wardrobe type
	 * @return List<AdminWardrobeTypeDTO>
	 */
	public List<AdminWardrobeTypeDTO> getWardrobeTypes() {
		List<AdminWardrobeTypeDTO> wardrobeTypeDTOs = new ArrayList<AdminWardrobeTypeDTO>();
		long modelCount = this.userModelRepository.countByUser_IdIsNotNull();
		long productCount = this.userProductRepository.count();
		AdminWardrobeTypeDTO wardrobeTypeDTOModel = new AdminWardrobeTypeDTO(null, "MODEL", modelCount);
		AdminWardrobeTypeDTO wardrobeTypeDTOProduct = new AdminWardrobeTypeDTO(null, "WARDROBE", productCount);
		wardrobeTypeDTOs.add(wardrobeTypeDTOModel);
		wardrobeTypeDTOs.add(wardrobeTypeDTOProduct);
		return wardrobeTypeDTOs;
	}
	
	/**
	 * Get user product collection type
	 * @return List<AdminWardrobeTypeDTO>
	 */
	public List<AdminWardrobeTypeDTO> getUserProductCollectionType() {
		List<AdminWardrobeTypeDTO> wardrobeTypeDTOs = this.userProductRepository.countUserProductCollection();
		List<AdminWardrobeTypeDTO> wardrobeTypeDTOsMapping = this.userProductService.getListUserProductCollection().stream().map(userProduct -> {
			AdminWardrobeTypeDTO adminWardrobeTypeDTO = wardrobeTypeDTOs.stream()
				.filter(data -> userProduct.getId().equals(data.getId()))
				.findFirst()
				.orElse(new AdminWardrobeTypeDTO(userProduct.getId(), userProduct.getName(), 0));
			return adminWardrobeTypeDTO;
		}).collect(Collectors.toList());
		wardrobeTypeDTOsMapping.add(0, new AdminWardrobeTypeDTO(0, "All", wardrobeTypeDTOs.stream().mapToLong(i -> i.getAmount()).sum()));
		return wardrobeTypeDTOsMapping;
	}
	
	/**
	 * Get user model collection type
	 * @return List<AdminWardrobeTypeDTO>
	 */
	public List<AdminWardrobeTypeDTO> getUserModelCollectionType() {
		List<AdminWardrobeTypeDTO> wardrobeTypeDTOs = this.userModelRepository.countUserModelCollection();
		List<AdminWardrobeTypeDTO> wardrobeTypeDTOsMapping = this.userModelService.getListUserModelCollection().stream().map(userProduct -> {
			AdminWardrobeTypeDTO adminWardrobeTypeDTO = wardrobeTypeDTOs.stream()
				.filter(data -> userProduct.getId().equals(data.getId()))
				.findFirst()
				.orElse(new AdminWardrobeTypeDTO(userProduct.getId(), userProduct.getName(), 0));
			return adminWardrobeTypeDTO;
		}).collect(Collectors.toList());
		wardrobeTypeDTOsMapping.add(0, new AdminWardrobeTypeDTO(0, "All", wardrobeTypeDTOs.stream().mapToLong(i -> i.getAmount()).sum()));
		return wardrobeTypeDTOsMapping;
	}
}
