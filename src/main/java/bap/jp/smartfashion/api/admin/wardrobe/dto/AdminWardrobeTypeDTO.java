package bap.jp.smartfashion.api.admin.wardrobe.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminWardrobeTypeDTO {
	
	@JsonInclude(value = Include.NON_NULL)
	private Integer id;
	private String name;
	private long amount = 0;
	
	
	
	public AdminWardrobeTypeDTO(String name, long amount) {
		this.name = name;
		this.amount = amount;
	}

	public AdminWardrobeTypeDTO(Integer id, String name, long amount) {
		this.id = id;
		this.name = name;
		this.amount = amount;
	}

	public AdminWardrobeTypeDTO() {
	}
}
