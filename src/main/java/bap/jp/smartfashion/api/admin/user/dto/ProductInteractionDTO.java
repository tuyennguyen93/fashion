package bap.jp.smartfashion.api.admin.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ProductInteractionDTO {
    private String actionCode;
    private long actionCount;
}