package bap.jp.smartfashion.api.admin.product.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ProductStatusDTO {
    private String status;
    private long amount;
}
