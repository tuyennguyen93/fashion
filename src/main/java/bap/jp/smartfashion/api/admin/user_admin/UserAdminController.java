package bap.jp.smartfashion.api.admin.user_admin;

import bap.jp.smartfashion.api.admin.user_admin.dto.UserAdminDTO;
import bap.jp.smartfashion.api.admin.user_admin.dto.UserEmailDTO;
import bap.jp.smartfashion.api.admin.user_admin.dto.UserProperty;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/user-admin")
@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN')")
public class UserAdminController {

    @Autowired
    private UserAdminService userAdminService;

    /**
     * Set role_admin for user
     *
     * @param userProperty {@link UserProperty}
     * @return {@link APIResponse} notice in case update successfully
     * @throws NotFoundException if user isn't found
     * @throws BadRequestException
     */
    @PutMapping("/roles")
    @ApiOperation(value = "Set role_admin for user")
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
            @ApiResponse(code = APIConstants.ERROR_BAD_REQUEST_CODE, message = APIConstants.ERROR_ACCOUNT_IS_BLOCKED),
            @ApiResponse(code = APIConstants.ERROR_BAD_REQUEST_CODE, message = APIConstants.ERROR_ACCOUNT_IS_DELETED)
    })
    public APIResponse<?> setAdminRoleForUser(
            @ApiParam(value = "User ID", required = true)
            @RequestBody UserProperty userProperty) throws NotFoundException, BadRequestException {
        return APIResponse.okStatus(this.userAdminService.setAdminRoleForUser(userProperty));
    }

    /**
     * Delete Role_Admin to Users
     *
     * @param objectID {@link ObjectID}
     * @return {@link APIResponse} notice in case delete successfully
     * @throws NotFoundException
     */
    @DeleteMapping("/roles")
    @ApiOperation(value = "Delete Role_Admin to Users")
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
    })
    public APIResponse<?> deleteAdminRoleOfUsers(
            @ApiParam(value = "List of Users's ID")
            @RequestBody ObjectID objectID) throws NotFoundException {
        return APIResponse.okStatus(this.userAdminService.deleteAdminRoleOfUsers(objectID));
    }

    /**
     * get list of User Admin
     *
     * @param query Search Info: user's name or user's email
     * @param page current page
     * @param limit amount record in a page
     * @return {@link PageInfo} {@link UserAdminDTO}
     */
    @GetMapping
    @ApiOperation(value = "Get list of User Admin")
    public APIResponse<PageInfo<UserAdminDTO>> getUserAdminsList(
            @ApiParam(value = "Search Info: user's name or user's email", required = false)
            @RequestParam(value = "query", required = false) String query,
            @ApiParam(value = "Page", required = false)
            @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "Limit", required = false)
            @RequestParam(value = "limit", required = false) Integer limit) {
        return APIResponse.okStatus(this.userAdminService.getUserAdminsList(query, page, limit));
    }

    @GetMapping("/emails")
    @ApiOperation(value = "Get list of User_Emails")
    public APIResponse<PageInfo<UserEmailDTO>> getUserEmailsList(
            @ApiParam(value = "Search Info: user's email", required = false)
            @RequestParam(value = "query", required = false) String query,
            @ApiParam(value = "Page", required = false)
            @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "Limit", required = false)
            @RequestParam(value = "limit", required = false) Integer limit) {
        return APIResponse.okStatus(this.userAdminService.getUserEmailsList(query, page, limit));
    }
}
