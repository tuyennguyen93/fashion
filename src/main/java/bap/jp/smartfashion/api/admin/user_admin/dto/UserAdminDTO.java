package bap.jp.smartfashion.api.admin.user_admin.dto;

import bap.jp.smartfashion.common.model.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Setter
@Getter
public class UserAdminDTO {
    private Integer id;
    private String userName;
    private String email;
    private Integer gender;
    private Date contributorDate;

    public UserAdminDTO(Integer id, String name, String email, Integer gender, LocalDateTime modified){
        this.id = id;
        this.userName = name;
        this.email = email;
        this.gender = gender;
        this.contributorDate = 	Date.from(modified.atZone( ZoneId.systemDefault()).toInstant());
    }
}
