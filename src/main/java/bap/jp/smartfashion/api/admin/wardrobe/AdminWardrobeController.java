package bap.jp.smartfashion.api.admin.wardrobe;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeDTO;
import bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeTypeDTO;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller to managing wardrobes by admin
 */
@RestController
@RequestMapping("/api/v1/admin/wardrobes")
@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
public class AdminWardrobeController {

	@Autowired
	private AdminWardrobeService adminWardrobeService;
	
	/**
	 * Get wardrobe list
	 * @param type Model or product(wardrobe)
	 * @param query data to search
	 * @param page page to get data
	 * @param limit limit on each page
	 * @param sort sort by
	 * @param collectionType collection tpe
	 * @return PageInfo<AdminWardrobeDTO>
	 * @throws BadRequestException
	 */
	@GetMapping
	@ApiOperation(value = "Get wardrobe list")
	public APIResponse<PageInfo<AdminWardrobeDTO>> getWardrobeList(
		@ApiParam(value = "type", required = true, allowableValues = "MODEL, WARDROBE")
		@RequestParam(value = "type", required = true) String type,
		@ApiParam(value ="Wardrobe seach by user name and email", required = false)
		@RequestParam(value = "query", required = false) String query,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",OLDEST_TO_NEWEST, NEWEST_TO_OLDEST")
		@RequestParam(value = "sort", required = false) String sort,
		@ApiParam(value ="Collection type (UserProductCollectionId or UserModelCollectionId. (Get all then set = 0))", required = true)
		@RequestParam(value = "collectionType", required = true) int collectionType) throws BadRequestException {
		if (type.equals("MODEL")) {
			return APIResponse.okStatus(this.adminWardrobeService.getUserModelList(page, limit, sort, query, collectionType));
		} else if (type.equals("WARDROBE")) {
			return APIResponse.okStatus(this.adminWardrobeService.getUserProductList(page, limit, sort, query, collectionType));
		} else {
			throw new BadRequestException("Type not found");
		}
	}
	
	/**
	 * Get wardrobe type
	 * @param type Model or product(wardrobe)
	 * @return List<AdminWardrobeTypeDTO>
	 * @throws BadRequestException
	 */
	@GetMapping("/types/{type}")
	@ApiOperation(value = "Get detail wardrobe type")
	public APIResponse<List<AdminWardrobeTypeDTO>> getWardrobeTypes(
		@ApiParam(value = "type", required = true, allowableValues = "MODEL, WARDROBE")
		@PathVariable(value = "type", required = true) String type) throws BadRequestException {
		if ("WARDROBE".equals(type)) {
			return APIResponse.okStatus(this.adminWardrobeService.getUserProductCollectionType());
		} else if ("MODEL".equals(type)) {
			return APIResponse.okStatus(this.adminWardrobeService.getUserModelCollectionType());
		} else {
			throw new BadRequestException("Type not found");
		}
	}
	
	/**
	 * Get wardrobe type
	 * @return List<AdminWardrobeTypeDTO>
	 */
	@GetMapping("/types")
	@ApiOperation(value = "Get wardrobe type")
	public APIResponse<List<AdminWardrobeTypeDTO>> getWardrobeTypes() {
		return APIResponse.okStatus(this.adminWardrobeService.getWardrobeTypes());
	}
}
