package bap.jp.smartfashion.api.admin.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ShopStatusDTO {
    private String status;
    private long amount;

    public ShopStatusDTO() {}
}
