package bap.jp.smartfashion.api.admin.product.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductWearingPurposeDTO;
import bap.jp.smartfashion.common.model.ProductWearingPurpose;
import bap.jp.smartfashion.common.repository.ProductWearingPurposeRepository;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

@Service
public class ProductWearingPurposeService extends BaseService {

    @Autowired
    private ProductWearingPurposeRepository productWearingPurposeRepository;

    public List<ProductWearingPurposeDTO> getProductWearingPurposeList(String wearingPurposeType) {
        List<ProductWearingPurposeDTO> productWearingPurposeDTOs = this.productWearingPurposeRepository.findAllByType(wearingPurposeType).stream().map(ProductWearingPurposeDTO::new).collect(Collectors.toList());
        return productWearingPurposeDTOs;
    }

    public ProductWearingPurposeDTO createWearingPurpose(ProductWearingPurposeDTO productWearingPurposeDto) throws ConflictException {
        if (this.findOneByName(productWearingPurposeDto) != null) {
            throw new ConflictException(ConflictException.ERROR_PRODUCT_WEARING_PURPOSE_EXITED,
                String.format(APIConstants.ERROR_EXISTED_MSG, "Product wearing purpose"));
        }
        ProductWearingPurpose productWearingPurpose = new ProductWearingPurpose();
        BeanUtils.copyProperties(productWearingPurposeDto, productWearingPurpose);

        productWearingPurposeDto = new ProductWearingPurposeDTO(this.productWearingPurposeRepository.save(productWearingPurpose));

        return productWearingPurposeDto;
    }

    public ProductWearingPurposeDTO updateWearingPurpose(Integer productWearingPurposeId, ProductWearingPurposeDTO productWearingPurposeDto)
            throws ConflictException, NotFoundException {

        ProductWearingPurpose existproductWearingPurpose = this.findOneByName(productWearingPurposeDto);
        if (existproductWearingPurpose != null && productWearingPurposeId != existproductWearingPurpose.getId()) {
            throw new ConflictException(ConflictException.ERROR_PRODUCT_WEARING_PURPOSE_EXITED,
                String.format(APIConstants.ERROR_EXISTED_MSG, "Product wearing purpose"));
        }
        ProductWearingPurpose productWearingPurpose = this.findProductWearingPurposeById(productWearingPurposeId);
        productWearingPurpose.setName(productWearingPurposeDto.getName());
        productWearingPurposeDto = new ProductWearingPurposeDTO(this.productWearingPurposeRepository.save(productWearingPurpose));

        return productWearingPurposeDto;
    }

    public void deleteWearingPurpose(Integer productWearingPurposeId) throws NotFoundException {
        this.findProductWearingPurposeById(productWearingPurposeId);
        this.productWearingPurposeRepository.deleteById(productWearingPurposeId);
    }

    private ProductWearingPurpose findOneByName(ProductWearingPurposeDTO productWearingPurposeDTO) {
        return this.productWearingPurposeRepository.findOneByName(productWearingPurposeDTO.getName()).orElse(null);
    }

    private ProductWearingPurpose findProductWearingPurposeById(Integer productWearingPurposeId) throws NotFoundException {
        return this.productWearingPurposeRepository.findById(productWearingPurposeId).orElseThrow(() ->
                new NotFoundException(NotFoundException.ERROR_PRODUCT_WEARING_PURPOSE_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product wearing purpose"), HttpStatus.NOT_FOUND));
    }
}
