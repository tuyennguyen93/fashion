package bap.jp.smartfashion.api.admin.user.dto;

import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserProduct;
import lombok.Getter;
import lombok.Setter;

import java.time.ZoneId;
import java.util.Date;

@Getter
@Setter
public class AdminUserCollectionDTO {
    private Integer id;
    private String url;
    private Integer collectionId;
    private Date created;

    public AdminUserCollectionDTO(Object object, boolean isModel) {
        if(isModel) {
            UserModel userModel = (UserModel) object;
            this.id = userModel.getId();
            this.url = userModel.getUrl();
            this.collectionId = userModel.getUserModelCollection().getId();
            this.created = Date.from(userModel.getCreated().atZone( ZoneId.systemDefault()).toInstant());
        } else {
            UserProduct userProduct = (UserProduct) object;
            this.id = userProduct.getId();
            this.url = userProduct.getUrl();
            this.collectionId = userProduct.getUserProductCollection().getId();
            this.created = Date.from(userProduct.getCreated().atZone( ZoneId.systemDefault()).toInstant());
        }
    }
}
