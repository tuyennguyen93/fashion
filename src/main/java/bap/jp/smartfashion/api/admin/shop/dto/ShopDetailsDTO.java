package bap.jp.smartfashion.api.admin.shop.dto;

import bap.jp.smartfashion.common.dto.ShopDTO;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.ShopBranch;
import bap.jp.smartfashion.common.model.ShopWarehouse;
import bap.jp.smartfashion.common.model.UserBankAccount;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ShopDetailsDTO {
    private UserDTO user;
    private ShopDTO shop;
    private List<ShopBranch> shopBranches;
    private List<ShopWarehouse> shopWarehouses;
    private UserBankAccount userBankAccount;
}
