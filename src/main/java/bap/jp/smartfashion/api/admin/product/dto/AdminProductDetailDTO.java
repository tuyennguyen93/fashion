package bap.jp.smartfashion.api.admin.product.dto;

import bap.jp.smartfashion.common.dto.ProductTypeDTO;
import bap.jp.smartfashion.common.model.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class AdminProductDetailDTO {
    private Integer id;
    private String name;
    private String avatarImage;
    private String status;
    private String productCode;
    private String gender;
    private Shop shop;
    private ProductTypeDTO productType;
    private Integer triedTime = 0;
    private Integer totalItems;
    private Integer passedItems;
    private List<PassedItems> photosList;

    public AdminProductDetailDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.status = product.getStatus();
        this.productCode = product.getProductCode();
        this.gender = product.getProductCollection().getName();
        this.shop = new Shop(product.getShop());
        this.productType = new ProductTypeDTO(product.getProductType());
        this.totalItems = product.getProductDetails().size();
    }

    @Setter
    @Getter
    private class Shop{
        private Integer id;
        private String name;

        private Shop(bap.jp.smartfashion.common.model.Shop shop) {
            this.id = shop.getId();
            this.name = shop.getName();
        }
    }

    @Setter
    @Getter
    public class PassedItems {
        private Integer index;
        private String photoUrl;

        public PassedItems(Integer index, String url) {
            this.index = index;
            this.photoUrl = url;
        }
    }
}