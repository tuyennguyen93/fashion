package bap.jp.smartfashion.api.admin.wardrobe.dto;

import java.time.ZoneId;
import java.util.Date;

import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserProduct;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminWardrobeDTO {
	private Integer id;
	private String url;
	private Date created;
	private User user;
	
	
	@Getter
	@Setter
	class User {
		private Integer id;
		private String name;
		private String email;
		
		public User(bap.jp.smartfashion.common.model.User user) {
			if (user != null) {
				this.id = user.getId();
				this.name = user.getName();
				this.email = user.getEmail();
			}
		}
		
	}


	public AdminWardrobeDTO(Object object, boolean isModel) {

		if (isModel) {
			UserModel userModel = (UserModel) object;
			this.id = userModel.getId();
			this.url = userModel.getUrl();
			this.created = Date.from(userModel.getCreated().atZone( ZoneId.systemDefault()).toInstant());
			this.user = new User(userModel.getUser());
		} else {
			UserProduct userProduct = (UserProduct) object;
			this.id = userProduct.getId();
			this.url = userProduct.getUrl();
			this.created = Date.from(userProduct.getCreated().atZone( ZoneId.systemDefault()).toInstant());
			this.user = new User(userProduct.getUser());
		}
	}
}
