package bap.jp.smartfashion.api.admin.user.dto;

import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * A DTO for Admin user management
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class AdminUserStatusDTO {
	private String status;
	private Integer amount;
	
	public AdminUserStatusDTO() {
		super();
	}
}
