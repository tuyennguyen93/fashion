package bap.jp.smartfashion.api.admin.product.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import bap.jp.smartfashion.common.dto.ShopProductCollectionDTO;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.ShopProductCollection;
import bap.jp.smartfashion.common.repository.ShopProductCollectionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

@Service
public class ProductCollectionService extends BaseService {
    @Autowired
    private ProductCollectionRepository productCollectionRepository;

    @Autowired
    private ShopProductCollectionRepository shopProductCollectionRepository;

    public List<ProductCollectionDTO> getProductCollectionList() {
        List<ProductCollectionDTO> productCollectionDTOs = this.productCollectionRepository.findAll().stream()
                .map(ProductCollectionDTO::new).collect(Collectors.toList());
        return productCollectionDTOs;
    }

    public ProductCollectionDTO createCollection(ProductCollectionDTO productCollectionDto) throws ConflictException {
        if (this.findProductCollectionName(productCollectionDto.getName()) != null) {
            throw new ConflictException(ConflictException.ERROR_PRODUCT_COLLECTION_EXITED,
                String.format(APIConstants.ERROR_EXISTED_MSG, "Product collection"));
        }
        ProductCollection productCollection = new ProductCollection();
        BeanUtils.copyProperties(productCollectionDto, productCollection);
        productCollectionDto = new ProductCollectionDTO(this.productCollectionRepository.save(productCollection));
        return productCollectionDto;
    }

    public ProductCollectionDTO updateCollection(Integer productCollectionId, ProductCollectionDTO productCollectionDto)
            throws ConflictException {
        ProductCollection productCollection = this.findProductCollectionName(productCollectionDto.getName());
        if (productCollection != null && productCollectionId != productCollection.getId()) {
            throw new ConflictException(ConflictException.ERROR_PRODUCT_COLLECTION_EXITED,
                String.format(APIConstants.ERROR_EXISTED_MSG, "Product collection"));
        }

        productCollection.setName(productCollectionDto.getName());
        productCollection.setImage(productCollectionDto.getImage());
        productCollection.setDescription(productCollectionDto.getDescription());

        productCollectionDto = new ProductCollectionDTO(this.productCollectionRepository.save(productCollection));
        return productCollectionDto;
    }

    public void deleteCollection(Integer productCollectionId) throws NotFoundException {
        Optional<ProductCollection> productCollectionOptional = this.productCollectionRepository
                .findById(productCollectionId);
        if (productCollectionOptional.isPresent()) {
            this.productCollectionRepository.deleteById(productCollectionId);
        }
        throw new NotFoundException(NotFoundException.ERROR_PRODUCT_COLLECTION_NOT_FOUND,
                String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product collection"), HttpStatus.NOT_FOUND);
    }

    private ProductCollection findProductCollectionName(String name) {
        return this.productCollectionRepository.findOneByName(name).orElse(null);
    }

    /**
     * Get list of product_collections for shop
     *
     * @param shopId {@link Shop}
     * @return list of {@link ShopProductCollection}
     */
    public List<ProductCollectionDTO> getProductCollectionsForShop(Integer shopId) {
        Optional<List<ShopProductCollection>> shopProductCollections = this.shopProductCollectionRepository.findByShop_Id(shopId);
        List<ProductCollectionDTO> productCollectionDTOs = new ArrayList<>();
        if(shopProductCollections.isPresent()) {
            productCollectionDTOs = shopProductCollections.get().stream().map(ProductCollectionDTO::new).collect(Collectors.toList());
        }

        return productCollectionDTOs;
    }
}
