package bap.jp.smartfashion.api.admin.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserProductCollectionDTO {
    private Integer id;
    private String name;
    private long amount;
}