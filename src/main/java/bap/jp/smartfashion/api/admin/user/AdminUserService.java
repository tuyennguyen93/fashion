package bap.jp.smartfashion.api.admin.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import bap.jp.smartfashion.api.admin.user.dto.AdminUserCollectionDTO;
import bap.jp.smartfashion.api.admin.user.dto.AdminUserDTO;
import bap.jp.smartfashion.api.admin.user.dto.AdminUserDetailDTO;
import bap.jp.smartfashion.api.admin.user.dto.ModelCollection;
import bap.jp.smartfashion.api.admin.user.dto.ProductCollection;
import bap.jp.smartfashion.api.admin.user.dto.ProductInteractionDTO;
import bap.jp.smartfashion.api.admin.user.dto.UserModelCollectionDTO;
import bap.jp.smartfashion.api.admin.user.dto.UserProductCollectionDTO;
import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserProductCollection;
import bap.jp.smartfashion.common.model.UserRole;
import bap.jp.smartfashion.common.repository.UserModelCollectionRepository;
import bap.jp.smartfashion.common.repository.UserModelRepository;
import bap.jp.smartfashion.common.repository.UserProductCollectionRepository;
import bap.jp.smartfashion.common.repository.UserProductInteractionRepository;
import bap.jp.smartfashion.common.repository.UserProductRepository;
import bap.jp.smartfashion.common.repository.UserRoleRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.api.admin.user.dto.AdminUserStatus;
import bap.jp.smartfashion.api.admin.user.dto.AdminUserStatusDTO;
import bap.jp.smartfashion.api.user.UserService;
import bap.jp.smartfashion.api.user.shop.UserShopService;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.mapper.UserMapper;
import bap.jp.smartfashion.common.model.Role;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.repository.RoleRepository;
import bap.jp.smartfashion.common.repository.UserAuthorityRepository;
import bap.jp.smartfashion.common.repository.UserRepository;
import bap.jp.smartfashion.enums.RoleEnum;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.enums.UserEnum.UserStatus;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;
/**
 * Service class for managing users by admin.
 */
@Service
public class AdminUserService extends BaseService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private UserProductInteractionRepository userProductInteractionRepository;

	@Autowired
	private UserModelCollectionRepository userModelCollectionRepository;

	@Autowired
	private UserModelRepository userModelRepository;

	@Autowired
	private UserProductCollectionRepository userProductCollectionRepository;

	@Autowired
	private UserProductRepository userProductRepository;

	@Autowired
	private MessageService messageService;
	
	private final Logger log = LoggerFactory.getLogger(AdminUserService.class);
	
	/** Delete {@link User} by User ID
	 * 
	 * @param userId {@link User} ID
	 * @return {@link APIResponse}
	 * @throws NotFoundException if {@link User} not found
	 */
	@Caching(evict = {
		@CacheEvict(value = UserService.USER_BY_ID_CACHE, key = "#userId"),
		@CacheEvict(value = UserService.USER_LIST_CACHE, allEntries = true)
	})
	public User deleteUser(int userId) throws NotFoundException {
		User user = this.findUserByUserId(userId);
		user.setDeleted(true);
		this.userRepository.save(user);
		log.info("Deleted the user : {}", user);
		
		return user;
	}
	
	/** Block {@link User} by ID
	 * 
	 * @param userId {@link User} ID
	 * @return {@link User}
	 * @throws NotFoundException if {@link User} not found
	 */
	@CachePut(value = UserService.USER_BY_ID_CACHE, key = "#userId")
	@Caching(evict = {
		@CacheEvict(value = UserService.USER_LIST_CACHE, allEntries = true)
	})
	public User blockUser(int userId) throws NotFoundException {
		User user = this.findUserByUserId(userId);
		user.setBlocked(true);
		this.userRepository.save(user);
		log.info("Blocked the user : {}", user);
		
		return user;
	}
	
	public List<AdminUserStatusDTO> getAdminUserStatusList() {
		List<AdminUserStatusDTO> userStatusDTOs = new ArrayList<AdminUserStatusDTO>();
		AdminUserStatus adminUserStatus = this.userRepository.findAllStatusUser();
		AdminUserStatusDTO userStatusDTOAll = new AdminUserStatusDTO(UserStatus.ALL.name(), adminUserStatus.getAllStatus());
		AdminUserStatusDTO userStatusDTOActive = new AdminUserStatusDTO(UserStatus.ACTIVE.name(), adminUserStatus.getActiveStatus());
		AdminUserStatusDTO userStatusDTOPending = new AdminUserStatusDTO(UserStatus.PENDING.name(), adminUserStatus.getPendingStatus());
		AdminUserStatusDTO userStatusDTODeleted = new AdminUserStatusDTO(UserStatus.DELETED.name(), adminUserStatus.getDeletedStatus());
		userStatusDTOs.add(userStatusDTOAll);
		userStatusDTOs.add(userStatusDTOActive);
		userStatusDTOs.add(userStatusDTOPending);
		userStatusDTOs.add(userStatusDTODeleted);
		
		return userStatusDTOs;
	}

	/**
	 * Get User's detail
	 *
	 * @param userId {@link User}
	 * @return {@link AdminUserDetailDTO}
	 * @throws NotFoundException
	 */
	public AdminUserDetailDTO getUserDetail(Integer userId) throws NotFoundException {
		User exitedUser = this.userRepository.findByIdForAdmin(userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("user-with-ID") + ": " + userId), HttpStatus.NOT_FOUND));
		AdminUserDetailDTO userDetail = new AdminUserDetailDTO(exitedUser);
		List<ProductInteractionDTO> productInteractionList = this.userProductInteractionRepository.findAllByUser_IdAndStatus(userId);
		for(ProductInteractionDTO productInteractionDTO : productInteractionList) {
			if(ProductEnum.ProductInteraction.TRY_PRODUCT.getCode().equals(productInteractionDTO.getActionCode())) {
				userDetail.setTried(productInteractionDTO.getActionCount());
			} else if(ProductEnum.ProductInteraction.FAVORITE_PRODUCT.getCode().equals(productInteractionDTO.getActionCode())) {
				userDetail.setFavorited(productInteractionDTO.getActionCount());
			}
		}
		return userDetail;
	}

	/**
	 * Get User Model Collection with amount
	 *
	 * @param userId {@link User}
	 * @return list of {@link UserModelCollectionDTO}
	 * @throws NotFoundException
	 */
	public List<UserModelCollectionDTO> getUserModelCollectionWithAmount(Integer userId) throws NotFoundException {
		this.userRepository.findByIdForAdmin(userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("user-with-ID") + ": " + userId), HttpStatus.NOT_FOUND));

		List<UserModelCollectionDTO> userModelCollectionList = new ArrayList<>();
		List<UserModelCollection> userModelCollections = this.userModelCollectionRepository.findAll();
		List<ModelCollection> modelCollections = this.userModelRepository.findAllByUser_Id(userId);
		for(UserModelCollection userModelCollection : userModelCollections) {
			boolean exited = false;
			for(ModelCollection modelCollection : modelCollections) {
				if(userModelCollection.getId().equals(modelCollection.getCollectionId())) {
					userModelCollectionList.add(new UserModelCollectionDTO(userModelCollection.getId(), userModelCollection.getName(), modelCollection.getAmount()));
					exited = true;
					break;
				}
			}
			if(!exited) {
				userModelCollectionList.add(new UserModelCollectionDTO(userModelCollection.getId(), userModelCollection.getName(), 0));
			}
		}
		userModelCollectionList.add(0, new UserModelCollectionDTO(0, "All", modelCollections.stream().mapToLong(modelCollection -> modelCollection.getAmount()).sum()));
		return userModelCollectionList;
	}

	/**
	 * Get List of User Model
	 *
	 * @param userId {@link User}
	 * @param collectionId {@link UserModelCollection}
	 * @param page current page
	 * @param limit amount of record in a page
	 * @return {@link PageInfo} of {@link AdminUserCollectionDTO}
	 * @throws NotFoundException
	 */
	public PageInfo<AdminUserCollectionDTO> getUserModelsList(Integer userId, Integer collectionId, Integer page, Integer limit) throws NotFoundException {
		this.userRepository.findByIdForAdmin(userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("user-with-ID") + ": " + userId), HttpStatus.NOT_FOUND));

		Sort sort = Sort.by(Sort.Order.desc("created"));
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Integer checkedCollectionId;
		if(collectionId == null || collectionId <= 0) {
			checkedCollectionId = -1;
		} else {
			checkedCollectionId = collectionId;
		}
		Page<AdminUserCollectionDTO> data = this.userModelRepository
				.findAllByUserIdAndAndUserModelCollection_Id(userId, checkedCollectionId, pageRequest)
				.map(userModel -> {return new AdminUserCollectionDTO(userModel, true);});

		PageInfo<AdminUserCollectionDTO> userModelsList = SmartFashionUtils.pagingResponse(data);
		return userModelsList;
	}

	/**
	 * Get list of User Product Collection with amount
	 *
	 * @param userId {@link User}
	 * @return list of {@link UserProductCollectionDTO}
	 */
	public List<UserProductCollectionDTO> getUserProductCollectionWithAmount(Integer userId) throws NotFoundException {
		this.userRepository.findByIdForAdmin(userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("user-with-ID") + ": " + userId), HttpStatus.NOT_FOUND));

		List<UserProductCollectionDTO> userProductCollectionList = new ArrayList<>();
		List<UserProductCollection> userProductCollections = this.userProductCollectionRepository.findAll();
		List<ProductCollection> productCollections = this.userProductRepository.findProductCollectionByUser_Id(userId);
		for(UserProductCollection userProductCollection : userProductCollections) {
			boolean exited = false;
			for(ProductCollection productCollection : productCollections) {
				if(userProductCollection.getId().equals(productCollection.getCollectionId())) {
					userProductCollectionList.add(new UserProductCollectionDTO(userProductCollection.getId(), userProductCollection.getName(), productCollection.getAmount()));
					exited = true;
					break;
				}
			}
			if(!exited) {
				userProductCollectionList.add(new UserProductCollectionDTO(userProductCollection.getId(), userProductCollection.getName(), 0));
			}
		}
		userProductCollectionList.add(0, new UserProductCollectionDTO(0, "All", productCollections.stream().mapToLong(productCollection -> productCollection.getAmount()).sum()));
		return userProductCollectionList;
	}

	/**
	 * Get list of User Products's detail
	 *
	 * @param userId {@link User}
	 * @param collectionId {@link UserProductCollection}
	 * @param page current Page
	 * @param limit amount record in a page
	 * @return list of {@link AdminUserCollectionDTO}
	 * @throws NotFoundException
	 */
	public PageInfo<AdminUserCollectionDTO> getUserProductsList(Integer userId, Integer collectionId, Integer page, Integer limit) throws NotFoundException {
		this.userRepository.findByIdForAdmin(userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("user-with-ID") + ": " + userId), HttpStatus.NOT_FOUND));

		Sort sort = Sort.by(Sort.Order.desc("created"));
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Integer checkedCollectionId;
		if(collectionId == null || collectionId <= 0) {
			checkedCollectionId = -1;
		} else {
			checkedCollectionId = collectionId;
		}
		Page<AdminUserCollectionDTO> data = this.userProductRepository
				.findAllByUserIdAndAndUserProductCollection_Id(userId, checkedCollectionId, pageRequest)
				.map(userProduct -> {return new AdminUserCollectionDTO(userProduct, false);});

		PageInfo<AdminUserCollectionDTO> userProductsList = SmartFashionUtils.pagingResponse(data);

		return userProductsList;
	}

	/**
	 * Set status for user's sccount
	 *
	 * @param currentStatus values = {"ACTIVE, PENDING, DELETED"}
	 * @param updatedStatus values = {"DELETE, ACTIVE, REOPEN"}
	 * @param objectID list og users's ID
	 * @return {@link APIResponse} data is list of {@link AdminUserDTO}
	 * @throws BadRequestException
	 * @throws NotFoundException
	 */
	public List<AdminUserDTO> setStatusForUserAccount(String currentStatus, String updatedStatus, ObjectID userID) throws BadRequestException, NotFoundException {
		List<User> usersList = new ArrayList<>();
		List<String> statusList = Arrays.asList("ACTIVE", "DELETED");
		Optional<String> resultStatus = statusList.stream().filter(status -> status.equalsIgnoreCase(currentStatus)).findFirst();
		boolean checkedUpdatedStatus = false;
		if(!resultStatus.isPresent()) {
			throw new BadRequestException(BadRequestException.ERROR_USER_STATUS_BAD_REQUEST,
					String.format(this.messageService.getMessage("error.bad-value-msg"),
							this.messageService.getMessage("current-status")), false);
		}
		switch (currentStatus) {
			case "ACTIVE": {
				if(("DELETE").equalsIgnoreCase(updatedStatus)) {
					checkedUpdatedStatus = true;
				}
			}
			break;
			case "DELETED": {
				if(("REACTIVE").equalsIgnoreCase(updatedStatus)) {
					checkedUpdatedStatus = true;
				}
			}
		}
		if(!checkedUpdatedStatus) {
			throw new BadRequestException(BadRequestException.ERROR_USER_STATUS_BAD_REQUEST,
					String.format(this.messageService.getMessage("error.bad-value-msg"),
							this.messageService.getMessage("updated-status")), false);
		}
		if(userID.getIdsList().isEmpty()) {
			throw new BadRequestException(BadRequestException.ERROR_EMPTY_USER_ID_LIST,
					String.format(this.messageService.getMessage("error.bad-value-msg"),
							this.messageService.getMessage("list-users-ID")), false);
		}
		for(Integer userId : userID.getIdsList()) {
			User exitedUser = this.userRepository.findById(userId).orElseThrow(
					() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
							String.format(this.messageService.getMessage("error.not-found-msg"),
									this.messageService.getMessage("user-with-ID") + ": " + userId), HttpStatus.NOT_FOUND));
			if (exitedUser.isRegisterStatus() && !exitedUser.isDeleted()) {
				exitedUser.setDeleted(true);
			} else if (exitedUser.isDeleted()){
				exitedUser.setDeleted(false);
			}
			usersList.add(exitedUser);
		}
		List<User> savedUsers = this.userRepository.saveAll(usersList);
		List<AdminUserDTO> adminUsers = savedUsers.stream().map(AdminUserDTO::new).collect(Collectors.toList());

		return adminUsers;
	}
}
