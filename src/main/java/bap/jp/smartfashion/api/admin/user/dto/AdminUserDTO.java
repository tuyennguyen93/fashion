package bap.jp.smartfashion.api.admin.user.dto;

import java.time.ZoneId;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.enums.UserEnum.UserStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminUserDTO {
	private int id;
	private String name;
	private String email;
	private Integer gender;
	private String userType;
	private long userModelCount;
	private long userProductCount;
	private String status;
	private String[] userRoles;
	private Date created;
	
	
	public AdminUserDTO(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.email = user.getEmail();
		this.gender = user.getGender();
		this.userType = user.getUserType();
		if (user.isRegisterStatus()) {
			this.status = UserStatus.ACTIVE.name();
		} else {
			this.status = UserStatus.PENDING.name();
		}
		if (user.isDeleted()) {
			this.status = UserStatus.DELETED.name();
		}
		this.created = Date.from(user.getCreated().atZone( ZoneId.systemDefault()).toInstant());
		this.userRoles = user.getRoles().stream().map(role -> role.getName()).toArray(String[]::new);
	}
	
	public AdminUserDTO() {
	}
	
}
