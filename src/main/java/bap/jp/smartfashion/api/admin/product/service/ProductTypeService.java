package bap.jp.smartfashion.api.admin.product.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductTypeDTO;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;

@Service
public class ProductTypeService extends BaseService {

    @Autowired
    private ProductTypeRepository productTypeRepository;

    public List<ProductTypeDTO> getProductTypeByCollectionId(Integer collectionId) {
        List<ProductTypeDTO> productTypeDTOs = this.productTypeRepository.findAllByProductCollection_Id(collectionId).stream().map(ProductTypeDTO::new).collect(Collectors.toList());
        List<Integer> productTypeParentIds = new ArrayList<Integer>();
        for (ProductTypeDTO productType : productTypeDTOs) {
            productTypeParentIds.add(productType.getId());
        }
        Map<Object, List<ProductType>> productSizes = this.productTypeRepository.findAllByProductTypeParentIdIn(productTypeParentIds)
                .stream().collect(Collectors.groupingBy(ProductType::getProductTypeParentId));
        for(Map.Entry<Object, List<ProductType>> entry : productSizes.entrySet()) {
            Object key = entry.getKey();
            List<ProductTypeDTO> listChild =  entry.getValue().stream().map(ProductTypeDTO::new).collect(Collectors.toList());
            Optional<ProductTypeDTO> matchingObject = productTypeDTOs.stream().filter(productType -> productType.getId() == key).findFirst();
            if (matchingObject.isPresent()) {
                matchingObject.get().setChildList(listChild);
            }
        }

        return productTypeDTOs;
    }
}
