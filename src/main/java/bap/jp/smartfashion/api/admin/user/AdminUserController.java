package bap.jp.smartfashion.api.admin.user;

import java.util.List;
import javax.validation.Valid;

import bap.jp.smartfashion.api.admin.user.dto.AdminUserCollectionDTO;
import bap.jp.smartfashion.api.admin.user.dto.AdminUserDetailDTO;
import bap.jp.smartfashion.api.admin.user.dto.UserModelCollectionDTO;
import bap.jp.smartfashion.api.admin.user.dto.UserProductCollectionDTO;
import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserProductCollection;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.common.vo.UserRoleVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.buf.UEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bap.jp.smartfashion.api.admin.user.dto.AdminUserDTO;
import bap.jp.smartfashion.api.admin.user.dto.AdminUserStatusDTO;
import bap.jp.smartfashion.api.user.UserService;
import bap.jp.smartfashion.common.base.BaseController;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.Message;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.UserFilter;
import bap.jp.smartfashion.common.vo.UserManagement;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller to managing users by admin
 */
@RestController
@RequestMapping("/api/v1/admin/users")
@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_ADMIN')")
public class AdminUserController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AdminUserService adminService;
	
	private final Logger log = LoggerFactory.getLogger(AdminUserController.class);
	
	/**
	 * Get {@link User} by ID
	 * 
	 * @param userId {@link User} ID
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws  NotFoundException if {@link User} not found
	 */
	@GetMapping("/{userId}")
	@ApiOperation(value = "Find user by user id")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public APIResponse<UserDTO> findUserById(
		@ApiParam(value = "User ID", required =  true)
		@PathVariable(value = "userId") int userId) throws NotFoundException {
		log.info("REST request to find user by user Id: {}", userId);
		UserDTO userDTO = new UserDTO(this.userService.findUserById(userId));
		return APIResponse.okStatus(userDTO);
	}
	
	/**
	 * Get {@link User} list
	 * @param <T>
	 *
	 * @param query query
	 * @param page page number
	 * @param limit Limit on per page
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 */
	@GetMapping
	@ApiOperation(value = "Find user list")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public APIResponse<PageInfo<AdminUserDTO>> findUserList(
		@ApiParam(value ="Query<Email, Name>", required = false)
		@RequestParam(value = "query", required = false) String query,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",OLDEST_TO_NEWEST, NEWEST_TO_OLDEST")
		@RequestParam(value = "sort", required = false) String sort,
		@ApiParam(value ="Find by status", required = false, allowableValues = "ALL, ACTIVE, PENDING, DELETED")
		@RequestParam(value = "status", required = false) String status) {
		log.info("REST request to find user list");
		if (StringUtils.isEmpty(query)) {
			return APIResponse.okStatus(this.userService.findUserList(page, limit, sort, status));
		}
		return APIResponse.okStatus(this.userService.findUserListQuery(query, page, limit, sort, status));
	}
	
	/**
	 * Get {@link User} list
	 * @param <T>
	 *
	 * @param query query
	 * @param page page number
	 * @param limit Limit on per page
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 */
	@GetMapping(value = "/status")
	@ApiOperation(value = "Get status list of user management")
	public APIResponse<List<AdminUserStatusDTO>> getListStatus() {
		return APIResponse.okStatus(this.adminService.getAdminUserStatusList());
	}
	
	/** 
	 * Delete {@link User} by id
	 *
	 * @param userId {@link User} id
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws ConflictException if email is existed
	 * @throws NotFoundException if user not found
	 */
	@DeleteMapping(value = "/{userId}")
	@ApiOperation(value = "Delete the user by id")
	@ApiResponses(value = {
		@ApiResponse(code = 401 , message = "Unauthorized"),
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public APIResponse<UserDTO> deleteUser(
		@ApiParam(value = "User ID", required = true)
		@PathVariable(value = "userId", required = true) int userId) throws ConflictException, NotFoundException {
		log.info("REST request to detele user id: {}", userId);
		return APIResponse.okStatus(new UserDTO(this.adminService.deleteUser(userId)));
	}
	
	/** 
	 * Block {@link User} by user id
	 *
	 * @param id {@link User} id
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws ConflictException if email is existed
	 * @throws NotFoundException if user not found
	 */
	@PutMapping(value = "/{userId}/block")
	@ApiOperation(value = "Block the user by id")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public APIResponse<UserDTO> blockUser(
		@ApiParam(value = "User ID", required = true)
		@PathVariable(value = "userId", required = true) int userId) throws ConflictException, NotFoundException {
		log.info("REST request to find user id : {}", userId);
		return APIResponse.okStatus(new UserDTO(this.adminService.blockUser(userId)));
	}

	/**
	 * Get user's detail
	 *
	 * @param userId {@link User}
	 * @return {@link APIResponse} data is {@link AdminUserDetailDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/{userId}/details")
	@ApiOperation(value = "Get User's detail")
	public APIResponse<AdminUserDetailDTO> getUserDetail(
			@ApiParam(value = "User ID", required = true)
			@PathVariable(value = "userId", required = true) Integer userId) throws NotFoundException {
		return APIResponse.okStatus(this.adminService.getUserDetail(userId));
	}

	/**
	 * Get User Model Collection with amount
	 *
	 * @param userId {@link User}
	 * @return {@link APIResponse} data is list of {@link UserModelCollectionDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/{userId}/model-collection")
	@ApiOperation(value = "Get list of User Model Collection  with amount")
	public APIResponse<List<UserModelCollectionDTO>> getUserModelCollectionWithAmount(
			@ApiParam(value = "User ID", required = true)
			@PathVariable(value = "userId", required = true) Integer userId) throws NotFoundException {
		return APIResponse.okStatus(this.adminService.getUserModelCollectionWithAmount(userId));
	}

	/**
	 * Get list of User Product Collection with amount
	 *
	 * @param userId {@link User}
	 * @return {@link APIResponse} data is list of {@link UserProductCollectionDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/{userId}/product-collection")
	@ApiOperation(value = "Get list of User Product Collection with amount")
	public APIResponse<List<UserProductCollectionDTO>> getUserProductCollectionWithAmount(
			@ApiParam(value = "User ID", required = true)
			@PathVariable(value = "userId", required = true) Integer userId) throws NotFoundException {
		return APIResponse.okStatus(this.adminService.getUserProductCollectionWithAmount(userId));
	}

	/**
	 * Get list of User Collection's detail (Model or Wardrobe)
	 *
	 * @param userId {@link User}
	 * @param collectionId {@link UserProductCollection} or {@link UserModelCollection}
	 * @param page current page
	 * @param limit amount record in a page
	 * @return {@link APIResponse} data is {@link PageInfo} of {@link AdminUserCollectionDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/{userId}/user-collection/detail")
	@ApiOperation(value = "Get list of User Collection's detail")
	public APIResponse<PageInfo<AdminUserCollectionDTO>> getUserProductsList(
			@ApiParam(value = "User ID", required = true)
			@PathVariable(value = "userId", required = true) Integer userId,
			@ApiParam(value = "Type", required = true, allowableValues = "MODEL, WARDROBE")
			@RequestParam(value = "type", required = true) String type,
			@ApiParam(value = "Collection ID (set 0 in case get all)", required = false)
			@RequestParam(value = "collectionId", required = false) Integer collectionId,
			@ApiParam(value = "Page", required = false)
			@RequestParam(value = "page", required = false) Integer page,
			@ApiParam(value = "Limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit) throws BadRequestException, NotFoundException {
		if(("MODEL").equalsIgnoreCase(type)) {
			return APIResponse.okStatus(this.adminService.getUserModelsList(userId, collectionId, page, limit));
		} else if (("WARDROBE").equalsIgnoreCase(type)) {
			return APIResponse.okStatus(this.adminService.getUserProductsList(userId, collectionId, page, limit));
		} else {
			throw new BadRequestException(BadRequestException.ERROR_INVALID_USER_COLLECTION_TYPE, "Type not found", false);
		}
	}

	/**
	 * Set status for user's account
	 *
	 * @param currentStatus values = {"ACTIVE, PENDING, DELETED"}
	 * @param updatedStatus values = {"DELETE, ACTIVE, REOPEN"}
	 * @param objectID list og users's ID
	 * @return {@link APIResponse} data is list of {@link AdminUserDTO}
	 * @throws BadRequestException
	 * @throws NotFoundException
	 */
	@PutMapping("/status")
	@ApiOperation(value = "Set status for user's account")
	public APIResponse<List<AdminUserDTO>> setStatusForUserAccount(
			@ApiParam(value = "Current Status", required = true, allowableValues = "ACTIVE, DELETED")
			@RequestParam(value = "currentStatus", required = true) String currentStatus,
			@ApiParam(value = "Updated Status", required = true, allowableValues = "DELETE, REACTIVE")
			@RequestParam(value = "updatedStatus", required = true) String updatedStatus,
			@ApiParam(value = "list of Users' ID")
			@RequestBody ObjectID objectID) throws BadRequestException, NotFoundException {
		return APIResponse.okStatus(this.adminService.setStatusForUserAccount(currentStatus, updatedStatus, objectID));
	}
}
