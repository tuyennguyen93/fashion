package bap.jp.smartfashion.api.admin.product;

import bap.jp.smartfashion.api.admin.product.dto.AdminProductDTO;
import bap.jp.smartfashion.api.admin.product.dto.AdminProductDetailDTO;
import bap.jp.smartfashion.api.admin.product.dto.ProductStatusDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.NotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/admin/products")
@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
public class AdminProductController {

    @Autowired
    private AdminProductService adminProductService;

    @GetMapping
    @ApiOperation("Get list of products's details")
    @ApiResponses(value = {

    })
    public APIResponse<PageInfo<AdminProductDTO>> getProductsList(
            @ApiParam(value = "Search by product's name, product code, gender, shop's name", required = false)
            @RequestParam(value = "query", required = false) String query,
            @ApiParam(value = "Current Status", required = false, allowableValues = "ACTIVE, PENDING")
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Page", required = false)
            @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "Limit", required = false)
            @RequestParam(value = "limit", required = false) Integer limit,
            @ApiParam(value ="Sort by", required = false, allowableValues = ",OLDEST_TO_NEWEST, NEWEST_TO_OLDEST")
            @RequestParam(value = "sort", required = false) String sortStr) {
        return APIResponse.okStatus(this.adminProductService.getProductsList(status, sortStr, query, page, limit));
    }

    /**
     * Get list of products' status with amount
     *
     * @return {@link APIResponse} data is list of {@link ProductStatusDTO}
     */
    @GetMapping("/status")
    @ApiOperation(value = "Get List of Product's status with amount")
    @ApiResponses(value = {})
    public APIResponse<List<ProductStatusDTO>> getProducts_StatusList() {
        return APIResponse.okStatus(this.adminProductService.getProducts_StatusList());
    }

    /**
     * Get Product's detail
     *
     * @param productId {@link Product}
     * @return {@link APIResponse} data is {@link AdminProductDetailDTO}
     * @throws NotFoundException
     */
    @GetMapping("/{productId}")
    public APIResponse<AdminProductDetailDTO> getProductDetails(
            @ApiParam(value = "Product ID", required = true)
            @PathVariable(value = "productId", required = true) Integer productId) throws NotFoundException {
        System.out.println(productId);
        return APIResponse.okStatus(this.adminProductService.getProductDetails(productId));
    }
}
