package bap.jp.smartfashion.api.admin.user.dto;

import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * A interface for projection jpa
 *
 */
public interface AdminUserStatus {
	int getAllStatus();
	int getActiveStatus();
	int getPendingStatus();
	int getDeletedStatus();
}
