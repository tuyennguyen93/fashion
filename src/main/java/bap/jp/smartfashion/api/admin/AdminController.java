package bap.jp.smartfashion.api.admin;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.util.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import bap.jp.smartfashion.api.authentication.AuthenticationService;
import bap.jp.smartfashion.api.authentication.dto.AuthenticationDTO;
import bap.jp.smartfashion.api.shop.ShopService;
import bap.jp.smartfashion.api.user.UserService;
import bap.jp.smartfashion.api.user.shop.UserShopService;
import bap.jp.smartfashion.common.base.BaseController;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.JwtToken;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;

/**
 * Controller to managing admin
 */
@RestController
@RequestMapping("/api/v1/admin")
@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_ANONYMOUS')")
public class AdminController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AdminService adminService;
	
	
	@Autowired
	private ShopService shopService;
	
	private final Logger log = LoggerFactory.getLogger(AdminController.class);
	/**
	 * Clear cache
	 * 
	 */
	@GetMapping("/cache")
	@ApiOperation(value = "Clear cache of user")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public APIResponse<?> clearCache(){
		log.info("REST request to clear cache");
		this.userService.clearCache();
		this.shopService.clearCache();
		return APIResponse.okStatus(null);
		}

	/**
	 * Get admin info by token
	 *
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@PostMapping("/token")
	@ApiOperation(value = "Get admin info by token")
	@ApiResponses(value = {
	})
	public APIResponse<UserDTO> getUserInfoByToken(HttpServletRequest httpServletRequest) throws NotFoundException {
		String jwtToken = httpServletRequest.getHeader(Constants.AUTHORIZATION_HEADER);
		log.info("REST request to get admin info by token");

		return APIResponse.okStatus(this.adminService.getAdminInfoByToken(jwtToken));
	}
}
