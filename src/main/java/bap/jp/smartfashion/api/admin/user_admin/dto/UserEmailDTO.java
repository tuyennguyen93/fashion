package bap.jp.smartfashion.api.admin.user_admin.dto;

import bap.jp.smartfashion.common.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class UserEmailDTO {
    private Integer id;
    private String name;
    private String email;
}
