package bap.jp.smartfashion.api.admin.product.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import bap.jp.smartfashion.common.dto.ProductSizeGroup;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductSizeDTO;
import bap.jp.smartfashion.common.model.ProductSize;
import bap.jp.smartfashion.common.repository.ProductSizeRepository;
import bap.jp.smartfashion.common.vo.ProductProperty;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

@Service
public class ProductSizeService extends BaseService {

	@Autowired
	private ProductSizeRepository productSizeRepository;

	public List<ProductProperty> getProductSizeList(String collectionType) {
		List<ProductProperty> productSizeVOs = new ArrayList<>();
		List<String> sizeLists = new ArrayList<>();
		Map<String, List<ProductSize>> productSizes = this.productSizeRepository.findAllByTypeOrderById(collectionType)
				.stream().collect(Collectors.groupingBy(a -> a.getProductSizeChart().getName(), LinkedHashMap::new,
						Collectors.toList()));
		if (!productSizes.isEmpty()) {
			sizeLists.add("ALL");
		}
		for (Map.Entry<String, List<ProductSize>> entry : productSizes.entrySet()) {
			String key = entry.getKey();
			List<String> sizes = new ArrayList<>();
			sizes.add("ALL");
			for (ProductSize productSize : entry.getValue()) {
				sizes.add(productSize.getSize());
				if (!sizeLists.contains(productSize.getSize())) {
					sizeLists.add(productSize.getSize());
				}
			}
			productSizeVOs.add(new ProductProperty(key, sizes));
		}
		productSizeVOs.add(0, new ProductProperty("ALL", sizeLists));
		return productSizeVOs;
	}

	public void deleteSize(Integer productSizeId) throws NotFoundException {
		this.findProductSizeById(productSizeId);
		this.productSizeRepository.deleteById(productSizeId);
	}

	private ProductSize findProductSizeById(Integer productSizeId) throws NotFoundException {
		return this.productSizeRepository.findById(productSizeId)
				.orElseThrow(() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_SIZE_NOT_FOUND,
						String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product size"), HttpStatus.NOT_FOUND));
	}

	/**
	 * Get List of product's size with collection
	 *
	 * @param collectionType {@link ProductEnum.SizeType}
	 * @return List<ProductProperty> {@link ProductProperty}
	 */
	public List<ProductProperty> getProductSizeWithCollectionList(String collectionType) {
		List<ProductProperty> productSizeVOs = new ArrayList<>();
		Map<String, List<ProductSize>> productSizes = this.productSizeRepository.findAllByTypeOrderById(collectionType)
				.stream().collect(Collectors.groupingBy(a -> a.getProductSizeChart().getName(), LinkedHashMap::new,
						Collectors.toList()));
		for (Map.Entry<String, List<ProductSize>> entry : productSizes.entrySet()) {
			String key = entry.getKey();
			List<ProductSizeGroup> sizes = new ArrayList<>();
			for (ProductSize productSize : entry.getValue()) {
				sizes.add(new ProductSizeGroup(productSize));
			}
			productSizeVOs.add(new ProductProperty(key, sizes));
		}
		return productSizeVOs;
	}
}
