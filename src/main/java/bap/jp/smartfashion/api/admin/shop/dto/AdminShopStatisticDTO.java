package bap.jp.smartfashion.api.admin.shop.dto;

import java.time.LocalDateTime;

public interface AdminShopStatisticDTO {
    Integer getId();
    String getName();
    String getStatus();
    LocalDateTime getCreated();
    long getPostedProducts();
    long getPendingProducts();
    String getCollections();
    String getBusinessAddress();
}
