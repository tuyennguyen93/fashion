package bap.jp.smartfashion.api.admin.shop.dto;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.ShopBranch;
import bap.jp.smartfashion.enums.ShopEnum.ShopStatus;
import bap.jp.smartfashion.util.CodeEnumUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class AdminShopDTO {

	private Integer id;
	private String name;
	private List<String> locations;
	private List<String> categories;
	private long postedProducts;
	private long pendingProducts;
	private String status;
	private Date created;
	
	public AdminShopDTO() {}
	
	public AdminShopDTO(Shop shop) {
		super();
		this.id = shop.getId();
		this.name = shop.getName();
		this.locations = this.buildLocation(shop.getShopBranches());
//		this.categories = categories;
//		this.postedProduct = postedProduct;
//		this.pendingProduct = pendingProduct;
		if(!StringUtils.isEmpty(shop.getStatus())) {
			this.status = CodeEnumUtils.fromCodeQuietly(ShopStatus.class, shop.getStatus()).getCode();
		}
		this.created = Date.from(shop.getCreated().atZone( ZoneId.systemDefault()).toInstant());
	}

	public AdminShopDTO(AdminShopStatisticDTO adminShopStatisticDTO) {
		this.id = adminShopStatisticDTO.getId();
		this.name = adminShopStatisticDTO.getName();
		this.status = adminShopStatisticDTO.getStatus();
		this.postedProducts = adminShopStatisticDTO.getPostedProducts();
		this.pendingProducts = adminShopStatisticDTO.getPendingProducts();
		this.created = Date.from(adminShopStatisticDTO.getCreated().atZone( ZoneId.systemDefault()).toInstant());
	}
	
	private List<String> buildLocation(List<ShopBranch> shopBranches) {
		List<String> locations = new ArrayList<String>();
		shopBranches.forEach(shopBranch -> {
			String[] businessAddress = shopBranch.getBusinessAddress().split(", ");
			int indexAdd = businessAddress.length > 1 ? businessAddress.length-2 : 0;
			String branch = businessAddress[indexAdd];
			locations.add(branch);
		});
		return locations;
	}

}
