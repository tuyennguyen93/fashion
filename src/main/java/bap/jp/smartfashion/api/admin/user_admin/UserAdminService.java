package bap.jp.smartfashion.api.admin.user_admin;

import bap.jp.smartfashion.api.admin.user_admin.dto.UserAdminDTO;
import bap.jp.smartfashion.api.admin.user_admin.dto.UserEmailDTO;
import bap.jp.smartfashion.api.admin.user_admin.dto.UserProperty;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.model.Role;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserRole;
import bap.jp.smartfashion.common.repository.UserRepository;
import bap.jp.smartfashion.common.repository.UserRoleRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.enums.RoleEnum;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserAdminService extends BaseService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private MessageService messageService;

    /**
     * Set role_admin for normal user
     *
     * @param userProperty {@link UserProperty}
     * @return {@link APIResponse} notice in case set role successfully
     * @throws NotFoundException
     */
    public Object setAdminRoleForUser(UserProperty userProperty) throws NotFoundException, BadRequestException {
        Optional<User> savedUser = this.userRepository.findByIdAndRegisterStatusIsTrueAndBlockedIsFalseAndDeletedIsFalse(userProperty.getUserId());
        if(!savedUser.isPresent()) {
            throw new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
                    String.format(this.messageService.getMessage("error.not-found-msg"),
                            this.messageService.getMessage("info.user")), HttpStatus.NOT_FOUND);
        }
        if(!UserEnum.UserTypeEnum.LOCAL.toString().equals(savedUser.get().getUserType())) {
            throw new BadRequestException(BadRequestException.ERROR_USER_PLATFORM_BAD_REQUEST,
                this.messageService.getMessage("error.user-is-not-object-update"), false);
        }
        if(savedUser.get().isDeleted()) {
            throw new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
                String.format(this.messageService.getMessage("error.not-found-msg"),
                    this.messageService.getMessage("info.user")));
        } else if (savedUser.get().isBlocked()) {
            throw new BadRequestException(BadRequestException.ERROR_USER_IS_BLOCKED,
                this.messageService.getMessage("error.account-is-blocked"), false);
        }

        Optional<Role> foundRole = savedUser.get().getRoles().stream().filter(role -> role.getName().equals(RoleEnum.ApplicationRole.ROLE_ADMIN.toString())).findFirst();
        if(!foundRole.isPresent()) {
            UserRole newUserRole = new UserRole();
            newUserRole.setRoleName(RoleEnum.ApplicationRole.ROLE_ADMIN.toString());
            newUserRole.setUserId(savedUser.get().getId());
            this.userRoleRepository.save(newUserRole);
        }
        return null;
    }

    /**
     * Delete role_admin of user
     *
     * @param userID {@link ObjectID}
     * @return notice in case delete successfully
     * @throws NotFoundException
     */
    @Transactional(rollbackFor = Exception.class)
    public Object deleteAdminRoleOfUsers(ObjectID userID) throws NotFoundException {
        List<UserRole> userRolesList = new ArrayList<>();
        for(Integer userId : userID.getIdsList()) {
            Optional<User> user = this.userRepository.findByIdAndRegisterStatusIsTrueAndBlockedIsFalseAndDeletedIsFalse(userId);
            if(!user.isPresent()) {
                throw new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
                        String.format(this.messageService.getMessage("error.not-found-msg"),
                                this.messageService.getMessage("info.user")), HttpStatus.NOT_FOUND);
            }
            Optional<Role> foundRole = user.get().getRoles().stream().filter(role -> role.getName().equals(RoleEnum.ApplicationRole.ROLE_ADMIN.toString())).findFirst();
            if(foundRole.isPresent()) {
                Optional<UserRole> userRole = this.userRoleRepository.findByUserIdAndRoleName(user.get().getId(), RoleEnum.ApplicationRole.ROLE_ADMIN.toString());
                userRolesList.add(userRole.get());
            }
        }
        if(!userRolesList.isEmpty()) {
            this.userRoleRepository.deleteAll(userRolesList);
        }
        return null;
    }

    /**
     * Get list of User Admin
     *
     * @param query Search Info: user's name or user's email
     * @param page current page
     * @param limit amount record in a page
     * @return {@link PageInfo} {@link UserAdminDTO}
     */
    public PageInfo<UserAdminDTO> getUserAdminsList(String query, Integer page, Integer limit) {
        String resultQuery = StringUtils.isEmpty(query) ? "" : query;

        PageRequest pageRequest = this.buildPageRequest(page, limit);

        Page<UserAdminDTO> data = this.userRepository
                .findAllByConditionsForSuperAdmin(resultQuery, UserEnum.UserTypeEnum.LOCAL.toString(), pageRequest);
        PageInfo<UserAdminDTO> userAdminsList = SmartFashionUtils.pagingResponse(data);
        return userAdminsList;
    }

    /**
     * Get list of User_Emails
     *
     * @param query Search Info: user's email
     * @param page current page
     * @param limit amount record in a page
     * @return {@link PageInfo} {@link UserEmailDTO}
     */
    public PageInfo<UserEmailDTO> getUserEmailsList(String query, Integer page, Integer limit) {
        Sort sort = Sort.by(Sort.Order.asc("email"));
        PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
        String resultQuery = StringUtils.isEmpty(query) ? "" : query;

        Page<UserEmailDTO> data = this.userRepository.findAllByEmailInfo(resultQuery, UserEnum.UserTypeEnum.LOCAL.toString(), pageRequest);

        PageInfo<UserEmailDTO> userEmailsList = SmartFashionUtils.pagingResponse(data);

        return userEmailsList;
    }
}
