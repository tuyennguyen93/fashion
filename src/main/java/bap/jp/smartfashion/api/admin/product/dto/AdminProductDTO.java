package bap.jp.smartfashion.api.admin.product.dto;

import bap.jp.smartfashion.common.model.Product;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.ZoneId;
import java.util.Date;

@Setter
@Getter
public class AdminProductDTO {
    private Integer id;
    private String name;
    private String category;
    private String postedBy;
    private Integer items;
    private String itemsKeyPointStatus;
    private Integer triedTime = 0;
    private String status;
    private Date created;

    public AdminProductDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.category = StringUtils.substring(product.getProductCollection().getName(), 0, 1).toUpperCase();
        this.postedBy = product.getShop().getName();
        this.items = product.getProductDetails().size();
        this.status = product.getStatus();
        this.created = Date.from(product.getCreated().atZone( ZoneId.systemDefault()).toInstant());
    }
}
