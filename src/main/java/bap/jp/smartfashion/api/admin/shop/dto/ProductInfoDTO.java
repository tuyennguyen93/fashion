package bap.jp.smartfashion.api.admin.shop.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ProductInfoDTO {
    private Integer id;
    private String name;
    private String collection;
    private Integer items = 0;
    private String keyPointStatus;
    private Integer tried = 0;
    private Date created;
}
