package bap.jp.smartfashion.api.user.product;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.UserProductCollectionDTO;
import bap.jp.smartfashion.common.dto.UserProductDTO;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductPhoto;
import bap.jp.smartfashion.common.model.UserProduct;
import bap.jp.smartfashion.common.model.UserProductCollection;
import bap.jp.smartfashion.common.repository.ProductDetailRepository;
import bap.jp.smartfashion.common.repository.ProductPhotoRepository;
import bap.jp.smartfashion.common.repository.UserProductCollectionRepository;
import bap.jp.smartfashion.common.repository.UserProductRepository;
import bap.jp.smartfashion.common.repository.UserProductTryOnRepository;
import bap.jp.smartfashion.common.service.ai.AIService;
import bap.jp.smartfashion.common.service.ai.response.ClothingResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;


/**
 * Service class for managing products by shop.
 */
@Service
public class UserProductService extends BaseService {

	@Autowired
	private UserProductRepository userProductRepository;

	@Autowired
	private UserProductCollectionRepository userProductCollectionRepository;
	
	@Autowired
	private AIService aiService;

	@Autowired
	private ProductDetailRepository productDetailRepository;
	
	@Autowired
	private ProductPhotoRepository productPhotoRepository;
	
	@Autowired
	private UserProductTryOnRepository userProductTryOnRepository; 
	
	private final Logger log = LoggerFactory.getLogger(UserProductService.class);

	private static final String CACHE_USER_PRODUCT_COLLECTION_LIST = "getListUserProductCollection";
	private static final String CACHE_USER_PRODUCT_COLLECTION = "getUserProductCollectionById";
	private static final String CACHE_USER_PRODUCT_LIST = "getListUserProduct";
	private static final String CACHE_USER_PRODUCT = "getUserProduct";
	
	/**
	 * Get {@link UserProductCollection}
	 *
	 * @return List of {@link UserProductCollectionDTO}
	 */
	//@Cacheable(value = CACHE_USER_PRODUCT_COLLECTION_LIST)
	public List<UserProductCollectionDTO> getListUserProductCollection() {
		List<UserProductCollectionDTO> userProductCollectionDTOs = new ArrayList<UserProductCollectionDTO>();
		this.userProductCollectionRepository.findAll().forEach(userProductCollection -> {
			UserProductCollectionDTO userProductCollectionDTO = new UserProductCollectionDTO(userProductCollection);
			int count = this.userProductRepository.countByUser_IdAndUserProductCollection_Id(getCurrentUserId(), userProductCollection.getId());
			userProductCollectionDTO.setCount(count);
			userProductCollectionDTOs.add(userProductCollectionDTO);
		});
		return userProductCollectionDTOs;
	}
	
	/**
	 * Get {@link UserProductCollection} by Id
	 *
	 * @return {@link UserProductCollectionDTO}
	 * @throws NotFoundException 
	 */
	@Cacheable(value = CACHE_USER_PRODUCT_COLLECTION, key = "#id")
	private UserProductCollectionDTO getUserProductCollectionById(Integer id) throws NotFoundException {
		UserProductCollection userProductCollection= this.userProductCollectionRepository.findById(id).orElseThrow(() ->
			 new NotFoundException(NotFoundException.ERROR_PRODUCT_COLLECTION_NOT_FOUND, "User product collection not found"));
		
		return new UserProductCollectionDTO(userProductCollection);
	}
	

	/**
	 * Get list {@link UserProductCollection}
	 *
	 * @return PageInfo of {@link UserProductCollectionDTO}
	 * @throws NotFoundException 
	 */
	@Cacheable(value = CACHE_USER_PRODUCT_LIST, key = "#userId +  #page + #limit + #userProductCollectionId", condition="#page != null and #limit != null ")
	public PageInfo<UserProductDTO> getListProductByUserProductCollection(Integer userProductCollectionId, Integer page, Integer limit, Integer userId) throws NotFoundException {
		this.getUserProductCollectionById(userProductCollectionId);
		PageRequest pageRequest = this.buildPageRequest(page, limit);
		Page<UserProductDTO> data = this.userProductRepository.findAllByUser_IdAndUserProductCollection_Id(userId, userProductCollectionId, pageRequest).map(UserProductDTO::new);
		PageInfo<UserProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * Get list {@link UserProductCollection}
	 *
	 * @return PageInfo of {@link UserProductCollectionDTO}
	 */
	@Cacheable(value = CACHE_USER_PRODUCT_LIST, key = "#userId +  #page + #limit + #sortStr", condition="#page != null and #limit != null")
	public PageInfo<UserProductDTO> getListProduct(Integer page, Integer limit, String sortStr, Integer userId) {
		Sort sort = this.buildSortUserProduct(sortStr);
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Page<UserProductDTO> data = this.userProductRepository.findAllByUser_Id(userId, pageRequest).map(UserProductDTO::new);
		PageInfo<UserProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;

	}
	
	/**
	 * Get list {@link UserProductCollection}
	 *
	 * @return PageInfo of {@link UserProductCollectionDTO}
	 */
	public PageInfo<UserProductDTO> getListProductRandom(Integer page, Integer limit, String sortStr, Integer userId) {
		Sort sort = this.buildSortUserProduct(sortStr);
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Page<UserProductDTO> data = this.userProductRepository.findUserProductRandomByUser_Id(userId, pageRequest).map(UserProductDTO::new);
		PageInfo<UserProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * Create {@link UserProductDTO}
	 *
	 * @return {@link UserProductDTO}
	 * @throws NotFoundException 
	 * @throws SmartFashionException 
	 * @throws SocketTimeoutException 
	 */
	@Caching(evict = {
		@CacheEvict(value = CACHE_USER_PRODUCT_LIST, allEntries = true),
	})
	public UserProductDTO createUserProduct(UserProductDTO userProductDTO) throws NotFoundException, SmartFashionException {
		UserProductCollectionDTO userProductCollectionDTO = this.getUserProductCollectionById(userProductDTO.getUserProductCollectionId());
		UserProduct userProduct = new UserProduct();
		userProduct.setUser(this.getCurrentUserLogged());
		userProduct.setUrl(userProductDTO.getUrl());
		UserProductCollection userProductCollection = new UserProductCollection();
		BeanUtils.copyProperties(userProductCollectionDTO, userProductCollection);
		userProduct.setUserProductCollection(userProductCollection);
		
		ClothingResponse clothingResponse = null;
		try {
			clothingResponse = this.aiService.generateKeyPointForUserProduct(userProduct);
		} catch (ResourceAccessException | SocketTimeoutException e) {
			e.printStackTrace();
			throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CONNECTION, "AI connection error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (clothingResponse == null || clothingResponse.getData() == null) {
			throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_KEYPOINT, "Can't get key point", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		userProduct.setKeyPoint(clothingResponse.getData().get(0).getKeyPoint());
		userProduct.setKeyPointMessage(clothingResponse.getMessage());
		return new UserProductDTO(this.userProductRepository.save(userProduct));
	}
	
	/**
	 * Get {@link UserProductDTO}
	 *
	 * @return {@link UserProduct}
	 * @throws NotFoundException 
	 */
	@Cacheable(value = CACHE_USER_PRODUCT)
	public UserProduct getUserProduct(Integer userProductId) throws NotFoundException {
		return this.userProductRepository.findByIdAndUser_Id(userProductId, this.getCurrentUserId()).orElseThrow(() -> 
		 	new NotFoundException(NotFoundException.ERROR_USER_PRODUCT_NOT_FOUND, "User product not found"));
	}
	
	/**
	 * Delete {@link UserProductDTO}
	 *
	 * @return {@link UserProductDTO}
	 * @throws NotFoundException 
	 */
	@Caching(evict = {
		@CacheEvict(value = CACHE_USER_PRODUCT_LIST, allEntries = true),
		@CacheEvict(value = CACHE_USER_PRODUCT, allEntries = true)
	})
	@Transactional(rollbackFor = Exception.class)
	public UserProductDTO deleteUserProduct(Integer userProductId) throws NotFoundException {
		UserProduct userProduct = this.getUserProduct(userProductId);
		this.userProductRepository.delete(userProduct);
		this.userProductTryOnRepository.deleteByUserProduct_Id(userProduct.getId());
		return new UserProductDTO(userProduct);
	}
	
	
	public ProductDetail getShopProduct(Integer productId) throws NotFoundException {
		return this.productDetailRepository.findTop1ProductDetailByProductId(productId).orElseThrow(() -> 
			new NotFoundException(NotFoundException.ERROR_PRODUCT_DETAIL_NOT_FOUND, "Product detail not found"));
	}
	
	public ProductPhoto getShopProductPhoto(Integer productDetailId) throws NotFoundException {
		return this.productPhotoRepository.findTop1ProductPhotoByProductDetailId(productDetailId).orElseThrow(() -> 
			new NotFoundException(NotFoundException.ERROR_PRODUCT_PHOTO_NOT_FOUND, "Product photo not found"));
	}
	
	/**
	 * Build sort function for {@link UserProduct}
	 * @param sortBy
	 * @return {@link Sort}
	 */
	private Sort buildSortUserProduct(String sortBy) {
		sortBy = StringUtils.trim(sortBy);
		Sort sort = Sort.by(Sort.Order.asc("id"));
		if ("OLDEST_TO_NEWEST".equals(sortBy)) {
			sort = Sort.by(Sort.Order.asc("created"));
		} else if ("NEWEST_TO_OLDEST".equals(sortBy)) {
			sort = Sort.by(Sort.Order.desc("created"));
		}
		return sort;
	}
}
