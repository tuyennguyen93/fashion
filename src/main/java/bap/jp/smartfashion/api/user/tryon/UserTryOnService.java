package bap.jp.smartfashion.api.user.tryon;

import bap.jp.smartfashion.api.user.model.UserModelService;
import bap.jp.smartfashion.api.user.product.UserProductService;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.TryOnDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.model.ProductPhoto;
import bap.jp.smartfashion.common.model.ShopProductTryOn;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserProduct;
import bap.jp.smartfashion.common.model.UserProductInteraction;
import bap.jp.smartfashion.common.model.UserProductTryOn;
import bap.jp.smartfashion.common.repository.ProductDetailRepository;
import bap.jp.smartfashion.common.repository.ProductInteractionRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ShopProductTryOnRepository;
import bap.jp.smartfashion.common.repository.UserProductInteractionRepository;
import bap.jp.smartfashion.common.repository.UserProductTryOnRepository;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.common.service.ai.AIService;
import bap.jp.smartfashion.common.service.ai.request.TryOnRequest;
import bap.jp.smartfashion.common.service.ai.response.TryOnResponse;
import bap.jp.smartfashion.common.vo.Message;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;


/**
 * Service class for managing try on
 */
@Service
public class UserTryOnService extends BaseService {

	@Autowired
	private AIService aiService;

	@Autowired
	private UserModelService userModelService;
	
	@Autowired
	private UserProductService userProductService;

	@Autowired
	private ProductDetailRepository productDetailRepository;
	
	@Autowired
	private UserProductTryOnRepository userProductTryOnRepository; 
	
	@Autowired
	private ShopProductTryOnRepository shopProductTryOnRepository;
	
	@Autowired
	private ProductInteractionRepository productInteractionRepository;
	
	@Autowired
	private UserProductInteractionRepository userProductInteractionRepository;
	
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private MessageService messageService;
	
	private final Logger log = LoggerFactory.getLogger(UserTryOnService.class);
	
	/**
	 * Try on product
	 *
	 *@param tryOnDTO {@link TryOnDTO}
	 * @return {@link TryOnDTO}
	 * @throws NotFoundException 
	 * @throws SmartFashionException 
	 */
	public TryOnDTO tryOn(TryOnDTO tryOnDTO) throws NotFoundException, SmartFashionException {
		UserModel userModel = this.userModelService.getUserModelBelongUserAndSystem(tryOnDTO.getModelId());
		if (tryOnDTO.getIsUserProduct()) {
			tryOnDTO = tryOnUserProduct(tryOnDTO, userModel);
		} else {
			tryOnDTO = tryOnShopProduct(tryOnDTO, userModel);
		}
		
		return tryOnDTO;
		
	}
	
	@Transactional(rollbackFor = Exception.class)
	private TryOnDTO tryOnUserProduct(TryOnDTO tryOnDTO, UserModel userModel) throws NotFoundException, SmartFashionException {
		UserProduct userProduct = this.userProductService.getUserProduct(tryOnDTO.getProductId());
		Optional<UserProductTryOn> userProductTryOnOptional = this.userProductTryOnRepository.findByUser_IdAndUserModel_IdAndUserProduct_Id(getCurrentUserId(), userModel.getId(), userProduct.getId());
		if (!userProductTryOnOptional.isPresent()) {
			TryOnResponse tryOnResponse = null;
			try {
				tryOnResponse = this.aiService.tryOn(this.buildTryOnUserProductParams(userModel, userProduct));
			} catch (ResourceAccessException e) {
				e.printStackTrace();
				throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CONNECTION,
						this.messageService.getMessage("error.AI-connection"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			if (tryOnResponse == null || tryOnResponse.getData() == null) {
				throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CANT_TRY,
						this.messageService.getMessage("error.try-product"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			tryOnDTO.setTriedOnURL(tryOnResponse.getData().get(0).getTriedOnImage());
			
			UserProductTryOn userProductTryOn = new UserProductTryOn();
			userProductTryOn.setUser(getCurrentUserLogged());
			userProductTryOn.setUserProduct(userProduct);
			userProductTryOn.setUserModel(userModel);
			userProductTryOn.setUrl(tryOnDTO.getTriedOnURL());
			this.userProductTryOnRepository.save(userProductTryOn);
		} else {
			tryOnDTO.setTriedOnURL(userProductTryOnOptional.get().getUrl());
		}
		return tryOnDTO;
	}
	
	private TryOnDTO tryOnShopProduct(TryOnDTO tryOnDTO, UserModel userModel) throws NotFoundException, SmartFashionException {
		ProductDetail productDetail = this.productDetailRepository.findById(tryOnDTO.getProductId()).orElseThrow(()->
				new NotFoundException(String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("product-detail"))));
		ProductPhoto productPhoto = this.userProductService.getShopProductPhoto(productDetail.getId());
		Optional<ShopProductTryOn> userProductTryOnOptional = this.shopProductTryOnRepository.findByUser_IdAndUserModel_IdAndProductDetail_Id(getCurrentUserId(), userModel.getId(), productDetail.getId());
		if (!userProductTryOnOptional.isPresent()) {
			TryOnResponse tryOnResponse = null;
			try {
				tryOnResponse = this.aiService.tryOn(this.buildTryOnShopProductParams(userModel, productDetail, productPhoto.getUrlOriginal()));
			} catch (ResourceAccessException e) {
				e.printStackTrace();
				throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CONNECTION,
						this.messageService.getMessage("error.AI-connection"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			if (tryOnResponse == null || tryOnResponse.getData() == null) {
				throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CANT_TRY,
						this.messageService.getMessage("error.try-product"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			tryOnDTO.setTriedOnURL(tryOnResponse.getData().get(0).getTriedOnImage());
			
			ShopProductTryOn shopProductTryOn = new ShopProductTryOn();
			shopProductTryOn.setUser(getCurrentUserLogged());
			shopProductTryOn.setProductDetail(productDetail);
			shopProductTryOn.setUserModel(userModel);
			shopProductTryOn.setUrl(tryOnDTO.getTriedOnURL());
			this.shopProductTryOnRepository.save(shopProductTryOn);
			this.trackingUserTriedOnShopProduct(tryOnDTO.getProductId());
		} else {
			tryOnDTO.setTriedOnURL(userProductTryOnOptional.get().getUrl());
		}
		return tryOnDTO;
	}
	
	private TryOnRequest buildTryOnUserProductParams(UserModel userModel, UserProduct userProduct) {
		TryOnRequest tryOnRequest = new TryOnRequest();
		tryOnRequest.setUserId(getCurrentUserId());
		tryOnRequest.setIsUser(true);
		tryOnRequest.setClothId(userProduct.getId());
		tryOnRequest.setClothURL(userProduct.getUrl());
		tryOnRequest.setClothKeypoints(userProduct.getKeyPoint());
		tryOnRequest.setPersonId(userModel.getId());
		tryOnRequest.setPersonURL(userModel.getUrl());
		tryOnRequest.setPersonKeypoints(userModel.getKeyPoint());
		
		return tryOnRequest;
	}
	
	private TryOnRequest buildTryOnShopProductParams(UserModel userModel, ProductDetail productDetail, String url) {
		TryOnRequest tryOnRequest = new TryOnRequest();
		tryOnRequest.setUserId(getCurrentUserId());
		tryOnRequest.setIsUser(true);
		tryOnRequest.setClothId(productDetail.getId());
		tryOnRequest.setClothURL(url);
		tryOnRequest.setClothKeypoints(productDetail.getKeyPoint());
		tryOnRequest.setPersonId(userModel.getId());
		tryOnRequest.setPersonURL(userModel.getUrl());
		tryOnRequest.setPersonKeypoints(userModel.getKeyPoint());
		
		return tryOnRequest;
	}
	
	private void trackingUserTriedOnShopProduct(Integer productDetailId) {
		Integer userId = this.getCurrentUserId();
		Optional<UserProductInteraction> optional = this.userProductInteractionRepository.findByUser_IdAndProduct_IdAndActionCode(userId, productDetailId, ProductEnum.ProductInteraction.TRY_PRODUCT.getCode());
		Product product = this.productRepository.findByProductDetails_Id(productDetailId).orElse(null);
		if (!optional.isPresent()) {
			UserProductInteraction userProductInteraction = new UserProductInteraction();
			userProductInteraction.setUser(this.getCurrentUserLogged());
			userProductInteraction.setProduct(product);
			userProductInteraction.setActionCode(ProductEnum.ProductInteraction.TRY_PRODUCT.getCode());
			userProductInteraction.setActionCount(1);
			this.userProductInteractionRepository.save(userProductInteraction);
			Optional<ProductInteraction> productInteractionOptional =  this.productInteractionRepository.findByProduct_IdAndActionCode(product.getId(), ProductEnum.ProductInteraction.TRY_PRODUCT.getCode());
			ProductInteraction productInteraction = new ProductInteraction();
			if(productInteractionOptional.isPresent()) {
				productInteraction = productInteractionOptional.get();
				productInteraction.setActionCount(productInteraction.getActionCount() + 1);
				this.productInteractionRepository.save(productInteraction);
			} else {
				productInteraction.setProduct(product);
				productInteraction.setActionCode(ProductEnum.ProductInteraction.TRY_PRODUCT.getCode());
				productInteraction.setActionCount(1);
				this.productInteractionRepository.save(productInteraction);
			};
		}
	}
}
