package bap.jp.smartfashion.api.user.product;

import bap.jp.smartfashion.common.dto.UserProductDTO;
import bap.jp.smartfashion.common.dto.UserProductCollectionDTO;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.net.SocketTimeoutException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller to managing products by shop
 */
@RestController
@RequestMapping("/api/v1/users/products")
@PreAuthorize("hasAnyRole('ROLE_USER')")
public class UserProductController {

	private final Logger log = LoggerFactory.getLogger(UserProductController.class);
	
	@Autowired
	private UserProductService userProductService;

	/**
	 * Get {@link UserProductCollection}
	 *
	 * @return {@link APIResponse} type {@link UserProductCollectionDTO}
	 */
	@GetMapping(value = "/collections")
	@ApiOperation(value = "Get list user product collection")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<List<UserProductCollectionDTO>> getListUserProductCollection() {
		return APIResponse.okStatus(this.userProductService.getListUserProductCollection());
	}
	
	/**
	 * Get {@link UserProduct} list
	 *
	 * @return APIResponse  of {@link UserProductDTO} paging
	 * @throws NotFoundException 
	 */
	@GetMapping(value = "/{userProductCollectionId}")
	@ApiOperation(value = "Get list products by user collection id")
	@ApiResponses(value = {
	})
	public APIResponse<PageInfo<UserProductDTO>> getListProductByUserProductCollection(
		@ApiParam(value ="User product collection", required = true)
		@PathVariable(value = "userProductCollectionId", required = true) Integer userProductCollectionId,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit) throws NotFoundException {
		return APIResponse.okStatus(this.userProductService.getListProductByUserProductCollection(userProductCollectionId, page, limit,  this.userProductService.getCurrentUserId()));
	}
	
	/**
	 * Create {@link UserProduct}
	 *
	 * @param userProductDTO {@link UserProductDTO}
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 * @throws SocketTimeoutException 
	 * @throws BadRequestException 
	 */
	@PostMapping
	@ApiOperation(value = "Create user product")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<UserProductDTO> createUserProduct(
			@Valid @RequestBody UserProductDTO userProductDTO, Errors errors) throws NotFoundException, SmartFashionException, SocketTimeoutException, BadRequestException {
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(messages.toString(), true);
		}
		return APIResponse.createdStatus(this.userProductService.createUserProduct(userProductDTO));
	}

	/**
	 * Get product user for user loggin
	 *
	 * @param page page number
	 * @param limit limit on per page
	 * @return APIResponse {@link APIResponse}
	 */
	@GetMapping
	@ApiOperation(value = "Get list user product")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<PageInfo<UserProductDTO>> getListProduct(
			@ApiParam(value ="Page", required = false)
			@RequestParam(value = "page", required = false) Integer page,

			@ApiParam(value ="Limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit,
			
			@ApiParam(value ="Sort by", required = false, allowableValues = ", OLDEST_TO_NEWEST, NEWEST_TO_OLDEST")
			@RequestParam(value = "sort", required = false) String sort,

			@ApiParam(value ="Is random items", required = false)
			@RequestParam(value = "isRandom", required = false) boolean isRandom) throws NotFoundException {
		log.info("REST request to get list product user");
		PageInfo<UserProductDTO> pageInfo = null;
		if (isRandom) {
			pageInfo = this.userProductService.getListProductRandom(page, limit, sort, this.userProductService.getCurrentUserId());
		} else {
			pageInfo = this.userProductService.getListProduct(page, limit, sort, this.userProductService.getCurrentUserId());
		}
		return APIResponse.okStatus(pageInfo);
	}
	
	/**
	 * Delete product for user
	 *
	 * @param userProductId {@link UserProduct}
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws NotFoundException
	 */
	@DeleteMapping(value = "{userProductId}")
	@ApiOperation(value = "Delete user product")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<UserProductDTO> deleteUserProduct(
			@ApiParam(value = "UserProduct ID", required = true)
			@PathVariable(value = "userProductId", required = true) int userProductId) throws NotFoundException {
		log.info("REST request to detele userProduct id: {}", userProductId);
		return APIResponse.okStatus(this.userProductService.deleteUserProduct(userProductId));
	}
}
