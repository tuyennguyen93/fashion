package bap.jp.smartfashion.api.user.model;

import bap.jp.smartfashion.common.dto.UserModelCollectionDTO;
import bap.jp.smartfashion.common.dto.UserModelDTO;
import bap.jp.smartfashion.common.dto.UserModelCollectionDTO;
import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller to managing models by shop
 */
@RestController
@RequestMapping("/api/v1/users/models")
@PreAuthorize("hasAnyRole('ROLE_USER')")
public class UserModelController {

	private final Logger log = LoggerFactory.getLogger(UserModelController.class);
	
	@Autowired
	private UserModelService userModelService;
	/**
	 * Get {@link UserModelCollection}
	 *
	 * @return {@link APIResponse} type {@link UserModelCollectionDTO}
	 */
	@GetMapping(value = "/collections")
	@ApiOperation(value = "Get list user product collection")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<List<UserModelCollectionDTO>> getListUserModelCollection() {
		return APIResponse.okStatus(this.userModelService.getListUserModelCollection());
	}
	
	/**
	 * Get {@link UserModel} list
	 *
	 * @return APIResponse  of {@link UserModelDTO} paging
	 * @throws NotFoundException 
	 */
	@GetMapping(value = "/{userModelCollectionId}")
	@ApiOperation(value = "Get list models by user collection id")
	@ApiResponses(value = {
	})
	public APIResponse<PageInfo<UserModelDTO>> getListModelByUserModelCollection(
		@ApiParam(value ="User model collection", required = true)
		@PathVariable(value = "userModelCollectionId", required = true) Integer userModelCollectionId,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit) throws NotFoundException {
		return APIResponse.okStatus(this.userModelService.getListModelByUserModelCollection(userModelCollectionId, page, limit,  this.userModelService.getCurrentUserId()));
	}
	
	/**
	 * Get {@link UserModel} list
	 *
	 * @return APIResponse  of {@link UserModelDTO} paging
	 */
	@GetMapping
	@ApiOperation(value = "Get list models")
	@ApiResponses(value = {
	})
	
	public APIResponse<PageInfo<UserModelDTO>> getListModel(
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit) {
		return APIResponse.okStatus(this.userModelService.getListModel(page, limit, this.userModelService.getCurrentUserId()));
	}
	
	/**
	 * Create {@link UserModel}
	 *
	 * @param model {@link UserModelDTO}
	 * @return APIResponse of {@link UserModelDTO}
	 * @throws NotFoundException 
	 * @throws SmartFashionException 
	 * @throws BadRequestException 
	 */
	@PostMapping
	@ApiOperation(value = "Create new model")
	public APIResponse<UserModelDTO> createModel(@Valid @RequestBody UserModelDTO userModelDTO, Errors errors) throws NotFoundException, SmartFashionException, BadRequestException {
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(messages.toString(), true);
		}
		return APIResponse.okStatus(this.userModelService.createUserModel(userModelDTO));
	}
	
	/**
	 * Delete {@link UserModel}
	 *
	 * @param modelId {@link UserModel} Id
	 * @return APIResponse of {@link UserModelDTO}
	 * @throws NotFoundException 
	 */
	@DeleteMapping("/{modelId}")
	@ApiOperation(value = "Delete model")
	@ApiResponses(value = {
	})
	public APIResponse<UserModelDTO> deleteModel(@PathVariable(value = "modelId") Integer modelId) throws NotFoundException {
		return APIResponse.okStatus(this.userModelService.deleteUserModel(modelId));
	}
	
	/**
	 * Set {@link UserModel} default for user
	 *
	 * @param modelId {@link UserModel} Id
	 * @return APIResponse of {@link UserModelDTO}
	 * @throws NotFoundException 
	 */
	@PostMapping("/default/{modelId}")
	@ApiOperation(value = "Set default model for user")
	@ApiResponses(value = {
	})
	public APIResponse<UserModelDTO> setDefaultModel(@PathVariable(value = "modelId") Integer modelId) throws NotFoundException {
		return APIResponse.okStatus(this.userModelService.setDefaultModel(modelId));
	}
	
	/**
	 * Get {@link UserModel} default for user
	 *
	 * @return APIResponse of {@link UserModelDTO}
	 * @throws NotFoundException 
	 */
	@GetMapping("/default")
	@ApiOperation(value = "Get default model for user")
	@ApiResponses(value = {
	})
	public APIResponse<UserModelDTO> getDefaultModel() throws NotFoundException {
		return APIResponse.okStatus(this.userModelService.getDefaultModel());
	}
}
