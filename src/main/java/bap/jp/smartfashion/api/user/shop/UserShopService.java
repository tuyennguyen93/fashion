package bap.jp.smartfashion.api.user.shop;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import bap.jp.smartfashion.api.admin.product.service.ProductSizeService;
import bap.jp.smartfashion.api.admin.product.service.ProductWearingPurposeService;
import bap.jp.smartfashion.common.dto.ProductWearingPurposeDTO;
import bap.jp.smartfashion.common.repository.ProductColorRepository;
import bap.jp.smartfashion.common.repository.UserModelRepository;
import bap.jp.smartfashion.common.repository.UserModelSettingRepository;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.ProductColorVO;
import bap.jp.smartfashion.common.vo.ProductProperty;
import bap.jp.smartfashion.common.vo.ShopFilterMenu;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserModelSetting;
import bap.jp.smartfashion.common.model.UserProductBasket;
import bap.jp.smartfashion.common.model.UserProductInteraction;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ProductTypeCountRepository;
import bap.jp.smartfashion.common.repository.UserProductBasketRepository;
import bap.jp.smartfashion.common.repository.UserProductInteractionRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.common.specification.ProductForUserSpecificationBuilder;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ProductFilter;
import bap.jp.smartfashion.common.vo.ProductForUserFilter;
import bap.jp.smartfashion.common.vo.ShopMenu;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.enums.ProductEnum.ProductInteraction;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;

/**
 * Service class for managing users.
 */
@Service
public class UserShopService extends BaseService {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductTypeCountRepository productTypeCountRepository;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private UserProductBasketRepository userProductBasketRepository;
	
	
	@Autowired
	private UserProductInteractionRepository userProductInteractionRepository;

	@Autowired
	private ProductColorRepository productColorRepository;

	@Autowired
	private UserModelSettingRepository userModelSettingRepository;

	@Autowired
	private ProductSizeService productSizeService;

	@Autowired
	private ProductWearingPurposeService productWearingPurposeService;

	public PageInfo<ProductDTO> getProductList(String findType, boolean isRandom, ProductForUserFilter productFilter) throws BadRequestException, NotFoundException {
		UserModel userModel = this.getUserModelUserLogged();
		productFilter.setCollectionId(userModel.getUserModelCollection().getId());
		String productCollectionType = StringUtils.EMPTY;
		switch (StringUtils.defaultString(findType)) {
			case "TOPS":
				this.buildProductTypeForTops(productFilter);
				break;
	
			case "BOTTOMS":
				this.buildProductTypeForBottoms(productFilter);
				break;
				
			case "FAVORITED":
				productFilter.setProductFavorited(true);
				productCollectionType = ProductInteraction.FAVORITE_PRODUCT.getCode();
				break;
			case "TRIED_ON":
				productFilter.setProductTriedOn(true);
				productCollectionType = ProductInteraction.TRY_PRODUCT.getCode();
				break;
			case "BASKET":
				productFilter.setProductBasket(true);
				break;
				
		}
		Sort sort = productService.buildSortProduct(productFilter.getSortBy());
		PageRequest pageRequest = this.buildPageRequest(productFilter.getPage(), productFilter.getLimit(), sort);
		List<Integer> productTypes = productService.getProductTypes((Integer) productFilter.getProductTypeId());
		productFilter.setProductTypeId(productTypes);
		Page<ProductDTO> data = null;
		if (!isRandom) {
			if(productFilter.isProductFavorited() || productFilter.isProductTriedOn() || ProductEnum.ProductSort.TRIED_PRODUCTS.toString().equals(productFilter.getSortBy()) ||
					ProductEnum.ProductSort.FAVORITED_PRODUCTS.toString().equals(productFilter.getSortBy())) {
				data = this.productRepository.findAll(
						ProductForUserSpecificationBuilder.searchProductsWithInteraction(this.getCurrentUserId(), productFilter), pageRequest)
						.map(product -> this.convertProductToProductDTO(product));
			} else {
				data = this.productRepository.findAll(
						ProductForUserSpecificationBuilder.searchProducts(productFilter), pageRequest)
						.map(product -> this.convertProductToProductDTO(product));
			}
		} else {
			if (StringUtils.isNotEmpty(productFilter.getSortBy())) {
				throw new BadRequestException(BadRequestException.ERROR_SORT_RANDOM_DATA_BAD_REQUEST, this.messageService.getMessage("error.sort-with-random-data"), false);
			}
			if (StringUtils.isNotEmpty(productCollectionType)) {
				data = this.productRepository.findRandomProductByInteraction(this.getCurrentUserId(), productFilter.getCollectionId(), productCollectionType, pageRequest).map(ProductDTO::new);
			} else {
				if (productFilter.isProductBasket()) {
					data = this.productRepository.findRandomProductBasket(productFilter.getCollectionId(), pageRequest).map(ProductDTO::new);
				} else {
					data = this.productRepository.findRandomProduct(productFilter.getCollectionId(), pageRequest).map(ProductDTO::new);
				}
			}
		}
		PageInfo<ProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	private ProductDTO convertProductToProductDTO(Product product) {
		ProductDTO productDTO = new ProductDTO(product);
		List<UserProductInteraction> userProductInteractions = this.userProductInteractionRepository.findByUser_IdAndProduct_Id(getCurrentUserId(), product.getId());
		UserProductInteraction userProductInteractionTriedOn = userProductInteractions.stream()
				.filter(productInteraction -> productInteraction.getActionCode().equals(ProductInteraction.TRY_PRODUCT.getCode())).findFirst().orElse(null);
		productDTO.setIsTry(userProductInteractionTriedOn != null);
		
		UserProductInteraction userProductInteractionFavorited = userProductInteractions.stream()
				.filter(productInteraction -> productInteraction.getActionCode().equals(ProductInteraction.FAVORITE_PRODUCT.getCode())).findFirst().orElse(null);
		productDTO.setIsFavorite(userProductInteractionFavorited != null);
		return productDTO;
	}
	
	private ProductForUserFilter buildProductTypeForTops(ProductForUserFilter productFilter) {
		if(productFilter.getCollectionId() == 1) {
			productFilter.setProductTypeId(5);
		} else if(productFilter.getCollectionId() == 2) {
			productFilter.setProductTypeId(1);
		} else if(productFilter.getCollectionId() == 3) {
			productFilter.setProductTypeId(11);
		} else if(productFilter.getCollectionId() == 4) {
			productFilter.setProductTypeId(14);
		}
		return productFilter;
	}
	
	private ProductForUserFilter buildProductTypeForBottoms(ProductForUserFilter productFilter) {
		if(productFilter.getCollectionId() == 1) {
			productFilter.setProductTypeId(6);
		} else if(productFilter.getCollectionId() == 2) {
			productFilter.setProductTypeId(2);
		} else if(productFilter.getCollectionId() == 3) {
			productFilter.setProductTypeId(12);
		} else if(productFilter.getCollectionId() == 4) {
			productFilter.setProductTypeId(15);
		}
		return productFilter;
	}
	
	/**
	 * Get clothing menu
	 * @param collectionId {@link ProductCollection} id
	 * @return List {@link ShopMenu}
	 * @throws NotFoundException
	 */
	public List<ShopMenu> getClothingMenu(Integer collectionId) throws NotFoundException {
		ProductCollection productCollection = this.findProductCollectionById(collectionId);
		List<ShopMenu> shopMenus =new ArrayList<>();
		List<ShopMenu> menuParents = this.productTypeCountRepository.findByProductType_ProductCollection_Id(productCollection.getId());
		for (ShopMenu shopMenu : menuParents) {
			List<ShopMenu> productTypeCountChilds = this.productTypeCountRepository.findByProductType_ProductTypeParentIdOrderById(shopMenu.getId());
			List<ShopMenu> shopMenuChilds =new ArrayList<>();
			for (ShopMenu productTypeCountChild : productTypeCountChilds) {
				shopMenuChilds.add(productTypeCountChild);
			}
			shopMenu.setListChild(shopMenuChilds);
			shopMenus.add(shopMenu);
		}
		return shopMenus;
	}
	
	/***
	 * Add product to basket 
	 * 
	 * @param productIds List product ids use add to basket 
	 * @return {@link ObjectID} list product ids
	 */
	public ObjectID createProductBasket(ObjectID productIds) {
		List<UserProductBasket> userProductBaskets = new ArrayList<>();
		productIds.getIdsList().forEach(id -> {
			UserProductBasket userProductBasket = new UserProductBasket();
			userProductBasket.setUser(getCurrentUserLogged());
			userProductBasket.setProduct(new Product(id));
			userProductBaskets.add(userProductBasket);
		});
		this.userProductBasketRepository.saveAll(userProductBaskets);
		return productIds;
	}

	/**
	 * Get filter menu
	 *
	 * @return list of {@link ShopFilterMenu}
	 * @throws NotFoundException
	 */
	public List<ShopFilterMenu> getFilterMenu() throws NotFoundException {
		Integer userId = this.getCurrentUserId();
		UserModelSetting userModelSetting = this.userModelSettingRepository.findByUser_Id(userId).orElseThrow(
				()-> new NotFoundException(NotFoundException.ERROR_USER_MODEL_DEFAULT_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("error.user_model_default"))));

		Integer userModelCollectionId = userModelSetting.getUserModel().getUserModelCollection().getId();
		List<ShopFilterMenu> shopFilterMenus = new ArrayList<>();

		// Get collection type by user model default of current user
		String collectionType = "";
		if(userModelCollectionId == 1 || userModelCollectionId == 2) {
			collectionType =  ProductEnum.SizeType.ADULT.getCode();
		} else if (userModelCollectionId == 3 || userModelCollectionId == 4) {
			collectionType = ProductEnum.SizeType.KID.getCode();
		}

		// Get list of Colors
		List<ProductColorVO> colorsList = this.productColorRepository.findAll().stream().map(ProductColorVO::new).collect(Collectors.toList());
		shopFilterMenus.add(new ShopFilterMenu("colors", colorsList));

		// Get list product's sizes
		List<ProductProperty> productProperties = this.productSizeService.getProductSizeList(collectionType);
		shopFilterMenus.add(new ShopFilterMenu("sizes", productProperties));

		// set Price Range
		List<Integer> priceRange = new ArrayList<>();
		Integer priceMin = 0;
		Integer priceMax = 250;
		Optional<Product> optionalPriceMin = this.productRepository.findFirstByStatusOrderByPriceAsc(ProductEnum.ProductStatus.ACTIVE.getCode());
		if(optionalPriceMin.isPresent()) {
			priceMin = optionalPriceMin.get().getPrice().setScale(0, RoundingMode.DOWN).intValue();
		};
		Optional<Product> optionalPriceMax = this.productRepository.findFirstByStatusOrderByPriceDesc(ProductEnum.ProductStatus.ACTIVE.getCode());
		if(optionalPriceMax.isPresent()) {
			priceMax = optionalPriceMax.get().getPrice().setScale(0, RoundingMode.UP).intValue();
		};
		priceRange.add(priceMin);
		priceRange.add(priceMax);
		shopFilterMenus.add(new ShopFilterMenu("priceRange",priceRange));

		// set wearing purpose menu
		List<ProductWearingPurposeDTO> productWearingPurposeDTOs = this.productWearingPurposeService.getProductWearingPurposeList(collectionType);
		shopFilterMenus.add(new ShopFilterMenu("wearingPurposes", productWearingPurposeDTOs));

		return shopFilterMenus;
	}

	/**
	 * Get product list
	 * 
	 * @param productFilter {@link ProductFilter} 
	 * 
	 * @return {@link APIResponse}
	 * @throws NotFoundException 
	 */
	public PageInfo<ProductDTO> filterProduct(String findType, ProductForUserFilter productFilter) throws NotFoundException {
		UserModel userModel = this.getUserModelUserLogged();
		productFilter.setCollectionId(userModel.getUserModelCollection().getId());
		switch (StringUtils.defaultString(findType)) {
			case "TOPS":
				this.buildProductTypeForTops(productFilter);
				break;
	
			case "BOTTOMS":
				this.buildProductTypeForBottoms(productFilter);
				break;
				
			case "FAVORITED":
				productFilter.setProductFavorited(true);
				break;
			case "TRIED_ON":
				productFilter.setProductTriedOn(true);
				break;
			case "BASKET":
				productFilter.setProductBasket(true);
				break;
				
		}
		Sort sort = productService.buildSortProduct(productFilter.getSortBy());
		PageRequest	pageRequest = this.buildPageRequest(productFilter.getPage(), productFilter.getLimit(), sort);
		List<Integer> productTypes = productService.getProductTypes((Integer) productFilter.getProductTypeId());
		productFilter.setProductTypeId(productTypes);

		Page<ProductDTO> data;
		if(ProductEnum.ProductSort.TRIED_PRODUCTS.toString().equals(productFilter.getSortBy()) || ProductEnum.ProductSort.FAVORITED_PRODUCTS.toString().equals(productFilter.getSortBy())) {
			data = this.productRepository.findAll(ProductForUserSpecificationBuilder.filterProductInteraction(productFilter), pageRequest).map(ProductDTO::new);
		} else {
			data = this.productRepository.findAll(ProductForUserSpecificationBuilder.filterProduct(productFilter), pageRequest).map(ProductDTO::new);
		}
		PageInfo<ProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		
		return pageInfo;
	}
}
