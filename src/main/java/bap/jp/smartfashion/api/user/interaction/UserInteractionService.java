package bap.jp.smartfashion.api.user.interaction;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.dto.ProductRatingDTO;
import bap.jp.smartfashion.common.dto.ProductRatingReplyDTO;
import bap.jp.smartfashion.common.mapper.ProductMapper;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingCount;
import bap.jp.smartfashion.common.model.ProductRatingInteraction;
import bap.jp.smartfashion.common.model.ProductRatingReply;
import bap.jp.smartfashion.common.model.ProductRatingReplyInteraction;
import bap.jp.smartfashion.common.model.ProductRatingReplyPhoto;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserProductInteraction;
import bap.jp.smartfashion.common.repository.ProductInteractionRepository;
import bap.jp.smartfashion.common.repository.ProductRatingCountRepository;
import bap.jp.smartfashion.common.repository.ProductRatingInteractionRepository;
import bap.jp.smartfashion.common.repository.ProductRatingReplyInteractionRepository;
import bap.jp.smartfashion.common.repository.ProductRatingReplyPhotoRepository;
import bap.jp.smartfashion.common.repository.ProductRatingReplyRepository;
import bap.jp.smartfashion.common.repository.ProductRatingRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.UserProductInteractionRepository;
import bap.jp.smartfashion.common.repository.UserProductRepository;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.common.visitor.VistorStore;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;

import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;


/**
 * Service class for managing products by shop.
 */
@Service
public class UserInteractionService extends BaseService {

	@Autowired
	private ProductMapper productMapper;

	@Autowired
	private ProductInteractionRepository productInteractionRepository;

	@Autowired
	private UserProductInteractionRepository userProductInteractionRepository;

	@Autowired
	private ProductRatingRepository productRatingRepository;

	@Autowired
	private ProductRatingReplyRepository productRatingReplyRepository;

	@Autowired
	private ProductRatingCountRepository productRatingCountRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductService productService;

	@Autowired
	private UserProductRepository userProductRepository;

	@Autowired
	private VistorStore vistorStore;
	
	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	private ProductRatingInteractionRepository productRatingInteractionRepository;

	@Autowired
	private ProductRatingReplyInteractionRepository productRatingReplyInteractionRepository;

	@Autowired
	private ProductRatingReplyPhotoRepository productRatingReplyPhotoRepository;

	@Autowired
	private MessageService messageService;
	
	private final Logger log = LoggerFactory.getLogger(UserInteractionService.class);

	/**
	 * user favorite product
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @return APIResponse {@link APIResponse}
	 * @throws NotFoundException
	 */
	@Transactional(rollbackFor = Exception.class)
	public ProductDTO createFavoriteProduct(Integer shopId, Integer productId) throws NotFoundException {
		String actionCode = ProductEnum.ProductInteraction.FAVORITE_PRODUCT.getCode();
		User user = this.getCurrentUserLogged();
		Product product=this.productMapper.checkProduct(productId,shopId);
		if (user != null && product!= null) {
			Optional<UserProductInteraction> userProductInteraction = userProductInteractionRepository.findByUser_IdAndProduct_IdAndActionCode(user.getId(), productId, actionCode);
			if (userProductInteraction.isPresent()) {
				// flag delete
				userProductInteractionRepository.delete(userProductInteraction.get());
				// update productInteraction with count -1
				Optional<ProductInteraction> productInteraction = productInteractionRepository.findByProduct_IdAndActionCode(productId, actionCode);
				if (productInteraction.isPresent()) {
					Integer count = productInteraction.get().getActionCount();
					if (count >= 0) productInteraction.get().setActionCount(count - 1);
					productInteractionRepository.save(productInteraction.get());
				}
			} else {
				// flag create
				// save in userProducInteraction
				userProductInteractionRepository.save(new UserProductInteraction(user, product, actionCode, 1));
				// save in productInteraction
				Optional<ProductInteraction> productInteraction = productInteractionRepository.findByProduct_IdAndActionCode(productId, actionCode);
				if (!productInteraction.isPresent()) {
					productInteractionRepository.save(new ProductInteraction(product, actionCode, 1));
				} else {
					Integer count = productInteraction.get().getActionCount();
					productInteraction.get().setActionCount(count + 1);
					productInteractionRepository.save(productInteraction.get());
				}
			}
		}
		return null;
	}

	/**
	 * Review Product for users
	 *
	 * @param productId {@link Product}
	 * @param productRatingDTO {@link ProductRatingDTO}
	 * @return APIResponse {@link APIResponse} data is {@link ProductRatingDTO}
	 * @throws BadRequestException
	 * @throws ConflictException
	 */
	@Transactional(rollbackFor = Exception.class)
	public ProductRatingDTO reviewProduct(Integer productRatingId, Integer productId, ProductRatingDTO productRatingDTO, boolean isUpdated) throws NotFoundException, BadRequestException {
		Integer userId = this.getCurrentUserId();

		// Check product, exited or not
		this.checkExitedProduct(productId);
		ProductRating convertedProductRating;
		List<ProductRatingCount> productRatingCounts = new ArrayList<>();

		// Get current User's ProductRating
		ProductRating productRating = this.checkProductRatingByUser(userId, productId);
		if(isUpdated) {
			if(productRating == null) {
				throw new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("product-rating")));
			}
			productRatingCounts = this.checkProductRatingCountForCurrentProductRating(productId, productRating, productRatingDTO, isUpdated);
			convertedProductRating = this.productMapper.convertProductRatingDTOToProductRating(productRating, productRatingDTO, isUpdated);
		} else {
			if(productRating != null) {
				throw new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("product-rating")));
			}
			productRatingCounts = this.checkProductRatingCountForCurrentProductRating(productId, new ProductRating(), productRatingDTO, isUpdated);
			productRating = new ProductRating();
			productRating.setUser(new User(userId));
			productRating.setProduct(new Product(productId));
			convertedProductRating = this.productMapper.convertProductRatingDTOToProductRating(productRating, productRatingDTO, isUpdated);
		}
		if(!productRatingCounts.isEmpty()){
			this.productRatingCountRepository.saveAll(productRatingCounts);
		}
		convertedProductRating = this.productRatingRepository.save(convertedProductRating);
		
		return new ProductRatingDTO(convertedProductRating);
	}

	/**
	 * Reply to user's comment
	 *
	 * @param productId {@link Product}
	 * @param productRatingReplyDTO {@link ProductRatingReplyDTO}
	 * @return APIResponse {@link APIResponse} data is {@link ProductRatingReply}
	 * @throws NotFoundException
	 */
	public ProductRatingReplyDTO replyComment(Integer shopId, ProductRatingReplyDTO productRatingReplyDTO) throws NotFoundException {
		Integer userId = this.getCurrentUserId();

		ProductRating productRating = this.checkExitedProductRating(productRatingReplyDTO.getProductRatingId());

		// Check reply comment about shop's product valuable
		this.productMapper.checkProduct(productRating.getProduct().getId(), shopId);

		// Check user reply own's comment
		if(!productRating.getUser().getId().equals(userId)){
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_REPLY_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("user-product-rating")));
		}
		ProductRatingReply productRatingReply = new ProductRatingReply();
		productRatingReply.setProductRating(productRating);
		productRatingReply.setComment(productRatingReplyDTO.getComment());
		productRatingReply.setShop(new Shop(shopId));
		productRatingReply.setUser(new User(userId));
        productRatingReply.setLikeCount(0);

		List<ProductRatingReplyPhoto> commentPhotos = new ArrayList<>();
		if(productRatingReplyDTO.getImageUrls() != null) {
			for(String imageUrl : productRatingReplyDTO.getImageUrls()) {
				commentPhotos.add(new ProductRatingReplyPhoto(productRatingReply, imageUrl));
			}
			productRatingReply.setImageUrls(commentPhotos);
		}
		
		return new ProductRatingReplyDTO(this.productRatingReplyRepository.save(productRatingReply));
	}

	/**
	 * Update comment reply
	 *
	 * @param productRatingReplyId {@link ProductRatingReply}
	 * @param productRatingReplyDTO {@link ProductRatingReplyDTO}
	 * @return APIResponse {@link APIResponse} data is {@link ProductRatingReply}
	 * @throws NotFoundException
	 */
	@Transactional(rollbackFor = Exception.class)
	public ProductRatingReplyDTO updateCommentReply(Integer productRatingReplyId,
			ProductRatingReplyDTO productRatingReplyDTO) throws NotFoundException, BadRequestException {
		Integer userId = this.getCurrentUserId();

		ProductRating productRating = this.checkExitedProductRating(productRatingReplyDTO.getProductRatingId());
		if(!productRating.getProduct().getStatus().equals(ProductEnum.ProductStatus.ACTIVE.getCode())) {
			throw new BadRequestException(String.format(this.messageService.getMessage("error.bad-value-msg"),
					this.messageService.getMessage("info.product")));
		}
		// Check user reply own's comment
		if(!productRating.getUser().getId().equals(userId)){
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_REPLY_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("user-product-rating")));
		}

		ProductRatingReply productRatingReply = this.checkProductRatingReplyWithUser(userId, productRatingReplyId);
		productRatingReply.setComment(productRatingReplyDTO.getComment());

		List<ProductRatingReplyPhoto> commentPhotos = new ArrayList<>();
		if(productRatingReplyDTO.getImageUrls() != null) {
			if(productRatingReply.getImageUrls() != null) {
				this.productRatingReplyPhotoRepository.deleteAll(productRatingReply.getImageUrls());
			}
			for(String imageUrl : productRatingReplyDTO.getImageUrls()) {
				commentPhotos.add(new ProductRatingReplyPhoto(productRatingReply, imageUrl));
			}
			productRatingReply.setImageUrls(commentPhotos);
		}
		ProductRatingReplyDTO productRatingReplyRes = new ProductRatingReplyDTO(this.productRatingReplyRepository.save(productRatingReply));
		return productRatingReplyRes;
	}
	
	/**
	 * Like about comment for user
	 * Like about comment reply for user
	 *
	 * @param id {@link ProductRating} productRatingId or {@link ProductRatingReply} productRatingReply
	 * @param reply flag to check action like for ProductRating or ProductRatingReply
	 * @return {@link ProductRatingDTO}
	 * @throws NotFoundException
	 * @throws ConflictException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public ProductRatingDTO likeComment(Integer id, boolean reply) throws NotFoundException, BadRequestException, ConflictException {
		Integer productRatingId = null;
		Integer userId = this.getCurrentUserId();

		if(reply) {
			ProductRatingReply productRatingReply = this.checkExitedProductRatingReply(id);
			if(!productRatingReply.getProductRating().getProduct().getStatus()
					.equals(ProductEnum.ProductStatus.ACTIVE.getCode())) {
				throw new BadRequestException(String.format(this.messageService.getMessage("error.bad-value-msg"),
						this.messageService.getMessage("info.product")));
			}

			ProductRatingReplyInteraction productRatingReplyInteraction = this.checkCommentReply_LikeForUser(
					id, userId, ProductEnum.Product_CommentEmotion.LIKE.toString());

			if(productRatingReplyInteraction == null) {
				productRatingReplyInteraction = new ProductRatingReplyInteraction();
				productRatingReplyInteraction.setProductRatingReply(new ProductRatingReply(id));
				productRatingReplyInteraction.setUser(new User(userId));
				productRatingReplyInteraction.setType(ProductEnum.Product_CommentEmotion.LIKE.toString());

				productRatingReply.setLikeCount(productRatingReply.getLikeCount() + 1);
				this.productRatingReplyInteractionRepository.save(productRatingReplyInteraction);
			} else {
				this.productRatingReplyInteractionRepository.delete(productRatingReplyInteraction);
				productRatingReply.setLikeCount(productRatingReply.getLikeCount() - 1);
			}
			this.productRatingReplyRepository.save(productRatingReply);
			productRatingId = productRatingReply.getProductRating().getId();
		} else {
			ProductRating productRating = this.checkExitedProductRating(id);
			if(!productRating.getProduct().getStatus().equals(ProductEnum.ProductStatus.ACTIVE.getCode())) {
				throw new BadRequestException(String.format(this.messageService.getMessage("error.bad-value-msg"),
						this.messageService.getMessage("info.product")));
			}

			ProductRatingInteraction productRatingInteraction = this.checkCommentLikeForUser(id, userId,
					ProductEnum.Product_CommentEmotion.LIKE.toString());
			if(productRatingInteraction == null) {
				productRatingInteraction = new ProductRatingInteraction();
				productRatingInteraction.setProductRating(new ProductRating( id));
				productRatingInteraction.setUser(new User(userId));
				productRatingInteraction.setType(ProductEnum.Product_CommentEmotion.LIKE.toString());

				productRating.setLikeCount(productRating.getLikeCount() + 1);
				this.productRatingInteractionRepository.save(productRatingInteraction);
			} else {
				this.productRatingInteractionRepository.delete(productRatingInteraction);
				productRating.setLikeCount(productRating.getLikeCount() - 1);
			}
			this.productRatingRepository.save(productRating);
			productRatingId = productRating.getId();
		}
		return new ProductRatingDTO(this.productRatingRepository.findById(productRatingId).get());
	}
}
