package bap.jp.smartfashion.api.user.model;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.UserModelCollectionDTO;
import bap.jp.smartfashion.common.dto.UserModelDTO;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserModelSetting;
import bap.jp.smartfashion.common.repository.UserModelCollectionRepository;
import bap.jp.smartfashion.common.repository.UserModelRepository;
import bap.jp.smartfashion.common.repository.UserModelSettingRepository;
import bap.jp.smartfashion.common.repository.UserProductTryOnRepository;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.service.ai.AIService;
import bap.jp.smartfashion.common.service.ai.response.PersonResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

import java.net.SocketTimeoutException;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Service class for managing products by shop.
 */
@Service
public class UserModelService extends BaseService {
	@Autowired
	private UserModelCollectionRepository userModelCollectionRepository;

	@Autowired
	private UserModelRepository userModelRepository;

	@Autowired
	private UserModelSettingRepository userModelSettingRepository;
	
	@Autowired
	private AIService aiService;
	
	@Autowired
	private UserProductTryOnRepository userProductTryOnRepository;

	@Autowired
	private MessageService messageService;
	
	private final Logger log = LoggerFactory.getLogger(UserModelService.class);
	
	private static final String CACHE_USER_MODEL_COLLECTION_LIST = "getListUserModelCollection";
	private static final String CACHE_USER_MODEL_COLLECTION = "getUserModelCollectionById";
	private static final String CACHE_USER_MODEL_LIST = "getListUserModel";
	private static final String CACHE_USER_MODEL = "getUserModel";
	private static final String CACHE_USER_MODEL_AND_MODEL_SYSTEM = "getUserModelBelongUserAndSystem";
	
	/**
	 * Get {@link UserModelCollection}
	 *
	 * @return List of {@link UserModelCollectionDTO}
	 */
	@Cacheable(value = CACHE_USER_MODEL_COLLECTION_LIST)
	public List<UserModelCollectionDTO> getListUserModelCollection() {
		return this.userModelCollectionRepository.findAll().stream().map(UserModelCollectionDTO::new).collect(Collectors.toList());
	}
	
	/**
	 * Get {@link UserModelCollection} by Id
	 *
	 * @return {@link UserModelCollectionDTO}
	 * @throws NotFoundException 
	 */
	@Cacheable(value = CACHE_USER_MODEL_COLLECTION, key = "#id")
	private UserModelCollectionDTO getUserModelCollectionById(Integer id) throws NotFoundException {
		UserModelCollection userModelCollection= this.userModelCollectionRepository.findById(id).orElseThrow(() ->
			 new NotFoundException(String.format(this.messageService.getMessage("error.not-found-msg"),
					 this.messageService.getMessage("user-model-collection"))));
		
		return new UserModelCollectionDTO(userModelCollection);
	}
	
	/**
	 * Get list {@link UserModelCollection}
	 *
	 * @return PageInfo of {@link UserModelCollectionDTO}
	 * @throws NotFoundException 
	 */
//	@Cacheable(value = CACHE_USER_MODEL_LIST, key = "#userId +  #page + #limit + #userModelCollectionId", condition="#page != null and #limit != null ")
	public PageInfo<UserModelDTO> getListModelByUserModelCollection(Integer userModelCollectionId, Integer page, Integer limit, Integer userId) throws NotFoundException {
		this.getUserModelCollectionById(userModelCollectionId);
		PageRequest pageRequest = this.buildPageRequest(page, limit);
		Page<UserModelDTO> data = this.userModelRepository.findAllByUser_IdAndUserModelCollection_IdOrUser_IdIsNull(userId, userModelCollectionId, pageRequest)
				.map(userModel -> convertUserModelToUserModelDTO(userModel));
		PageInfo<UserModelDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * Get list {@link UserModelCollection}
	 *
	 * @return PageInfo of {@link UserModelCollectionDTO}
	 */
//	@Cacheable(value = CACHE_USER_MODEL_LIST, key = "#userId +  #page + #limit", condition="#page != null and #limit != null ")
	public PageInfo<UserModelDTO> getListModel(Integer page, Integer limit, Integer userId) {
		PageRequest pageRequest = this.buildPageRequest(page, limit);
		Page<UserModelDTO> data = this.userModelRepository.findAllByUser_IdOrUser_IdIsNull(userId, pageRequest)
				.map(userModel -> convertUserModelToUserModelDTO(userModel));
		PageInfo<UserModelDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	private UserModelDTO convertUserModelToUserModelDTO(UserModel userModel) {
		UserModelDTO userModelDTO = new UserModelDTO(userModel);
		UserModelSetting userModelSetting = this.userModelSettingRepository.findByUser_IdAndUserModel_Id(this.getCurrentUserId(), userModel.getId()).orElse(null);
		if (userModelSetting != null) {
			userModelDTO.setIsDefault(true);
		}
		return userModelDTO;
	};
	/**
	 * Create {@link UserModelDTO}
	 *
	 * @return {@link UserModelDTO}
	 * @throws NotFoundException 
	 * @throws SmartFashionException
	 */
	@Caching(evict = {
		@CacheEvict(value = CACHE_USER_MODEL_LIST, allEntries = true),
	})
	public UserModelDTO createUserModel(UserModelDTO userModelDTO) throws NotFoundException, SmartFashionException {
		UserModelCollectionDTO userModelCollectionDTO = this.getUserModelCollectionById(userModelDTO.getUserModelCollectionId());
		UserModel userModel = new UserModel();
		userModel.setUser(this.getCurrentUserLogged());
		userModel.setUrl(userModelDTO.getUrl());
		UserModelCollection userModelCollection = new UserModelCollection();
		BeanUtils.copyProperties(userModelCollectionDTO, userModelCollection);
		userModel.setUserModelCollection(userModelCollection);
		
		PersonResponse personResponse = null;
		try {
			personResponse = this.aiService.generateKeyPointForUserModel(userModel);
		} catch (ResourceAccessException | SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CONNECTION,
					this.messageService.getMessage("error.AI-connection"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (personResponse == null || personResponse.getData() == null) {
			throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_KEYPOINT,
					this.messageService.getMessage("error.get-keypoint"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		userModel.setKeyPoint(personResponse.getData().get(0).getKeyPoint());
		userModel.setKeyPointMessage(personResponse.getMessage());
		return new UserModelDTO(this.userModelRepository.save(userModel));
	}
	
	/**
	 * Get {@link UserModelDTO} by Id
	 *
	 * @return {@link UserModel}
	 * @throws NotFoundException 
	 */
	@Cacheable(value = CACHE_USER_MODEL, key = "#userModelId")
	public UserModel getUserModel(Integer userModelId) throws NotFoundException {
		return this.userModelRepository.findByIdAndUser_Id(userModelId, this.getCurrentUserId()).orElseThrow(() -> 
		 	new NotFoundException(NotFoundException.ERROR_USER_MODEL_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
					this.messageService.getMessage("user-model"))));
	}
	
	/**
	 * Get {@link UserModelDTO} by Id
	 *
	 * @return {@link UserModel}
	 * @throws NotFoundException 
	 */
	@Cacheable(value = CACHE_USER_MODEL_AND_MODEL_SYSTEM, key = "#userModelId")
	public UserModel getUserModelBelongUserAndSystem(Integer userModelId) throws NotFoundException {
		return this.userModelRepository.findByIdAndUser_IdOrUser_IdIsNull(userModelId, this.getCurrentUserId()).orElseThrow(() -> 
		new NotFoundException(NotFoundException.ERROR_USER_MODEL_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
				this.messageService.getMessage("user-model"))));
	}
	
	
	/**
	 * Delete {@link UserModelDTO}
	 *
	 * @return {@link UserModelDTO}
	 * @throws NotFoundException 
	 */
	@Caching(evict = {
		@CacheEvict(value = CACHE_USER_MODEL_LIST, allEntries = true),
		@CacheEvict(value = CACHE_USER_MODEL, allEntries = true)
	})
	@Transactional(rollbackFor = Exception.class)
	public UserModelDTO deleteUserModel(Integer userModelId) throws NotFoundException {
		UserModel userModel = this.getUserModel(userModelId);
		this.userModelRepository.delete(userModel);
		this.userProductTryOnRepository.deleteByUserModel_Id(userModel.getId());
		
		return new UserModelDTO(userModel);
	}
	
	/**
	 * Set {@link UserModelDTO} default
	 *
	 * @return {@link UserModelDTO}
	 * @throws NotFoundException 
	 */
	@Caching(evict = {
			@CacheEvict(value = CACHE_USER_MODEL_LIST, allEntries = true),
	})
	public UserModelDTO setDefaultModel(Integer userModelId) throws NotFoundException {
		UserModel userModel = this.getUserModelBelongUserAndSystem(userModelId);
		UserModelSetting userModelSetting = this.userModelSettingRepository.findByUser_Id(this.getCurrentUserId()).orElse(new UserModelSetting());
		userModelSetting.setUser(this.getCurrentUserLogged());
		userModelSetting.setUserModel(userModel);
		userModelSetting.setIsDefault(true);
		this.userModelSettingRepository.save(userModelSetting);
		userModel.setUserModelSetting(userModelSetting);
		return new UserModelDTO(userModel);
	}
	
	/**
	 * Get {@link UserModelDTO} default
	 * @return {@link UserModelDTO}
	 * @throws NotFoundException 
	 */
	public UserModelDTO getDefaultModel() throws NotFoundException {
		UserModelSetting userModelSetting = this.userModelSettingRepository.findByUser_Id(this.getCurrentUserId()).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_USER_MODEL_DEFAULT_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "User model default")));
		return this.convertUserModelToUserModelDTO(userModelSetting.getUserModel());
	}
}
