package bap.jp.smartfashion.api.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import bap.jp.smartfashion.api.user.dto.UserLangKey;
import bap.jp.smartfashion.common.dto.PasswordChangeDTO;
import bap.jp.smartfashion.common.dto.PasswordResetDTO;
import bap.jp.smartfashion.common.dto.UserModelDTO;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ShopProductCollection;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ShopProductCollectionRepository;
import bap.jp.smartfashion.common.repository.UserModelRepository;
import bap.jp.smartfashion.common.repository.UserProductRepository;

import bap.jp.smartfashion.exception.SmartFashionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import bap.jp.smartfashion.api.admin.user.dto.AdminUserDTO;
import bap.jp.smartfashion.api.authentication.dto.AuthenticationSNSDTO;
import bap.jp.smartfashion.api.authorization.vo.AuthorityActionVO;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ShopDTO;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.mapper.ShopMapper;
import bap.jp.smartfashion.common.mapper.UserMapper;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.model.ProductTypeCount;
import bap.jp.smartfashion.common.model.Role;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserAuthority;
import bap.jp.smartfashion.common.repository.ProductTypeCountRepository;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;
import bap.jp.smartfashion.common.repository.RoleRepository;
import bap.jp.smartfashion.common.repository.ShopRepository;
import bap.jp.smartfashion.common.repository.UserAuthorityRepository;
import bap.jp.smartfashion.common.repository.UserRepository;
import bap.jp.smartfashion.common.service.EmailService;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.ActivateUser;
import bap.jp.smartfashion.common.vo.EmailAddress;
import bap.jp.smartfashion.common.vo.JwtToken;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.Token;
import bap.jp.smartfashion.common.vo.UserManagement;
import bap.jp.smartfashion.config.jwt.JwtProvider;
import bap.jp.smartfashion.enums.RoleEnum;
import bap.jp.smartfashion.enums.TokenEnum;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.util.CodeEnumUtils;
import bap.jp.smartfashion.util.RandomUtils;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import bap.jp.smartfashion.util.constants.AccessControlListConstants;
import bap.jp.smartfashion.util.constants.Constants;

/**
 * Service class for managing users.
 */
@Service
public class UserService extends BaseService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private SpringTemplateEngine templateEngine;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserAuthorityRepository userAuthorityRepository;
	
	@Autowired
	private JwtProvider jwtProvider;
	
	@Autowired
	private ShopRepository shopRepository;
	
	@Autowired
	private ShopMapper shopMapper;
	
	@Autowired
	private ProductTypeRepository productTypeRepository;
	
	@Autowired
	private ProductTypeCountRepository productTypeCountRepository;

	@Autowired
	private UserModelRepository userModelRepository;

	@Autowired
	private UserProductRepository userProductRepository;

	@Autowired
	private MessageService messageService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private ProductCollectionRepository productCollectionRepository;

	@Autowired
	private ShopProductCollectionRepository shopProductCollectionRepository;
	
	private final Logger log = LoggerFactory.getLogger(UserService.class);
	
	public static final String USER_LIST_CACHE = "userList";
	public static final String USER_BY_ID_CACHE = "userById";
	
	private static final String RESET_PASSWORD_KEY = "resetPassword";
	private static final String RE_ACTIVE_USER_KEY = "reActiveUser";
	/**
	 * Create new users
	 * 
	 * @param userManagement {@link UserManagement} info
	 * @return {@link APIResponse}
	 * @throws ConflictException
	 * @throws NotFoundException 
	 * @throws BadRequestException 
	 */
	@Transactional(rollbackFor = Exception.class)
	@CacheEvict(value = USER_LIST_CACHE, allEntries = true)
	public User createUser(UserManagement userManagement) throws ConflictException, NotFoundException, BadRequestException {
		if (StringUtils.isEmpty(userManagement.getPlatform())) {
			userManagement.setPlatform(UserEnum.Platform.WEB.getCode());
		}
		UserEnum.Platform platform = CodeEnumUtils.fromCodeQuietly(UserEnum.Platform.class, userManagement.getPlatform());
		
		if(platform == null) {
			throw new BadRequestException(BadRequestException.ERROR_CREATE_USER_BAD_REQUEST,
					String.format(this.messageService.getMessage("error.bad-value-msg"),
							this.messageService.getMessage("info.platform")), false);
		}
		
		userManagement.setRegisterStatus(false);
		User user = this.userMapper.userManagementToUser(userManagement);

		/** Set default role **/
		Set<Role> roles = new HashSet<Role>();
		List<String> roleList = new ArrayList<>();
		roleList.add(RoleEnum.ApplicationRole.ROLE_USER.name());
		if (userManagement.getIsShop()) {
			roleList.add(RoleEnum.ApplicationRole.ROLE_SHOP.name());
		}

		this.roleRepository.findByNameIn(roleList).forEach(e -> {
			roles.add((Role) e);
		});
		user.setRoles(roles);
		

		User userResponse = this.userRepository.save(user);// save user
		/** Set default roles group **/
		this.setAuthorityGroupsOfUserNormal(userResponse);
		/*************/
		if (StringUtils.isNotEmpty(userResponse.getEmail())) {
			String token = this.buildTokenByPlatform(userManagement.getPlatform());
			this.redisSetValue(token, Integer.toString(userResponse.getId())); // set token into redis
			Date expired = DateUtils.addMinutes(new Date(), TokenEnum.TOKEN_CREATE_USER.getValue());
			this.redisSetExpire(token, expired); // set expired time for token in redis
			
			String reActiveKey = RE_ACTIVE_USER_KEY + user.getEmail();
			//tracking re-active email sent
			this.redisSetValue(reActiveKey, Integer.toString(user.getId()));
			this.redisSetExpire(reActiveKey, expired);
			userResponse.setPlatform(platform.getCode());
			this.sendEmailToVerifyUser(userResponse, token);
		}
		
		
		return userResponse;
	}
	/**
	 * Active the user created from the link in the email
	 * 
	 * @param token Token sent in email clicking on the link
	 * @param platform Platform activate
	 * @return {@link APIResponse}
	 * @throws BadRequestException
	 * @throws NotFoundException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void activeUser(String token, String platform) throws BadRequestException, NotFoundException {
		log.info("REST request to activate user with token: {}", token);
		SmartFashionUtils.checkFormatToken(token);
		String value = (String) this.redisGetValue(token);
		long expiredTime = this.redisGetExpire(token);
		if (expiredTime < 0) {
			throw new BadRequestException(BadRequestException.ERROR_TOKEN_IS_EXPIRED,
				this.messageService.getMessage("token-is-expired-msg"), false);
		}
		Optional<User> userOptional = this.userRepository.findById(Integer.parseInt(value));
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			if (user.isDeleted()) {
				throw new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found.msg"),
						this.messageService.getMessage("info.user")));
			} else if(user.isBlocked()) {
				throw new BadRequestException(BadRequestException.ERROR_USER_IS_BLOCKED,
					this.messageService.getMessage("user-blocked-msg"), false);
			}
			user.setRegisterStatus(true);
			this.userRepository.save(user);
			/**Create shop when create user **/
			if (token.length() != RandomUtils.DEFAULT_VERIFY_CODE_LENGTH) {
				ShopDTO shopDTO = new ShopDTO();
				shopDTO.setUser(user);
				shopDTO.setName(user.getName());
				shopDTO.setEmail(user.getEmail());
				shopDTO.setAvatar(user.getAvatar());
				shopDTO.setPhone(user.getPhone());
				Shop shop = this.shopMapper.shopDTOToShop(shopDTO);
				this.shopRepository.save(shop);// save shop
				List<ProductTypeCount> productTypeCountsShop = new ArrayList<>();
				List<ProductType> productTypes = this.productTypeRepository.findAll();
				for (ProductType productType : productTypes) {
					ProductTypeCount productTypeCount = new ProductTypeCount();
					productTypeCount.setProductType(productType);
					productTypeCount.setShop(shop);
					productTypeCount.setCount(0);
					productTypeCountsShop.add(productTypeCount);
				}
				this.productTypeCountRepository.saveAll(productTypeCountsShop);

				// Set list of default product collection for current shop
				List<ProductCollection> productCollections = this.productCollectionRepository.findAll();
				List<ShopProductCollection> shopProductCollections = new ArrayList<>();
				for(ProductCollection productCollection : productCollections) {
					ShopProductCollection shopProductCollection = new ShopProductCollection();
					shopProductCollection.setShop(shop);
					shopProductCollection.setProductCollection(productCollection);
					shopProductCollection.setUrl(productCollection.getImage());
					shopProductCollections.add(shopProductCollection);
				}
				this.shopProductCollectionRepository.saveAll(shopProductCollections);

			}
			String reActiveKey = RE_ACTIVE_USER_KEY + user.getEmail();
			this.redisDeleteKey(reActiveKey);
		}
		
		this.redisDeleteKey(token); // delete token
	}
	
	/**
	 * Reset password via email
	 * 
	 * @param activateUser {@link ActivateUser} use to get data
	 * @return {@link APIResponse}
	 * @throws NotFoundException 
	 * @throws ConflictException 
	 * @throws BadRequestException 
	 */
	public EmailAddress resetPassword(ActivateUser activateUser) throws NotFoundException, ConflictException, BadRequestException {
		log.info("REST request to reset password for email: {}", activateUser.getEmail());
		String platform = StringUtils.isEmpty(activateUser.getPlatform()) ? UserEnum.Platform.WEB.getCode() : activateUser.getPlatform(); // if null then login on website
		UserEnum.Platform codeEnum = CodeEnumUtils.fromCodeQuietly(UserEnum.Platform.class, platform);
		if(codeEnum == null) {
			throw new BadRequestException(BadRequestException.ERROR_USER_PLATFORM_BAD_REQUEST,
					String.format(this.messageService.getMessage("error.bad-value-msg"),
							this.messageService.getMessage("info.platform")), false);
		}
		User user = this.userRepository.findOneByEmailAndUserType(activateUser.getEmail(), UserEnum.UserTypeEnum.LOCAL.toString()).orElse(null);
		String resetPasswordKey = RESET_PASSWORD_KEY + activateUser.getEmail();
		long expiredTime = this.redisGetExpire(resetPasswordKey);
		if (expiredTime < 0 && user != null) {
			String token = this.buildTokenByPlatform(platform);
			this.redisSetValue(token, activateUser.getEmail()); // set token into redis
			Date expired = DateUtils.addMinutes(new Date(), TokenEnum.TOKEN_RESET_PASSWORD.getValue());
			this.redisSetExpire(token, expired); // set expired time for token in redis
			
			//tracking reset password
			this.redisSetValue(resetPasswordKey, activateUser.getEmail());
			this.redisSetExpire(resetPasswordKey, expired); 
			
			activateUser.setPlatform(platform);
			this.sendEmailToResetPassword(activateUser, token);
			
			return new EmailAddress(activateUser.getEmail());
		}
		if (expiredTime >= 0) {
            throw new BadRequestException(BadRequestException.ERROR_EMAIL_IS_SENT,
					this.messageService.getMessage("user-reset-password-send-msg"), false);
		}
		return null;
	}

	/**
	 * Send email to reset password
	 *
	 * @param email {@link User} email
	 * @param token Token used to reset
	 */
	private void sendEmailToResetPassword(ActivateUser activateUser, String token) {
		log.info("REST request to send email reset password: {}", activateUser.getEmail());
		Context context = new Context(); // set context email
		Map<String, Object> model = new HashMap<>();
		model.put("smartFashionWebsite", this.smartFashionWebsite); // website url
		String url = this.baseUrl + "/password" + Constants.SLASH + token; //create base url
		model.put("url", url);
		model.put("platform", activateUser.getPlatform());
		model.put("token", token);
		model.put("tokenTime", TokenEnum.TOKEN_RESET_PASSWORD.getValue());
		context.setVariables(model);

		String content = this.templateEngine.process("/email/email_reset_password", context);
		this.emailService.sendEmail(Constants.EMAIL_RESET_PASSWORD_TITLE, this.smartFashionEmail, activateUser.getEmail(), content);
	}

	/**
	 * Reset password
	 *
	 * @param token Token to get user information
	 * @param passwordResetDTO {@link PasswordResetDTO} to reset
	 * @return {@link APIResponse}
	 * @throws ConflictException
	 * @throws BadRequestException
	 */
	public void resetPassword(String token, PasswordResetDTO passwordResetDTO) throws NotFoundException, ConflictException, BadRequestException {
		log.info("REST request to reset password for token: {}", token);
		SmartFashionUtils.checkFormatToken(token);
		String value = (String) this.redisGetValue(token);
		long expiredTime = this.redisGetExpire(token);
		if (expiredTime < 0) {
			throw new BadRequestException(BadRequestException.ERROR_TOKEN_IS_EXPIRED,
				this.messageService.getMessage("token-is-expired-msg"), false);
		} else if (!passwordResetDTO.getPassword().equals(passwordResetDTO.getConfirmPassword())) {
			throw new ConflictException(ConflictException.ERROR_PASSWORD_DOESNT_MATCH,
				this.messageService.getMessage("password-not-matched-msg"));
		}

		Optional<User> optionalUser = this.userRepository.findOneByEmailAndUserType(value, UserEnum.UserTypeEnum.LOCAL.toString());

		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			if (user.isDeleted()) {
				throw new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
                        this.messageService.getMessage("info.user")));
			} else if(!user.isRegisterStatus()) {
				throw new BadRequestException(BadRequestException.ERROR_USER_UNACTIVED,
					this.messageService.getMessage("user-unactived-msg"), false);
			} else if(user.isBlocked()) {
				throw new BadRequestException(BadRequestException.ERROR_USER_IS_BLOCKED,
					this.messageService.getMessage("user-blocked-msg"), false);
			}
			user.setPassword(this.passwordEncoder.encode(passwordResetDTO.getPassword()));// password encode
			this.userRepository.save(user); // save user

			this.redisDeleteKey(token); // delete token
			String resetPasswordKey = RESET_PASSWORD_KEY + user.getEmail();
			this.redisDeleteKey(resetPasswordKey);
		}

	}

	/**
	 * Check reset token expired
	 *
	 * @param token Token want to check expired
	 * @return {@link APIResponse}
	 * @throws BadRequestException
	 * @throws NotFoundException
	 */
	public Token checkResetToken(String token) throws BadRequestException, NotFoundException {
		log.info("REST request to check token : {}", token);
		SmartFashionUtils.checkFormatToken(token);
		long expiredTime = this.redisGetExpire(token);
		if (expiredTime < 0) {
			throw new NotFoundException(BadRequestException.ERROR_TOKEN_IS_EXPIRED,
				this.messageService.getMessage("token-is-expired-msg"));
		}
		Token tk = new Token("token", token);

		return tk;
	}

	/** Update user by user id
	 *
	 * @param id {@link User} ID
	 * @param userDto {@link UserDTO} information
	 * @return {@link APIResponse}
	 * @throws ConflictException
	 * @throws NotFoundException
	 */
	@CachePut(value = USER_BY_ID_CACHE, key = "#id")
	@Caching(evict = {
			@CacheEvict(value = USER_LIST_CACHE, allEntries = true),
	})
	public User updateUser(int id, UserDTO userDTO) throws ConflictException, NotFoundException {
		User user = this.findUserByUserId(id);
		if (user.getId() != this.getCurrentUserId()) {
			throw new AccessDeniedException(HttpStatus.FORBIDDEN.toString());
		}
		Optional<User> existingUserOptional = this.userRepository.findOneByEmailAndUserType(userDTO.getEmail(),UserEnum.UserTypeEnum.LOCAL.toString()); //check email of user other
		if (existingUserOptional.isPresent() && (!existingUserOptional.get().getId().equals(id))) {
			throw new ConflictException(ConflictException.ERROR_EMAIL_EXISTED,
				this.messageService.getMessage("error.email-exited-msg")); //throw exception if email existed
		}
		user = this.userMapper.userDTOToUser(userDTO, user); //set data from userDTO to user;

		return this.userRepository.save(user);// save user
	}

	/** Find {@link User} by ID
	 *
	 * @param id {@link User} ID
	 * @return {@link APIResponse}
	 * @throws NotFoundException if {@link User} not found
	 */
	@Cacheable(value = USER_BY_ID_CACHE, key = "#id")
	@CacheEvict(value = USER_LIST_CACHE, allEntries = true)
	public User findUserById(int id) throws NotFoundException {
		log.info("REST request to find user id: {}", id);
		User user = this.userRepository.findByIdAndDeletedIsFalse(id).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
				String.format(this.messageService.getMessage("error.not-found-msg"),
                    this.messageService.getMessage("info.user"))));

		return user;
	}

	/**
	 * Re-Active user if user is not activated
	 *
	 * @param email Email to reactive
	 * @param platform Platform
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws BadRequestException
	 */
	public EmailAddress reActiveUser(String email, String platform) throws NotFoundException, ConflictException, BadRequestException {

		platform = StringUtils.isEmpty(platform) ? UserEnum.Platform.WEB.getCode() : platform; // if null then login on website
		UserEnum.Platform codeEnum = CodeEnumUtils.fromCodeQuietly(UserEnum.Platform.class, platform);
		if(codeEnum == null) {
			throw new BadRequestException(BadRequestException.ERROR_USER_PLATFORM_BAD_REQUEST, String.format(
			        this.messageService.getMessage("error.bad-value-msg"),
                    this.messageService.getMessage("info.platform")), false);
		}
		User user = this.userRepository.findOneByEmailAndUserType(email, UserEnum.UserTypeEnum.LOCAL.toString()).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
				String.format(this.messageService.getMessage("error.not-found-msg"),
                    this.messageService.getMessage("info.user"))));
		if (user.isDeleted()) {
			throw new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
				String.format(this.messageService.getMessage("error.not-found-msg"),
                    this.messageService.getMessage("info.user")));
		} else if(user.isBlocked()) {
			throw new BadRequestException(BadRequestException.ERROR_USER_IS_BLOCKED,
				this.messageService.getMessage("user-blocked-msg"), false);
		} else if(user.isRegisterStatus()) {
			throw new BadRequestException(BadRequestException.USER_IS_ACTIVED,
				this.messageService.getMessage("user-actived-user-msg"), false);
		}
		String reActiveKey = RE_ACTIVE_USER_KEY + user.getEmail();
		long expiredTime = this.redisGetExpire(reActiveKey);
		if (expiredTime < 0) {
			String token = this.buildTokenByPlatform(platform);
			//Send email re-active user
			this.redisSetValue(token, Integer.toString(user.getId())); // set token into redis
			Date expired = DateUtils.addMinutes(new Date(), TokenEnum.TOKEN_CREATE_USER.getValue());
			this.redisSetExpire(token, expired); // set expired time for token in redis

			//tracking re-active email sent
			this.redisSetValue(reActiveKey, Integer.toString(user.getId()));
			this.redisSetExpire(reActiveKey, expired);
			user.setPlatform(platform);
			this.sendEmailToVerifyUser(user, token);
			return new EmailAddress(email);
		}
		throw new ConflictException(ConflictException.ERROR_EMAIL_ACTIVE_USER_IS_SENT,
			this.messageService.getMessage("user-reactive-user-msg"));
	}

	/**
	 * Create user SNS
	 * @param authenticationSNSDto {@link AuthenticationSNSDTO}
	 * @return {@link User}
	 * @throws ConflictException
	 */
	@Caching(evict = {
			@CacheEvict(value = USER_LIST_CACHE, allEntries = true),
	})
	public User createUserSNS(AuthenticationSNSDTO authenticationSNSDto) throws ConflictException{
		log.info("REST request to create user SNS: {}", authenticationSNSDto);
		Optional<User> userOptional = this.userRepository.findOneByUidAndUserType(authenticationSNSDto.getUid(), authenticationSNSDto.getUserType());
		if (userOptional.isPresent()) {
			throw new ConflictException(ConflictException.ERROR_EMAIL_EXISTED, this.messageService.getMessage("error.email-exited-msg"));
		}
		authenticationSNSDto.setRegisterStatus(true); // active user
		User user = this.userMapper.userSNSToUser(authenticationSNSDto);
		/** Set default role **/
		Set<Role> roles = new HashSet<Role>();
		List<String> roleList = new ArrayList<>();
		roleList.add(RoleEnum.ApplicationRole.ROLE_USER.name());
		this.roleRepository.findByNameIn(roleList).forEach(e -> {
			roles.add((Role) e);
		});
		user.setRoles(roles);

		User userResponse = this.userRepository.save(user);
		/** Set default roles group **/
		this.setAuthorityGroupsOfUserNormal(userResponse);
		/*************/
		return userResponse;
	}

	/**
	 * Get user info by token
	 *
	 * @param jwtToken {@link JwtToken}
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public UserDTO getUserInfoByToken(String jwtToken) throws NotFoundException {
		String jwt = SmartFashionUtils.getJwtToken(jwtToken);
		int userId = Integer.parseInt(this.jwtProvider.getUserNameFromJwtToken(jwt)); // return userid

		UserDTO userDTO = this.userRepository.findById(userId).map(UserDTO::new).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
                        this.messageService.getMessage("info.user"))));
		Shop shop = this.getShopByUserId(userId);
		userDTO.setShop(shop);
		return userDTO;
	}

	public User findOneByEmailAndUserType(String email, String userType) throws NotFoundException {

		User user = this.userRepository.findOneByEmailAndUserType(email, userType).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
				String.format(this.messageService.getMessage("error.not-found-msg"),
                    this.messageService.getMessage("info.user"))));

		return user;
	}

	/**
	 *
	 * @param page page number
	 * @param limit Limit on per page
	 *
	 * @return {@link APIResponse}
	 */
	public PageInfo<AdminUserDTO> findUserList(Integer page, Integer limit, String sortStr, String status) {
		Sort sort = Sort.by(Sort.Order.asc("id"));
		if ("OLDEST_TO_NEWEST".equals(sortStr)) {
			sort = Sort.by(Sort.Order.asc("created"));
		} else if ("NEWEST_TO_OLDEST".equals(sortStr)) {
			sort = Sort.by(Sort.Order.desc("created"));
		}
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Page<AdminUserDTO> data =  this.userRepository.findAllByStatus(StringUtils.defaultString(status), pageRequest).map(user -> this.convertUserToAdminUserDTO(user));
		PageInfo<AdminUserDTO> pageInfo = SmartFashionUtils.pagingResponse(data);

		return pageInfo;
	}

	/**
	 * Get all {@link User}
	 *
	 * @param page page number
	 * @param limit Limit on per page
	 *
	 * @return {@link APIResponse}
	 */
	public PageInfo<AdminUserDTO> findUserListQuery(String query, Integer page, Integer limit, String sortStr, String status) {
		Sort sort = Sort.by(Sort.Order.asc("id"));
		if ("OLDEST_TO_NEWEST".equals(sortStr)) {
			sort = Sort.by(Sort.Order.asc("created"));
		} else if ("NEWEST_TO_OLDEST".equals(sortStr)) {
			sort = Sort.by(Sort.Order.desc("created"));
		}
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Page<AdminUserDTO> data = this.userRepository.findAllByQuery(query, StringUtils.defaultString(status), pageRequest).map(user -> this.convertUserToAdminUserDTO(user));
		PageInfo<AdminUserDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}

	/**
	 * Convert user to admin user
	 * @param user user to convert
	 * @return admin user dto
	 */
	private AdminUserDTO convertUserToAdminUserDTO(User user) {
		AdminUserDTO adminUserDTO = new AdminUserDTO(user);
		adminUserDTO.setUserModelCount(this.userModelRepository.countByUser_Id(user.getId()));
		adminUserDTO.setUserProductCount(this.userProductRepository.countByUser_Id(user.getId()));
		return adminUserDTO;
	}
	private void setAuthorityGroupsOfUserNormal(User user) {
		List<UserAuthority> userAuthorityList = new ArrayList<UserAuthority>();
		for (AuthorityActionVO roleActionVO : AccessControlListConstants.actionForUserGroup().getAuthorityActionVOs()) {
			UserAuthority userAuthority = new UserAuthority();
			userAuthority.setUser(user);
			userAuthority.setFunctionName(roleActionVO.getFunctionName());
			for (String actionName : roleActionVO.getActionName()) {
				userAuthority.setActionName(actionName);
				userAuthorityList.add(userAuthority);
			}
		}
		this.userAuthorityRepository.saveAll(userAuthorityList);
	}

	@Caching(evict = {
			@CacheEvict(value = USER_BY_ID_CACHE, allEntries = true),
			@CacheEvict(value = USER_LIST_CACHE, allEntries = true)
	})
	public void clearCache() {
		log.info("Cleared cache of user");
	}

	private String buildTokenByPlatform(String platform) {
		String token = RandomUtils.generateVerifycode();
		if (StringUtils.isEmpty(platform) || platform.equals(UserEnum.Platform.WEB.getCode())) {
			token = RandomUtils.generateToken();
		}

		return token;
	}
	
	/**
	 * Validation email
	 * @param errors {@link Errors}
	 * @param email Email address
	 */
	public void validationEmail(Errors errors, String email) {
		Optional<User> userOptional = this.userRepository.findOneByEmailAndUserType(email, UserEnum.UserTypeEnum.LOCAL.toString());
		if (userOptional.isPresent()) {
			String messageRes = String.format(this.messageService.getMessage("error.value-already-use"), this.messageService.getMessage("info.email"));
			errors.rejectValue("email", "email", messageRes);
		}
	}

	/**
	 * Change password
	 *
	 * @param passwordChangeDTO {@link PasswordChangeDTO}
	 * @return Object null in case change password successfully
	 * @throws NotFoundException
	 * @throws ConflictException
	 */
	public Object changePassword(PasswordChangeDTO passwordChangeDTO) throws NotFoundException, ConflictException, BadRequestException {
		Integer userId = getCurrentUserId();

		User user = this.userRepository.findById(userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND,
					String.format(APIConstants.ERROR_NOT_FOUND_MSG, "user")));
		if(!UserEnum.UserTypeEnum.LOCAL.toString().equals(user.getUserType())) {
			throw new BadRequestException(BadRequestException.ERROR_USER_PLATFORM_BAD_REQUEST,
				String.format(this.messageService.getMessage("error.bad-value-msg"),
                    this.messageService.getMessage("user-type")), false);
		}
		boolean samePassword = this.bCryptPasswordEncoder.matches(passwordChangeDTO.getOldPassword(), user.getPassword());
		if(samePassword) {
			String newPasswordEncoder = this.bCryptPasswordEncoder.encode(passwordChangeDTO.getConfirmedPassword());
			user.setPassword(newPasswordEncoder);
			this.userRepository.save(user);
			this.sendEmailToNoticeChangedPassword(user.getEmail());
		} else {
			throw new ConflictException(ConflictException.ERROR_INVALID_OLD_PASSWORD,
				this.messageService.getMessage("error.oldpassword-invalid-msg"));
		}
		return null;
	}

	/**
	 * Send email to notice change password successfully
	 *
	 * @param email String
	 */
	private void sendEmailToNoticeChangedPassword(String email) {
		log.info("REST request to send email notice password changed: {}", email);
		Context context = new Context(); // set context email
		Map<String, Object> model = new HashMap<>();
		model.put("smartFashionWebsite", this.smartFashionWebsite); // website url
		context.setVariables(model);

		String content = this.templateEngine.process("/email/email_notice_password_changed", context);
		this.emailService.sendEmail(Constants.EMAIL_NOTICE_PASSWORD_CHANGED_TITLE, this.smartFashionEmail, email, content);
	}

	/**
	 * Change language that user uses
	 *
	 * @param userLangKey {@link UserLangKey}
	 * @return {@link UserDTO}
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	public UserDTO changeLanguage(UserLangKey userLangKey) throws NotFoundException, BadRequestException {
		Integer userId = this.getCurrentUserId();

		User user = this.userRepository.findById(userId).orElseThrow(
				()-> new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("info.user"))));
		if(StringUtils.isEmpty(userLangKey.getLangKey())) {
			throw new NotFoundException(NotFoundException.ERROR_EMPTY_LANG_KEY, String.format(this.messageService.getMessage("error.not-found-msg"),
					this.messageService.getMessage("lang-key")));
		}

		// Check valid language
		String checkedLang = CodeEnumUtils.fromCodeQuietly(UserEnum.LanguageEnum.class, userLangKey.getLangKey()) == null ? StringUtils.EMPTY : userLangKey.getLangKey();
		if(StringUtils.isEmpty(checkedLang)) {
			throw new BadRequestException(BadRequestException.ERROR_INVALID_LANG_KEY, String.format(this.messageService.getMessage("error.bad-value-msg"),
					this.messageService.getMessage("lang-key")), false);
		}
		user.setLangKey(userLangKey.getLangKey());
		User savedUser = this.userRepository.save(user);

		return new UserDTO(savedUser);
	}
}