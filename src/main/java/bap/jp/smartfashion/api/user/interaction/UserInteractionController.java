package bap.jp.smartfashion.api.user.interaction;

import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.dto.ProductRatingDTO;
import bap.jp.smartfashion.common.dto.ProductRatingReplyDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingReply;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller to managing products by shop
 */
@RestController
@RequestMapping("/api/v1/users")
@PreAuthorize("hasAnyRole('ROLE_USER')")
public class UserInteractionController {

	private final Logger log = LoggerFactory.getLogger(UserInteractionController.class);
	
	@Autowired
	private UserInteractionService userInteractionService;




	/**
	 * user favorite product
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param actionCode {@link UserProductInteraction}
	 * @return APIResponse {@link APIResponse}
	 * @throws NotFoundException
	 */
	@PostMapping("/products/{productId}/favorite")
	@ApiOperation(value = "User FAVORITE product (note: Send data twice is considered as deletion )")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<ProductDTO> createFavoriteProduct(
			@ApiParam(value = "Shop Id", required = true)
			@RequestParam(value = "shopId") Integer shopId,

			@ApiParam(value = "product Id", required = true)
			@PathVariable(value = "productId") Integer productId) throws NotFoundException {
		log.info("REST request to favorite product");
		return APIResponse.createdStatus(this.userInteractionService.createFavoriteProduct(shopId, productId));
	}

	/**
	 * Review product by user
	 *
	 * @param productId {@link Product}
	 * @param productRatingDTO {@link ProductRatingDTO}
	 * @return APIResponse {@link APIResponse} data is {@link ProductRatingDTO}
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws BadRequestException 
	 */
	@PostMapping("/reviews")
	@ApiOperation("Review product by user")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message = APIConstants.ERROR_CONFLICT_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message = APIConstants.ERROR_BAD_VALUE_MSG),
	})
	public APIResponse<ProductRatingDTO> reviewProduct(
			@ApiParam(value = "Product Id", required = true)
			@RequestParam(value = "productId", required = true) Integer productId,
			@ApiParam(value = "Product Rating's information")
			@RequestBody ProductRatingDTO productRatingDTO) throws NotFoundException, ConflictException, BadRequestException {
		return APIResponse.okStatus(this.userInteractionService.reviewProduct(null, productId, productRatingDTO, false));
	}

	/**
	 * Update product review by user
	 *
	 * @param productId {@link Product}
	 * @param productRatingDTO {@link ProductRatingDTO}
	 * @return APIResponse {@link APIResponse} data is {@link ProductRatingDTO}
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws BadRequestException 
	 */
	@PutMapping("/reviews/{productRatingId}")
	@ApiOperation("Update product review by user")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message = APIConstants.ERROR_CONFLICT_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message = APIConstants.ERROR_BAD_VALUE_MSG),
	})
	public APIResponse<ProductRatingDTO> updateProductReview(
			@ApiParam(value = "productRatingId")
			@PathVariable(value = "productRatingId") Integer productRatingId,
			@ApiParam(value = "Product Id", required = true)
			@RequestParam(value = "productId", required = true) Integer productId,
			@ApiParam(value = "Product Rating's information")
			@RequestBody ProductRatingDTO productRatingDTO) throws NotFoundException, ConflictException, BadRequestException {
		return APIResponse.okStatus(this.userInteractionService.reviewProduct(productRatingId, productId, productRatingDTO, true));
	}

	/**
	 * Reply to user's comment
	 *
	 * @param productRatingReplyDTO {@link ProductRatingReplyDTO}
	 * @return APIResponse {@link APIResponse} data is {@link ProductRatingReply}
	 * @throws NotFoundException
	 */
	@PostMapping("/{shopId}/reviews/response")
	@ApiOperation("Reply to user's comment")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<ProductRatingReplyDTO> replyComment(
			@ApiParam(value = "shopId", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "Product Rating Reply's information")
			@RequestBody ProductRatingReplyDTO productRatingReplyDTO) throws NotFoundException {
		return APIResponse.okStatus(this.userInteractionService.replyComment(shopId, productRatingReplyDTO));
	}

	/**
	 * Update comment reply
	 *
	 * @param productRatingReplyId {@link ProductRatingReply}
	 * @param productRatingReplyDTO {@link ProductRatingReplyDTO}
	 * @return {@link APIResponse} data is {@link ProductRatingReplyDTO}
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws BadRequestException 
	 */
	@PutMapping("/reviews/response/{productRatingReplyId}")
	@ApiOperation("Update reply to user's comment")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message = APIConstants.ERROR_BAD_VALUE_MSG)

	})
	public APIResponse<ProductRatingReplyDTO> updateCommentReply(
			@ApiParam("Product Rating Reply Id")
			@PathVariable("productRatingReplyId") Integer productRatingReplyId,
			@ApiParam(value = "Product Rating Reply's information")
			@RequestBody ProductRatingReplyDTO productRatingReplyDTO) throws NotFoundException, ConflictException, BadRequestException {
		return APIResponse.okStatus(this.userInteractionService.updateCommentReply(productRatingReplyId, productRatingReplyDTO));
	}

	/**
	 * Like about comment for user
	 * Like about comment reply for user
	 *
	 * @param id {@link ProductRating} productRatingId or {@link ProductRatingReply} productRatingReply
	 * @param reply flag to check action like for ProductRating or ProductRatingReply
	 * @return {@link ProductRatingDTO}
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws BadRequestException 
	 */
	@PutMapping("/products/comment-like/{id}")
	@ApiOperation("Update status enjoy about comment for user")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message = APIConstants.ERROR_BAD_VALUE_MSG)
	})
	public APIResponse<ProductRatingDTO> likeComment(
			@ApiParam(value = "product Rating Id or Product Rating Reply Id", required = true)
			@PathVariable(value = "id", required = true) Integer id,
			@ApiParam(value = "comment reply", required = true, allowableValues = "true, false")
			@RequestParam("reply")	boolean reply) throws NotFoundException, ConflictException, BadRequestException {
		return APIResponse.okStatus(this.userInteractionService.likeComment(id, reply));
	}
}
