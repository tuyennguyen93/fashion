package bap.jp.smartfashion.api.user.shop;

import java.util.List;

import bap.jp.smartfashion.common.vo.ShopFilterMenu;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.ObjectID;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ProductFilter;
import bap.jp.smartfashion.common.vo.ProductForUserFilter;
import bap.jp.smartfashion.common.vo.ShopMenu;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller to managing shops by user
 */
@RestController
@RequestMapping("/api/v1/users")
@PreAuthorize("hasAnyRole('ROLE_USER')")
public class UserShopController {

	@Autowired
	private UserShopService userShopService;
	
	private final Logger log = LoggerFactory.getLogger(UserShopController.class);


	@PostMapping("/products-basket")
	@ApiOperation(value = "Add product to basket")
	public APIResponse<ObjectID> createProductBasket(
			@ApiParam(value ="Product IDs list", required = false)
			@RequestBody
			ObjectID productIds) throws NotFoundException, BadRequestException {
		return APIResponse.okStatus(this.userShopService.createProductBasket(productIds));
	}
	
	@DeleteMapping("/products-basket")
	@ApiOperation(value = "Delete product in basket")
	public APIResponse<ObjectID> deleteProductBasket(
			@ApiParam(value ="Product IDs list", required = false)
			@RequestBody
			ObjectID productIds) throws NotFoundException, BadRequestException {
		return APIResponse.okStatus(null);
	}
	
	/**
	 * Get product list
	 * 
	 * @param shopId {@link Shop} Id
	 * @param collectionId {@link ProductCollection} Id
	 * @param query {@link Product} name
	 * @param page page number
	 * @param limit Limit on per page
	 * @param sort Sort product
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 * @throws BadRequestException 
	 */
	@GetMapping("/shops/products")
	@ApiOperation(value = "Get shop products")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<PageInfo<ProductDTO>> getProductList(
		@ApiParam(value ="Find type", required = false, allowableValues = "TOPS, BOTTOMS, BASKET, FAVORITED, TRIED_ON")
		@RequestParam(value = "findType", required = false) String findType,
		@ApiParam(value ="Search by", required = false)
		@RequestParam(value = "query", required = false) String query,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",NEW_ARRIVAL, HIGHLIGHT_PRODUCTS, TRIED_PRODUCTS, FAVORITED_PRODUCTS,"
				+ " LOWEST_TO_HIGHEST_PRICE, HIGHEST_TO_LOWEST_PRICE, SALE_PRODUCTS")
		@RequestParam(value = "sort", required = false) String sort,
		@ApiParam(value ="Is random products", required = false)
		@RequestParam(value = "isRandom", required = false) boolean isRandom,
		@ApiParam(value ="Product IDs list", required = false)
		@RequestParam(value = "productIds", required = false) List<Integer> productIds) throws NotFoundException, BadRequestException {
		log.info("REST request to find product list");
		ProductForUserFilter productForUserFilter = new ProductForUserFilter();
		productForUserFilter.setPage(page);
		productForUserFilter.setLimit(limit);
		productForUserFilter.setSortBy(sort);
		query = StringUtils.isEmpty(query) ? StringUtils.EMPTY : query;
		productForUserFilter.setQuery(query);
		productForUserFilter.setProductIds(productIds);
		log.info("REST request to getProductList");
		return APIResponse.okStatus(this.userShopService.getProductList(findType, isRandom, productForUserFilter));
	}
	

	/**
	 * Get clothing menu for user
	 *
	 * @param collectionId {@link ProductCollection} Id
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/clothing-menu")
	@ApiOperation(value = "Get clothing menu")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<List<ShopMenu>> getClothingMenu(
		@ApiParam(value ="Collection Id", required = true)
		@RequestParam(value = "collectionId", required = false) Integer collectionId) throws NotFoundException {
		log.info("REST request to get clothing menu for collection id = {}", collectionId);
		return APIResponse.okStatus(this.userShopService.getClothingMenu(collectionId));
	}

	/**
	 * Get filter menu
	 *
	 * @return {@link APIResponse} data is list of {@link ShopFilterMenu}
	 * @throws NotFoundException
	 */
	@GetMapping("/products/filter-menu")
	@ApiOperation("Get filter menu")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<List<ShopFilterMenu>> getFilterMenu() throws NotFoundException {
		log.info("Rest request to get filter menu");
		return APIResponse.okStatus(this.userShopService.getFilterMenu());
	}
	
	/**
	 * Get product list
	 * 
	 * @param shopId {@link Shop} Id
	 * @param collectionId {@link ProductCollection} Id
	 * @param page page number
	 * @param limit Limit on per page
	 * 
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 */
	@GetMapping("/shops/products/filter")
	@ApiOperation(value = "Get shop products filter")
	@ApiResponses(value = {
	})
	public APIResponse<PageInfo<ProductDTO>> filter(
		@ApiParam(value ="Find type", required = false, allowableValues = "TOPS, BOTTOMS, BASKET, FAVORITED, TRIED_ON")
		@RequestParam(value = "findType", required = false) String findType,
		@ApiParam(value ="Search Product by key word", required = false)
		@RequestParam(value = "query", required = false) String query,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Color id ids", required = false, type = "List")
		@RequestParam(value = "colors", required = false) List<Integer> colorIds,
		@ApiParam(value ="Size charts", required = false)
		@RequestParam(value = "sizeChart", required = false) String sizeChart,
		@ApiParam(value ="Size list", required = false, type = "List")
		@RequestParam(value = "sizes", required = false) List<String> sizes,
		@ApiParam(value ="Wearing purpose ids", required = false, type = "List")
		@RequestParam(value = "wearingpurposes", required = false) List<Integer> wearingpurposeIds,
		@ApiParam(value ="Min price", required = false)
		@RequestParam(value = "minPrice", required = false) Integer minPrice,
		@ApiParam(value ="Max price", required = false)
		@RequestParam(value = "maxPrice", required = false) Integer maxPrice,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",NEW_ARRIVAL, HIGHLIGHT_PRODUCTS, TRIED_PRODUCTS, FAVORITED_PRODUCTS,"
				+ " LOWEST_TO_HIGHEST_PRICE, HIGHEST_TO_LOWEST_PRICE, SALE_PRODUCTS")
		@RequestParam(value = "sort", required = false) String sort,
		@ApiParam(value ="Product IDs list", required = false)
		@RequestParam(value = "productIds", required = false) List<Integer> productIds) throws NotFoundException {
		
		ProductForUserFilter productForUserFilter = new ProductForUserFilter();
		productForUserFilter.setPage(page);
		productForUserFilter.setLimit(limit);
		productForUserFilter.setColorIds(colorIds);
		productForUserFilter.setSizeChart(sizeChart);
		productForUserFilter.setSizes(sizes);
		productForUserFilter.setWearingPurposeIds(wearingpurposeIds);
		productForUserFilter.setMaxPrice(maxPrice);
		productForUserFilter.setMinPrice(minPrice);
		productForUserFilter.setSortBy(sort);
		query = StringUtils.isEmpty(query) ? StringUtils.EMPTY : query;
		productForUserFilter.setQuery(query);
		productForUserFilter.setProductIds(productIds);
		log.info("REST request to filter list");
		return APIResponse.okStatus(this.userShopService.filterProduct(findType, productForUserFilter));
	}
	
}
