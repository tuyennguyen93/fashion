package bap.jp.smartfashion.api.user.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLangKey {
    private String langKey;
}
