package bap.jp.smartfashion.api.user;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import bap.jp.smartfashion.api.user.dto.UserLangKey;
import bap.jp.smartfashion.common.dto.PasswordChangeDTO;
import bap.jp.smartfashion.common.dto.PasswordResetDTO;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.response.APIResponse.APIBody;

import bap.jp.smartfashion.common.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import bap.jp.smartfashion.common.base.BaseController;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.vo.ActivateUser;
import bap.jp.smartfashion.common.vo.EmailAddress;
import bap.jp.smartfashion.common.vo.Token;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import bap.jp.smartfashion.util.constants.Constants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller to managing users
 */
@RestController
@RequestMapping("/api/v1/users")
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER', 'ROLE_ANONYMOUS')")
public class UserController extends BaseController {
	
	private final Logger log = LoggerFactory.getLogger(UserController.class);
	
	private static final String AUTHOURIZE_UPDATE_USER = "hasAnyRole('ROLE_USER')";
	
	@Autowired
	private UserService userService;

	@Autowired
	private MessageService messageService;

	/**
	 * Active the user created from the link in the email
	 * 
	 * @param token Token sent in email clicking on the link
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws BadRequestException 
	 */
	@GetMapping("/activate/{token}")
	@ApiOperation(value = "Active the user created from the link in the email")
	@ApiResponses(value = {
	})
	public APIResponse<APIBody> activeUser(
		@ApiParam(value = "Token used to active users")
		@PathVariable(value = "token", required = true) String token,
		@ApiParam(value = "Platform used application", allowableValues = "WEB, MOBILE_ANDROID, MOBILE_IOS")
		@RequestParam(value = "platform", required = false) String platform) throws NotFoundException, BadRequestException {
		this.userService.activeUser(token, platform);
		return APIResponse.okStatus(null);
	}
	
	/**
	 * Reset password via email
	 * 
	 * @param email Email receives password reset information
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 * @throws ConflictException 
	 * @throws BadRequestException 
	 */
	@PostMapping("/password")
	@ApiOperation(value = "Reset password via email")
	@ApiResponses(value = {
	})
	public APIResponse<EmailAddress> resetPassword(
		@ApiParam(value = "Email receives password reset information")
		@Valid
		@RequestBody ActivateUser activateUser) throws NotFoundException, ConflictException, BadRequestException {
		if (!SmartFashionUtils.isEmail(activateUser.getEmail())) {
			throw new BadRequestException(BadRequestException.ERROR_USER_RESET_PASSWORD_BAD_REQUEST, this.messageService.getMessage("error.user-email-invalid-format-msg"), false);
		}
		return APIResponse.okStatus(this.userService.resetPassword(activateUser));
	}
	
	/**
	 * Check expired token
	 * 
	 * @param token Token wants to check expired
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 * @throws BadRequestException 
	 */
	@GetMapping("/password/{token}")
	@ApiOperation(value = "Reset password: Check expired token")
	@ApiResponses(value = {
	})
	public APIResponse<Token> checkToken(
		@ApiParam(value = "Check expired token")
		@Valid
		@PathVariable(value = "token") String token) throws NotFoundException, BadRequestException {
		
		return APIResponse.okStatus(this.userService.checkResetToken(token));
	}
	
	/**
	 * Reset password
	 * 
	 * @param token Token to get user information
	 * @param passwordDTO {@link PassworResetDTO} to reset
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws ConflictException 
	 * @throws BadRequestException 
	 * @throws JsonProcessingException 
	 */
	@PutMapping("/password/{token}")
	@ApiOperation(value = "Reset password information")
	@ApiResponses(value = {
	})
	public APIResponse<APIBody> resetPassword(
			@ApiParam(value = "Check expired token")
		@Valid
		@PathVariable(value = "token") String token,
			@Valid @RequestBody PasswordResetDTO passwordResetDTO, Errors errors) throws NotFoundException, ConflictException, BadRequestException, JsonProcessingException {
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_USER_RESET_PASSWORD_BAD_REQUEST, messages.toString(), true);
		}
		this.userService.resetPassword(token,passwordResetDTO);
		return APIResponse.okStatus(null);
	}
	
	/**
	 * Re-Active user if user is not activated
	 * 
	 * @param email Email to reactive
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 * @throws ConflictException 
	 * @throws BadRequestException 
	 */
	@PostMapping("/activate")
	@ApiOperation(value = "Re-Active user if user is not activated")
	@ApiResponses(value = {
	})
	public APIResponse<EmailAddress> reActiveUser(
		@ApiParam(value = "Informatoin to reactive ", required = true)
		@RequestBody ActivateUser activateUser) throws NotFoundException, ConflictException, BadRequestException {
		log.info("REST request to re-active user with email : {}", activateUser.getEmail());
		if (!SmartFashionUtils.isEmail(activateUser.getEmail())) {
			throw new BadRequestException(BadRequestException.ERROR_USER_REACTIVE_BAD_REQUEST, APIConstants.USER_EMAIL_INVALID_FORMAT_MSG, false);
		}
		
		return APIResponse.okStatus(this.userService.reActiveUser(activateUser.getEmail(), activateUser.getPlatform()));
	}
	
	/**
	 * Get user info by token
	 * 
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 */
	@PostMapping("/token")
	@ApiOperation(value = "Get user by token")
	@ApiResponses(value = {
	})
	public APIResponse<UserDTO> getUserInfoByToken(HttpServletRequest httpServletRequest) throws NotFoundException {
		String jwtToken = httpServletRequest.getHeader(Constants.AUTHORIZATION_HEADER);
		log.info("REST request to get user info by token");

		return APIResponse.okStatus(this.userService.getUserInfoByToken(jwtToken));
	}
	
	/**
	 * Get {@link User} by ID
	 * 
	 * @param id {@link User} ID
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws  NotFoundException if {@link User} not found
	 */
	@GetMapping("/{id}")
	@ApiOperation(value = "Find User")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<UserDTO> findUserById(
		@ApiParam(value = "User ID", required =  true)
		@PathVariable(value = "id") int id) throws NotFoundException {
		log.info("REST request to find user by user Id: {}", id);
		
		UserDTO userDTO = new UserDTO(this.userService.findUserById(id));
		return APIResponse.okStatus(userDTO);
	}
	
	/** 
	 * Update {@link User} by id
	 *
	 * @param id {@link User} id
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws ConflictException if email is existed
	 * @throws NotFoundException if user not found
	 * @throws JsonProcessingException 
	 * @throws BadRequestException 
	 */
	@PutMapping(value = "/{id}")
	@ApiOperation(value = "Update User")
	@ApiResponses(value = {
	})
	@PreAuthorize(AUTHOURIZE_UPDATE_USER)
	public APIResponse<UserDTO> updateUser(
		@ApiParam(value = "User ID", required = true)
		@PathVariable(value = "id", required = true) int id,
		
		@ApiParam(value = "Information to update user")
		@Valid
		@RequestBody UserDTO userDto, Errors errors) throws ConflictException, NotFoundException, JsonProcessingException, BadRequestException {
		log.info("REST request to update User ID : {}", userDto);
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_UPDATE_USER_BAD_REQUEST,messages.toString(), true);
		}
		this.userService.updateUser(id, userDto);
		
		return APIResponse.okStatus(userDto);
	}

	/**
	 * Change password
	 *
	 * @param passwordChangeDTO {@link PasswordChangeDTO}
	 * @return APIResponse
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws BadRequestException
	 */
	@PutMapping("/password")
	@ApiOperation("Change password login")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_CONFLICT_CODE, message =  APIConstants.ERROR_OLDPASSWORD_INVALID_MSG),
	})
	public APIResponse<?> changePassword (
			@ApiParam(value = "OldPassword and NewPassword's info")
			@Valid
			@RequestBody PasswordChangeDTO passwordChangeDTO, Errors errors) throws NotFoundException, ConflictException, BadRequestException {
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(BadRequestException.ERROR_USER_CHANGE_PASSWORD_BAD_REQUEST, messages.toString(), true);
		}
		return APIResponse.okStatus(this.userService.changePassword(passwordChangeDTO));
	}

	/**
	 * Change language that user uses
	 *
	 * @param userLangKey {@link UserLangKey}
	 * @return {@link APIResponse} data is {@link UserDTO}
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@PutMapping("/languages")
	@ApiOperation(value = "Change language that user uses")
	public APIResponse<UserDTO> changeLanguage(
			@ApiParam(value = "Language that user want to change", allowableValues = "en, vi")
			@RequestBody UserLangKey userLangKey) throws NotFoundException, BadRequestException {
		return APIResponse.okStatus(this.userService.changeLanguage(userLangKey));
	}
}
