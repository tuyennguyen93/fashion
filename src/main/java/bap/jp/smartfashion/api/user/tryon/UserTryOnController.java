package bap.jp.smartfashion.api.user.tryon;

import bap.jp.smartfashion.common.dto.TryOnDTO;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller to managing user try on
 */
@RestController
@RequestMapping("/api/v1/users/try-on")
@PreAuthorize("hasAnyRole('ROLE_USER')")
public class UserTryOnController {

	private final Logger log = LoggerFactory.getLogger(UserTryOnController.class);
	
	@Autowired
	private UserTryOnService userTryOnService;

	/**
	 * Try on product
	 *
	 *@param tryOn {@link TryOnDTO}
	 * @return {@link APIResponse} type {@link TryOnDTO}
	 * @throws NotFoundException 
	 * @throws SmartFashionException 
	 * @throws BadRequestException 
	 */
	@PostMapping
	@ApiOperation(value = "Try on product")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<TryOnDTO> tryOn(@Valid @RequestBody TryOnDTO tryOn, Errors errors) throws NotFoundException, SmartFashionException, BadRequestException {
		log.info("User tryon with model id {} and product id {} with user product = {}", tryOn.getModelId(), tryOn.getProductId(), tryOn.getIsUserProduct());
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(messages.toString(), true);
		}
		return APIResponse.okStatus(this.userTryOnService.tryOn(tryOn));
	}
	
}
