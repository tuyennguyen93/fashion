//package bap.jp.smartfashion.api.authorization;
//
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import bap.jp.smartfashion.api.authorization.dto.AuthorityGroupDTO;
//import bap.jp.smartfashion.api.user.UserService;
//import bap.jp.smartfashion.common.model.GroupAuthority;
//import bap.jp.smartfashion.common.model.User;
//import bap.jp.smartfashion.common.response.APIResponse;
//import bap.jp.smartfashion.exception.ConflictException;
//import bap.jp.smartfashion.exception.NotFoundException;
//import bap.jp.smartfashion.util.constants.APIConstants;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//
///**
// * Controller to authorization users
// * 
// */
//@RestController
//@RequestMapping("/api/v1/authorization")
//@Api(value = "Authorization REST Endpoint", description = "Authorization API")
//public class AuthorizationController {
//	
//	@Autowired
//	private AuthorizationService authorizationService;
//	
//	private final Logger log = LoggerFactory.getLogger(UserService.class);
//	
//	/**
//	 * Get user roles by user id
//	 * 
//	 * @param userId userId of {@link User}
//	 * @return {@link ResponseEntity} of {@link APIResponse}
//	 * @throws NotFoundException 
//	 */
//	@GetMapping(value = "{userId}")
//	@ApiOperation(value = "Get user roles by user id")
//	@ApiResponses(value = {
//		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
//	})
//	public APIResponse<List<String>> getUserAuthorityByUserId(@PathVariable(value = "userId") int userId) throws NotFoundException {
//		log.info("REST request to get user roles for user id: {}", userId);
//		return APIResponse.okStatus(this.authorizationService.findUserAuthorityByUserId(userId));
//	}
//	
//	/**
//	 * Get authority group
//	 * @return {@link ResponseEntity} of {@link APIResponse}
//	 * @throws NotFoundException 
//	 */
//	@GetMapping(value = "/groups")
//	@ApiOperation(value = "Get Access Control List by group Type")
//	public APIResponse<?> rpcAuthorityGroup(
//		@ApiParam(allowableValues = "GET_AUTHOR_GROUP_USER, GET_AUTHOR_GROUP_SHOP, LIST_AUTHOR_GROUP_USER, LIST_AUTHOR_GROUP_SHOP", required = true)
//		@RequestParam(value = "actionName", required = true) String actionName ) throws NotFoundException {
//		log.info("REST request to get all authority group");
//		return APIResponse.okStatus(this.authorizationService.rpcAuthorityGroup(actionName));
//	}
//	
//	/**
//	 * Get authority group
//	 * @return {@link ResponseEntity} of {@link APIResponse}
//	 * @throws NotFoundException 
//	 */
//	@GetMapping(value = "/groups/{authorityGroupId}")
//	@ApiOperation(value = "Get authority group by id")
//	public APIResponse<GroupAuthority> getAuthorityGroupById(
//		@PathVariable(value = "authorityGroupId", required = true) Integer authorityGroupId ) throws NotFoundException {
//		log.info("getAuthorityGroupById");
//		return APIResponse.okStatus(this.authorizationService.getAuthorityGroup(authorityGroupId));
//	}
//	
//	/**
//	 * Get authority group
//	 * @return {@link ResponseEntity} of {@link APIResponse}
//	 * @throws NotFoundException 
//	 * @throws ConflictException 
//	 */
//	@PostMapping(value = "/groups")
//	@ApiOperation(value = "Create authority group")
//	public APIResponse<AuthorityGroupDTO> createAuthorityGroup(
//		@ApiParam(allowableValues = "USER, SHOP", required = true)
//		@RequestBody AuthorityGroupDTO authorityGroupDTO) throws NotFoundException, ConflictException {
//		log.info("REST request to get all authority group");
//		return APIResponse.okStatus(this.authorizationService.createAuthorityGroup(authorityGroupDTO));
//	}
//	
//	/**
//	 * Get authority group
//	 * @return {@link ResponseEntity} of {@link APIResponse}
//	 * @throws NotFoundException 
//	 * @throws ConflictException 
//	 */
//	@PutMapping(value = "/groups/{groupId}")
//	@ApiOperation(value = "Update authority group")
//	public APIResponse<AuthorityGroupDTO> updateAuthorityGroup(
//			
//		@PathVariable(value = "groupId", required = true) Integer groupId,
//		@ApiParam(allowableValues = "USER, SHOP", required = true)
//		@RequestBody AuthorityGroupDTO authorityGroupDTO) throws NotFoundException, ConflictException {
//		log.info("REST request to get all authority group");
//		return APIResponse.okStatus(this.authorizationService.updateAuthorityGroup(groupId, authorityGroupDTO));
//	}
//
//	@DeleteMapping(value = "/{groupId}")
//	@ApiOperation(value = "Delete the authority group by id")
//	@ApiResponses(value = {
//		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
//	})
//	public APIResponse<?> deleteAuthorityGroup(
//		@ApiParam(value = "Authority Group ID", required = true)
//		@PathVariable(value = "groupId", required = true) int groupId) throws ConflictException, NotFoundException {
//		log.info("REST request to detele groupId id: {}", groupId);
//		this.authorizationService.deleteAuthorityGroup(groupId);
//		
//		return APIResponse.okStatus(null);
//	}
//}