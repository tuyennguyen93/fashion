package bap.jp.smartfashion.api.authorization.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;

import bap.jp.smartfashion.common.model.GroupAuthority;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a {@link GroupAuthority}
 *
 */
@Getter
@Setter
public class AuthorityGroupDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Group name can't empty")
	@ApiModelProperty(notes = "Group name", required = true)
	private String groupName;
	
	@ApiModelProperty(allowableValues = "USER, SHOP", notes = "Group type")
	private String groupType;
	
	private List<AuthorityGroupDetailDTO> authorityGroupDetailDTOs;

	public AuthorityGroupDTO(GroupAuthority groupAuthority) {
		this.groupName = groupAuthority.getGroupName();
		this.groupType = groupAuthority.getGroupType();
	}

	public AuthorityGroupDTO() {
	}

}
