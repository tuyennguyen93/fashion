//package bap.jp.smartfashion.api.authorization;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import bap.jp.smartfashion.common.service.MessageService;
//import org.apache.commons.lang3.EnumUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import bap.jp.smartfashion.api.authorization.dto.AuthorityGroupDTO;
//import bap.jp.smartfashion.api.authorization.dto.AuthorityGroupDetailDTO;
//import bap.jp.smartfashion.api.authorization.vo.AuthorityActionVO;
//import bap.jp.smartfashion.api.user.UserService;
//import bap.jp.smartfashion.common.base.BaseService;
//import bap.jp.smartfashion.common.model.GroupAuthority;
//import bap.jp.smartfashion.common.model.GroupAuthorityDetail;
//import bap.jp.smartfashion.common.model.Role;
//import bap.jp.smartfashion.common.model.User;
//import bap.jp.smartfashion.common.repository.GroupAuthorityDetailRepository;
//import bap.jp.smartfashion.common.repository.GroupAuthorityRepository;
//import bap.jp.smartfashion.exception.ConflictException;
//import bap.jp.smartfashion.exception.NotFoundException;
//import bap.jp.smartfashion.util.SmartFashionUtils;
//import bap.jp.smartfashion.util.constants.APIConstants;
//import bap.jp.smartfashion.util.constants.AccessControlListConstants;
//import bap.jp.smartfashion.util.constants.Constants;
//import bap.jp.smartfashion.util.constants.GroupAuthorityConstants;
//
///**
// * Service class for managing authorization.
// */
//@Service
//public class AuthorizationService extends BaseService {
//
//	private final Logger log = LoggerFactory.getLogger(UserService.class);
//
//	@Autowired
//	private GroupAuthorityRepository groupAuthorityRepository;
//
//	@Autowired
//	private GroupAuthorityDetailRepository groupAuthorityDetailRepository;
//
//	@Autowired
//	private MessageService messageService;
//
//	/**
//	 * Get user roles by user id
//	 *
//	 * @param userId userId of {@link User}
//	 * @return {@link APIResponse}
//	 * @throws NotFoundException
//	 */
//	public List<String> findUserAuthorityByUserId(int userId) throws NotFoundException {
//
//		List<String> roles = new ArrayList<String>();
//		User user = this.findUserByUserId(userId);
//
//		// set role application
//		for (Role role : user.getRoles()) {
//			roles.add(role.getName());
//		}
//		// set role function
//		List<GroupAuthorityDetail> groupAuthorityDetailList = this.groupAuthorityDetailRepository.findByObjectIdAndGroupType(user.getId(), GroupAuthorityConstants.GET_AUTHOR_GROUP_USER);
//		for (GroupAuthorityDetail groupAuthorityDetail : groupAuthorityDetailList) {
//			String groupAuthorityName = Constants.ROLE_PREFIX + groupAuthorityDetail.getFunctionName() + Constants.CHAR_UNDERSCORE + groupAuthorityDetail.getActionName();
//			roles.add(groupAuthorityName);
//		}
//		return roles;
//	}
//
//
//	public Object rpcAuthorityGroup(String actionName) throws NotFoundException {
//		this.checkAuthorityGroupAction(actionName);
//		Object data = new Object();
//		switch (actionName) {
//		case GroupAuthorityConstants.GET_AUTHOR_GROUP_USER:
//			data = AccessControlListConstants.actionForUserGroup();
//			break;
//
//		case GroupAuthorityConstants.GET_AUTHOR_GROUP_SHOP:
//			data = AccessControlListConstants.actionForShopGroup();
//			break;
//
//		case GroupAuthorityConstants.LIST_AUTHOR_GROUP_USER:
//			data = this.getAuthorityGroupByGroupType(GroupAuthorityConstants.GROUP_TYPE_USER);
//			break;
//
//		case GroupAuthorityConstants.LIST_AUTHOR_GROUP_SHOP:
//			data = this.getAuthorityGroupByGroupType(GroupAuthorityConstants.GROUP_TYPE_SHOP);
//			break;
//		default:
//			throw new NotFoundException(APIConstants.ERR,
//					String.format(this.messageService.getMessage("error.bad-value-msg"), actionName), HttpStatus.NOT_FOUND);
//		}
//		
//		return data;
//	}
//	private List<GroupAuthority> getAuthorityGroupByGroupType(String groupType) {
//		return this.groupAuthorityRepository.findByGroupType(groupType);
//	}
//	/**
//	 * Get role groups
//	 *
//	 * @return {@link APIResponse}
//	 * @throws NotFoundException
//	 */
//	public GroupAuthority getAuthorityGroup(Integer groupAuthorityId) throws NotFoundException {
//
//		GroupAuthority groupAuthority = this.groupAuthorityRepository.findById(groupAuthorityId)
//				.orElseThrow(() -> new NotFoundException(APIConstants.ERROR_NOT_FOUND_CODE,
//						String.format(this.messageService.getMessage("error.not-found-msg"),
//								this.messageService.getMessage("authority-group")), HttpStatus.NOT_FOUND));
//		
//		return groupAuthority;
//	}
//
//	/**
//	 * Get role groups
//	 *
//	 * @return {@link APIResponse}
//	 * @throws NotFoundException
//	 * @throws ConflictException
//	 */
//	@Transactional(rollbackFor = Exception.class)
//	public AuthorityGroupDTO createAuthorityGroup(AuthorityGroupDTO authorityGroupDTO) throws NotFoundException, ConflictException {
//		this.checkAuthorityGroupType(authorityGroupDTO.getGroupType());
//		this.checkAuthorityGroupExisted(null, authorityGroupDTO);
//		GroupAuthority groupAuthority = new GroupAuthority();
//		BeanUtils.copyProperties(authorityGroupDTO, groupAuthority);
//		GroupAuthority groupAuthorityRes = this.groupAuthorityRepository.save(groupAuthority);
//
//		List<GroupAuthorityDetail> groupAuthorityDetails = new ArrayList<>();
//
//		for (AuthorityGroupDetailDTO authorityGroupDetailDTO : authorityGroupDTO.getAuthorityGroupDetailDTOs()) {
//			this.checkActionList("GET_AUTHOR_GROUP_" + authorityGroupDTO.getGroupType(), authorityGroupDetailDTO);
//			GroupAuthorityDetail groupAuthorityDetail = new GroupAuthorityDetail();
//			groupAuthorityDetail.setGroupAuthority(groupAuthorityRes);
//			groupAuthorityDetail.setFunctionName(authorityGroupDetailDTO.getFunctionName());
//			for (String actionName : authorityGroupDetailDTO.getActionList()) {
//				groupAuthorityDetail.setActionName(actionName);
//				groupAuthorityDetails.add(groupAuthorityDetail);
//			}
//		}
//		this.groupAuthorityDetailRepository.saveAll(groupAuthorityDetails);
//		return authorityGroupDTO;
//	}
//
//	@Transactional(rollbackFor = Exception.class)
//	public AuthorityGroupDTO updateAuthorityGroup(Integer groupId, AuthorityGroupDTO authorityGroupDTO) throws NotFoundException, ConflictException {
//
//		GroupAuthority groupAuthority = this.groupAuthorityRepository.findById(groupId).orElseThrow(() ->
//			new NotFoundException(APIConstants.ERROR_NOT_FOUND_CODE, String.format(this.messageService.getMessage("error.not-found-msg"),
//					this.messageService.getMessage("authority-group")), HttpStatus.NOT_FOUND));
//
//		this.checkAuthorityGroupType(authorityGroupDTO.getGroupType());
//		this.checkAuthorityGroupExisted(groupId, authorityGroupDTO);
//		BeanUtils.copyProperties(authorityGroupDTO, groupAuthority);
//		GroupAuthority groupAuthorityRes = this.groupAuthorityRepository.save(groupAuthority);
//
//		List<GroupAuthorityDetail> groupAuthorityDetails = new ArrayList<>();
//
//		for (AuthorityGroupDetailDTO authorityGroupDetailDTO : authorityGroupDTO.getAuthorityGroupDetailDTOs()) {
//			this.checkActionList("GET_AUTHOR_GROUP_" + authorityGroupDTO.getGroupType(), authorityGroupDetailDTO);
//			GroupAuthorityDetail groupAuthorityDetail = new GroupAuthorityDetail();
//			groupAuthorityDetail.setGroupAuthority(groupAuthorityRes);
//			groupAuthorityDetail.setFunctionName(authorityGroupDetailDTO.getFunctionName());
//			for (String actionName : authorityGroupDetailDTO.getActionList()) {
//				groupAuthorityDetail.setActionName(actionName);
//				groupAuthorityDetails.add(groupAuthorityDetail);
//			}
//		}
//		this.groupAuthorityDetailRepository.deleteByGroupAuthority_Id(groupId);
//		this.groupAuthorityDetailRepository.saveAll(groupAuthorityDetails);
//
//		return authorityGroupDTO;
//	}
//
//	@Transactional(rollbackFor = Exception.class)
//	public void deleteAuthorityGroup(Integer groupId) throws NotFoundException, ConflictException {
//		GroupAuthority groupAuthority = this.groupAuthorityRepository.findById(groupId).orElseThrow(() ->
//			new NotFoundException(APIConstants.ERROR_NOT_FOUND_CODE,
//					String.format(this.messageService.getMessage("error.not-found-msg"),
//							this.messageService.getMessage("authority-group")), HttpStatus.NOT_FOUND));
//
//		this.groupAuthorityRepository.deleteById(groupAuthority.getId());
//	}
//
//	private void checkAuthorityGroupExisted(Integer groupId, AuthorityGroupDTO authorityGroupDTO) throws ConflictException {
//		Optional<GroupAuthority> groupAuthorityOptional = this.groupAuthorityRepository
//				.findOneByGroupNameAndGroupType(authorityGroupDTO.getGroupName(), authorityGroupDTO.getGroupType());
//		if (groupAuthorityOptional.isPresent() && groupId != groupAuthorityOptional.get().getId()) {
//			throw new ConflictException(String.format(this.messageService.getMessage("error.conflict-msg"),
//					this.messageService.getMessage("group-name")));
//		}
//	}
//
//	private void checkAuthorityGroupAction(String actionName) throws NotFoundException {
//		boolean isExisted = EnumUtils.isValidEnum(GroupAuthorityConstants.AuthorityGroupAction.class, actionName);
//		if (!isExisted) {
//			throw new NotFoundException(APIConstants.ERROR_NOT_FOUND_CODE,
//					String.format(this.messageService.getMessage("error.not-found-msg"),
//							this.messageService.getMessage("action")), HttpStatus.NOT_FOUND);
//		}
//	}
//
//	private void checkAuthorityGroupType(String groupType) throws NotFoundException {
//		boolean isExisted = EnumUtils.isValidEnum(GroupAuthorityConstants.AuthorityGroupType.class, groupType);
//		if (!isExisted) {
//			throw new NotFoundException(APIConstants.ERROR_NOT_FOUND_CODE,
//					String.format(this.messageService.getMessage("error.not-found-msg"),
//							this.messageService.getMessage("group-type")), HttpStatus.NOT_FOUND);
//		}
//	}
//
//	private void checkActionList(String groupType, AuthorityGroupDetailDTO authorityGroupDetailDTO) throws NotFoundException {
//		switch (groupType) {
//			case GroupAuthorityConstants.GET_AUTHOR_GROUP_USER:
//				List<AuthorityActionVO> authorityActionVOsUser = AccessControlListConstants.actionForUserGroup().getAuthorityActionVOs();
//				List<String> functionNameListUser = new ArrayList<>();
//				for (AuthorityActionVO authorityActionVO : authorityActionVOsUser) {
//					functionNameListUser.add(authorityActionVO.getFunctionName());
//					if (authorityActionVO.getFunctionName().equals(authorityGroupDetailDTO.getFunctionName())) {
//						for (String actionName : authorityGroupDetailDTO.getActionList()) {
//							if (!authorityActionVO.getActionName().contains(actionName)) {
//								throw new NotFoundException(APIConstants.ERROR_CODE,
//										String.format(this.messageService.getMessage("error.bad-value-msg"), actionName), HttpStatus.NOT_FOUND);
//							}
//						}
//					}
//				}
//
//				if (!functionNameListUser.contains(authorityGroupDetailDTO.getFunctionName())) {
//					throw new NotFoundException(APIConstants.ERROR_CODE,
//							String.format(this.messageService.getMessage("error.bad-value-msg"), authorityGroupDetailDTO.getFunctionName()), HttpStatus.NOT_FOUND);
//				}
//				break;
//
//			case GroupAuthorityConstants.GET_AUTHOR_GROUP_SHOP:
//				List<AuthorityActionVO> authorityActionVOsShop = AccessControlListConstants.actionForShopGroup().getAuthorityActionVOs();
//				List<String> functionNameListShop = new ArrayList<>();
//				for (AuthorityActionVO authorityActionVO : authorityActionVOsShop) {
//					functionNameListShop.add(authorityActionVO.getFunctionName());
//					if (authorityActionVO.getFunctionName().equals(authorityGroupDetailDTO.getFunctionName())) {
//						for (String actionName : authorityGroupDetailDTO.getActionList()) {
//							if (!authorityActionVO.getActionName().contains(actionName)) {
//								throw new NotFoundException(APIConstants.ERROR_CODE,
//										String.format(this.messageService.getMessage("error.bad-value-msg"), actionName), HttpStatus.NOT_FOUND);
//							}
//						}
//					}
//				}
//
//				if (!functionNameListShop.contains(authorityGroupDetailDTO.getFunctionName())) {
//					throw new NotFoundException(APIConstants.ERROR_CODE,
//							String.format(this.messageService.getMessage("error.bad-value-msg"), authorityGroupDetailDTO.getFunctionName()), HttpStatus.NOT_FOUND);
//				}
//				break;
//		}
//	}
//}
