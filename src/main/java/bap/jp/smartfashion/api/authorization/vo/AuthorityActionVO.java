package bap.jp.smartfashion.api.authorization.vo;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Authority Action VO
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class AuthorityActionVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String functionName;
	private List<String> actionName;

	public AuthorityActionVO() {
	}
}
