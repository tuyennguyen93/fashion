package bap.jp.smartfashion.api.authorization.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a lisst authority group detail
 *
 */
@Getter
@Setter
public class AuthorityGroupDetailDTO {
	@ApiModelProperty(notes = "Action list",allowableValues = "VIEW, CREATE, EDIT, DELETE, ALL")
	private List<String> actionList;
	
	@ApiModelProperty(notes = "Function name")
	private String functionName;
}
