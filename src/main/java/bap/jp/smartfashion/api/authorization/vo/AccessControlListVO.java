package bap.jp.smartfashion.api.authorization.vo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Access Control List VO
 *
 */
@Getter
@Setter
public class AccessControlListVO {
	private String groupType;
	private List<AuthorityActionVO> authorityActionVOs;
}
