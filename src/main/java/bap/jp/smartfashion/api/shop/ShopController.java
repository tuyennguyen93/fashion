package bap.jp.smartfashion.api.shop;

import bap.jp.smartfashion.api.admin.product.service.ProductCollectionService;
import bap.jp.smartfashion.common.dto.DashboardDTO;
import bap.jp.smartfashion.common.dto.ImageWrapperDTO;
import bap.jp.smartfashion.common.dto.ImageUploadDTO;
import bap.jp.smartfashion.common.dto.MixMatchDTO;
import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.dto.ProductImageListDTO;
import bap.jp.smartfashion.common.dto.ProductInteractionDTO;
import bap.jp.smartfashion.common.dto.ProfileDTO;
import bap.jp.smartfashion.common.dto.ShopProductCollectionDTO;
import bap.jp.smartfashion.common.dto.ShopVisitorInfo;
import bap.jp.smartfashion.enums.ShopEnum;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;


import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ReturningVisitor;
import bap.jp.smartfashion.common.vo.ShopFilterMenu;
import bap.jp.smartfashion.common.vo.ShopMenu;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * Controller to managing shops
 */
@RestController
@RequestMapping("/api/v1/shops")
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_SHOP','ROLE_USER', 'ROLE_ANONYMOUS')")
public class ShopController {

	@Autowired
	private ShopService shopService;

	@Autowired
    private ProductCollectionService productCollectionService;

	private final Logger log = LoggerFactory.getLogger(ShopController.class);

	/**
	 * Get clothing menu for shop
	 *
	 * @param shopId {@link Shop} id
	 * @param collectionId {@link ProductCollection} Id
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/clothing-menu")
	@ApiOperation(value = "Get clothing menu for shop")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<List<ShopMenu>> getClothingMenu(
		@ApiParam(value ="Shop Id", required = true)
		@PathVariable(value = "shopId") Integer shopId,
		@ApiParam(value ="Collection Id", required = true)
		@RequestParam(value = "collectionId", required = false) Integer collectionId) throws NotFoundException {
		log.info("REST request to get clothing menu for shop id = {} with collection id = {}", shopId, collectionId);
		return APIResponse.okStatus(this.shopService.getClothingMenu(shopId, collectionId));
	}

	/**
	 * Get clothing menu for shop
	 *
	 * @param shopId {@link Shop} id
	 * @param collectionId {@link ProductCollection} Id
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/filter-menu")
	@ApiOperation(value = "Get filter menu for shop")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})

	public APIResponse<List<ShopFilterMenu>> getFilterMenu(
		@PathVariable(value = "shopId", required = true) Integer shopId,
		@ApiParam(value ="Collection Type", required = true, allowableValues = "ADULT, KID")
		@RequestParam(value = "collectionType", required = false) String collectionType) throws NotFoundException {
		log.info("REST request to get filter menu with collection type = {}", collectionType);

		return APIResponse.okStatus(this.shopService.getFilterMenu(collectionType, shopId));
	}

	/**
	 * Update avatar shop
	 *
	 * @param photo  {@link Shop} avatar
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws AuthenticationException 
	 */
	@PutMapping("/photo")
	@ApiOperation(value = "Update profile avatar shop")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<Shop> updateProfilePhoto(
			@RequestBody ImageWrapperDTO photo) throws NotFoundException, AuthenticationException {
		return APIResponse.okStatus(this.shopService.updateProfilePhoto( photo));
	}


	/**
	 * Get profile
	 *
	 * @return ProfileDTO {@link ProfileDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/profile")
	@ApiOperation(value = "Get profile for user")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<ProfileDTO> getProfile() throws NotFoundException {
		log.info("REST request to get profile");
		return APIResponse.okStatus(this.shopService.getProfile());
	}

	/**
	 * Update profile
	 *
	 * @param profileDto {@link ProfileDTO}
	 * @param errors {@link Errors}
	 * @return APIResponse data is {@link ProfileDTO}
	 * @throws ConflictException
	 * @throws NotFoundException
	 * @throws BadRequestException
	 * @throws AuthenticationException 
	 */
	@PutMapping("/profile")
	@ApiOperation(value = "Update profile")
	@ApiResponses(value = {
	})
	public APIResponse<ProfileDTO> updateProfileInformation(
			@Valid
			@RequestBody ProfileDTO profileDto, Errors errors) throws ConflictException, NotFoundException, BadRequestException, AuthenticationException {
		this.shopService.validationNotEmptyBranchAddressAndWarehouseAddress(errors, profileDto.getShopBranchList(), profileDto.getWarehouseAddressList());
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(messages.toString(), true);
		}
		return APIResponse.okStatus(this.shopService.updateProfileInformation(profileDto));
	}


	/**
	 * Get statistic for {@link Shop} by {@link Shop} Id
	 * @param shopId {@link Shop} Id
	 * @param collectionId {@link ProductCollection} Id
	 * @param actionCode action code
	 * @param top limit value
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/statistics")
	@ApiOperation(value = "Get statistic for shop")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<List<ProductInteractionDTO>> getStatisticForShop(
		@ApiParam(value ="Shop Id", required = true)
		@PathVariable(value = "shopId") Integer shopId,
		@ApiParam(value ="Product collection Id", required = true)
		@RequestParam(value = "collectionId") Integer collectionId,
		@ApiParam(value ="actionCode", required = true, allowableValues = "TRY,FAVORITE")
		@RequestParam(value = "actioncode") String actionCode,
		@ApiParam(value ="Top", required = true)
		@RequestParam(value = "top") Integer top) throws NotFoundException {
			log.info("REST request to get for shop id {}", shopId);
		return APIResponse.okStatus(this.shopService.getStatistic(shopId, collectionId, actionCode, top));
	}

	/**
	 *
	 * @param collectionId {@link ProductCollection} Id
	 * @param actionCode action code
	 * @param top limit value
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/statistics")
	@ApiOperation(value = "Get statistic for smart fashion app")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<List<ProductInteractionDTO>> getStatisticForApp(
		@ApiParam(value ="Product collection Id", required = true)
		@RequestParam(value = "collectionId") Integer collectionId,
		@ApiParam(value ="actionCode", required = true, allowableValues = "TRY,FAVORITE")
		@RequestParam(value = "actioncode") String actionCode,
		@ApiParam(value ="Top", required = true)
		@RequestParam(value = "top") Integer top) throws NotFoundException {
			log.info("REST request to get for app");
		return APIResponse.okStatus(this.shopService.getStatistic(null, collectionId, actionCode, top));
	}

	/**
	 * Get data for dashboard
	 *
	 * @param shopId {@link Shop} Id
	 * @param topTried number to get top tried
	 *
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/dashboards")
	@ApiOperation(value = "Get dashboards information ")
	@ApiResponses(value = {
	})
	public APIResponse<DashboardDTO> getDashboards(
			@ApiParam(value ="Shop Id", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value ="Get number of tried", required = true)
			@RequestParam(value = "topTried", required = true) Integer topTried) throws NotFoundException {

		log.info("REST request to get dashboards information");
		return APIResponse.okStatus(this.shopService.getDashboards(shopId, topTried));
	}

	/**
	 * Get visitor's information by conditions
	 *
	 * @param shopId {@link Shop}
	 * @param type role of visitor {@link ShopEnum.VisitorType}
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @param startDay day starts
	 * @param endDay day ends
	 * @param limit limit to count data
	 * @return {@link APIResponse} data is {@link ShopVisitorInfo}
	 * @throws NotFoundException
	 * @throws BadRequestException
	 */
	@GetMapping("/{shopId}/visitors")
	@ApiOperation("Get visitor's information by conditions")
	@ApiResponses(value = {
	})
	public APIResponse<List<ShopVisitorInfo>> getVisitorsInformation(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "visitor type", required = true, allowableValues = "NEW_VISITOR,RETURN_VISITOR")
			@RequestParam(value = "type", required = true) String type,
			@ApiParam(value = "timeline", required = true, allowableValues = "HOUR, DAY, WEEK, MONTH, YEAR")
			@RequestParam(value = "timeline", required = true) String timeline,
			@ApiParam(value = "Day starts", required = true)
			@RequestParam(value = "startDay", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDay,
			@ApiParam(value = "Day ends", required = true)
			@RequestParam(value = "endDay", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDay,
			@ApiParam(value = "limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit) throws NotFoundException, BadRequestException {
		return APIResponse.okStatus(this.shopService.getVisitorInformation(shopId, type, timeline, startDay, endDay, limit));
	}

	/**
	 * Get percent returning Visitors
	 *
	 * @param shopId {@link Shop}
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/returning-visitors")
	@ApiOperation("Get percent returning visitors")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<List<ReturningVisitor>> getReturningVisitors(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "timeline", required = true, allowableValues = "DAY, WEEK, MONTH, YEAR")
			@RequestParam(value = "timeline", required = true) String timeline) throws NotFoundException {
		return APIResponse.okStatus(this.shopService.getReturningVisitors(shopId, timeline));
	}

    /**
     * Change product_collection's background image in shop
     *
     * @param shopId {@link Shop}
     * @param productCollectionId {@link ProductCollection}
     * @param imageWrapperDTO {@link ImageWrapperDTO}
     * @return {@link APIResponse} data is {@link ShopProductCollectionDTO}
     * @throws NotFoundException
	 * @throws BadRequestException
     */
    @PutMapping("/{shopId}/product-collection/{productCollectionId}/images")
    @ApiOperation("Change product_collection's background image for shop")
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
			@ApiResponse(code = APIConstants.ERROR_BAD_REQUEST_CODE, message = APIConstants.ERROR_BAD_VALUE_MSG)
    })
    public APIResponse<ShopProductCollectionDTO> changeProductCollectionImage(
            @ApiParam(value = "Shop ID", required = true)
            @PathVariable(value = "shopId") Integer shopId,
            @ApiParam(value = "Product Collection ID", required = true)
            @PathVariable(value = "productCollectionId") Integer productCollectionId,
            @ApiParam(value = "product_collection's image url")
            @RequestBody ImageWrapperDTO imageWrapper) throws NotFoundException, BadRequestException {
	    return APIResponse.okStatus(this.shopService.changeProductCollectionImage(shopId, productCollectionId, imageWrapper));
    }

    /**
     * Get list of product_collections for shop
     *
     * @param shopId {@link Shop}
     * @return {@link APIResponse} data is {@link ProductCollectionDTO}
     */
    @GetMapping("/{shopId}/product-collections")
    @ApiOperation(value = "Get product collection list for shop")
    @ApiResponses(value = {})
    public APIResponse<List<ProductCollectionDTO>> getProductCollectionsForShop(
            @ApiParam(value = "Shop ID", required = true)
            @PathVariable(value = "shopId") Integer shopId) {
        log.info("REST request to get product_collections for shop");
        return APIResponse.okStatus(this.productCollectionService.getProductCollectionsForShop(shopId));
    }
}
