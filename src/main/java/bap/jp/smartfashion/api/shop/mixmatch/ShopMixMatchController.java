package bap.jp.smartfashion.api.shop.mixmatch;

import bap.jp.smartfashion.common.dto.MixMatchDTO;
import bap.jp.smartfashion.common.dto.ProductImageListDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;


import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller to managing mix match
 */
@RestController
@RequestMapping("/api/v1/shops/{shopId}/mix-match")
@PreAuthorize("hasAnyRole('ROLE_SHOP')")
public class ShopMixMatchController {

	@Autowired
	private ShopMixMatchService shopMixMatchService;

	private final Logger log = LoggerFactory.getLogger(ShopMixMatchController.class);

	/**
	 * Create new Mix & Match
	 *
	 * @param mixMatchDto {@link MixMatchDTO}
	 * @param shopId {@link Shop}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws ConflictException
	 * @throws SmartFashionException
	 */
	@PostMapping("/{productCollectionId}")
	@ApiOperation(value = "Create mix-match")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<MixMatchDTO> createMixMatch(
			@RequestBody MixMatchDTO mixMatchDto,
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@PathVariable(value = "productCollectionId", required = true) Integer productCollectionId) throws NotFoundException, ConflictException, SmartFashionException {
		return APIResponse.createdStatus(this.shopMixMatchService.createMixMatch(mixMatchDto, shopId, productCollectionId));
	}

	/**
	 * Delete mix match
	 *
	 * @param shopId {@link Shop}
	 * @param mixMatchId {@link MixMatch}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	@DeleteMapping("/{mixMatchId}")
	@ApiOperation(value = "Delete mix match")
	@ApiResponses(value = {
	})
	public APIResponse<MixMatchDTO> deleteMixMatch(
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@PathVariable(value = "mixMatchId", required = true) Integer mixMatchId) throws NotFoundException, SmartFashionException {
		return APIResponse.okStatus(this.shopMixMatchService.deleteMixMatch(shopId, mixMatchId));
	}

	/**
	 * Update mix match
	 *
	 * @param mixMatchDto {@link MixMatchDTO}
	 * @param mixMatchId {@link MixMatch}
	 * @param shopId {@link Shop}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	@PutMapping("/{mixMatchId}")
	@ApiOperation(value = "Update mix-match")
	@ApiResponses(value = {
	})
	public APIResponse<MixMatchDTO> updateMixMatch(
			@RequestBody MixMatchDTO mixMatchDto,
			@PathVariable(value = "mixMatchId", required = true) Integer mixMatchId,
			@PathVariable(value = "shopId", required = true) Integer shopId) throws NotFoundException, SmartFashionException {
		return APIResponse.okStatus(this.shopMixMatchService.updateMixMatch(mixMatchDto, shopId, mixMatchId));
	}

	/**
	 * Get list mix match
	 *
	 * @param shopId {@link Shop}
	 * @param collectionId {@link ProductCollection}
	 * @param page
	 * @param limit
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping
	@ApiOperation(value = "Get mix match list")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<PageInfo<MixMatchDTO>> getMixMatchList(
			@ApiParam(value ="Shop Id", required = true)
			@PathVariable(value = "shopId") int shopId,

			@ApiParam(value ="Collection Id", required = true)
			@RequestParam(value = "collectionId") int collectionId,

			@ApiParam(value ="Page", required = false)
			@RequestParam(value = "page", required = false) Integer page,

			@ApiParam(value ="Limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit,
			
			@ApiParam(value ="Search mix match by mix match name or product", required = false)
			@RequestParam(value = "query", required = false) String query) throws NotFoundException {
		log.info("REST request to find mix match list");
		return APIResponse.okStatus(this.shopMixMatchService.getMixMatchList(shopId, collectionId, page, limit, query));
	}

	/**
	 *  Get Mix & Match detail (to view) by mixMatchId
	 *
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/{mixMatchId}")
	@ApiOperation(value = "Get Mix Match id ")
	@ApiResponses(value = {
	})
	public APIResponse<MixMatchDTO> getMixMatchId(
			@PathVariable(value = "mixMatchId", required = true) Integer mixMatchId,
			@PathVariable(value = "shopId", required = true) Integer shopId) throws NotFoundException {

		log.info("REST request to get mix match id");
		return APIResponse.okStatus(this.shopMixMatchService.getMixMatchId(shopId,mixMatchId));
	}

	/**
	 *  Get image list by collectionId, productTypeId , or( Product ID or Product name)
	 *
	 * @param collectionId {@link ProductCollection}
	 * @param productTypeId {@link ProductType}
	 * @param productCodeOrName {@link Product}
	 * @param flag = 0 find by ProductCollection_Id And ProductType_Id ,flag = 1 find By Name
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 **/
	@GetMapping("/images")
	@ApiOperation(value = "Get product image list ")
	@ApiResponses(value = {
	})
	public APIResponse<PageInfo<ProductImageListDTO>> getProductImage(
			@ApiParam(value ="Product collection Id", required = true)
			@RequestParam(value = "collectionId", required = true) Integer collectionId,

			@ApiParam(value ="Product type Id", required = false)
			@RequestParam(value = "productTypeId", required = false) Integer productTypeId,

			@ApiParam(value ="Product coder or name", required = false)
			@RequestParam(value = "productCodeOrName", required = false) String productCodeOrName,

			@ApiParam(value ="flag=(0:Get image list by collectionId, productTypeId, 1: collectionId ,product Code Or Name)", required = true)
			@RequestParam(value = "flag", required = true) Integer flag,

			@ApiParam(value ="Shop Id", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			
			@ApiParam(value ="Page", required = false)
			@RequestParam(value = "page", required = false) Integer page,

			@ApiParam(value ="Limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit) throws NotFoundException {

		log.info("REST request to get product image list");
		return APIResponse.okStatus(this.shopMixMatchService.getProductImage(shopId, collectionId, productTypeId, productCodeOrName, flag, page, limit));
	}
}
