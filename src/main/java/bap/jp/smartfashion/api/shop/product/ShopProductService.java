package bap.jp.smartfashion.api.shop.product;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import bap.jp.smartfashion.api.admin.product.service.ProductSizeService;
import bap.jp.smartfashion.api.admin.product.service.ProductTypeService;

import bap.jp.smartfashion.api.shop.ShopService;
import bap.jp.smartfashion.api.shop.mixmatch.ShopMixMatchService;
import bap.jp.smartfashion.common.dto.ImageWrapperDTO;
import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.dto.ProductDetailDTO;
import bap.jp.smartfashion.common.dto.ProductMixMatchDTO;
import bap.jp.smartfashion.common.dto.ProductPhotoDTO;
import bap.jp.smartfashion.common.dto.ProductRatingDTO;
import bap.jp.smartfashion.common.dto.ProductRatingReplyDTO;
import bap.jp.smartfashion.common.dto.ProductSizeDTO;
import bap.jp.smartfashion.common.dto.ProductSubDetailDTO;
import bap.jp.smartfashion.common.dto.ProductTypeDTO;
import bap.jp.smartfashion.common.dto.ProductUpdateDTO;
import bap.jp.smartfashion.common.dto.ProductWearingPurposeDTO;
import bap.jp.smartfashion.common.model.MixMatch;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductColor;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductMixMatch;
import bap.jp.smartfashion.common.model.ProductPhoto;
import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingCount;
import bap.jp.smartfashion.common.model.ProductRatingReply;
import bap.jp.smartfashion.common.model.ProductSize;
import bap.jp.smartfashion.common.model.ProductSizeChart;
import bap.jp.smartfashion.common.model.ProductSubDetail;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.model.ProductTypeCount;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.repository.MixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ProductDetailRepository;
import bap.jp.smartfashion.common.repository.ProductMixMatchPhotoRepository;
import bap.jp.smartfashion.common.repository.ProductMixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductPhotoRepository;
import bap.jp.smartfashion.common.repository.ProductRatingCountRepository;
import bap.jp.smartfashion.common.repository.ProductRatingReplyRepository;
import bap.jp.smartfashion.common.repository.ProductRatingRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ProductSubDetailRepository;
import bap.jp.smartfashion.common.repository.ProductTypeCountRepository;
import bap.jp.smartfashion.common.repository.ProductWearingPurposeRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.common.service.ai.AIService;
import bap.jp.smartfashion.common.service.ai.response.ClothingResponse;
import bap.jp.smartfashion.common.vo.Currency;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ProductFilter;
import bap.jp.smartfashion.common.vo.ProductProperties;
import bap.jp.smartfashion.common.vo.ProductProperty;
import bap.jp.smartfashion.common.vo.ProductRatingVO;
import bap.jp.smartfashion.common.vo.ProductSearchInfo;
import bap.jp.smartfashion.common.vo.ProductSizeWithCollection;
import bap.jp.smartfashion.common.vo.ProductTypeWithCollection;
import bap.jp.smartfashion.common.vo.ProductWearingPurposeWithType;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.mapper.ProductMapper;
import bap.jp.smartfashion.common.specification.ProductSpecificationsBuilder;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

/**
 * Service class for managing products by shop.
 */
@Service
public class ShopProductService extends BaseService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductMapper productMapper;
	
	@Autowired
	private ProductDetailRepository productDetailRepository;
	
	@Autowired
	private ProductSubDetailRepository productSubDetailRepository;
	
	@Autowired
	private ProductPhotoRepository productPhotoRepository;

	@Autowired
	private ProductCollectionRepository productCollectionRepository;

	@Autowired
	private ProductWearingPurposeRepository productWearingPurposeRepository;

	@Autowired
	private ProductTypeService productTypeService;

	@Autowired
	private ProductSizeService productSizeService;

	@Autowired
	private ProductRatingCountRepository productRatingCountRepository;

	@Autowired
	private ProductRatingRepository productRatingRepository;
	
	@Autowired
	private ProductRatingReplyRepository productRatingReplyRepository;

	@Autowired
	private ProductTypeCountRepository productTypeCountRepository;

	@Autowired
	private ProductService productService;

	@Autowired
	private MessageService messageSevice;
	
	@Autowired
	private AIService aiService;
	
	@Autowired
	private ShopMixMatchService shopMixMatchService;
	
	@Autowired
	private MixMatchRepository mixMatchRepository;
	
	@Autowired
	private ProductMixMatchPhotoRepository productMixMatchPhotoRepository;
	
	@Autowired
	private ProductMixMatchRepository productMixMatchRepository;
	
	private final Logger log = LoggerFactory.getLogger(ShopProductService.class);
	
	/**
	 * Get product list
	 * 
	 * @param name {@link Product} name
	 * @param productFilter {@link ProductFilter} data
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public PageInfo<ProductDTO> getProductList(String name, ProductFilter productFilter) throws NotFoundException {
		this.checkShopBelongUserLogged(productFilter.getShopId());
		Sort sort = productService.buildSortProduct(productFilter.getSortBy());
		PageRequest pageRequest = this.buildPageRequest(productFilter.getPage(), productFilter.getLimit(), sort);
		name = StringUtils.isEmpty(name) ? StringUtils.EMPTY : name;
		List<Integer> productTypes = productService.getProductTypes((Integer) productFilter.getProductTypeId());
		productFilter.setProductTypeId(productTypes);
		Page<ProductDTO> data;
		if(ProductEnum.ProductSort.TRIED_PRODUCTS.toString().equals(productFilter.getSortBy()) ||
				ProductEnum.ProductSort.FAVORITED_PRODUCTS.toString().equals(productFilter.getSortBy())) {
			data = this.productRepository.findAll(
					ProductSpecificationsBuilder.searchProductsWithInteraction(name, productFilter), pageRequest)
					.map(ProductDTO::new);
		} else {
			data = this.productRepository.findAll(
					ProductSpecificationsBuilder.searchProducts(name, productFilter), pageRequest)
					.map(ProductDTO::new);
		}
		PageInfo<ProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * Get product list
	 * 
	 * @param productFilter {@link ProductFilter} 
	 * 
	 * @return {@link APIResponse}
	 * @throws NotFoundException 
	 */
	public PageInfo<ProductDTO> filterProduct(String keyWord, ProductFilter productFilter) throws NotFoundException {
		this.checkShopBelongUserLogged(productFilter.getShopId());

		Sort sort = productService.buildSortProduct(productFilter.getSortBy());
		PageRequest	pageRequest = this.buildPageRequest(productFilter.getPage(), productFilter.getLimit(), sort);
		List<Integer> productTypes = productService.getProductTypes((Integer) productFilter.getProductTypeId());
		productFilter.setProductTypeId(productTypes);

		Page<ProductDTO> data;
		if(ProductEnum.ProductSort.TRIED_PRODUCTS.toString().equals(productFilter.getSortBy()) || ProductEnum.ProductSort.FAVORITED_PRODUCTS.toString().equals(productFilter.getSortBy())) {
			data = this.productRepository.findAll(ProductSpecificationsBuilder.filterProductInteraction(keyWord, productFilter), pageRequest).map(ProductDTO::new);
		} else {
			data = this.productRepository.findAll(ProductSpecificationsBuilder.filterProduct(keyWord, productFilter), pageRequest).map(ProductDTO::new);
		}
		PageInfo<ProductDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		
		return pageInfo;
	}

	/**
	 * Update product
	 * 
	 * @param productDTO {@link ProductDTO}
	 * @param shopId {@link Shop} Id
	 * @param productId {@link Product} 
	 * 
	 * @return {@link Product}
	 * 
	 * @throws NotFoundException
	 * @throws ConflictException 
	 * @throws SocketTimeoutException 
	 * @throws SmartFashionException 
	 * @throws BadRequestException 
	 */
	@CacheEvict(value = ShopService.SHOP_MENU_BY_SHOP_ID_CACHE, allEntries = true)
	@Transactional(rollbackFor = Exception.class)
	public Product updateProduct(ProductUpdateDTO productUpdateDTO, Integer shopId, Integer productId) throws NotFoundException, ConflictException, SocketTimeoutException, SmartFashionException, BadRequestException {
		this.checkShopCanPostProduct(shopId);
		Product product = this.productMapper.productUpdateDTOToProduct(productUpdateDTO, shopId, productId);

		if(!productUpdateDTO.isQuickUpdate() && productUpdateDTO.getProductDetailDTOs().isEmpty()) {
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(this.messageSevice.getMessage("error.not-found-msg"),
					this.messageSevice.getMessage("list-products-detail")), HttpStatus.NOT_FOUND);
		}
		if(!productUpdateDTO.isQuickUpdate()) {
			List<ProductDetail> productDetails =  new ArrayList<>();
			List<Integer> productPhotoIdsDelete =  new ArrayList<>();
			this.checkColorDuplicate(productUpdateDTO.getProductDetailDTOs());
			for (ProductDetailDTO productDetailDTO : productUpdateDTO.getProductDetailDTOs()) {
				if(productDetailDTO.getAction().equals(ProductEnum.ProductAction.NEW.getCode())) {
					ProductDetail productDetail = this.productMapper.findProductDetailById(productDetailDTO, true);
					ProductColor productColor = this.productMapper.productColorDTOToProductColor(productDetailDTO.getProductColor());
					productDetail.setProduct(product);
					productDetail.setProductColor(productColor);
					List<ProductSubDetail> productSubDetails =  new ArrayList<>();
					List<ProductSizeDTO> productSizeDTOs = productDetailDTO.getProductSubDetails().stream().map(ProductSubDetailDTO::getProductSize).collect(Collectors.toList());
					this.checkDuplicateSizeInProductSubDetail(productSizeDTOs);
					for (ProductSubDetailDTO productSubDetailDTO : productDetailDTO.getProductSubDetails()) {
						ProductSubDetail productSubDetail = this.productMapper.productSubDetailDTOToProductSubDetail(productDetail, productSubDetailDTO, true);
						productSubDetails.add(productSubDetail);
					}
					List<ProductPhoto> productPhotosNewList = new ArrayList<>();
					for (ProductPhotoDTO productPhotoDTO : productDetailDTO.getProductPhotos()) {
						ProductPhoto productPhoto = this.productMapper.productPhotoDTOToProductPhoto(productDetail, productPhotoDTO, true);
						productPhotosNewList.add(productPhoto);
					}
					productDetail.setProductPhotos(productPhotosNewList);
					productDetail.setProductSubDetails(productSubDetails);
					
					//generate keypoint
					ProductPhotoDTO clothPhoto = productDetailDTO.getProductPhotos().get(0);
					ClothingResponse clothingResponse = this.aiService.generateKeyPointForShopProduct(clothPhoto.getId(), clothPhoto.getUrlOriginal(), shopId);
					if (!clothingResponse.getData().isEmpty()) {
						productDetail.setKeyPoint(clothingResponse.getData().get(0).getKeyPoint());
					}
					productDetail.setKeyPointMessage(clothingResponse.getMessage());
					productDetails.add(productDetail);
					
				} else if(productDetailDTO.getAction().equals(ProductEnum.ProductAction.EDIT.getCode())) {
					if (productDetailDTO.getId() == null || productDetailDTO.getId() == 0) {
						throw new BadRequestException(BadRequestException.ERROR_PRODUCT_DETAIL_BAD_REQUEST,
								String.format(this.messageSevice.getMessage("error.bad-value-msg"),
										this.messageSevice.getMessage("product-detail-ID")), false);
					}
					ProductDetail productDetail = this.productMapper.findProductDetailById(productDetailDTO, false);
					if (productDetailDTO.getProductColor() == null || productDetailDTO.getProductColor().getId() == null) {
						productDetail.setProductColor(productDetail.getProductColor());
					} else {
						ProductColor productColor = this.productMapper.productColorDTOToProductColor(productDetailDTO.getProductColor());
						productDetail.setProductColor(productColor);
					}
					productDetail.setProduct(product);
					List<ProductSubDetail> productSubDetails =  new ArrayList<>();
					List<ProductSizeDTO> productSizeDTOs = productDetailDTO.getProductSubDetails().stream().map(ProductSubDetailDTO::getProductSize).collect(Collectors.toList());
					this.checkDuplicateSizeInProductSubDetail(productSizeDTOs);
					List<ProductSubDetail> savedProductSubDetails = this.productSubDetailRepository.findAllByProductDetail_Id(productDetailDTO.getId());
					if(!savedProductSubDetails.isEmpty()) {
						this.productSubDetailRepository.deleteAll(savedProductSubDetails);
					}
					for (ProductSubDetailDTO productSubDetailDTO : productDetailDTO.getProductSubDetails()) {
						ProductSubDetail productSubDetail = this.productMapper.productSubDetailDTOToProductSubDetail(productDetail, productSubDetailDTO, true);
						productSubDetails.add(productSubDetail);
					}
					productDetail.setProductSubDetails(productSubDetails);

					List<ProductPhoto> productPhotosList = new ArrayList<>();

					for (ProductPhotoDTO productPhotoDTO : productDetailDTO.getProductPhotos()) {
						if (productPhotoDTO.getAction().equals(ProductEnum.ProductAction.NEW.getCode())) {
							ProductPhoto productPhoto = this.productMapper.productPhotoDTOToProductPhoto(productDetail, productPhotoDTO, true);
							productPhotosList.add(productPhoto);

						} else if (productPhotoDTO.getAction().equals(ProductEnum.ProductAction.EDIT.getCode())) {
							if (productPhotoDTO.getId() == null || productPhotoDTO.getId() == 0) {
								throw new BadRequestException(BadRequestException.ERROR_PRODUCT_COLOR_BAD_REQUEST,
										String.format(this.messageSevice.getMessage("error.bad-value-msg"),
												this.messageSevice.getMessage("product-photo-ID")), false);
							}
							ProductPhoto productPhoto = this.productMapper.productPhotoDTOToProductPhoto(productDetail, productPhotoDTO, false);
							productPhotosList.add(productPhoto);
							
						} else if (productPhotoDTO.getAction().equals(ProductEnum.ProductAction.DELETE.getCode())) {
							if (productPhotoDTO.getId() == null || productPhotoDTO.getId() == 0) {
								throw new BadRequestException(BadRequestException.ERROR_PRODUCT_COLOR_BAD_REQUEST,
										String.format(this.messageSevice.getMessage("error.bad-value-msg"),
												this.messageSevice.getMessage("product-photo-ID")), false);
							}
							productPhotoIdsDelete.add(productPhotoDTO.getId());
						} else {
							throw new NotFoundException(NotFoundException.ERROR_PRODUCT_COLOR_ACTION_TYPE_NOT_FOUND,
									String.format(this.messageSevice.getMessage("error.not-found-msg"),
											this.messageSevice.getMessage("product-photo-action") + productPhotoDTO.getAction()));
						}
					}
					
					//generate keypoint
					if (productDetail.getProductPhotos().get(0).getId() != productPhotosList.get(0).getId()) {
						ProductPhoto clothPhoto = productPhotosList.get(0);
						ClothingResponse clothingResponse = this.aiService.generateKeyPointForShopProduct(clothPhoto.getId(), clothPhoto.getUrlOriginal(), shopId);
						if (!clothingResponse.getData().isEmpty()) {
							productDetail.setKeyPoint(clothingResponse.getData().get(0).getKeyPoint());
						}
						productDetail.setKeyPointMessage(clothingResponse.getMessage());
						this.productService.deleteShopProductTriedOn(productDetail.getId());
					}
					
					productDetail.setProductPhotos(productPhotosList);
					productDetails.add(productDetail);
				} else {
					throw new NotFoundException(NotFoundException.ERROR_PRODUCT_DETAIL_NOT_FOUND,
							String.format(this.messageSevice.getMessage("error.not-found-msg"),
									 this.messageSevice.getMessage("product-detail-action") + productDetailDTO.getAction()), HttpStatus.NOT_FOUND);
				}
			}
			//update mixmatch 
			this.productMixMatchPhotoRepository.updateByProduct_Id(productId, productDetails.get(0).getProductPhotos().get(0).getUrlOriginal());
			
			if (!productPhotoIdsDelete.isEmpty()) {
				this.productPhotoRepository.deleteProductPhotoWithIds(productPhotoIdsDelete);
			}
			this.productDetailRepository.saveAll(productDetails);
		}

		this.productRepository.save(product);
		
		return product;
	}

	/**
	 * Delete product detail when updating product
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param productDetailId {@link ProductDetail}
	 * @return {@link ProductDetailDTO} deleted in case delete successfully
	 * @throws NotFoundException
	 * @throws SocketTimeoutException 
	 * @throws SmartFashionException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public ProductDetailDTO deleteProductDetail(Integer shopId, Integer productId, Integer productDetailId) throws NotFoundException, SocketTimeoutException, SmartFashionException {
		this.checkShopCanPostProduct(shopId);

    	this.productMapper.checkProduct(productId, shopId);
    	Optional<ProductDetail> productDetail = this.productDetailRepository.findById(productDetailId);
    	if(!productDetail.isPresent()) {
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_DETAIL_NOT_FOUND, String.format(this.messageSevice.getMessage("error.not-found-msg"),
					this.messageSevice.getMessage("product-detail-ID")), HttpStatus.NOT_FOUND);
		}
    	ProductDetailDTO productDetailDTO = new ProductDetailDTO(productDetail.get());
		List<ProductSubDetail> productSubDetails = this.productSubDetailRepository.findAllByProductDetail_Id(productDetailId);
		List<ProductPhoto> productPhotos = this.productPhotoRepository.findAllByProductDetail_Id(productDetailId);
		this.productSubDetailRepository.deleteAll(productSubDetails);
		this.productPhotoRepository.deleteAll(productPhotos);
		this.productDetailRepository.delete(productDetail.get());
		List<ProductDetail> productDetails = this.productDetailRepository.findAllByProduct_Id(productId);
		if (!productDetails.isEmpty()) {
			ProductDetail productDetail2 =  productDetails.get(0);
			this.productMixMatchPhotoRepository.updateByProduct_Id(productId,productDetail2.getProductPhotos().get(0).getUrlOriginal());
			if (StringUtils.isEmpty(productDetail2.getKeyPoint())) {
				ClothingResponse clothingResponse = this.aiService.generateKeyPointForShopProduct(productDetail2.getProductPhotos().get(0).getId()
						, productDetail2.getProductPhotos().get(0).getUrlOriginal()
						, shopId);
				if (!clothingResponse.getData().isEmpty()) {
					productDetail2.setKeyPoint(clothingResponse.getData().get(0).getKeyPoint());
				}
				productDetail2.setKeyPointMessage(clothingResponse.getMessage());
				this.productDetailRepository.save(productDetail2);
				this.productService.deleteShopProductTriedOn(productDetailId);
			}
		}

    	return productDetailDTO;
	}

	/**
	 * Get product list
	 * 
	 * @param productFilter {@link ProductFilter} 
	 * 
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public ProductDTO getProductDetail(Integer shopId, Integer productId) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);
		ProductDTO productDTO = this.productRepository.findByIdAndShop_IdAndStatus(
				productId, shopId, ProductEnum.ProductStatus.ACTIVE.getCode())
				.map(ProductDTO::new).orElseThrow(() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND,
					String.format(this.messageSevice.getMessage("error.not-found-msg"),
							this.messageSevice.getMessage("info.product")), HttpStatus.NOT_FOUND));
		List<ProductRatingCount> list = this.productRatingCountRepository.findAllByProduct_Id(productDTO.getId());

		// Set productTypeParent's name to product
		String productTypeParentName = this.productService.getProductTypeParentName(productDTO.getProductType().getProductTypeParentId());
		productDTO.getProductType().setProductTypeParentName(productTypeParentName);

		ProductRatingVO productRatingVO = this.productService.setDataForProductRatingVO(list);
		productDTO.setProductRating(productRatingVO);
		
		//get product mixmatch
		List<ProductMixMatchDTO> productMixMatchDTOs = this.productService.getProductMixMatchs(productDTO.getId());
		productDTO.setProductMixMatchs(productMixMatchDTOs);
		
		return productDTO;
	}

	/**
	 * Set highlight for product's information
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param highlight {@link Product}
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	public ProductDTO setHighlightForProductInfo(Integer shopId, Integer productId, Boolean highlight) throws NotFoundException, SmartFashionException {
		this.checkShopCanPostProduct(shopId);
		Product product = this.getProductWithShop(productId, shopId);
		product.setHighlight(highlight);
		this.productRepository.save(product);
		return new ProductDTO(product);
	}

	/**
	 * Get properties for creating product
	 *
	 * @return APIResponse {@link APIResponse}
	 * @throws NotFoundException
	 */
	public ProductProperties getPropertiesForProduct() throws NotFoundException {
		// Get all collection
		List<ProductCollectionDTO> productCollections = this.productCollectionRepository.findAll().stream().map(ProductCollectionDTO::new)
				.collect(Collectors.toList());

		// Get all product types
		List<ProductTypeDTO> productTypeDTOs;
		List<ProductTypeWithCollection> productTypeWithCollections = new ArrayList<>();
		for (ProductCollectionDTO productCollectionDTO : productCollections) {
			productTypeDTOs = this.productTypeService.getProductTypeByCollectionId(productCollectionDTO.getId());
			productTypeWithCollections.add(new ProductTypeWithCollection(productCollectionDTO.getId(), productTypeDTOs));
		}

		// Get all product's wearingPurposes
		List<ProductWearingPurposeDTO> productWearingPurposeDTOs;
		List<ProductWearingPurposeWithType> productWearingPurposeWithTypes = new ArrayList<>();
		for(ProductEnum.SizeType type : ProductEnum.SizeType.values()) {
			productWearingPurposeDTOs = this.productWearingPurposeRepository.findAllByType(type.getCode()).stream()
					.map(ProductWearingPurposeDTO::new).collect(Collectors.toList());
			productWearingPurposeWithTypes.add(new ProductWearingPurposeWithType(type.getCode(), productWearingPurposeDTOs));
		}

		// Get product's size for ADULT and KID
		List<ProductSizeWithCollection> productSizeWithCollections = new ArrayList<>();
		List<ProductProperty> productSizeProperties;
		for(ProductEnum.SizeType type : ProductEnum.SizeType.values()) {
			productSizeProperties = this.productSizeService.getProductSizeWithCollectionList(type.getCode());
			productSizeWithCollections.add(new ProductSizeWithCollection(type.getCode(), productSizeProperties));
		}

		// Get currency for product
		List<Currency> currencies = new ArrayList<>();
		for (ProductEnum.Currency currency : ProductEnum.Currency.values()) {
			currencies.add(new Currency(currency.getCode(), currency.getDisplay()));
		}
				
		// Get List of Product's Colors
		List<ProductColor> productColors = this.getProductColors();
		
		// Get List of Product's size chart
		List<ProductSizeChart> productSizeCharts = this.getProductSizeCharts();
		ProductProperties productPropertiesDTO = new ProductProperties(productCollections, productTypeWithCollections,
				productWearingPurposeWithTypes, productSizeWithCollections, currencies, productColors, productSizeCharts);
		
		return productPropertiesDTO;
	}

	/**
	 * create Product
	 * case 1: save data to new product
	 * case 2: save data to draft product
	 *
	 * @param productDTO {@link ProductDTO}
	 * @param shopId {@link Shop}
	 * @return APIResponse {@link APIResponse} data is {@link Product}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 * @throws BadRequestException 
	 */
	public ProductDTO createProduct(ProductDTO productDTO, Integer shopId) throws NotFoundException, ConflictException, SmartFashionException, BadRequestException {
		Shop shop = this.checkShopCanPostProduct(shopId);
		Product draftProduct = this.checkDraftProduct(shopId);
		Product product;

		// Set data to product if don't have draft product before
		if(draftProduct == null) {
			product = this.productMapper.setProductData(shop, productDTO, new Product(new Shop(shopId), ProductEnum.ProductStatus.DRAFT.getCode()));
		}
		// Set data to draft product
		else
		{
			product = this.productMapper.setProductData(shop, productDTO, draftProduct);
		}
		this.productRepository.save(product);
		
		return new ProductDTO(product);
	}

	/**
	 * Get draft product
	 *
	 * @param shopId {@link Shop}
	 * @return APIResponse {@link APIResponse} data is {@link ProductDTO}
	 * @throws NotFoundException
	 */
	public ProductDTO getDraftProduct(Integer shopId) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);

		Product product = this.checkDraftProduct(shopId);
		ProductDTO productDTO;
		if(product != null) {
			productDTO = new ProductDTO(product);
		} else {
			productDTO = new ProductDTO();
		}
		
		return productDTO;
	}

	/**
	 * Create new product's details
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param productDetails {@link ProductDetail}
	 * @return APIResponse {link {@link APIResponse} data is productDTO {@link ProductDTO}
	 * @throws NotFoundException
	 * @throws SocketTimeoutException 
	 * @throws SmartFashionException 
	 */
	@CacheEvict(value = ShopService.SHOP_MENU_BY_SHOP_ID_CACHE, allEntries = true)
	@Transactional(rollbackFor = Exception.class)
	public List<ProductDetail> createProductDetails(Integer shopId, Integer productId, List<ProductDetailDTO> productDetailDTOs) throws NotFoundException, ConflictException, SocketTimeoutException, SmartFashionException {
		this.checkShopCanPostProduct(shopId);

		// Check draft product
		Product draftProduct = this.checkDraftProduct(shopId);

		List<ProductDetail> productDetails = new ArrayList<>();
		if (draftProduct != null && draftProduct.getId().equals(productId)) {

			 // Set status draft product to PENDING after create product's details
			 draftProduct.setStatus(ProductEnum.ProductStatus.PENDING.getCode());

			 // check Color Duplicate
			this.checkColorDuplicate(productDetailDTOs);

			for (ProductDetailDTO productDetailDTO : productDetailDTOs) {

				// Save product's detail
				ProductDetail productDetail = new ProductDetail();
				productDetail.setProduct(draftProduct);
				ProductColor productColor = this.productMapper.productColorDTOToProductColor(productDetailDTO.getProductColor());
				productDetail.setProductColor(productColor);

				// Check ProductSize if it duplicates
				List<ProductSizeDTO> productSizeDTOs = productDetailDTO.getProductSubDetails()
						.stream().map(ProductSubDetailDTO::getProductSize).collect(Collectors.toList());
				this.checkDuplicateSizeInProductSubDetail(productSizeDTOs);

				// Save color with each product's sub detail
				for (ProductSubDetailDTO productSubDetailDTO : productDetailDTO.getProductSubDetails()) {
					ProductSubDetail productSubDetail = new ProductSubDetail();
					productSubDetail.setProductDetail(productDetail);
					ProductSize productSize = this.productMapper.findProductSizeById(productSubDetailDTO.getProductSize().getId());
					productSubDetail.setProductSize(productSize);
					productSubDetail.setTotal(productSubDetailDTO.getTotal());
					productDetail.getProductSubDetails().add(productSubDetail);
				}

				// Save product's photos with product detail
				for (ProductPhotoDTO productPhotoDTO : productDetailDTO.getProductPhotos()) {
					ProductPhoto productPhoto = this.productMapper.productPhotoDTOToProductPhoto(productDetail, productPhotoDTO, true);
					productDetail.getProductPhotos().add(productPhoto);
				}
				
				//generate keypoint
				ProductPhotoDTO clothPhoto = productDetailDTO.getProductPhotos().get(0);
				ClothingResponse clothingResponse = this.aiService.generateKeyPointForShopProduct(clothPhoto.getId(), clothPhoto.getUrlOriginal(), shopId);
				if (!clothingResponse.getData().isEmpty()) {
					productDetail.setKeyPoint(clothingResponse.getData().get(0).getKeyPoint());
					draftProduct.setStatus(ProductEnum.ProductStatus.ACTIVE.getCode());
				}
				productDetail.setKeyPointMessage(clothingResponse.getMessage());
				productDetails.add(productDetail);
			}
			this.productDetailRepository.saveAll(productDetails);

			List<ProductTypeCount> productTypeCounts = this.productService
					.setProductTypeCountForNewProduct(draftProduct, shopId);

			// Save all productTypeCount above
			this.productTypeCountRepository.saveAll(productTypeCounts);

		} else {
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(this.messageSevice.getMessage("error.not-found-msg"),
					this.messageSevice.getMessage("draft-product"), HttpStatus.NOT_FOUND));
		}
		Product product = this.productRepository.findById(productId).orElse(null);
		return product.getProductDetails();
	}

	/**
	 * Get {@link ProductRating} reviews
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link ProductCategory}
	 * @param page page number
	 * @param limit Limit on per page
	 * @return APIResponse {@link APIResponse} 
	 * @throws NotFoundException
	 */
	public PageInfo<ProductRatingDTO> getProductReviews(Integer shopId, Integer productId, Integer page, Integer limit) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);
		Sort sort = Sort.by(Sort.Order.desc("created"));
		this.getProductWithShop(productId, shopId);
		PageRequest pageRequest = this.buildPageRequest(page, limit, sort);
		Page<ProductRatingDTO> data = this.productRatingRepository.findAllByProduct_IdAndStatus(
				productId, ProductEnum.ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductRatingDTO::new);
		PageInfo<ProductRatingDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
		return pageInfo;
	}
	
	/**
	 * answwer {@link ProductRating} reviews
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link ProductCategory}
	 * @param productRatingReplyDTO {@link ProductRatingReplyDTO}
	 * @return APIResponse {@link APIResponse} 
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	public ProductRatingReplyDTO answerProductReviews(Integer shopId, Integer productId,
			ProductRatingReplyDTO productRatingReplyDTO) throws NotFoundException, SmartFashionException {
		Shop shop = this.checkShopCanPostProduct(shopId);
		ProductRating productRating = this.getProductRating(productRatingReplyDTO.getProductRatingId(), productId);

		ProductRatingReply productRatingReply = new ProductRatingReply();
		productRatingReply.setShop(shop);
		productRatingReply.setUser(this.getCurrentUserLogged());
		productRatingReply.setComment(productRatingReplyDTO.getComment());
		productRatingReply.setProductRating(productRating);
		this.productRatingReplyRepository.save(productRatingReply);
		productRatingReplyDTO = new ProductRatingReplyDTO(productRatingReply);
		
		return productRatingReplyDTO;
	}
	
	/**
	 * Get {@link ProductRating}
	 *
	 * @param productRatingId {@link ProductRating} id
	 * @return {@link ProductRating} 
	 * @throws NotFoundException
	 */
	private ProductRating getProductRating(Integer productId, Integer productRatingId) throws NotFoundException {
		return this.productRatingRepository.findByIdAndProduct_IdAndStatus(productId, productRatingId,
			ProductEnum.ProductStatus.ACTIVE.getCode()).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_NOT_FOUND,
					String.format(this.messageSevice.getMessage("error.not-found-msg"),
							this.messageSevice.getMessage("product-rating")), HttpStatus.NOT_FOUND));
	}

	/**
	 * Get list of Product's information by search saved product
	 *
	 * @param shopId {@link Shop}
	 * @param productName {@link Product}
	 * @param page page number
	 * @param limit limit record in a page
	 * @return APIResponse {@link APIResponse} data is list of CategorySearchInformation {@link ProductSearchInfo}
	 * @throws NotFoundException
	 */
	public PageInfo<ProductSearchInfo> getProductListBySearch(Integer shopId, String name, Integer page, Integer limit) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);

		PageRequest pageRequest = this.buildPageRequest(page, limit);
		name = StringUtils.isEmpty(name) ? StringUtils.EMPTY : name;

		// Get products's information from searched productName
		Page<ProductSearchInfo> productSearchInfos = this.productRepository
				.findAllByNameOrProductCodeAndShop_IdAndStatus(name, shopId, ProductEnum.ProductStatus.ACTIVE.getCode(), pageRequest)
				.map(ProductSearchInfo::new);
		PageInfo<ProductSearchInfo> pageInfo = SmartFashionUtils.pagingResponse(productSearchInfos);
		return pageInfo;
	}

	/**
	 * Get Product's detail for creating new product quickly
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @return APIResponse {@link APIResponse} data is {@link ProductDTO}
	 * @throws NotFoundException
	 */
	public ProductDTO getProductDetailForCreatingNew(Integer shopId, Integer productId) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);

		// Get product in current Shop
		Product product = this.getProductWithShop(productId, shopId);
		ProductDTO productDTO = new ProductDTO(product);

		return productDTO;
	}

	/**
	 * Delete products by set status to DELETED
	 *
	 * @param shopId {@link Shop}
	 * @param productIdList list of product ID {@link Product}
	 * @return Object null in case delete products
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 * @throws ConflictException
	 */
	@CacheEvict(value = ShopService.SHOP_MENU_BY_SHOP_ID_CACHE, allEntries = true)
	@Transactional(rollbackFor = Exception.class)
	public Object deleteProductsList(Integer shopId, List<Integer> productIdList) throws NotFoundException, BadRequestException, SmartFashionException {
		this.checkShopCanPostProduct(shopId);

		List<Product> deletedProducts = new ArrayList<>();
		List<ProductTypeCount> productTypeCounts = new ArrayList<>();

		if(productIdList == null) {
			throw new BadRequestException(BadRequestException.ERROR_EMPTY_PRODUCT_ID_LIST, String.format(this.messageSevice.getMessage("error.bad-value-msg"),
					this.messageSevice.getMessage("list-products-ID")), true);
		}

		// Check product valuable and set product type count again
		for(Integer productId : productIdList) {

			// Check product in current shop, exits or not
			Product product = this.getProductWithShop(productId, shopId);

			// Set status to DELETED in case delete product
			product.setStatus(ProductEnum.ProductStatus.DELETED.toString());
			deletedProducts.add(product);

			ProductType productType = this.getExitedProductType(product.getProductType().getId());
			if(productType != null) {
				if(productType.getProductTypeParentId() != null) {
					ProductTypeCount productTypeCountChild = this.productTypeCountRepository
							.findByProductType_IdAndShop_Id(productType.getId(), shopId).orElse(null);
					productTypeCountChild.setCount(productTypeCountChild.getCount() - 1);
					productTypeCounts.add(productTypeCountChild);

					ProductTypeCount productTypeCountParent = this.productTypeCountRepository
							.findByProductType_IdAndShop_Id(productType.getProductTypeParentId(), shopId).orElse(null);
					productTypeCountParent.setCount(productTypeCountParent.getCount() - 1);
					productTypeCounts.add(productTypeCountParent);
				} else {
					ProductTypeCount productTypeCountParent = this.productTypeCountRepository
							.findByProductType_IdAndShop_Id(productType.getId(), shopId).orElse(null);
					productTypeCountParent.setCount(productTypeCountParent.getCount() - 1);
					productTypeCounts.add(productTypeCountParent);
				}
			}
		}
		
		this.productTypeCountRepository.saveAll(productTypeCounts);
		this.productRepository.saveAll(deletedProducts);
		
		List<MixMatch> mixMatches = this.mixMatchRepository.findAllByProductId(productIdList);
		this.mixMatchRepository.deleteAll(mixMatches);

		// Get product mix-match of child product in mix-match group by list of products's ID
		List<ProductMixMatch> productMixMatches = this.productMixMatchRepository.findAllByProductIdsList(productIdList);
		List<Integer> mixMatchIds = new ArrayList<>();
		if(!productMixMatches.isEmpty()) {
			mixMatchIds = productMixMatches.parallelStream().map(productMixMatch -> productMixMatch.getMixMatch().getId()).collect(Collectors.toList());
		}

		// delete with child mix match
		this.shopMixMatchService.deleteProductMixMatchByProductId(productIdList);

		// If list of mix-match found by get product mix-match, then check to delete mix-match doesn't have product mix-match
		if(!mixMatchIds.isEmpty()) {
			// get list of mix-match again after deleted product mix-match
			List<MixMatch> mixMatchList = this.mixMatchRepository.findAllById(mixMatchIds);
			List<MixMatch> deletedMixMatches = new ArrayList<>();

			// check mix-match doesn't have any product mix-match
			for(MixMatch mixMatch : mixMatchList) {
				if(mixMatch.getProductMixMatches().size() == 1 && mixMatch.getProductMixMatches().get(0).getPriority().equals(1)) {
					deletedMixMatches.add(mixMatch);
				}
			}
			if(!deletedMixMatches.isEmpty()) {
				this.mixMatchRepository.deleteAll(deletedMixMatches);
			}
		}

		return productIdList;
	}
	
	/**
	 * Validation {@link Product} name
	 * @param errors {@link Errors}
	 * @param shopId {@link Shop} Id
	 * @param productId {@link Product} Id
	 * @param productName {@link Product} name
	 */
	public void validationProductName(Errors errors, Integer shopId, Integer productId, String productName) {
		Product exitedProduct = this.productMapper.checkProductName(shopId, productName);

		// Check in case create new product
		if(productId == null && exitedProduct != null) {
			String messageRes = String.format(this.messageSevice.getMessage("error.value-already-use"), this.messageSevice.getMessage("info.product.name"));
			errors.rejectValue("name", "name", messageRes);
		}

		// Check in case update product
		else if (exitedProduct != null && !productId.equals(exitedProduct.getId())) {
			String messageRes = String.format(this.messageSevice.getMessage("error.value-already-use"), this.messageSevice.getMessage("info.product.name"));
			errors.rejectValue("name", "name", messageRes);
		}
	}
	
	/**
	 * Upload image cropped for product
	 * @param shopId {@link Shop} Id
	 * @param productId {@link Product} Id
	 * @param productName {@link Product} name
	 * @param imageWrapperDTO {@link ImageWrapperDTO}
	 * @throws NotFoundException 
	 */
	public ImageWrapperDTO cropProductImage(Integer shopId, Integer productId, ImageWrapperDTO imageWrapperDTO) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);

		// Get product in current Shop
		Product product = this.productRepository.findByIdAndShop_Id(productId, shopId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(this.messageSevice.getMessage("error.not-found-msg"),
						this.messageSevice.getMessage("info.product"))));
		product.setUrl(imageWrapperDTO.getPhoto());
		this.productRepository.save(product);
		
		return imageWrapperDTO;
	}

	/**
	 * Check duplicate product's name
	 *
	 * @param shopId {@link Shop}
	 * @param productName {@link Product}
	 * @param productId {@link Product}
	 * @return result {true, false}
	 * @throws NotFoundException
	 */
	public Boolean checkDuplicateProductName(Integer shopId, Integer productId, String productName) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);

		// Get product by shopId and productName;
		Product exitedProduct = this.productMapper.checkProductName(shopId, productName);

		// Check in case create new product
		if(productId == null) {
			if (exitedProduct != null) {
				return true;
			} else {
				return false;
			}
		}
		// Check in case update product
		else {
			if (exitedProduct != null && !productId.equals(exitedProduct.getId())) {
				return true;
			} else if (exitedProduct != null && productId.equals(exitedProduct.getId())) {
				return false;
			} else {
				return false;
			}
		}
	}
}
