package bap.jp.smartfashion.api.shop.mixmatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import java.util.stream.Collectors;
import bap.jp.smartfashion.common.dto.MixMatchDTO;
import bap.jp.smartfashion.common.dto.ProductImageListDTO;
import bap.jp.smartfashion.common.dto.ProductMixMatchDTO;
import bap.jp.smartfashion.common.model.MixMatch;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductMixMatch;
import bap.jp.smartfashion.common.model.ProductMixMatchPhoto;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.repository.MixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductMixMatchPhotoRepository;
import bap.jp.smartfashion.common.repository.ProductMixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.enums.ProductEnum.ProductAction;
import bap.jp.smartfashion.enums.ProductEnum.ProductStatus;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;

/**
 * Service class for managing mix match.
 */
@Service
public class ShopMixMatchService extends BaseService {

	public static final String SHOP_MENU_BY_SHOP_ID_CACHE = "shopMenuByShopId";
	public static final String SHOP_FILTER_MENU_CACHE = "shopFilterMenu";

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private MixMatchRepository mixMatchRepository;

	@Autowired
	private ProductMixMatchRepository productMixMatchRepository;

	@Autowired
	private ProductMixMatchPhotoRepository productMixMatchPhotoRepository;

	@Autowired
	private ProductTypeRepository productTypeRepository;

	@Autowired
	private MessageService messageService;

	private final Logger log = LoggerFactory.getLogger(ShopMixMatchService.class);

	/**
	 * Create mix match
	 *
	 * @param mixMatchDTO {@link MixMatchDTO}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws ConflictException 
	 * @throws SmartFashionException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public MixMatchDTO createMixMatch(MixMatchDTO mixMatchDTO, Integer shopId, Integer productCollectionId) throws NotFoundException, ConflictException, SmartFashionException {
		Shop shop = this.checkShopCanPostProduct(shopId);

		// save at mix match
		MixMatch mixMatch = new MixMatch();
		mixMatch.setName(mixMatchDTO.getName());
		mixMatch.setShop(shop);
		mixMatch.setProductCollection(this.findProductCollectionById(productCollectionId));
		MixMatch mixMatchSave = this.mixMatchRepository.save(mixMatch);
		mixMatchDTO.setId(mixMatchSave.getId());
		
		List<ProductMixMatch> productMixMatchs = new ArrayList<>();
		HashSet<Integer> uniqueProductMixMatch = new HashSet<Integer>();
		for (ProductMixMatchDTO productMixMatch : mixMatchDTO.getProductMixMatches()) {
			Product product = this.checkProductWithShop(productMixMatch.getProductId(), shopId);
			ProductMixMatch productMixMatchNew = new ProductMixMatch();
			productMixMatchNew.setProduct(product);
			productMixMatchNew.setMixMatch(mixMatchSave);
			productMixMatchNew.setPriority(productMixMatch.getPriority());
			List<ProductMixMatchPhoto> productMixMatchPhotos = new ArrayList<>();
			productMixMatch.getProductMixMatchPhotos().forEach(productMixMatchPhoto -> {
				ProductMixMatchPhoto productMixMatchPhotoNew = new ProductMixMatchPhoto();
				productMixMatchPhotoNew.setProductMixMatch(productMixMatchNew);
				productMixMatchPhotoNew.setUrl(productMixMatchPhoto.getUrl());
				productMixMatchPhotos.add(productMixMatchPhotoNew);
			});
			productMixMatchNew.setProductMixMatchPhotos(productMixMatchPhotos);
			productMixMatchs.add(productMixMatchNew);
			 if(!uniqueProductMixMatch.add(product.getId())){
				throw new ConflictException(ConflictException.ERROR_PRODUCT_MIXMATCH_EXITED,
					this.messageService.getMessage("error.duplicate-product-mix-match"));
			 }
		}
		this.productMixMatchRepository.saveAll(productMixMatchs);
		return mixMatchDTO;
	}

	/**
	 * Delete mix match
	 *
	 * @param shopId {@link Shop}
	 * @param mixMatchId {@link MixMatch}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public MixMatchDTO deleteMixMatch(Integer shopId, Integer mixMatchId) throws NotFoundException, SmartFashionException {
		this.checkShopCanPostProduct(shopId);
		Optional<MixMatch> mixMatchOptional = mixMatchRepository.findByIdAndAndShopId(mixMatchId,shopId);
		MixMatchDTO mixMatchDto = new MixMatchDTO();
		if (mixMatchOptional.isPresent()) {
			MixMatch mixMatch = mixMatchOptional.get();
			// delete in mix-match
			this.mixMatchRepository.deleteById(mixMatch.getId());
			// delete in product_match
			this.productMixMatchRepository.deleteByMixMatch_IdQuery(Arrays.asList(new Integer[] {mixMatchId}));
			// delete in product mix-match photo
			List<Integer> ids = mixMatch.getProductMixMatches().parallelStream().map(ProductMixMatch::getId).collect(Collectors.toList());
			
			if(!ids.isEmpty()) {
				this.productMixMatchPhotoRepository.deleteAllByProductMixMatch_Id(ids);
			}
			
			mixMatchDto = new MixMatchDTO(mixMatch);
		}
		return mixMatchDto;
	}

	/**
	 * Delete mix match
	 *
	 * @param shopId {@link Shop}
	 * @param mixMatchId {@link MixMatch}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	@Transactional(rollbackFor = Exception.class)
	public void deleteProductMixMatchByProductId(List<Integer> productIds) throws NotFoundException {
		this.productMixMatchRepository.deleteByProduct_IdIn(productIds);
	}
	
	/**
	 * Update mix match
	 *
	 * @param mixMatchDTO {@link MixMatchDTO}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	@Transactional(rollbackFor = Exception.class)
	public MixMatchDTO updateMixMatch(MixMatchDTO mixMatchDTO, Integer shopId, Integer mixMatchId) throws NotFoundException, SmartFashionException {
		this.checkShopCanPostProduct(shopId);

		// update mix match
		MixMatch mixMatch = this.checkMixMatchByShopId(mixMatchId, shopId);
		if (mixMatch != null) {
			// up mix match
			mixMatch.setName(mixMatchDTO.getName());
			mixMatch = this.mixMatchRepository.save(mixMatch);
			
			List<ProductMixMatch> productMixMatchAdds = new ArrayList<>();
			List<Integer> productMixMatchDels = new ArrayList<>();
			
			for (ProductMixMatchDTO productMixMatch : mixMatchDTO.getProductMixMatches()) {
				if(productMixMatch.getAction().equals(ProductAction.NEW.getCode())) {
					ProductMixMatch productMixMatchNew = new ProductMixMatch();
					productMixMatchNew.setMixMatch(mixMatch);
					Product product = this.getProductWithShop(productMixMatch.getProductId(), shopId);
					productMixMatchNew.setProduct(product);
					productMixMatchNew.setPriority(productMixMatch.getPriority());
					List<ProductMixMatchPhoto> productMixMatchPhotos = new ArrayList<>();
					productMixMatch.getProductMixMatchPhotos().forEach(productMixMatchPhoto -> {
						ProductMixMatchPhoto productMixMatchPhotoNew = new ProductMixMatchPhoto();
						productMixMatchPhotoNew.setProductMixMatch(productMixMatchNew);
						productMixMatchPhotoNew.setUrl(productMixMatchPhoto.getUrl());
						productMixMatchPhotos.add(productMixMatchPhotoNew);
					});
					productMixMatchNew.setProductMixMatchPhotos(productMixMatchPhotos);
					productMixMatchAdds.add(productMixMatchNew);
				} else if(productMixMatch.getAction().equals(ProductAction.EDIT.getCode())) {
					this.productMixMatchRepository.findById(productMixMatch.getId()).ifPresent(productMixMatchEdit -> {
						productMixMatchEdit.setPriority(productMixMatch.getPriority());
						productMixMatchAdds.add(productMixMatchEdit);
					});
				} else if(productMixMatch.getAction().equals(ProductAction.DELETE.getCode())) {
					productMixMatchDels.add(productMixMatch.getId());
				}
			}
			
			this.productMixMatchRepository.saveAll(productMixMatchAdds);
			if(!productMixMatchDels.isEmpty()) {
				this.productMixMatchRepository.deleteByMixMatch_IdQuery(productMixMatchDels);
				this.productMixMatchPhotoRepository.deleteAllByProductMixMatch_Id(productMixMatchDels);
			}
			mixMatchDTO.setId(mixMatchId);
		}
		
		return mixMatchDTO;
	}

	/**
	 * Get list mix match
	 *
	 * @param shopId {@link Shop}
	 * @param collectionId {@link ProductCollection}
	 * @param page
	 * @param limit
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	public PageInfo<MixMatchDTO> getMixMatchList(int shopId, int collectionId, Integer page, Integer limit, String query) throws NotFoundException {
		this.checkShopUserLogged(shopId);

		PageRequest pageRequest = this.buildPageRequest(page, limit);
		query = StringUtils.defaultString(query);
		Page<MixMatchDTO> mixMatches = this.mixMatchRepository.findAllByShop_IdAndProductCollection_Id(shopId,collectionId, query, pageRequest).map(MixMatchDTO::new);
		PageInfo<MixMatchDTO> pageInfo = SmartFashionUtils.pagingResponse(mixMatches);
		
		return pageInfo;

	}

	/**
	 * Get mix match by mix match id
	 *
	 * @param mixMatchId {@link MixMatch}
	 * @param shopId {@link Shop}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 */
	public MixMatchDTO getMixMatchId(Integer shopId, Integer mixMatchId) {

		Optional<MixMatch> mixMatch = mixMatchRepository.findByIdAndAndShopId(mixMatchId, shopId);
		MixMatchDTO mixMatchDTO = new MixMatchDTO();
		if (mixMatch.isPresent()) {
			mixMatchDTO = new MixMatchDTO(mixMatch.get());
		}

		return mixMatchDTO;
	}

	/**
	 * Get product Image
	 *
	 * @param shopId {@link Shop}
	 * @param collectionId {@link ProductCollection}
	 * @param productTypeId {@link ProductType}
	 * @param productCodeOrName {@link Product}
	 * @param flag = 0 find by ProductCollection_Id And ProductType_Id ,flag = 1 find By Name
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	public PageInfo<ProductImageListDTO> getProductImage(Integer shopId, Integer collectionId, Integer productTypeId, String productCodeOrName, Integer flag, Integer page, Integer limit)
			throws NotFoundException {
		PageRequest pageRequest = this.buildPageRequest(page, limit);
		if (flag == 0) {
			// check Product Type Parent
			List<ProductType> productTypeList = productTypeRepository.findAllByProductTypeParentId(productTypeId);
			List<Integer> ids = Arrays.asList(new Integer[] {productTypeId});
			if (! productTypeList.isEmpty()) {
				ids = productTypeList.parallelStream().map(ProductType::getId).collect(Collectors.toList());
			}
			Page<ProductImageListDTO> productImages;
			if (productTypeId == null) {
				productImages = productRepository.findByShop_IdAndProductCollection_IdAndStatus(shopId, collectionId,
						ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductImageListDTO::new);
			} else {
				productImages = productRepository.findByShop_IdAndProductCollection_IdAndProductType_IdAndStatus(shopId, collectionId, 
						ids, ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductImageListDTO::new);
			}
			PageInfo<ProductImageListDTO> pageInfo = SmartFashionUtils.pagingResponse(productImages);
			return pageInfo;
		} else {
			Page<ProductImageListDTO> productImages = productRepository.findProductByCondition(productCodeOrName, shopId, collectionId, 
					ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductImageListDTO::new);
			PageInfo<ProductImageListDTO> pageInfo = SmartFashionUtils.pagingResponse(productImages);
			return pageInfo;
		}
	}
}
