package bap.jp.smartfashion.api.shop;

import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import java.util.stream.Collectors;
import java.util.stream.LongStream;

import bap.jp.smartfashion.common.dto.DashboardDTO;
import bap.jp.smartfashion.common.dto.ImageWrapperDTO;
import bap.jp.smartfashion.common.dto.MixMatchDTO;
import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.dto.ProductImageListDTO;
import bap.jp.smartfashion.common.dto.ProductInteractionDTO;
import bap.jp.smartfashion.common.dto.ProductWearingPurposeDTO;
import bap.jp.smartfashion.common.dto.ProfileDTO;
import bap.jp.smartfashion.common.dto.ShopNewVistorDTO;
import bap.jp.smartfashion.common.dto.ShopProductCollectionDTO;
import bap.jp.smartfashion.common.dto.ShopVisitorInfo;
import bap.jp.smartfashion.common.model.MixMatch;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.model.ProductTypeCount;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.ShopBranch;

import bap.jp.smartfashion.common.model.ShopProductCollection;
import bap.jp.smartfashion.common.model.ShopWarehouse;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserBankAccount;
import bap.jp.smartfashion.common.repository.MixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ProductColorRepository;
import bap.jp.smartfashion.common.repository.ProductInteractionRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ProductTypeCountRepository;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;
import bap.jp.smartfashion.common.repository.ShopBranchRepository;
import bap.jp.smartfashion.common.repository.ShopProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ShopRepository;
import bap.jp.smartfashion.common.repository.ShopVisitorRepository;
import bap.jp.smartfashion.common.repository.ShopWarehouseRepository;
import bap.jp.smartfashion.common.repository.UserBankAccountRepository;
import bap.jp.smartfashion.common.repository.UserRepository;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ProductColorVO;
import bap.jp.smartfashion.common.vo.ReturningVisitor;
import bap.jp.smartfashion.common.vo.ProductProperty;
import bap.jp.smartfashion.common.vo.ShopFilterMenu;
import bap.jp.smartfashion.common.vo.ShopMenu;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.enums.ProductEnum.ProductStatus;
import bap.jp.smartfashion.enums.ShopEnum;
import bap.jp.smartfashion.enums.ShopEnum.Timeline;
import bap.jp.smartfashion.enums.ShopEnum.VisitorType;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import bap.jp.smartfashion.api.admin.product.service.ProductSizeService;
import bap.jp.smartfashion.api.admin.product.service.ProductWearingPurposeService;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;

import bap.jp.smartfashion.util.constants.APIConstants;
import org.springframework.validation.Errors;

/**
 * Service class for managing shop.
 */
@Service
public class ShopService extends BaseService {

	public static final String SHOP_MENU_BY_SHOP_ID_CACHE = "shopMenuByShopId";
	public static final String SHOP_FILTER_MENU_CACHE = "shopFilterMenu";

	@Autowired
	private ProductTypeCountRepository productTypeCountRepository;

	@Autowired
	private ProductWearingPurposeService productWearingPurposeService;

	@Autowired
	private ProductColorRepository productColorRepository;

	@Autowired
	private ProductSizeService productSizeService;

	@Autowired
	private ShopRepository shopRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserBankAccountRepository userBankAccountRepository;

	@Autowired
	private ShopBranchRepository shopBranchRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private MixMatchRepository mixMatchRepository;

	@Autowired
	private ProductCollectionRepository productCollectionRepository;

	@Autowired
	private ProductInteractionRepository productInteractionRepository;

	@Autowired
	private ShopVisitorRepository shopVisitorRepository;

	@Autowired
	private ProductTypeRepository productTypeRepository;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ShopProductCollectionRepository shopProductCollectionRepository;

	@Autowired
	private ShopWarehouseRepository shopWarehouseRepository;

	private final Logger log = LoggerFactory.getLogger(ShopService.class);

	/**
	 * Get clothing menu for shop
	 *
	 * @param shopId {@link Shop} id
	 * @param collectionId {@link ProductCollection} Id
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	@Cacheable(value = SHOP_MENU_BY_SHOP_ID_CACHE, key = "{#shopId, #collectionId}")
	public List<ShopMenu> getClothingMenu(Integer shopId, Integer collectionId) throws NotFoundException {
		ProductCollection productCollection = this.findProductCollectionById(collectionId);
		List<ShopMenu> shopMenus =new ArrayList<>();
		List<ProductTypeCount> productTypeCounts = this.productTypeCountRepository.findByShop_IdAndProductType_ProductCollection_IdOrderById(shopId, productCollection.getId());
		for (ProductTypeCount productTypeCount : productTypeCounts) {
			ShopMenu shopMenu = new ShopMenu();
			shopMenu.setId(productTypeCount.getProductType().getId());
			shopMenu.setName(productTypeCount.getProductType().getName());
			shopMenu.setCount(productTypeCount.getCount());
			List<ProductTypeCount> productTypeCountChilds = this.productTypeCountRepository.findByShop_IdAndProductType_ProductTypeParentIdOrderById(shopId, productTypeCount.getProductType().getId());
			List<ShopMenu> shopMenuChilds =new ArrayList<>();
			for (ProductTypeCount productTypeCountChild : productTypeCountChilds) {
				ShopMenu shopMenuChild = new ShopMenu();
				shopMenuChild.setId(productTypeCountChild.getProductType().getId());
				shopMenuChild.setName(productTypeCountChild.getProductType().getName());
				shopMenuChild.setCount(productTypeCountChild.getCount());
				shopMenuChilds.add(shopMenuChild);
			}
			shopMenu.setListChild(shopMenuChilds);
			shopMenus.add(shopMenu);
		}
		return shopMenus;
	}

	/**
	 * Get product color list
	 *
	 * @return {@link List} of {@link ProductColorVO}
	 * @throws NotFoundException
	 */
	public List<ProductColorVO> getProductColorList() {
		return this.productColorRepository.findAll().stream().map(ProductColorVO::new).collect(Collectors.toList());
	}

	/**
	 * Get filter menu
	 *
	 * @param collectionType {@link ProductCollection} type
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public List<ShopFilterMenu> getFilterMenu(String collectionType, Integer shopId) throws NotFoundException {
		
		List<ShopFilterMenu> shopFilterMenus = new ArrayList<>();
		// set color menu
		shopFilterMenus.add(new ShopFilterMenu("colors", this.getProductColorList()));
		// set sizes menu
		List<ProductProperty> sizeList = this.productSizeService.getProductSizeList(collectionType);
		shopFilterMenus.add(new ShopFilterMenu("sizes",sizeList));

		// set Price Range
		List<Integer> priceRange = new ArrayList<>();
		Integer priceMin = 0;
		Integer priceMax = 250;
		Optional<Product> optionalPriceMin = this.productRepository.findFirstByShop_IdAndStatusOrderByPriceAsc(shopId, ProductStatus.ACTIVE.getCode());
		if(optionalPriceMin.isPresent()) {
			priceMin = optionalPriceMin.get().getPrice().setScale(0, RoundingMode.DOWN).intValue();
		};
		Optional<Product> optionalPriceMax = this.productRepository.findFirstByShop_IdAndStatusOrderByPriceDesc(shopId, ProductStatus.ACTIVE.getCode());
		if(optionalPriceMax.isPresent()) {
			priceMax = optionalPriceMax.get().getPrice().setScale(0, RoundingMode.UP).intValue();
		};
		priceRange.add(priceMin);
		priceRange.add(priceMax);
		shopFilterMenus.add(new ShopFilterMenu("priceRange",priceRange));

		// set wearing purpose menu
		List<ProductWearingPurposeDTO> productWearingPurposeDTOs = this.productWearingPurposeService.getProductWearingPurposeList(collectionType);
		shopFilterMenus.add(new ShopFilterMenu("wearingPurposes", productWearingPurposeDTOs));

		return shopFilterMenus;
	}

	/**
	 * Update avatar shop
	 *
	 * @param shopId
	 * @param photo
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 * @throws AuthenticationException 
	 */
	public Shop updateProfilePhoto(ImageWrapperDTO photo)
			throws NotFoundException, AuthenticationException {
		Shop shop = this.checkShopBlockingLogin(this.getCurrentUserId());
		shop.setAvatar(photo.getPhoto());
		this.shopRepository.save(shop);
		return shop;
	}

	/**
	 * Get profile
	 *
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	public ProfileDTO getProfile()
			throws NotFoundException {
		ProfileDTO profile = this.getProfileCurrentUserLogged();
		return profile;
	}

	/**
	 * Update profile
	 *
	 * @param profileDto {@link ProfileDTO}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws ConflictException
	 * @throws NotFoundException
	 * @throws AuthenticationException
	 */
	public ProfileDTO updateProfileInformation(ProfileDTO profileDto)
			throws ConflictException, NotFoundException, AuthenticationException {
		// update user
		// get user info
		Integer userId= this.getCurrentUserId();
		Shop shop = this.checkShopBlockingLogin(userId);
		User userProfile = this.getUserByUserId(userId);
		if (userProfile != null) {
			if(userProfile.getUserType().equals(UserEnum.UserTypeEnum.LOCAL.toString())) {
				if(StringUtils.isEmpty(profileDto.getEmail()) || !profileDto.getEmail().equals(userProfile.getEmail())) {
					throw new ConflictException(ConflictException.ERROR_USER_TYPE_DONT_CHANGE_EMAIL,
						this.messageService.getMessage("error.change-email"));
				}
			}

			if(StringUtils.isEmpty(profileDto.getTaxNumber())) {
				userProfile.setTaxNumber(null);
			} else if (this.checkTaxNumberUnique(userId, profileDto.getTaxNumber(), userProfile.getUserType())){
				userProfile.setTaxNumber(profileDto.getTaxNumber());
			}

			if (this.checkPasspostNumberUnique(userId, profileDto.getPassportNumber(), userProfile.getUserType())) {
				userProfile.setPassportNumber(profileDto.getPassportNumber());
			}
			userProfile.setGender(profileDto.getGender());
			userProfile.setPassportBackSide(profileDto.getPassportBackSide());
			userProfile.setPassportFrontSide(profileDto.getPassportFrontSide());
			userProfile.setPhone(profileDto.getShopPhone());
			userProfile.setName(profileDto.getOwnerName());

			userRepository.save(userProfile);
		}

		// update shop
		shop.setName(profileDto.getShopName());
		// set time
		shop.setOpenTime(LocalTime.parse(profileDto.getOpenTime()));
		shop.setCloseTime(LocalTime.parse(profileDto.getCloseTime()));

		shop.setPhone(profileDto.getShopPhone());
		shop.setDescription(profileDto.getDescription());
		shopRepository.save(shop);

		// update shop branch
		if (profileDto.getShopBranchList() != null) {
			List<ShopBranch> branches = new ArrayList<>();
			branches.addAll(this.shopBranchRepository.findByShopId(shop.getId()));
			if(!branches.isEmpty()) {
				this.shopBranchRepository.deleteAll(branches);
				branches.clear();
			}
			profileDto.getShopBranchList().forEach(branchAddress -> {
				ShopBranch shopBranch = new ShopBranch();
				shopBranch.setShop(new Shop(shop.getId()));
				shopBranch.setBusinessAddress(branchAddress.getBusinessAddress());
				branches.add(shopBranch);
			});
			List<ShopBranch> branchesAddress = shopBranchRepository.saveAll(branches);
			profileDto.setShopBranchList(branchesAddress);
		}

		// Update list of warehouse's addresses for shop
		if(profileDto.getWarehouseAddressList() != null) {
			List<ShopWarehouse> shopWarehouses = new ArrayList<>();
			Optional<List<ShopWarehouse>> exitedShopWarehouses = this.shopWarehouseRepository.findAllByShop_Id(shop.getId());

			// Delete list of exited shop warehouse's addresses before update
			if(exitedShopWarehouses.isPresent()) {
				this.shopWarehouseRepository.deleteAll(exitedShopWarehouses.get());
			}
			profileDto.getWarehouseAddressList().forEach(wareHouse -> {
				ShopWarehouse shopWarehouse = new ShopWarehouse();
				shopWarehouse.setShop(new Shop(shop.getId()));
				shopWarehouse.setWarehouseAddress(wareHouse.getWarehouseAddress());
				shopWarehouses.add(shopWarehouse);
			});
			List<ShopWarehouse> shopWarehouseList = this.shopWarehouseRepository.saveAll(shopWarehouses);
			profileDto.setWarehouseAddressList(shopWarehouseList);
		}

		// update bank account
		// get bank account
		UserBankAccount userBankAccountProfile = this.getUserBankByUserId(userId);
		if(userBankAccountProfile!=null) {
			userBankAccountProfile.setBankName(profileDto.getBankName());
			userBankAccountProfile.setBankBranch(profileDto.getBankBranch());
			userBankAccountProfile.setOwnerName(profileDto.getOwnerBankName());
			userBankAccountProfile.setAccountNumber(profileDto.getAccountNumber());
			userBankAccountRepository.save(userBankAccountProfile);
		} else{
			UserBankAccount userBankAccountProf= new UserBankAccount();
			userBankAccountProf.setBankName(profileDto.getBankName());
			userBankAccountProf.setUser(userProfile);
			userBankAccountProf.setBankBranch(profileDto.getBankBranch());
			userBankAccountProf.setOwnerName(profileDto.getOwnerBankName());
			userBankAccountProf.setAccountNumber(profileDto.getAccountNumber());
			userBankAccountRepository.save(userBankAccountProf);
		}
		return profileDto;
	}

	/**
	 * Get Statistic for shop and application
	 * @param shopId {@link Shop} Id
	 * @param productCollectionId {@link ProductCollection} Id
	 * @param actionCode action code to get statistic
	 * @param top limit value
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public List<ProductInteractionDTO> getStatistic(Integer shopId, Integer productCollectionId, String actionCode, Integer top) throws NotFoundException {
		PageRequest pageRequest = this.buildPageRequest(null, top);
		Page<ProductInteractionDTO> productInteractionPage = null;
		if (shopId == null) {
			productInteractionPage = this.productInteractionRepository.findTopInteractionByProductCollectionAndAction(productCollectionId, actionCode, pageRequest).map(ProductInteractionDTO::new);
		} else {
			this.checkShopBelongUserLogged(shopId);
			productInteractionPage = this.productInteractionRepository.findTopInteractionByProductCollectionAndAction(shopId, productCollectionId, actionCode, pageRequest).map(ProductInteractionDTO::new);
		}
		return productInteractionPage.getContent();
	}


	/**
	 * Get mix match by mix match id
	 *
	 * @param mixMatchId {@link MixMatch}
	 * @param shopId {@link Shop}
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 */
	public MixMatchDTO getMixMatchId(Integer shopId, Integer mixMatchId) {

		Optional<MixMatch> mixMatch = mixMatchRepository.findByIdAndAndShopId(mixMatchId, shopId);
		MixMatchDTO mixMatchDTO = new MixMatchDTO();
		if (mixMatch.isPresent()) {
			mixMatchDTO = new MixMatchDTO(mixMatch.get());
		}

		return mixMatchDTO;
	}

	/**
	 * Get product Image
	 *
	 * @param shopId {@link Shop}
	 * @param collectionId {@link ProductCollection}
	 * @param productTypeId {@link ProductType}
	 * @param productCodeOrName {@link Product}
	 * @param flag = 0 find by ProductCollection_Id And ProductType_Id ,flag = 1 find By Name
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException
	 */
	public PageInfo<ProductImageListDTO> getProductImage(Integer shopId, Integer collectionId, Integer productTypeId, String productCodeOrName, Integer flag, Integer page, Integer limit)
			throws NotFoundException {
		PageRequest pageRequest = this.buildPageRequest(page, limit);
		if (flag == 0) {
			// check Product Type Parent
			List<ProductType> productTypeList = productTypeRepository.findAllByProductTypeParentId(productTypeId);
			List<Integer> ids = Arrays.asList(new Integer[] {productTypeId});
			if (! productTypeList.isEmpty()) {
				ids = productTypeList.parallelStream().map(ProductType::getId).collect(Collectors.toList());
			}
			Page<ProductImageListDTO> productImages;
			if (productTypeId == null) {
				productImages = productRepository.findByShop_IdAndProductCollection_IdAndStatus(shopId, collectionId,
						ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductImageListDTO::new);
			} else {
				productImages = productRepository.findByShop_IdAndProductCollection_IdAndProductType_IdAndStatus(shopId, collectionId, 
						ids, ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductImageListDTO::new);
			}
			PageInfo<ProductImageListDTO> pageInfo = SmartFashionUtils.pagingResponse(productImages);
			return pageInfo;
		} else {
			Page<ProductImageListDTO> productImages = productRepository.findProductByCondition(productCodeOrName, shopId, collectionId, 
					ProductStatus.ACTIVE.getCode(), pageRequest).map(ProductImageListDTO::new);
			PageInfo<ProductImageListDTO> pageInfo = SmartFashionUtils.pagingResponse(productImages);
			return pageInfo;
		}
	}
	
	/**
	 * Get data for dashboards
	 * @param shopId {@link Shop} ID
	 * @param topTried number to get top tried
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 */
	public DashboardDTO getDashboards(Integer shopId, Integer topTried) throws NotFoundException {
		this.checkShopUserLogged(shopId);
		List<ProductCollectionDTO> productCollectionDTOs = this.productCollectionRepository.findAll().stream().map(ProductCollectionDTO::new).collect(Collectors.toList());
		PageRequest pageRequest = this.buildPageRequest(1, topTried);
		List<ProductInteractionDTO> productInteractionDTOs = this.productInteractionRepository
				.findTopInteractionByAndAction(shopId, ProductEnum.ProductInteraction.TRY_PRODUCT.getCode(), pageRequest)
				.stream().map(ProductInteractionDTO::new).collect(Collectors.toList());
		LocalDate startDate = LocalDate.now().minusDays(6);
		LocalDate endDate = LocalDate.now();
		List<ShopNewVistorDTO> shopVisitors = this.shopVisitorRepository
				.findNewvisitorBetweenForShop(shopId, Timeline.DAY.toString(), VisitorType.NEW_VISITOR.getCode(), startDate, endDate.plusDays(1))
				.stream().map(ShopNewVistorDTO::new).collect(Collectors.toList());
		List<LocalDate> localDatesBetween = this.getDatesBetweenTwoDates(startDate, endDate);
		for (LocalDate localDate : localDatesBetween) {
			ShopNewVistorDTO shopNewVistorDTO = shopVisitors.stream().filter(sv -> localDate.isEqual(sv.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()))
					.findAny().orElse(null);
			if (shopNewVistorDTO == null) {
				shopNewVistorDTO = new ShopNewVistorDTO();
				shopNewVistorDTO.setDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
				shopNewVistorDTO.setTotal(0);
				shopNewVistorDTO.setType(VisitorType.NEW_VISITOR.getCode());
				shopVisitors.add(shopNewVistorDTO);
			}
		};
		
		shopVisitors = shopVisitors.stream().sorted(Comparator.comparing(ShopNewVistorDTO::getDate)).collect(Collectors.toList());
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardDTO.setProductCollections(productCollectionDTOs);
		dashboardDTO.setProductsInteractions(productInteractionDTOs);
		dashboardDTO.setShopVisitors(shopVisitors);
		return dashboardDTO;
	}
	
	/**
	 * Get list date between two 2 date
	 * @param startDate Start date to get
	 * @param endDate End date to stop
	 * @return List {@link LocalDate}
	 */
	private List<LocalDate> getDatesBetweenTwoDates(LocalDate startDate, LocalDate endDate) { 
		long daysBetween = ChronoUnit.DAYS.between(startDate, endDate)+1;
		List<LocalDate> totalDates = LongStream.iterate(0,i -> i+1)
				.limit(daysBetween).mapToObj(i->startDate.plusDays(i))
				.collect(Collectors.toList());
		return totalDates;
	}

	/**
	 * Get visitor's information
	 *
	 * @param shopId {@link Shop}
	 * @param type role of visitor {@link ShopEnum.VisitorType}
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @param startDay day starts
	 * @param endDay dat ends
	 * @param limit limit to count data
	 * @return {@link APIResponse} data is {@link ShopVisitorInfo}
	 * @throws NotFoundException
	 * @throws ConflictException
	 */
	public List<ShopVisitorInfo> getVisitorInformation(Integer shopId, String type, String timeline, LocalDate startDay,
			LocalDate endDay, Integer limit) throws NotFoundException, BadRequestException {
		this.checkShopBelongUserLogged(shopId);

		if(limit == null) {
			if(ShopEnum.Timeline.HOUR.toString().equalsIgnoreCase(timeline)) {
				limit = (int)(ChronoUnit.DAYS.between(startDay, endDay)+1) * 24;
			} else if (Timeline.DAY.toString().equalsIgnoreCase(timeline)){
				limit = (int)(ChronoUnit.DAYS.between(startDay, endDay)+1);
			} else if (Timeline.WEEK.toString().equalsIgnoreCase(timeline)) {
				limit = (int)ChronoUnit.WEEKS.between(startDay, endDay)+1;
			} else if (Timeline.MONTH.toString().equalsIgnoreCase(timeline)) {
				limit = (int)ChronoUnit.MONTHS.between(startDay, endDay)+1;
			} else if (Timeline.YEAR.toString().equalsIgnoreCase(timeline)) {
				limit = endDay.getYear() - startDay.getYear() + 1;
			}
		}
		PageRequest pageRequest = PageRequest.of(0, limit);
		List<ShopVisitorInfo> visitorInfos = this.getVisitorInfoByConditions(shopId, type, timeline,
				startDay, endDay, pageRequest);

		return visitorInfos;
	}

	/**
	 * Get returning visitors
	 *
	 * @param shopId {@link Shop}
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @return {@link APIResponse} data is {@link ReturningVisitor}
	 * @throws NotFoundException
	 */
	public List<ReturningVisitor> getReturningVisitors(Integer shopId, String timeline) throws NotFoundException {
		this.checkShopBelongUserLogged(shopId);

		List<ReturningVisitor> returningVisitors = this.getShopCollectionReturningVisitors(shopId, timeline);

		return returningVisitors;
	}

	/**
	 * Validation NotEmpty Branch's Address
	 *
	 * @param errors {@link Errors}
	 * @param shopBranchList list of {@link ShopBranch}
	 * @throws NotFoundException
	 */
	public void validationNotEmptyBranchAddressAndWarehouseAddress(Errors errors, List<ShopBranch> shopBranchList, List<ShopWarehouse> shopWarehouses) throws NotFoundException {

		// Check list of shop's branches
		if(!shopBranchList.isEmpty()) {
			Optional<ShopBranch> foundBranchAddress = shopBranchList.stream().filter(branchAddress -> StringUtils.isEmpty(branchAddress.getBusinessAddress())).findFirst();
			if(foundBranchAddress.isPresent()) {
				String messageRes = String.format(this.messageService.getMessage("error.empty-property"),
						this.messageService.getMessage("branches-address"));
				errors.rejectValue("shopBranchList", "shopBranchList", messageRes);
			}
		} else {
			throw new NotFoundException(NotFoundException.ERROR_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("list-shop-branch")));
		}

		// Check list of shop's warehouses
		if(!shopWarehouses.isEmpty()) {
			Optional<ShopWarehouse> foundWarehouseAddresses = shopWarehouses.stream().filter(shopWarehouse -> StringUtils.isEmpty(shopWarehouse.getWarehouseAddress())).findFirst();
			if(foundWarehouseAddresses.isPresent()) {
				String messageRes = String.format(this.messageService.getMessage("error.empty-property"),
						this.messageService.getMessage("warehouse-address"));
				errors.rejectValue("warehouseAddressList", "warehouseAddressList", messageRes);
			}
		} else {
			throw new NotFoundException(NotFoundException.ERROR_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("list-shop-warehouse")));
		}
	}

	/**
	 * Change product_collection's background image in shop
	 *
	 * @param shopId {@link Shop}
	 * @param productCollectionId {@link ProductCollection}
	 * @param imageWrapperDTO {@link ImageWrapperDTO}
	 * @return {@link ShopProductCollectionDTO}
	 * @throws NotFoundException
	 */
	public ShopProductCollectionDTO changeProductCollectionImage(Integer shopId, Integer productCollectionId,
					ImageWrapperDTO imageWrapper) throws NotFoundException, BadRequestException {
		Shop shop = this.checkShopBelongUserLogged(shopId);

		if(!ShopEnum.ShopStatus.ACTIVE.getCode().equals(shop.getStatus())) {
			throw new BadRequestException(BadRequestException.ERROR_SHOP_STATUS_BAD_REQUEST,
					this.messageService.getMessage("error.status-shop-update-product-collection-image"), false);
		}
		ShopProductCollection shopProductCollection = this.shopProductCollectionRepository
					.findByShop_IdAndProductCollection_Id(shopId, productCollectionId).orElseThrow(
						()-> new NotFoundException(NotFoundException.ERROR_SHOP_PRODUCT_COLLECTION_NOT_FOUND,
								String.format(this.messageService.getMessage("error.not-found-msg"),
										this.messageService.getMessage("info.shop.product.collection"))));
		shopProductCollection.setUrl(imageWrapper.getPhoto());
		ShopProductCollection savedShopProductCollection = this.shopProductCollectionRepository.save(shopProductCollection);

		return new ShopProductCollectionDTO(savedShopProductCollection);
	}
	
	@Caching(evict = {
			@CacheEvict(value = SHOP_MENU_BY_SHOP_ID_CACHE, allEntries = true),
			@CacheEvict(value = SHOP_FILTER_MENU_CACHE, allEntries = true)
	})
	public void clearCache() {
		log.info("Cleared cache of user");
	}
}
