package bap.jp.smartfashion.api.shop.product;

import java.net.SocketTimeoutException;
import java.util.List;

import javax.validation.Valid;

import bap.jp.smartfashion.common.dto.ImageWrapperDTO;
import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.dto.ProductDetailDTO;
import bap.jp.smartfashion.common.dto.ProductRatingDTO;
import bap.jp.smartfashion.common.dto.ProductRatingReplyDTO;
import bap.jp.smartfashion.common.dto.ProductUpdateDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.response.APIResponse;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ProductFilter;
import bap.jp.smartfashion.common.vo.ProductProperties;
import bap.jp.smartfashion.common.vo.ProductSearchInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;

import bap.jp.smartfashion.api.admin.product.service.ProductCollectionService;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Controller to managing products by shop
 */
@RestController
@RequestMapping("/api/v1/shops")
@PreAuthorize("hasAnyRole('ROLE_SHOP')")
public class ShopProductController {

	private final Logger log = LoggerFactory.getLogger(ShopProductController.class);
	
	@Autowired
	private ShopProductService shopProductService;
	
	@Autowired
	private ProductCollectionService productCollectionService;
	/**
	 * Get product list
	 * 
	 * @param shopId {@link Shop} Id
	 * @param collectionId {@link ProductCollection} Id
	 * @param query {@link Product} name
	 * @param page page number
	 * @param limit Limit on per page
	 * @param sort Sort product
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 */
	@GetMapping("/{shopId}/products")
	@ApiOperation(value = "Find product list")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<PageInfo<ProductDTO>> getProductList(
		@ApiParam(value ="Shop Id", required = true)
		@PathVariable(value = "shopId") int shopId,
		@ApiParam(value ="Product Type Id", required = false)
		@RequestParam(value = "productTypeId" ,required = false) Integer productTypeId,
		@ApiParam(value ="Collection Id", required = true)
		@RequestParam(value = "collectionId") int collectionId,
		@ApiParam(value ="Search Product by key word", required = false)
		@RequestParam(value = "query", required = false) String keyWord,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",NEW_ARRIVAL, HIGHLIGHT_PRODUCTS, TRIED_PRODUCTS, FAVORITED_PRODUCTS,"
				+ " LOWEST_TO_HIGHEST_PRICE, HIGHEST_TO_LOWEST_PRICE, SALE_PRODUCTS")
		@RequestParam(value = "sort", required = false) String sort,
		@ApiParam(value = "Sale", required = false)
		@RequestParam(value = "isSale", required = false) boolean isSale) throws NotFoundException {
		log.info("REST request to find product list");
		ProductFilter productFilter = new ProductFilter();
		productFilter.setShopId(shopId);
		productFilter.setProductTypeId(productTypeId);
		productFilter.setCollectionId(collectionId);
		productFilter.setPage(page);
		productFilter.setLimit(limit);
		productFilter.setSortBy(sort);
		productFilter.setSale(isSale);

		return APIResponse.okStatus(this.shopProductService.getProductList(keyWord, productFilter));
	}
	
	/**
	 * Get product list
	 * 
	 * @param shopId {@link Shop} Id
	 * @param collectionId {@link ProductCollection} Id
	 * @param page page number
	 * @param limit Limit on per page
	 * 
	 * @return {@link ResponseEntity} of {@link APIResponse}
	 * @throws NotFoundException 
	 */
	@GetMapping("/{shopId}/products/filter")
	@ApiOperation(value = "Get product list filter")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<PageInfo<ProductDTO>> filter(
		@ApiParam(value ="Shop Id", required = true)
		@PathVariable(value = "shopId") int shopId,
		@ApiParam(value ="Collection Id", required = true)
		@RequestParam(value = "collectionId") int collectionId,
		@ApiParam(value ="Search Product by key word", required = false)
		@RequestParam(value = "query", required = false) String keyWord,
		@ApiParam(value ="Product Type Id", required = false)
		@RequestParam(value = "productTypeId", required = false) Integer productTypeId,
		@ApiParam(value ="Page", required = false)
		@RequestParam(value = "page", required = false) Integer page,
		@ApiParam(value ="Limit", required = false)
		@RequestParam(value = "limit", required = false) Integer limit,
		@ApiParam(value ="Color list", required = false, type = "List")
		@RequestParam(value = "colors", required = false) List<Integer> colorIds,
		@ApiParam(value ="Size chart", required = false, type = "List")
		@RequestParam(value = "sizeChart", required = false) String sizeChart,
		@ApiParam(value ="Size list", required = false, type = "List")
		@RequestParam(value = "sizes", required = false) List<String> sizes,
		@ApiParam(value ="Wearing purpose list", required = false, type = "List")
		@RequestParam(value = "wearingpurposes", required = false) List<Integer> wearingpurposeIds,
		@ApiParam(value ="Min price", required = false)
		@RequestParam(value = "minPrice", required = false) Integer minPrice,
		@ApiParam(value ="Max price", required = false)
		@RequestParam(value = "maxPrice", required = false) Integer maxPrice,
		@ApiParam(value ="Sort by", required = false, allowableValues = ",NEW_ARRIVAL, HIGHLIGHT_PRODUCTS, TRIED_PRODUCTS, FAVORITED_PRODUCTS,"
				+ " LOWEST_TO_HIGHEST_PRICE, HIGHEST_TO_LOWEST_PRICE, SALE_PRODUCTS")
		@RequestParam(value = "sort", required = false) String sort,
		@ApiParam(value = "Sale", required = false)
		@RequestParam(value = "isSale", required = false) boolean isSale) throws NotFoundException {
		
		ProductFilter productFilter = new ProductFilter();
		productFilter.setShopId(shopId);
		productFilter.setProductTypeId(productTypeId);
		productFilter.setCollectionId(collectionId);
		productFilter.setPage(page);
		productFilter.setLimit(limit);
		productFilter.setColorIds(colorIds);
		productFilter.setSizeChart(sizeChart);
		productFilter.setSizes(sizes);
		productFilter.setWearingPurposeIds(wearingpurposeIds);
		productFilter.setMaxPrice(maxPrice);
		productFilter.setMinPrice(minPrice);
		productFilter.setSortBy(sort);
		productFilter.setSale(isSale);
		log.info("REST request to filter list");
		return APIResponse.okStatus(this.shopProductService.filterProduct(keyWord, productFilter));
	}
	
	/** 
	 * Update {@link Product} by id
	 *
	 * @param shopId {@link Shop} id
	 * @param productId {@link Product} id
	 * @param productDTO {@link ProductDTO}
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws NotFoundException if user not found
	 * @throws JsonProcessingException 
	 * @throws BadRequestException 
	 * @throws ConflictException 
	 * @throws SocketTimeoutException 
	 * @throws SmartFashionException 
	 */
	@PutMapping(value = "/{shopId}/products/{productId}")
	@ApiOperation(value = "Update product")
	@ApiResponses(value = {
		})
	public APIResponse<?> updateProduct(
			@ApiParam(value = "Shop Id", required = true)
		@PathVariable(value = "shopId", required = true) int shopId,
			@ApiParam(value = "Product Id", required = true)
		@PathVariable(value = "productId", required = true) int productId,
			@ApiParam(value = "Information to update product")
		@Valid
		@RequestBody ProductUpdateDTO product, Errors errors) throws NotFoundException, JsonProcessingException, BadRequestException, ConflictException, SocketTimeoutException, SmartFashionException {
		log.info("REST request to update product ID : {}", productId);
		this.shopProductService.validationProductName(errors, shopId, productId, product.getName());
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(messages.toString(), true);
		}
		Product productRes = this.shopProductService.updateProduct(product, shopId, productId);
		return APIResponse.okStatus(new ProductDTO(productRes));
	}
	
	/** 
	 * Get {@link Product} by id
	 *
	 * @param shopId {@link Shop} id
	 * @param productId {@link Product} id
	 * @return {@link APIResponse} type {@link ResponseEntity}
	 * @throws NotFoundException if user not found
	 */
	@GetMapping(value = "/{shopId}/products/{productId}")
	@ApiOperation(value = "Get product by product id")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
		})
	public APIResponse<ProductDTO> getProductDetail(
		@ApiParam(value = "Shop Id", required = true)
		@PathVariable(value = "shopId", required = true) int shopId,
		@ApiParam(value = "Product Id", required = true)
		@PathVariable(value = "productId", required = true) int productId) throws NotFoundException {
		log.info("REST request to find product ID : {}", productId);
		return APIResponse.okStatus(this.shopProductService.getProductDetail(shopId, productId));
	}

	/**
	 * Set highlight for product's information
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product} id
	 * @param highlight {@link Product}
	 * @return {@link APIResponse}
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	@PutMapping("/{shopId}/products/{productId}/highlight")
	@ApiOperation(value = "Set highlight for product's information")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<ProductDTO> setHighlightForProductInfo(
			@ApiParam(value = "shopId",  required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "productId", required = true)
			@PathVariable(value = "productId", required = true) Integer productId,
			@ApiParam(value = "highlight", required = true, allowableValues = "true, false")
			@RequestParam(value = "highlight", required = true) Boolean highlight) throws NotFoundException, SmartFashionException {
		
		ProductDTO productDTO = this.shopProductService.setHighlightForProductInfo(shopId, productId, highlight);
		return APIResponse.okStatus(productDTO);
	}

	/**
	 * Get properties for creating product
	 *
	 * @param shopId {@link Shop}
	 * @return APIResponse {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/products/properties")
	@ApiOperation(value = "Get properties relate to product")
	@ApiResponses(value = {
		@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<ProductProperties> getPropertiesForProduct() throws NotFoundException {
		return APIResponse.okStatus(this.shopProductService.getPropertiesForProduct());
	}

	/**
	 * Create product
	 *
	 * @param shopId {@link Shop}
	 * @param productDTO {@link ProductDTO}
	 * @return APIResponse {@link APIResponse}
	 * @throws NotFoundException
	 * @throws BadRequestException 
	 * @throws JsonProcessingException 
	 * @throws SmartFashionException 
	 */
	@PostMapping("/{shopId}/products")
	@ApiOperation(value = "Create Product")
	@ApiResponses(value = {
	})
	public APIResponse<ProductDTO> createProduct(
			@ApiParam(value = "shopId", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "General information about new product")
			@Valid
			@RequestBody ProductDTO productDTO, Errors errors) throws NotFoundException, ConflictException, BadRequestException, JsonProcessingException, SmartFashionException {
		log.info("REST request to create product");
		this.shopProductService.validationProductName(errors, shopId, productDTO.getId(), productDTO.getName());
		if (errors.hasErrors()) {
			JSONObject messages = SmartFashionUtils.getMessageListFromErrorsValidation(errors);
			throw new BadRequestException(messages.toString(), true);
		}
		return APIResponse.createdStatus(this.shopProductService.createProduct(productDTO, shopId));
	}

	/**
	 * Get draft product
	 *
	 * @param shopId {@link Shop}
	 * @return @return APIResponse {@link APIResponse} data is {@link ProductDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/products/draft")
	@ApiOperation(value = "Get draft product")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG),
	})
	public APIResponse<ProductDTO> getDraftProduct(
			@ApiParam(value = "shopId")
			@PathVariable(value = "shopId", required = true) Integer shopId) throws NotFoundException {
		return APIResponse.okStatus(this.shopProductService.getDraftProduct(shopId));
	}
	
	/**
	 * Get {@link ProductCollection}
	 *
	 * @return APIResponse {@link APIResponse}
	 * @throws NotFoundException
	 */
	@GetMapping("/products/product-collections")
	@ApiOperation(value = "Get product colletion list")
	public APIResponse<List<ProductCollectionDTO>> getProductCollections() throws NotFoundException {
		return APIResponse.okStatus(this.productCollectionService.getProductCollectionList());
	}

	/**
	 * Create new product's details
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param productDetails {@link ProductDetail}
	 * @return APIResponse {link {@link APIResponse} data is productDTO {@link ProductDTO}
	 * @throws NotFoundException
	 * @throws SocketTimeoutException 
	 * @throws SmartFashionException 
	 */
	@PostMapping("/{shopId}/products/{productId}/products-details")
	@ApiOperation(value = "Create product's details")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<List<ProductDetail>> createProductDetails(
			@ApiParam(value = "shopId", required = true)
			@PathVariable(value = "shopId") Integer shopId,
			@ApiParam(value = "productId", required = true)
			@PathVariable(value = "productId") Integer productId,
			@ApiParam(value = "product's detail information", required = true)
			@RequestBody List<ProductDetailDTO> productDetailDTOs) throws NotFoundException, ConflictException, SocketTimeoutException, SmartFashionException {
		return APIResponse.createdStatus(this.shopProductService.createProductDetails(shopId, productId, productDetailDTOs));
	}
	
	/**
	 * Get {@link ProductRating} reviews
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param page page number
	 * @param limit Limit on per page
	 * @return APIResponse {@link APIResponse} 
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/products/{productId}/reviews")
	@ApiOperation(value = "Get reviews for product")
	@ApiResponses(value = {
	})
	public APIResponse<PageInfo<ProductRatingDTO>> getProductsReview(
			@ApiParam(value = "Shop Id", required = true)
			@PathVariable(value = "shopId") Integer shopId,
			@ApiParam(value = "Product Id", required = true)
			@PathVariable(value = "productId") Integer productId,
			@ApiParam(value ="Page", required = false)
			@RequestParam(value = "page", required = false) Integer page,
			@ApiParam(value ="Limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit) throws NotFoundException {
		log.info("REST request to get product rating");
		return APIResponse.okStatus(this.shopProductService.getProductReviews(shopId, productId, page, limit));
	}
	
	/**
	 * Answer {@link ProductRating} reviews
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param productRatingReply {@link ProductRatingReplyDTO}
	 * @return APIResponse {@link APIResponse} 
	 * @throws NotFoundException
	 * @throws SmartFashionException 
	 */
	@PostMapping("/{shopId}/products/{productId}/answer-reviews")
	@ApiOperation(value = "Answer reviews for product")
	@ApiResponses(value = {
	})
	public APIResponse<ProductRatingReplyDTO> answerProductReply(
			@ApiParam(value = "Shop Id", required = true)
			@PathVariable(value = "shopId") Integer shopId,
			@ApiParam(value = "Product Id", required = true)
			@PathVariable(value = "productId") Integer productId,
			@RequestBody ProductRatingReplyDTO productRatingReply) throws NotFoundException, SmartFashionException {
		log.info("REST request to answer product rating");
		return APIResponse.okStatus(this.shopProductService.answerProductReviews(shopId, productId, productRatingReply));
	}

	/**
	 * Get list of Product's information by search saved product
	 *
	 * @param shopId {@link Shop}
	 * @param name {@link Product} productName or productCode
	 * @param page page number
	 * @param limit limit record in a page
	 * @return APIResponse {@link APIResponse} data is list of {@link ProductSearchInfo}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/searched-products")
	@ApiOperation(value = "Get list of Product by search saved products")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<PageInfo<ProductSearchInfo>> getProductListBySearch(
			@ApiParam(value = "Shop Id", required = true)
			@PathVariable(value = "shopId") Integer shopId,
			@ApiParam(value = "Search product by name or product's code", required = false)
			@RequestParam(value = "name", required = false) String name,
			@ApiParam(value = "Page", required = false)
			@RequestParam(value = "page", required = false) Integer page,
			@ApiParam(value = "Limit", required = false)
			@RequestParam(value = "limit", required = false) Integer limit) throws  NotFoundException {
		log.info("REST request to search product");
		return APIResponse.okStatus(this.shopProductService.getProductListBySearch(shopId, name, page, limit));
	}

	/**
	 * Get Product's detail for creating new product quickly
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @return APIResponse {@link APIResponse} data is {@link ProductDTO}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/searched-products/{productId}")
	@ApiOperation(value = "Get product's detail for creating new product quickly")
	@ApiResponses(value = {
			@ApiResponse(code = 400 , message = "Bad Request"),
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<ProductDTO> getProductDetailForCreatingNew(
			@ApiParam(value = "shopId", required = true)
			@PathVariable(value = "shopId") Integer shopId,
			@ApiParam(value = "productId", required = true)
			@PathVariable(value = "productId") Integer productId) throws NotFoundException {
		log.info("REST request to get product_category's detail");
		return APIResponse.okStatus(this.shopProductService.getProductDetailForCreatingNew(shopId, productId));
	}

	/**
	 * Delete products by set status to "DELETED"
	 *
	 * @param shopId {@link Shop}
	 * @param productIdList list of product ID {@link Product}
	 * @return APIResponse set null in case delete
	 * @throws NotFoundException
	 * @throws BadRequestException
	 * @throws SmartFashionException 
	 */
	@DeleteMapping("/{shopId}/products")
	@ApiOperation("Delete many products")
	@ApiResponses(value = {
	})
	public APIResponse<?> deleteProductsList(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "List of Products's id", required = true)
			@RequestBody List<Integer> productIdList) throws NotFoundException, BadRequestException, SmartFashionException {
		return APIResponse.okStatus(this.shopProductService.deleteProductsList(shopId, productIdList));
	}

	/**
	 * Delete product detail when updating product
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param productDetailId {@link ProductDetail}
	 * @return {@link APIResponse} return {@link ProductDetailDTO} with notice in case delete successfully
	 * @throws NotFoundException
	 * @throws SocketTimeoutException 
	 * @throws SmartFashionException 
	 */
	@DeleteMapping("/{shopId}/products/{productId}/product-details/{productDetailId}")
	@ApiOperation("Delete Product detail when Updating product")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<ProductDetailDTO> deleteProductDetail(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "Product ID", required = true)
			@PathVariable(value = "productId", required = true) Integer productId,
			@ApiParam(value = "Product Detail ID", required = true)
			@PathVariable(value = "productDetailId", required = true) Integer productDetailId) throws NotFoundException, SocketTimeoutException, SmartFashionException {
		return APIResponse.okStatus(this.shopProductService.deleteProductDetail(shopId, productId, productDetailId));
	}
	

	/**
	 * Upload image cropped for product
	 * @param shopId {@link Shop} Id
	 * @param productId {@link Product} Id
	 * @param productName {@link Product} name
	 * @param imageWrapperDTO {@link ImageWrapperDTO}
	 * @throws NotFoundException 
	 */
	@PostMapping("/{shopId}/products/{productId}/images")
	@ApiOperation("Upload crop image for product")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<ImageWrapperDTO> cropProductImage(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId", required = true) Integer shopId,
			@ApiParam(value = "Product ID", required = true)
			@PathVariable(value = "productId", required = true) Integer productId,
			@ApiParam(value = "Image info", required = true)
			@RequestBody ImageWrapperDTO imageWrapper) throws NotFoundException {
		return APIResponse.okStatus(this.shopProductService.cropProductImage(shopId, productId, imageWrapper));
	}

	/**
	 * Check duplicate product's name
	 *
	 * @param shopId {@link Shop}
	 * @param productId {@link Product}
	 * @param productName {@link Product}
	 * @return {@link APIResponse} data is result {true, false}
	 * @throws NotFoundException
	 */
	@GetMapping("/{shopId}/products/name-checking")
	@ApiOperation("Check duplicate product's name")
	@ApiResponses(value = {
			@ApiResponse(code = APIConstants.ERROR_NOT_FOUND_CODE, message = APIConstants.ERROR_NOT_FOUND_MSG)
	})
	public APIResponse<Boolean> checkDuplicateProductName(
			@ApiParam(value = "Shop ID", required = true)
			@PathVariable(value = "shopId") Integer shopId,
			@ApiParam(value = "Product ID")
			@RequestParam(value = "productId", required = false) Integer productId,
			@ApiParam(value = "Product's name", required = true)
			@RequestParam(value = "productName") String productName) throws NotFoundException {
		return APIResponse.okStatus(this.shopProductService.checkDuplicateProductName(shopId, productId, productName));
	}
}
