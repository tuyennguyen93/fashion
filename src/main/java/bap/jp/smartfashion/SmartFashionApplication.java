package bap.jp.smartfashion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SmartFashionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartFashionApplication.class, args);
	}

}
