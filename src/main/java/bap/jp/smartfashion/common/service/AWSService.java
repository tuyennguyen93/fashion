package bap.jp.smartfashion.common.service;

import java.net.URL;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import bap.jp.smartfashion.common.vo.PreSignedAWS;

/**
 * Service class for managing function from AWS.
 */
@Configuration
public class AWSService {

	@Autowired
	private AmazonS3 s3client;

	@Value("${app.s3.aws.bucket-name}")
	private String bucketName;
	
	@Value("${app.pre-signed-expired}")
	private int preSignedExpired;
	
	public PreSignedAWS uploadPresignedUrl(String fileName) {
		
		PreSignedAWS preSignedAWS = new PreSignedAWS();
		Date date = new Date();
		date = DateUtils.addMinutes(date, this.preSignedExpired);
		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(this.bucketName, fileName);
		generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
		generatePresignedUrlRequest.setExpiration(date);
		URL url = this.s3client.generatePresignedUrl(generatePresignedUrlRequest);
		preSignedAWS.setPreSignedURL(url);
		String urlRes = url.getProtocol() + "://" + this.bucketName + ".s3.amazonaws.com" + url.getPath();
		preSignedAWS.setUrl(urlRes);
		
		return preSignedAWS;
	}

	/**
	 * Get all object in bucket by prefix
	 * 
	 * @param bucketName The name of the Amazon S3 bucket to list.
	 * @param prefix     An optional parameter restricting the response to keys
	 *                   beginning with the specified prefix.
	 */
	public ObjectListing getListObject(String prefix) {
		return this.s3client.listObjects(bucketName, prefix);
	}

	/**
	 * Delete file in Bucket
	 * 
	 * @param bucketName bucket name
	 * @param key        file patch
	 * @throws Exception if bucket not exist
	 */
	public void deleteObjectInBucket(String key) throws Exception {
		if (this.s3client.doesBucketExist(bucketName)) {
			try {
				this.s3client.deleteObject(bucketName, key);
			} catch (AmazonServiceException e) {
				e.printStackTrace();
			}
		} else {
			throw new Exception("Bucket " + bucketName + " does not exist!");
		}
	}
}
