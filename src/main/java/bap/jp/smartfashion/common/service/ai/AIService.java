package bap.jp.smartfashion.common.service.ai;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserProduct;
import bap.jp.smartfashion.common.service.ai.request.ClothingRequest;
import bap.jp.smartfashion.common.service.ai.request.PersonRequest;
import bap.jp.smartfashion.common.service.ai.request.TryOnRequest;
import bap.jp.smartfashion.common.service.ai.response.ClothingResponse;
import bap.jp.smartfashion.common.service.ai.response.PersonResponse;
import bap.jp.smartfashion.common.service.ai.response.TryOnResponse;
import bap.jp.smartfashion.exception.SmartFashionException;

/**
 * Service class for managing function from AI.
 */
@Service
public class AIService {

	private final Logger log = LoggerFactory.getLogger(AIService.class);
	@Value("${ai.api-url}")
	private String apiURL;

	@Autowired
	private RestTemplate aiRestTemplate;

	/**
	 * Upload model for {@link User}
	 * 
	 * @param modelPersons List of {@link PersonRequest}
	 * @return {@link PersonResponse}
	 */
	public PersonResponse uploadPerson(PersonRequest personRequest) throws ResourceAccessException {
		log.info("Uploading model requests to AI to get keyPoint");
		PersonResponse personResponse = new PersonResponse();
		try {
			HttpEntity<PersonRequest> request = new HttpEntity<PersonRequest>(personRequest);
			personResponse = this.aiRestTemplate.postForObject(this.getApiUrl("/openpose_client/openpose_plus"),
					request, PersonResponse.class);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			personResponse.getMessage();
		}

		return personResponse;
	}

	/**
	 * Upload clothing for {@link User}
	 * 
	 * @param clothingRequests List of {@link ClothingRequest}
	 * @return {@link ClothingResponse}
	 */
	public ClothingResponse uploadClothing(ClothingRequest clothingRequest) throws ResourceAccessException {
		log.info("Uploading clothing requests to AI to get keyPoint");
		ClothingResponse clothingResponse = new ClothingResponse();
		try {
			HttpEntity<ClothingRequest> request = new HttpEntity<ClothingRequest>(clothingRequest);
			clothingResponse = this.aiRestTemplate.postForObject(this.getApiUrl("/cpn_client/cpn"), request,
					ClothingResponse.class);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			clothingResponse.getMessage();
		}
		return clothingResponse;
	}

	/**
	 * Upload clothing for {@link User}
	 * 
	 * @param clothingRequests List of {@link ClothingRequest}
	 * @return {@link TryOnResponse}
	 * @throws SmartFashionException 
	 */
	public TryOnResponse tryOn(TryOnRequest tryOnRequest) throws ResourceAccessException, SmartFashionException {
		log.info("Try on product and get product tried on");
		TryOnResponse tryOnResponse = new TryOnResponse();
		try {
			HttpEntity<TryOnRequest> request = new HttpEntity<TryOnRequest>(tryOnRequest);
			tryOnResponse = this.aiRestTemplate.postForObject(this.getApiUrl("/try_ons_client/try_ons"), request,
					TryOnResponse.class);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			tryOnResponse.getMessage();
		} catch (Exception e) {
			throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_AI_CANT_TRY, "Can't try", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return tryOnResponse;
	}

	public PersonResponse generateKeyPointForUserModel(UserModel userModel) throws SocketTimeoutException {
		List<PersonRequest> modelPersons = new ArrayList<>();
		PersonRequest modelPerson = new PersonRequest();
		modelPerson.setUserId(userModel.getUser().getId());
		modelPerson.setPersonId(0);
		modelPerson.setPersonImage(userModel.getUrl());
		modelPersons.add(modelPerson);
		return this.uploadPerson(modelPerson);
	}

	public ClothingResponse generateKeyPointForShopProduct(Integer clothId, String clothImage, Integer id)
			throws SocketTimeoutException {
		ClothingRequest clothingRequest = new ClothingRequest();
		clothingRequest.setUserId(id);
		clothingRequest.setIsUser(false);
		clothingRequest.setClothId(0);
		clothingRequest.setClothImage(clothImage);
		return this.uploadClothing(clothingRequest);
	}

	public ClothingResponse generateKeyPointForUserProduct(UserProduct userProduct) throws SocketTimeoutException {
		ClothingRequest clothingRequest = new ClothingRequest();
		clothingRequest.setUserId(userProduct.getUser().getId());
		clothingRequest.setIsUser(true);
		clothingRequest.setClothId(0);
		clothingRequest.setClothImage(userProduct.getUrl());
		return this.uploadClothing(clothingRequest);
	}

	/**
	 * Get API URL
	 * 
	 * @param path Path url
	 * @return API URL
	 */
	private String getApiUrl(String path) {
		return String.format("%s%s", this.apiURL, path);
	}
}
