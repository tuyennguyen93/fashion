package bap.jp.smartfashion.common.service.ai.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clothing request for {@link TryOnResponse}
 *
 */
@Getter
@Setter
@ToString
public class TryOnRequest {
	private Integer userId;
	private Boolean isUser;
	private Integer clothId;
	private String clothURL;
	private String clothKeypoints;
	private Integer personId;
	private String personURL;
	private String personKeypoints;
}
