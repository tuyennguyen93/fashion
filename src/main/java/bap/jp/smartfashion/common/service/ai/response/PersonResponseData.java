package bap.jp.smartfashion.common.service.ai.response;

import bap.jp.smartfashion.common.service.ai.request.ClothingRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clothing response for {@link ClothingRequest}
 *
 */
@Getter
@Setter
@ToString
public class PersonResponseData {
	private String keyPoint;
	private int userId;
	private int personId;
	private String personImage;
		
	public PersonResponseData() {}
}
