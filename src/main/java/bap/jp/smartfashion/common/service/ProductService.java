package bap.jp.smartfashion.common.service;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.ProductMixMatchDTO;
import bap.jp.smartfashion.common.dto.ProductRatingDTO;
import bap.jp.smartfashion.common.dto.ProductUpdateDTO;
import bap.jp.smartfashion.common.model.MixMatch;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductMixMatch;
import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingCount;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.model.ProductTypeCount;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.UserProductInteraction;
import bap.jp.smartfashion.common.repository.MixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductMixMatchRepository;
import bap.jp.smartfashion.common.repository.ProductRatingRepository;
import bap.jp.smartfashion.common.repository.ProductTypeCountRepository;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;
import bap.jp.smartfashion.common.repository.ShopProductTryOnRepository;
import bap.jp.smartfashion.common.repository.UserProductInteractionRepository;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.common.vo.ProductRatingVO;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.enums.ProductEnum.ProductStatus;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing product
 */
@Service
public class ProductService extends BaseService {
    @Autowired
    private MixMatchRepository mixMatchRepository;

    @Autowired
    private ProductRatingRepository productRatingRepository;

    @Autowired
    private ProductMixMatchRepository productMixMatchRepository;

    @Autowired
    private UserProductInteractionRepository userProductInteractionRepository;

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Autowired
    private ProductTypeCountRepository productTypeCountRepository;


    @Autowired
    private ShopProductTryOnRepository shopProductTryOnRepository;
    
    /**
     * set Data For Product RatingVO
     * @param list {@link List<ProductRatingCount>}
     * @return ProductRatingVO {@link ProductRatingVO}
     */
    public ProductRatingVO setDataForProductRatingVO(List<ProductRatingCount> list) {
        int[] data = {0, 0, 0, 0, 0, 0};
        for (ProductRatingCount productRatingCount : list) {
            int ratingCount = productRatingCount.getRatingCount() == null ? 0 : productRatingCount.getRatingCount();
            if(productRatingCount.getRatingStar() == 0) {
            	data[0] = ratingCount;
            } else if(productRatingCount.getRatingStar() == 1) {
            	data[1] = ratingCount;
            } else if(productRatingCount.getRatingStar() == 2) {
            	data[2] = ratingCount;
            } else if(productRatingCount.getRatingStar() == 3) {
            	data[3] = ratingCount;
            } else if(productRatingCount.getRatingStar() == 4) {
            	data[4] = ratingCount;
            } else if(productRatingCount.getRatingStar() == 5) {
            	data[5] = ratingCount;
            }
        }

        ProductRatingVO productRatingVO = new ProductRatingVO();
        productRatingVO.setProductRatingCount(data);
        productRatingVO.setRatingStar(this.calRatingStar(productRatingVO));
        return productRatingVO;
    }

    /**
     * Calulator Rating star for {@link Product}
     * @param productRatingVO {@link ProductRatingVO}
     * @return rating star of product
     */
    public Double calRatingStar(ProductRatingVO productRatingVO) {
        double sumRating = new Double((productRatingVO.getProductRatingCount()[1] * 1)
                + (productRatingVO.getProductRatingCount()[2] * 2)
                + (productRatingVO.getProductRatingCount()[3] * 3)
                + (productRatingVO.getProductRatingCount()[4] * 4)
                + (productRatingVO.getProductRatingCount()[5] * 5));

        double ratingStar = 0.0;
        if (productRatingVO.getProductRatingCount()[0] != 0) {
            ratingStar = (double) (sumRating/productRatingVO.getProductRatingCount()[0]);
        }
        DecimalFormat df = new DecimalFormat("#.#");
        return Double.valueOf(df.format(ratingStar));
    }


    /**
	 * Get List product mixmatch
     * @param productId
     * @return
	 */
	public List<ProductMixMatchDTO> getProductMixMatchs(Integer productId){
		List<ProductMixMatch> productMixMatchs = this.productMixMatchRepository.findAllByProduct_IdAndProduct_Status(productId, ProductStatus.ACTIVE.getCode());
		List<MixMatch> mixMatchs = productMixMatchs.parallelStream().map(ProductMixMatch::getMixMatch).collect(Collectors.toList());
		List<Integer> ids = mixMatchs.parallelStream().map(MixMatch::getId).collect(Collectors.toList());
		List<ProductMixMatchDTO> productMixMatchDTOs = new ArrayList<>();
		if (!ids.isEmpty()) {
			List<ProductMixMatch> productMixMatchsByMixIds = this.productMixMatchRepository.findDistinctProduct_IdByMixMatch_IdIn(ids);
			for (ProductMixMatch productMixMatch : productMixMatchs) {
				//get parent
				if (productId != productMixMatch.getProduct().getId() && productMixMatch.getPriority() == 1) {
					productMixMatchDTOs.add(new ProductMixMatchDTO(productMixMatch));
				}
				
				for (ProductMixMatch productMixMatch1 : productMixMatchsByMixIds) {
					
					// get parent list belong to product
					if (productMixMatch1.getPriority() == 1 && productMixMatch.getPriority() != 1) {
						productMixMatchDTOs.add(new ProductMixMatchDTO(productMixMatch1));
					}
					
					// get child list belong to parent
					if (productMixMatch.getMixMatch().getId() == productMixMatch1.getMixMatch().getId() 
							&& productMixMatch.getProduct().getId() != productMixMatch1.getProduct().getId()
							&& productMixMatch.getPriority() == 1) {
						productMixMatchDTOs.add(new ProductMixMatchDTO(productMixMatch1));
					}
			}
			// remove product duplicate
			productMixMatchDTOs = productMixMatchDTOs.stream().filter(SmartFashionUtils.distinctByKey(p -> p.getProduct().getId()))
				.filter(mixMatch -> productId != mixMatch.getProduct().getId()).sorted(Comparator.comparing(ProductMixMatchDTO::getId)).collect(Collectors.toList());
				}
		}
		return productMixMatchDTOs;
	}

    /**
     * Get {@link ProductRating} reviews
     *
     * @param shopId {@link Shop}
     * @param productId {@link ProductCategory}
     * @param page page number
     * @param limit Limit on per page
     * @return APIResponse {@link APIResponse}
     */
    public PageInfo<ProductRatingDTO> getProductReviews(Integer productId, Integer page, Integer limit)  {
        PageRequest pageRequest = this.buildPageRequest(page, limit);
        Page<ProductRatingDTO> data = this.productRatingRepository
                .findAllByProduct_IdAndStatus(productId, ProductStatus.ACTIVE.getCode(), pageRequest)
                .map(ProductRatingDTO::new);
        PageInfo<ProductRatingDTO> pageInfo = SmartFashionUtils.pagingResponse(data);
        return pageInfo;
    }

    /**
     * get action Code (favorite or try) for user
     * @param userId {@link User}
     * @param productId {@link Product}
     * @param actionCode {@link UserProductInteraction}
     * @return UserProductInteraction {@link UserProductInteraction}
     */
    public UserProductInteraction getUserProductInteraction(Integer userId, Integer productId, String actionCode){
        return userProductInteractionRepository.findByUser_IdAndProduct_IdAndActionCode(this.getCurrentUserId(), productId, actionCode).orElse(null);
    }
    
    /**
     * Build sort function for {@link Product}
     * @param sortBy
     * @return {@link Sort}
     */
    public Sort buildSortProduct(String sortBy) {
    	sortBy = StringUtils.trim(sortBy);
        Sort sort = Sort.by(Sort.Order.asc("name"));
        if (ProductEnum.ProductSort.NEW_ARRIVAL.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.desc("created"));
        } else if (ProductEnum.ProductSort.HIGHLIGHT_PRODUCTS.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.desc("highlight"));
        } else if (ProductEnum.ProductSort.TRIED_PRODUCTS.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.desc("productInteractions.actionCount"));
        } else if (ProductEnum.ProductSort.FAVORITED_PRODUCTS.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.desc("productInteractions.actionCount"));
        } else if (ProductEnum.ProductSort.LOWEST_TO_HIGHEST_PRICE.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.asc("price"));
        } else if (ProductEnum.ProductSort.HIGHEST_TO_LOWEST_PRICE.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.desc("price"));
        } else if (ProductEnum.ProductSort.SALE_PRODUCTS.toString().equals(sortBy)) {
            sort = Sort.by(Sort.Order.desc("discount"));
        }
        return sort;
    }

    /**
     * Get {@link ProductType} to check {@link ProductType} Id
     * @param productTypeId {@link ProductType} id
     * @return List Id of {@link ProductType}
     */
    public List<Integer> getProductTypes(Integer productTypeId) {
        List<Integer> list = new ArrayList<>();
        if (productTypeId == null) {
            return null;
        }
        Optional<ProductType> productTypeOptional = this.productTypeRepository.findById(productTypeId);
        if (productTypeOptional.isPresent()) {
            ProductType productType = productTypeOptional.get();
            if (productType.getProductTypeParentId() == null) {
                List<Integer> productTypes = this.productTypeRepository.findAllByProductTypeParentId(productTypeId)
                        .stream().map(ProductType::getId).collect(Collectors.toList());
                list.addAll(productTypes);
            } else {
                list.add(productTypeId);
            }
        } else {
            list.add(productTypeId);
        }

        if (list.isEmpty()) {
            list.add(productTypeId);
        }
        return list;
    }
    
    /**
     * Calculate price discount
     *
     * @param price product's price
     * @param discount product's discount
     * @return price after discount
     */
    public static BigDecimal calPriceDiscount(BigDecimal price, BigDecimal discount) {
        BigDecimal priceDiscount = price.multiply(discount).divide(new BigDecimal(100));
        return price.subtract(priceDiscount).setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    /**
     * Set Product Type Count for new product
     * increase count of product type count when adding new product
     *
     * @param draftProduct {@link Product}
     * @param shopId {@link Shop}
     * @return List of {@link ProductTypeCount}
     */
    public List<ProductTypeCount> setProductTypeCountForNewProduct(Product draftProduct, Integer shopId) {
        List<ProductTypeCount> productTypeCounts = new ArrayList<>();
        ProductType productType = this.getExitedProductType(draftProduct.getProductType().getId());
        if(productType.getProductCollection() == null) {
            // Check if productTypeCount for TypeChild exits
            ProductTypeCount productTypeChild = this.getProductTypeCount(productType.getId(), shopId);
            if(productTypeChild != null) {
                productTypeChild.setCount(productTypeChild.getCount() + 1);
            } else {
                productTypeChild = new ProductTypeCount();
                productTypeChild.setCount(1);
                productTypeChild.setShop(new Shop(shopId));
                productTypeChild.setProductType(new ProductType(productType.getProductTypeParentId()));
            }
            productTypeCounts.add(productTypeChild);

            // Check if productTypeCount for TypeParent exits
            ProductTypeCount productTypeParent = this.getProductTypeCount(productType.getProductTypeParentId(), shopId);
            if(productTypeParent != null) {
                productTypeParent.setCount(productTypeParent.getCount() + 1);
            } else {
                productTypeParent = new ProductTypeCount();
                productTypeParent.setCount(1);
                productTypeParent.setShop(new Shop(shopId));
                productTypeParent.setProductType(new ProductType(productType.getProductTypeParentId()));
            }
            productTypeCounts.add(productTypeParent);
        } else {
            // Check if productTypeCount for TypeParent exits
            ProductTypeCount productTypeParent = this.getProductTypeCount(productType.getId(), shopId);
            if(productTypeParent != null) {
                productTypeParent.setCount(productTypeParent.getCount() + 1);
            } else {
                productTypeParent = new ProductTypeCount();
                productTypeParent.setCount(1);
                productTypeParent.setShop(new Shop(shopId));
                productTypeParent.setProductType(new ProductType(productType.getId()));
            }
            productTypeCounts.add(productTypeParent);
        }
        return productTypeCounts;
    }

    /**
     * Get product type parent's name by product type id
     *
     * @param productTypeId {@link ProductType}
     * @return Product type parent's name
     */
    public String getProductTypeParentName(Integer productTypeId) {
         if(productTypeId == null) {
             return null;
         }
         ProductType productType = this.productTypeRepository.findById(productTypeId).orElse(null);
         if(productType != null) {
             return productType.getName();
         }else {
            return null;
         }
    }

    public List<ProductTypeCount> changeProductTypeCountForProductUpdateByCondition(Integer shopId, Integer productTypeParentId, Integer productTypeChildId) {
        List<ProductTypeCount> productTypeCounts = new ArrayList<>();
        if(productTypeParentId != null && productTypeChildId != null) {
            ProductTypeCount productTypeCountChild = this.productTypeCountRepository
                    .findByProductType_IdAndShop_Id(productTypeChildId, shopId).orElse(null);
            productTypeCountChild.setCount(productTypeCountChild.getCount() - 1);
            productTypeCounts.add(productTypeCountChild);

            ProductTypeCount productTypeCountParent = this.productTypeCountRepository
                    .findByProductType_IdAndShop_Id(productTypeParentId, shopId).orElse(null);
            productTypeCountParent.setCount(productTypeCountParent.getCount() - 1);
            productTypeCounts.add(productTypeCountParent);
        } else if(productTypeChildId == null) {
            ProductTypeCount productTypeCountParent = this.productTypeCountRepository
                    .findByProductType_IdAndShop_Id(productTypeParentId, shopId).orElse(null);
            productTypeCountParent.setCount(productTypeCountParent.getCount() - 1);
            productTypeCounts.add(productTypeCountParent);
        }
        return productTypeCounts;
    }

    public void checkProductTypeCountForProductUpdate(Product product, ProductType productType, Integer shopId) {
        if(product.getProductType().getId() != productType.getId()) {
            List<ProductTypeCount> productTypeCounts = new ArrayList<>();

            // case 1: before this type parent and type child after type parent and type child other
            if (product.getProductType().getProductTypeParentId() != null
                    && productType.getProductTypeParentId() != null
                    && !product.getProductType().getProductTypeParentId().equals(productType.getProductTypeParentId())) {
                productTypeCounts.addAll(this.changeProductTypeCountForProductUpdateByCondition(shopId,
                        product.getProductType().getProductTypeParentId(), product.getProductType().getId()));
            }

            // case 2: before this type child after type child other in a type parent
            else if (product.getProductType().getProductTypeParentId() != null
                    && productType.getProductTypeParentId() != null
                    && product.getProductType().getProductTypeParentId().equals(productType.getProductTypeParentId())) {
                productTypeCounts.addAll(this.changeProductTypeCountForProductUpdateByCondition(shopId,
                        product.getProductType().getProductTypeParentId(), product.getProductType().getId()));
            }

            // case 3: before this type parent but dont have any type child after set type child in that type parent
            else if (product.getProductType().getProductTypeParentId() == null
                    && productType.getProductTypeParentId().equals(product.getProductType().getProductTypeParentId())) {
                productTypeCounts.addAll(this.changeProductTypeCountForProductUpdateByCondition(shopId,
                        product.getProductType().getProductTypeParentId(), null));
            }

            // case 4: before this type parent after type parent and type child other
            else if (product.getProductType().getProductTypeParentId() == null
                    && productType.getProductTypeParentId() != null
                    && !productType.getProductTypeParentId().equals(product.getProductType().getId())) {
                productTypeCounts.addAll(this.changeProductTypeCountForProductUpdateByCondition(shopId,
                        product.getProductType().getProductTypeParentId(), null));
            }

            // case 5: before this type parent and type child after set type parent other
            else if (product.getProductType().getProductTypeParentId() != null
                    && productType.getProductTypeParentId() == null
                    && !product.getProductType().getProductTypeParentId().equals(productType.getId())) {
                productTypeCounts.addAll(this.changeProductTypeCountForProductUpdateByCondition(shopId,
                        product.getProductType().getProductTypeParentId(), product.getProductType().getId()));
            }
            product.setProductType(productType);
            productTypeCounts.addAll(this.setProductTypeCountForNewProduct(product, shopId));
            this.productTypeCountRepository.saveAll(productTypeCounts);
        }
    }
    
    public void deleteShopProductTriedOn(Integer productDetailId) {
    	this.shopProductTryOnRepository.deleteByProductDetail_Id(productDetailId);
    }
}

