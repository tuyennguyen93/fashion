package bap.jp.smartfashion.common.service.ai.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Model response for {@link PersonRequest}
 *
 */
@Getter
@Setter
@ToString
public class PersonResponse {
	private boolean status;
	private String message;
	private List<PersonResponseData> data;
}
