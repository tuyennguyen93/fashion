package bap.jp.smartfashion.common.service.ai.request;

import bap.jp.smartfashion.common.service.ai.response.TryOnResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * Clothing request for {@link TryOnResponse}
 *
 */
@Getter
@Setter
public class ClothingRequest {
	private Integer userId;
	private Integer clothId;
	private Boolean isUser;
	private String clothImage;
}
