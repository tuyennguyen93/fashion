package bap.jp.smartfashion.common.service.ai.request;

import lombok.Getter;
import lombok.Setter;

/**
 * Clothing request for {@link PersonResponse}
 *
 */
@Getter
@Setter
public class PersonRequest {
	private Integer userId;
	private Integer personId;
	private String personImage;
}
