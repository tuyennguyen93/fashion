package bap.jp.smartfashion.common.service;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import bap.jp.smartfashion.config.UserPrinciple;

/**
 * Message bundle.
 *
 */
@Component
public class MessageService {

	private MessageSource messageSource;

	public MessageService(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessage(String code) {
		return messageSource.getMessage(code, null, this.buildLocaleUserLogged());
	}
	
	/**
	 * Get lang key of user logged
	 *
	 */
	public Locale buildLocaleUserLogged() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			String langKey = ((UserPrinciple) principal).getLangKey();
			return Locale.forLanguageTag(langKey);
		}
		return Locale.US;
	}

}
