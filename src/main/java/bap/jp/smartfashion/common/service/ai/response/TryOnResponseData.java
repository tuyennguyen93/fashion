package bap.jp.smartfashion.common.service.ai.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Model response for {@link ModelRequest}
 *
 */
@Getter
@Setter
@ToString
public class TryOnResponseData {
	private int userId;
	private boolean isUser;
	private int clothingId;
	private int personId;
	private String triedOnImage;
}
