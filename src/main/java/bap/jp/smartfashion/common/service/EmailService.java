package bap.jp.smartfashion.common.service;

import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 * Service class for managing function from email.
 */
@Service
public class EmailService {
	@Autowired
	private JavaMailSender javaMailSender;


	/**
	 * Call SES to send email type mime message.
	 *
	 * @param emailSubject email subject
	 * @param emailTo email list to send
	 * @param content content of email
	 */
	public void sendEmail(String emaiSubject, String emailFrom, String[] emailTo, String content) {
		MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
		MimeMessageHelper helper = null;
		try {
			helper = new MimeMessageHelper(mimeMessage, false, StandardCharsets.UTF_8.name());
			helper.setFrom(emailFrom);
			helper.setTo(emailTo);
			helper.setText(content, true);
			helper.setSubject(emaiSubject);
			this.javaMailSender.send(mimeMessage);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Call SES to send email type mime message.
	 *
	 * @param emailSubject email subject
	 * @param emailTo email to send
	 * @param content content of email
	 */
	public void sendEmail(String emaiSubject, String emailFrom, String emailTo, String content) {
		MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
		MimeMessageHelper helper = null;
		try {
			helper = new MimeMessageHelper(mimeMessage, false, StandardCharsets.UTF_8.name());
			helper.setFrom(emailFrom);
			helper.setTo(emailTo);
			helper.setText(content, true);
			helper.setSubject(emaiSubject);
			this.javaMailSender.send(mimeMessage);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
