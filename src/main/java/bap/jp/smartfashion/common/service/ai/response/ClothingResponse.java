package bap.jp.smartfashion.common.service.ai.response;

import java.util.List;

import bap.jp.smartfashion.common.service.ai.request.ClothingRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clothing response for {@link ClothingRequest}
 *
 */
@Getter
@Setter
@ToString
public class ClothingResponse {
	private Boolean status;
	private String message;
	private List<ClothingResponseData> data;

	
	public ClothingResponse() {
		super();
	}
}
