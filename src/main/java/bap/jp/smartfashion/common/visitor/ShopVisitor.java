package bap.jp.smartfashion.common.visitor;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * {@link ShopVisitor} value object
 *
 */
@Getter
@Setter
public class ShopVisitor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer 	shopId;
	private String 		type;
	private Integer 	typeCount;
	private String 		timeline;
	private Date 		date;

	public ShopVisitor() {
	}
}
