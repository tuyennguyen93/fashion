package bap.jp.smartfashion.common.visitor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * {@link Visitor} value object
 *
 */
@Getter
@Setter
public class Visitor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private Integer shopId;
	private Integer collectionId;
	private Date lastAccessed;
	private Date created;
}
