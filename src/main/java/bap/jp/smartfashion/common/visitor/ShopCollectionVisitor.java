package bap.jp.smartfashion.common.visitor;

import lombok.Setter;
import bap.jp.smartfashion.common.model.ProductCollection;
import lombok.Getter;
/**
 * {@link ShopCollectionVisitor} value object
 *
 */
@Getter
@Setter
public class ShopCollectionVisitor extends ShopVisitor {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer productCollectionId;
	
	public ShopCollectionVisitor() {
	}
}
