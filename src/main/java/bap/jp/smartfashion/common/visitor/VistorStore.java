package bap.jp.smartfashion.common.visitor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.repository.ShopCollectionVisitorRepository;
import bap.jp.smartfashion.common.repository.ShopVisitorRepository;
import bap.jp.smartfashion.config.jwt.JwtProvider;
import bap.jp.smartfashion.enums.ShopEnum.Timeline;
import bap.jp.smartfashion.enums.ShopEnum.VisitorType;
import bap.jp.smartfashion.util.constants.Constants;

/**
 * {@link VistorStore} Run Visitor store
 */

@Component
public class VistorStore {
	
	private final Logger log = LoggerFactory.getLogger(VistorStore.class);
	private static final int TIME_CAL_RETURN_VISITOR = 5; // minutes
	private static final int TIME_CAL_NEW_VISITOR = 15; // minutes
	private static final String SHOP_KEY = "shop:"; 
	private static final String SHOPTRACKING_KEY = "shoptracking:"; 
	private static final String COLLECTION_KEY = ":collection:"; 
	private static final String SESSION_ID_KEY = ":session_id:"; 
	private static final String NEW_KEY = ":new:"; 
	private static final String RETURN_KEY = ":return:"; 
	private static final String LASTVIEW_ATTR = "lastView"; 
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	private ShopCollectionVisitorRepository shopCollectionVisitorRepository;

	@Autowired
	private ShopVisitorRepository shopVisitorRepository;
	
	@Autowired
	private JwtProvider jwtProvider;
	
	/**
	 * {@link Visitor} tracking
	 * @param shopId {@link Shop} Id
	 * @param collectionId {@link ProductCollection} Id
	 * @param httpServletRequest
	 */
	public void visitorTracking(Integer shopId, Integer collectionId, HttpServletRequest httpServletRequest) {
		String userName = null;
		String authHeader = httpServletRequest.getHeader(Constants.AUTHORIZATION_HEADER);
		if (authHeader != null && authHeader.startsWith(Constants.JWT_TOKEN_TYPE)) {
			authHeader = authHeader.replace(Constants.JWT_TOKEN_TYPE, "");
			userName = this.jwtProvider.getUserNameFromJwtToken(authHeader);
		}
		HttpSession session = httpServletRequest.getSession();
		Visitor visitor = new Visitor();
		if (userName == null) {
			visitor.setId(session.getId());
		} else {
			visitor.setId(userName);
		}
		visitor.setCreated(new Date(session.getCreationTime()));
		visitor.setShopId(shopId);
		visitor.setCollectionId(collectionId);
		Date lastView = (Date) session.getAttribute(LASTVIEW_ATTR) == null ? new Date() : (Date) session.getAttribute(LASTVIEW_ATTR);
		visitor.setLastAccessed(lastView);
		this.checkUserAccessedToShop(visitor, session);
	}

	/**
	 * Check user access to shop
	 * @param visitor {@link Visitor}
	 * @param session {@link HttpSession}
	 */
	private void checkUserAccessedToShop(Visitor visitor, HttpSession session) {
		this.calNewAndReturnShopVistor(visitor, session);
		this.calNewAndReturnShopCollectionVistor(visitor, session);
	}

	/**
	 * Calculate new visitor and return visitor
	 *
	 * @param visitor {@link Visitor}
	 * @param session {@link HttpSession}
	 */
	@SuppressWarnings("null")
	private void calNewAndReturnShopVistor(Visitor visitor, HttpSession session) {
		List<String> timelineList = new ArrayList<String>();
		for (Timeline timeline : Timeline.values()) { 
			timelineList.add(timeline.toString()); 
		}
		
		String keyTracking = SHOPTRACKING_KEY + visitor.getShopId() + NEW_KEY + SESSION_ID_KEY + visitor.getId();
		String keyReturnTracking = SHOPTRACKING_KEY + visitor.getShopId() + RETURN_KEY + SESSION_ID_KEY + visitor.getId();
		if (this.redisTemplate.opsForValue().get(keyTracking) == null) {
			for (String timeline : timelineList) {
				String timelineNew = timeline + "_";
				switch (timeline) {
				case "HOUR":
					timelineNew = timeline + LocalDateTime.now().getHour() + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "DATE":
					timelineNew = timeline + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "WEEK":
					timelineNew = timeline + getCurrentWeek() + LocalDateTime.now().getYear();
					break;
				case "MONTH":
					timelineNew = timeline + LocalDateTime.now().getMonthValue()+ LocalDateTime.now().getYear();
					break;
				case "YEAR":
					timelineNew = timeline + LocalDateTime.now().getYear();
					break;
				default:
					break;
				}
				String keyShopNew = SHOP_KEY + visitor.getShopId() + NEW_KEY + timelineNew;
				ShopVisitor shopVisitorDayNew = (ShopVisitor) this.redisTemplate.opsForValue().get(keyShopNew);
				if (shopVisitorDayNew == null) {
					shopVisitorDayNew = new ShopVisitor();
					shopVisitorDayNew.setShopId(visitor.getShopId());
					shopVisitorDayNew.setType(VisitorType.NEW_VISITOR.getCode());
					shopVisitorDayNew.setTimeline(timeline);
					shopVisitorDayNew.setTypeCount(1);
					//shopVisitorDayNew.setDate(new Date);
				} else {
					shopVisitorDayNew.setShopId(visitor.getShopId());
					shopVisitorDayNew.setType(VisitorType.NEW_VISITOR.getCode());
					shopVisitorDayNew.setTimeline(timeline);
					shopVisitorDayNew.setTypeCount(shopVisitorDayNew.getTypeCount() + 1);
				}
				this.redisTemplate.opsForValue().set(keyShopNew, shopVisitorDayNew);
			}
			this.redisTemplate.opsForValue().set(keyTracking, 1);
			Date dateNew = DateUtils.addMinutes(visitor.getLastAccessed(), TIME_CAL_NEW_VISITOR);
			this.redisTemplate.expireAt(keyTracking, dateNew);
		} else if (this.redisTemplate.opsForValue().get(keyReturnTracking) == null) {
			
			for (String timeline : timelineList) {
				String timelineReturn = timeline + "_";
				switch (timeline) {
				case "HOUR":
					timelineReturn = timeline + LocalDateTime.now().getHour() + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "DATE":
					timelineReturn = timeline + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "WEEK":
					timelineReturn = timeline + getCurrentWeek() + LocalDateTime.now().getYear();
					break;
				case "MONTH":
					timelineReturn = timeline + LocalDateTime.now().getMonthValue()+ LocalDateTime.now().getYear();
					break;
				case "YEAR":
					timelineReturn = timeline + LocalDateTime.now().getYear();
					break;
				default:
					break;
				}
				String keyShopReturn = SHOP_KEY + visitor.getShopId() + RETURN_KEY + timelineReturn;
				ShopVisitor shopVisitorDayReturn = (ShopVisitor) this.redisTemplate.opsForValue().get(keyShopReturn);
				if (shopVisitorDayReturn == null) {
					shopVisitorDayReturn = new ShopVisitor();
					shopVisitorDayReturn.setShopId(visitor.getShopId());
					shopVisitorDayReturn.setType(VisitorType.RETURN_VISITOR.getCode());
					shopVisitorDayReturn.setTimeline(timeline);
					shopVisitorDayReturn.setTypeCount(1);
				//	shopVisitorDayReturn.setDate(date);
				} else {
					shopVisitorDayReturn.setShopId(visitor.getShopId());
					shopVisitorDayReturn.setType(VisitorType.RETURN_VISITOR.getCode());
					shopVisitorDayReturn.setTimeline(timeline);
					shopVisitorDayReturn.setTypeCount(shopVisitorDayReturn.getTypeCount() + 1);
				}
				this.redisTemplate.opsForValue().set(keyShopReturn, shopVisitorDayReturn);
		}
			this.redisTemplate.opsForValue().set(keyReturnTracking, 1);
			Date dateReturn = DateUtils.addMinutes(visitor.getLastAccessed(), TIME_CAL_RETURN_VISITOR);
			this.redisTemplate.expireAt(keyReturnTracking, dateReturn);
			}
		 session.setAttribute(LASTVIEW_ATTR, new Date());
	}
	
	/**
	 * Calculate new visitor and return visito
	 *
	 * @param visitor {@link Visitor}
	 * @param session {@link HttpSession}
	 */
	@SuppressWarnings("null")
	private void calNewAndReturnShopCollectionVistor(Visitor visitor, HttpSession session) {
		List<String> timelineList = new ArrayList<String>();
		for (Timeline timeline : Timeline.values()) { 
			timelineList.add(timeline.toString()); 
		}
		String keyNewTracking = SHOPTRACKING_KEY + visitor.getShopId() + COLLECTION_KEY + visitor.getCollectionId() + NEW_KEY + SESSION_ID_KEY + visitor.getId();
		String keyReturnTracking = SHOPTRACKING_KEY + visitor.getShopId() + COLLECTION_KEY + visitor.getCollectionId() + RETURN_KEY + SESSION_ID_KEY + visitor.getId();
		if (this.redisTemplate.opsForValue().get(keyNewTracking) == null) {
			for (String timeline : timelineList) {
				String timelineNew = timeline + "_";
				switch (timeline) {
				case "HOUR":
					timelineNew = timeline + LocalDateTime.now().getHour() + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "DATE":
					timelineNew = timeline + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "WEEK":
					timelineNew = timeline + getCurrentWeek() + LocalDateTime.now().getYear();
					break;
				case "MONTH":
					timelineNew = timeline + LocalDateTime.now().getMonthValue()+ LocalDateTime.now().getYear();
					break;
				case "YEAR":
					timelineNew = timeline + LocalDateTime.now().getYear();
					break;
				default:
					break;
				}
				String keyShopCollectionVisitorNew = SHOP_KEY + visitor.getShopId() + COLLECTION_KEY + visitor.getCollectionId() + NEW_KEY + timelineNew;
				ShopCollectionVisitor shopCollectionVisitorNew = (ShopCollectionVisitor) this.redisTemplate.opsForValue().get(keyShopCollectionVisitorNew);
				if (shopCollectionVisitorNew == null) {
					shopCollectionVisitorNew = new ShopCollectionVisitor();
					shopCollectionVisitorNew.setShopId(visitor.getShopId());
					shopCollectionVisitorNew.setType(VisitorType.NEW_VISITOR.getCode());
					shopCollectionVisitorNew.setTimeline(timeline);
					shopCollectionVisitorNew.setProductCollectionId(visitor.getCollectionId());
					shopCollectionVisitorNew.setTypeCount(1);
				} else {
					shopCollectionVisitorNew.setShopId(visitor.getShopId());
					shopCollectionVisitorNew.setType(VisitorType.NEW_VISITOR.getCode());
					shopCollectionVisitorNew.setTimeline(timeline);
					shopCollectionVisitorNew.setProductCollectionId(visitor.getCollectionId());
					shopCollectionVisitorNew.setTypeCount(shopCollectionVisitorNew.getTypeCount() + 1);
				}
				this.redisTemplate.opsForValue().set(keyShopCollectionVisitorNew, shopCollectionVisitorNew);
			}
			this.redisTemplate.opsForValue().set(keyNewTracking, 1);
			Date dateNew = DateUtils.addMinutes(visitor.getLastAccessed(), TIME_CAL_NEW_VISITOR);
			this.redisTemplate.expireAt(keyNewTracking, dateNew);
		} else if (this.redisTemplate.opsForValue().get(keyReturnTracking) == null) {
			for (String timeline : timelineList) {
				String timelineReturn = timeline + "_";
				switch (timeline) {
				case "HOUR":
					timelineReturn = timeline + LocalDateTime.now().getHour() + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "DATE":
					timelineReturn = timeline + LocalDateTime.now().getDayOfMonth() + LocalDateTime.now().getMonthValue() + LocalDateTime.now().getYear();
					break;
				case "WEEK":
					timelineReturn = timeline + getCurrentWeek() + LocalDateTime.now().getYear();
					break;
				case "MONTH":
					timelineReturn = timeline + LocalDateTime.now().getMonthValue()+ LocalDateTime.now().getYear();
					break;
				case "YEAR":
					timelineReturn = timeline + LocalDateTime.now().getYear();
					break;
				default:
					break;
				}
				String keyShopCollectionReturn = SHOP_KEY + visitor.getShopId() + COLLECTION_KEY + visitor.getCollectionId() + RETURN_KEY + timelineReturn;
				ShopCollectionVisitor shopCollectionVisitorReturn = (ShopCollectionVisitor) this.redisTemplate.opsForValue().get(keyShopCollectionReturn);
				if (shopCollectionVisitorReturn == null) {
					shopCollectionVisitorReturn = new ShopCollectionVisitor();
					shopCollectionVisitorReturn.setShopId(visitor.getShopId());
					shopCollectionVisitorReturn.setType(VisitorType.RETURN_VISITOR.getCode());
					shopCollectionVisitorReturn.setTimeline(timeline);
					shopCollectionVisitorReturn.setProductCollectionId(visitor.getCollectionId());
					shopCollectionVisitorReturn.setTypeCount(1);
				} else {
					shopCollectionVisitorReturn.setShopId(visitor.getShopId());
					shopCollectionVisitorReturn.setType(VisitorType.RETURN_VISITOR.getCode());
					shopCollectionVisitorReturn.setTimeline(timeline);
					shopCollectionVisitorReturn.setProductCollectionId(visitor.getCollectionId());
					shopCollectionVisitorReturn.setTypeCount(shopCollectionVisitorReturn.getTypeCount() + 1);
				}
				this.redisTemplate.opsForValue().set(keyShopCollectionReturn, shopCollectionVisitorReturn);
			}
			this.redisTemplate.opsForValue().set(keyReturnTracking, 1);
			Date dateReturn = DateUtils.addMinutes(visitor.getLastAccessed(), TIME_CAL_RETURN_VISITOR);
			this.redisTemplate.expireAt(keyReturnTracking, dateReturn);
		}
		 session.setAttribute(LASTVIEW_ATTR, new Date());
	}
	
	/**
	 * Run save data each a minute
	 */
	@Scheduled(cron = "0 0/1 * * * ?") 
	private void autoSaveVisitorData() {
		log.info("Run update visitor tracking");
		Set<String> redisKeys = redisTemplate.keys("shop:*");
		List<ShopVisitor> shopVisitors = new ArrayList<>();
		List<ShopCollectionVisitor> shopCollectionVisitors = new ArrayList<>();
		for (String key : redisKeys) {
			if (key.contains("collection")) {
				ShopCollectionVisitor collectionVisitor = (ShopCollectionVisitor) this.redisTemplate.opsForValue().get(key);
				shopCollectionVisitors.add(collectionVisitor);
			} else {
				ShopVisitor shopVisitor = (ShopVisitor) this.redisTemplate.opsForValue().get(key);
				shopVisitors.add(shopVisitor);
			}
			this.redisTemplate.delete(key);
		}
		// Set data for shop visitor
		List<bap.jp.smartfashion.common.model.ShopVisitor> sVisitors = new ArrayList<>();
		for (ShopVisitor shopVisitor : shopVisitors) {
			bap.jp.smartfashion.common.model.ShopVisitor sVisitor = new bap.jp.smartfashion.common.model.ShopVisitor();
			Optional<bap.jp.smartfashion.common.model.ShopVisitor> shopVisitorOptional = null;
			// set for hour
			if (shopVisitor.getTimeline().equals(Timeline.HOUR.toString())) {
				shopVisitorOptional = this.shopVisitorRepository.findAllByConditionForHour(shopVisitor.getShopId(),
						shopVisitor.getTimeline(), shopVisitor.getType(), LocalDateTime.now().getHour(), new Date());
				if (!shopVisitorOptional.isPresent()) {
					shopVisitorOptional = null;
				}
			} else if (shopVisitor.getTimeline().equals(Timeline.DAY.toString())) {
				shopVisitorOptional = this.shopVisitorRepository.findAllByConditionForDate(shopVisitor.getShopId(),
						shopVisitor.getTimeline(), shopVisitor.getType(), new Date());
				if (!shopVisitorOptional.isPresent()) {
					shopVisitorOptional = null;
				}
			} else if (shopVisitor.getTimeline().equals(Timeline.WEEK.toString())) {
				shopVisitorOptional = this.shopVisitorRepository.findAllByConditionForWeek(shopVisitor.getShopId(), 
						shopVisitor.getTimeline(), shopVisitor.getType(), getCurrentWeek());
				if (!shopVisitorOptional.isPresent()) {
					shopVisitorOptional = null;
				}
			} else if (shopVisitor.getTimeline().equals(Timeline.MONTH.toString())) {
				shopVisitorOptional = this.shopVisitorRepository.findAllByConditionForMonth(shopVisitor.getShopId(), 
						shopVisitor.getTimeline(), shopVisitor.getType(), LocalDateTime.now().getMonthValue(), LocalDate.now().getYear());
				if (!shopVisitorOptional.isPresent()) {
					shopVisitorOptional = null;
				}
			} else if (shopVisitor.getTimeline().equals(Timeline.YEAR.toString())) {
				shopVisitorOptional = this.shopVisitorRepository.findAllByConditionForYear(shopVisitor.getShopId(), 
						shopVisitor.getTimeline(), shopVisitor.getType(), LocalDateTime.now().getYear());
				if (!shopVisitorOptional.isPresent()) {
					shopVisitorOptional = null;
				}
			} 
			
			if (shopVisitorOptional != null) {
				sVisitor = shopVisitorOptional.get();
				Integer count = sVisitor.getTypeCount() + shopVisitor.getTypeCount();
				sVisitor.setTypeCount(count);
			} else {
				sVisitor.setShop(new Shop(shopVisitor.getShopId()));
				sVisitor.setType(shopVisitor.getType());
				sVisitor.setTypeCount(1);
				sVisitor.setTimeline(shopVisitor.getTimeline());
			}
			sVisitors.add(sVisitor);
		}
		
		// Set data for shop visitor
		List<bap.jp.smartfashion.common.model.ShopCollectionVisitor> sCollectionVisitors = new ArrayList<>();
		for (ShopCollectionVisitor shopCollectionVisitor : shopCollectionVisitors) {
			bap.jp.smartfashion.common.model.ShopCollectionVisitor sCollectionVisitor = new bap.jp.smartfashion.common.model.ShopCollectionVisitor();
			Optional<bap.jp.smartfashion.common.model.ShopCollectionVisitor> sCollectionVisitorOptional = null;
			// set for hour
			if (shopCollectionVisitor.getTimeline().equals(Timeline.HOUR.toString())) {
				sCollectionVisitorOptional = this.shopCollectionVisitorRepository.findAllByConditionForHour(shopCollectionVisitor.getShopId(),shopCollectionVisitor.getProductCollectionId(),
						shopCollectionVisitor.getTimeline(), shopCollectionVisitor.getType(), LocalDateTime.now().getHour(), new Date());
				if (!sCollectionVisitorOptional.isPresent()) {
					sCollectionVisitorOptional = null;
				}
			} else if (shopCollectionVisitor.getTimeline().equals(Timeline.DAY.toString())) {
				sCollectionVisitorOptional = this.shopCollectionVisitorRepository.findAllByConditionForDate(shopCollectionVisitor.getShopId(),shopCollectionVisitor.getProductCollectionId(),
						shopCollectionVisitor.getTimeline(), shopCollectionVisitor.getType(), new Date());
				if (!sCollectionVisitorOptional.isPresent()) {
					sCollectionVisitorOptional = null;
				}
			} else if (shopCollectionVisitor.getTimeline().equals(Timeline.WEEK.toString())) {
				sCollectionVisitorOptional = this.shopCollectionVisitorRepository.findAllByConditionForWeek(shopCollectionVisitor.getShopId(),shopCollectionVisitor.getProductCollectionId(),
						shopCollectionVisitor.getTimeline(), shopCollectionVisitor.getType(), getCurrentWeek());
				if (!sCollectionVisitorOptional.isPresent()) {
					sCollectionVisitorOptional = null;
				}
			} else if (shopCollectionVisitor.getTimeline().equals(Timeline.MONTH.toString())) {
				sCollectionVisitorOptional = this.shopCollectionVisitorRepository.findAllByConditionForMonth(shopCollectionVisitor.getShopId(),shopCollectionVisitor.getProductCollectionId(),
						shopCollectionVisitor.getTimeline(), shopCollectionVisitor.getType(), LocalDateTime.now().getMonthValue(), LocalDate.now().getYear());
				if (!sCollectionVisitorOptional.isPresent()) {
					sCollectionVisitorOptional = null;
				}
			} else if (shopCollectionVisitor.getTimeline().equals(Timeline.YEAR.toString())) {
				sCollectionVisitorOptional = this.shopCollectionVisitorRepository.findAllByConditionForYear(shopCollectionVisitor.getShopId(),shopCollectionVisitor.getProductCollectionId(), 
						shopCollectionVisitor.getTimeline(), shopCollectionVisitor.getType(), LocalDateTime.now().getYear());
				if (!sCollectionVisitorOptional.isPresent()) {
					sCollectionVisitorOptional = null;
				}
			} 
			
			if (sCollectionVisitorOptional != null) {
				sCollectionVisitor = sCollectionVisitorOptional.get();
				Integer count = sCollectionVisitor.getTypeCount() + sCollectionVisitor.getTypeCount();
				sCollectionVisitor.setTypeCount(count);
			} else {
				sCollectionVisitor.setShop(new Shop(shopCollectionVisitor.getShopId()));
				sCollectionVisitor.setProductCollection(new ProductCollection(shopCollectionVisitor.getProductCollectionId()));
				sCollectionVisitor.setType(shopCollectionVisitor.getType());
				sCollectionVisitor.setTypeCount(1);
				sCollectionVisitor.setTimeline(shopCollectionVisitor.getTimeline());
			}
			sCollectionVisitors.add(sCollectionVisitor);
		}
		this.shopVisitorRepository.saveAll(sVisitors);
		this.shopCollectionVisitorRepository.saveAll(sCollectionVisitors);
		log.info("End - Update visitors tracking");
	}
	
	/**
	 * Get current week of year
	 * @return week of year
	 */
	private Integer getCurrentWeek() {
		LocalDate date = LocalDate.now();
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		int week = date.get(weekFields.weekOfWeekBasedYear())-1;
		return Integer.valueOf(date.getYear() + StringUtils.EMPTY + week);
	}
}
