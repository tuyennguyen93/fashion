package bap.jp.smartfashion.common.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductSizeChart;

/**
 * Spring Data JPA repository for the {@link ProductSizeChart} entity.
 */
@Repository
public interface ProductSizeChartRepository extends JpaRepository<ProductSizeChart, Integer> {
	Optional<ProductSizeChart> findOneByName(String name);
}