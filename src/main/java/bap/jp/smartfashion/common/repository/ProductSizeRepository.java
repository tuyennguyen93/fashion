package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductSize;

/**
 * Spring Data JPA repository for the {@link ProductSize} entity.
 */
@Repository
public interface ProductSizeRepository extends JpaRepository<ProductSize, Integer> {
	List<ProductSize> findAllByTypeOrderById(String sizeType);
}