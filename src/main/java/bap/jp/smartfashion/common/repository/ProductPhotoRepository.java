package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductPhoto;

/**
 * Spring Data JPA repository for the {@link ProductPhoto} entity.
 */
@Repository
public interface ProductPhotoRepository extends JpaRepository<ProductPhoto, Integer> {
	@Modifying
	@Query("DELETE FROM ProductPhoto productPhoto where productPhoto.id in ?1")
	void deleteProductPhotoWithIds(List<Integer> ids);

	List<ProductPhoto> findAllByProductDetail_Id(Integer id);
	
	@Query( value = "SELECT *"
			+ " FROM product_photo productPhoto"
			+ " INNER JOIN product_detail productDetail"
			+ " ON productDetail.id = productPhoto.product_detail_id"
			+ " WHERE productDetail.id = :productDetailId"
			+ " ORDER BY productPhoto.id"
			+ " LIMIT 1", nativeQuery = true)
	Optional<ProductPhoto> findTop1ProductPhotoByProductDetailId(@Param("productDetailId") Integer productDetailId);
}
