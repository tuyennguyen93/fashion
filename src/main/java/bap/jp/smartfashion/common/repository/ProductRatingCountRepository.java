package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingCount;

/**
 * Spring Data JPA repository for the {@link ProductRatingCount} entity.
 */
@Repository
public interface ProductRatingCountRepository extends JpaRepository<ProductRatingCount, Integer> {
	Optional<ProductRatingCount> findByProduct_IdAndRatingStar(Integer productId, Integer ratingStar);
	
	List<ProductRatingCount> findAllByProduct_Id(Integer productId);
}