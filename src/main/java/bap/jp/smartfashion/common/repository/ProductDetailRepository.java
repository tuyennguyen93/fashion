package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductDetail;

/**
 * Spring Data JPA repository for the {@link ProductDetail} entity.
 */
@Repository
public interface ProductDetailRepository extends JpaRepository<ProductDetail, Integer> {
	List<ProductDetail> findAllByProduct_Id(Integer id);
	
	@Modifying
	@Query("DELETE FROM ProductDetail productDetail where productDetail.id in ?1")
	void deleteProductDetailWithIds(List<Integer> ids);

	@Query("SELECT productPhoto from ProductDetail productDetail "
			+ " join fetch ProductPhoto productPhoto "
			+ " ON productDetail.id = productPhoto.productDetail.id"

			+ " WHERE productDetail.id = :id")
	List<ProductDetail> findAllByProduct_IdQuery(@Param("id") Integer id);
	
	@Query( value = "SELECT *"
			+ " FROM product_detail productDetail"
			+ " INNER JOIN product product"
			+ " ON product.id = productDetail.product_id AND product.status = 'ACTIVE'"
			+ " WHERE product.id = :productId"
			+ " ORDER BY productDetail.id"
			+ " LIMIT 1", nativeQuery = true)
	Optional<ProductDetail> findTop1ProductDetailByProductId(@Param("productId") Integer productId);
}