package bap.jp.smartfashion.common.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductCollection;

/**
 * Spring Data JPA repository for the {@link ProductCollection} entity.
 */
@Repository
public interface ProductCollectionRepository extends JpaRepository<ProductCollection, Integer> {
	Optional<ProductCollection> findOneByName(String name);
}