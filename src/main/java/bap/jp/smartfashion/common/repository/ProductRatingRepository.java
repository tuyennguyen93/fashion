package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import bap.jp.smartfashion.enums.ProductEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductRating;

/**
 * Spring Data JPA repository for the {@link ProductRating} entity.
 */
@Repository
public interface ProductRatingRepository extends JpaRepository<ProductRating, Integer> {

	@Query("SELECT productRating "
			+ "FROM ProductRating productRating "
			+ "INNER JOIN Product product ON productRating.product.id = product.id "
			+ "WHERE productRating.product.id = :productId "
			+ "AND product.status = :status")
	Page<ProductRating> findAllByProduct_IdAndStatus(@Param("productId") Integer productId,
			@Param("status") String status, Pageable pageable);

	@Query("SELECT productRating "
			+ "FROM ProductRating productRating "
			+ "INNER JOIN Product product ON productRating.product.id = product.id "
			+ "WHERE productRating.id = :id "
			+ "AND productRating.product.id = :productId "
			+ "AND product.status = :status")
	Optional<ProductRating> findByIdAndProduct_IdAndStatus(@Param("id") Integer id,
			@Param("productId") Integer productId, @Param("status") String status);

	@Query("SELECT productRating "
			+ "FROM ProductRating productRating "
			+ "INNER JOIN Product product ON productRating.product.id = product.id "
			+ "WHERE productRating.product.id = :productId "
			+ "AND productRating.user.id = :userId "
			+ "AND product.status = :status")
	Optional<ProductRating> findByProduct_IdAndUser_IdAndStatus(@Param("productId") Integer productId ,
			@Param("userId") Integer userId, @Param("status") String status);

	List<ProductRating> findByProduct_IdOrderByCreatedDesc(Integer productId);

}