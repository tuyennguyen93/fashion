package bap.jp.smartfashion.common.repository;

import java.util.Optional;

import bap.jp.smartfashion.api.admin.user_admin.dto.UserAdminDTO;
import bap.jp.smartfashion.api.admin.user_admin.dto.UserEmailDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.api.admin.user.dto.AdminUserStatus;
import bap.jp.smartfashion.common.model.User;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

	@Query("SELECT user "
			+ " FROM User user"
			+ " INNER JOIN UserRole userRole"
			+ " ON user.id = userRole.userId"
			+ " WHERE (user.email = :userName OR user.phone = :userName)"
			+ " AND user.deleted = 0"
			+ " AND userRole.roleName = :roleType"
			+ " AND user.userType = :userType")
	Optional<User> findUserLoginWithLocal(@Param("userName") String userName,
			@Param("userType") String userType,
			@Param("roleType") String roleType);

	@Query("SELECT user"
			+ " FROM User user"
			+ " WHERE (user.uid = :uid AND user.userType = :userType)"
			+ " AND user.deleted = 0")
	Optional<User> findUserLoginWithSNS(@Param("uid") String uid, @Param("userType") String userType);

	Optional<User> findOneByUidAndUserType(String uid, String userType);

	Optional<User> findOneByEmailAndUserType(String email, String userType);

	Optional<User> findOneByPhone(String phone);

	Optional<User> findOneByEmailOrPhone(String email, String phone);

	Optional<User> findByIdAndDeletedIsFalse(int id);

	Page<User> findAllByDeletedIsFalse(Pageable pageable);

	@Query( value = "SELECT *"
			+ " FROM user user"
			+ " WHERE (CASE "
			+ " 	WHEN :status = 'ACTIVE' THEN (user.register_status = 1 AND user.deleted = 0)"
			+ " 	WHEN :status = 'PENDING' THEN (user.register_status = 0 AND user.deleted = 0)"
			+ " 	WHEN :status = 'DELETED' THEN user.deleted = 1"
			+ "		ELSE 1=1"
			+ " 	END)", nativeQuery = true)
	Page<User> findAllByStatus(@Param("status") String status, Pageable pageable);
	
	@Query( value = "SELECT *"
			+ " FROM user user"
			+ " WHERE (user.email LIKE %:query%"
			+ " OR user.name LIKE %:query%)"
			+ " AND (CASE "
			+ "		WHEN :status = 'ACTIVE' THEN (user.register_status = 1 AND user.deleted = 0)"
			+ " 	WHEN :status = 'PENDING' THEN (user.register_status = 0 AND user.deleted = 0)"
			+ " 	WHEN :status = 'DELETED' THEN user.deleted = 1"
			+ "		ELSE 1=1"
			+ " END)", nativeQuery = true)
	Page<User> findAllByQuery(@Param("query") String query, @Param("status") String status, Pageable pageable);

	Optional<User> findOneByTaxNumberAndUserType(String taxNumber, String userType);

	Optional<User> findOneByPassportNumberAndUserType(String taxNumber, String userType);

	Optional<User> findByNameOrEmailAndUserTypeAndRegisterStatusIsTrue(String name, String email, String userType);

	Optional<User> findByIdAndRegisterStatusIsTrueAndBlockedIsFalseAndDeletedIsFalse(Integer userId);
	
	@Query(value = ("SELECT "
			+ " COUNT(*) as allStatus,"
			+ " COUNT(CASE WHEN user.register_status AND !user.deleted THEN 1 END) as activeStatus,"
			+ " COUNT(CASE WHEN !user.register_status AND !user.deleted THEN 1 END) as pendingStatus,"
			+ " COUNT(CASE WHEN user.deleted THEN 1 END) as deletedStatus"
			+ " FROM user user"), nativeQuery = true)
	AdminUserStatus findAllStatusUser();

	@Query(value = "SELECT user"
			+ " FROM User user"
			+ " WHERE user.id = :id AND (user.registerStatus = 1 OR user.registerStatus = 0)")
	Optional<User> findByIdForAdmin(@Param("id") Integer id);

	@Query(value = "select DISTINCT new bap.jp.smartfashion.api.admin.user_admin.dto.UserAdminDTO(user.id, user.name, user.email, user.gender, userRole.modified)"
			+ " FROM User user INNER JOIN UserRole userRole ON user.id = userRole.userId"
			+ " WHERE userRole.roleName = 'ROLE_ADMIN' AND user.id NOT IN"
			+ " (   select userRole.userId"
			+ " FROM UserRole userRole INNER JOIN Role role ON userRole.roleName = role.name"
			+ " WHERE role.name = 'ROLE_SUPER_ADMIN')"
			+ " AND user.id IN"
			+ " (   SELECT userRole.userId"
			+ " FROM UserRole userRole INNER JOIN Role role ON userRole.roleName = role.name"
			+ " WHERE role.name = 'ROLE_ADMIN')"
			+ " AND (user.name LIKE %:query% OR user.email LIKE %:query%)"
			+ " AND user.userType = :userType"
			+ " ORDER BY userRole.modified DESC")
	Page<UserAdminDTO> findAllByConditionsForSuperAdmin(@Param("query") String query, @Param("userType") String userType, Pageable pageable);

	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.user_admin.dto.UserEmailDTO(user.id, user.name, user.email)"
			+ " FROM User user"
			+ " WHERE user.registerStatus = 1"
			+ " AND user.blocked <> 1"
			+ " AND user.deleted <> 1"
			+ " AND user.userType = :userType"
			+ " AND user.id NOT IN"
			+ " (   SELECT userRole.userId"
			+ " FROM UserRole userRole INNER JOIN Role role ON userRole.roleName = role.name"
			+ " WHERE role.name = 'ROLE_ADMIN')"
			+ " AND user.email LIKE %:query%")
	Page<UserEmailDTO> findAllByEmailInfo(@Param("query") String query, @Param("userType") String userType, Pageable pageable);
}
