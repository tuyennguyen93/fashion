package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ProductRatingInteraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRatingInteractionRepository extends JpaRepository<ProductRatingInteraction, Integer> {
    Optional<ProductRatingInteraction> findByProductRating_IdAndUser_IdAndType(Integer id, Integer userId, String type);
}
