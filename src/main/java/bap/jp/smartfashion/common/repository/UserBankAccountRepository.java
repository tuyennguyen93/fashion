package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.UserBankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link bap.jp.smartfashion.common.model.UserBankAccount} entity.
 */
@Repository
public interface UserBankAccountRepository extends JpaRepository<UserBankAccount, Integer> {
    Optional<UserBankAccount> findOneByUser_Id(int userId);

}