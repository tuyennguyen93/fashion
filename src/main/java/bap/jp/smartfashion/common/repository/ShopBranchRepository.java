package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.ShopBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link bap.jp.smartfashion.common.model.ShopBranch} entity.
 */
@Repository
public interface ShopBranchRepository extends JpaRepository<ShopBranch,Integer> {
    List<ShopBranch> findByShopId(int shopId);
}
