package bap.jp.smartfashion.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.Role;

/**
 * Spring Data JPA repository for the {@link Role} entity.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
	List<Role> findByNameIn(List<String> roleGroups);
}
