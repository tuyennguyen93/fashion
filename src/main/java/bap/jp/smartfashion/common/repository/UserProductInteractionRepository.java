package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.api.admin.user.dto.ProductInteractionDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import bap.jp.smartfashion.common.model.UserProductInteraction;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link UserProductInteraction} entity.
 */
@Repository
public interface UserProductInteractionRepository extends JpaRepository<UserProductInteraction, Integer> {

	Optional<UserProductInteraction> findByUser_IdAndProduct_IdAndActionCode(Integer userId, Integer productId,
			String actionCode);

	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.user.dto.ProductInteractionDTO(interaction.actionCode, SUM(interaction.actionCount) AS actionCount)"
			+ " FROM UserProductInteraction  interaction"
			+ " WHERE interaction.user.id = :userId"
			+ " GROUP BY interaction.user.id, interaction.actionCode")
	List<ProductInteractionDTO> findAllByUser_IdAndStatus(@Param("userId") Integer userId);
	
	List<UserProductInteraction> findByUser_IdAndProduct_Id(Integer userId, Integer productId);
}
