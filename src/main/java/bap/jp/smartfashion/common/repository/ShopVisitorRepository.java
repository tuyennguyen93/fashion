package bap.jp.smartfashion.common.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ShopVisitor;

/**
 * Spring Data JPA repository for the {@link ShopVisitor} entity.
 */
@Repository
public interface ShopVisitorRepository extends JpaRepository<ShopVisitor,Integer> {
	
	@Query("SELECT shopVisitor"
			+ " FROM ShopVisitor shopVisitor"
			+ " WHERE shopVisitor.shop.id = :shopId"
			+ " AND shopVisitor.type = :type"
			+ " AND shopVisitor.timeline = :timeline"
			+ " AND HOUR(shopVisitor.created) = :hour"
			+ " AND DATE(shopVisitor.created) = :date")
	Optional<ShopVisitor> findAllByConditionForHour(@Param("shopId") Integer shopId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("hour") Integer hour, @Param("date") Date date);
	
	@Query("SELECT shopVisitor"
			+ " FROM ShopVisitor shopVisitor"
			+ " WHERE shopVisitor.shop.id = :shopId"
			+ " AND shopVisitor.type = :type"
			+ " AND shopVisitor.timeline = :timeline"
			+ " AND YEARWEEK(shopVisitor.created) = :yearweek")
	Optional<ShopVisitor> findAllByConditionForWeek(@Param("shopId") Integer shopId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("yearweek") Integer yearweek);
	
	@Query("SELECT shopVisitor"
			+ " FROM ShopVisitor shopVisitor"
			+ " WHERE shopVisitor.shop.id = :shopId"
			+ " AND shopVisitor.type = :type"
			+ " AND shopVisitor.timeline = :timeline"
			+ " AND DATE(shopVisitor.created) = :date")
	Optional<ShopVisitor> findAllByConditionForDate(@Param("shopId") Integer shopId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("date") Date date);
	
	@Query("SELECT shopVisitor"
			+ " FROM ShopVisitor shopVisitor"
			+ " WHERE shopVisitor.shop.id = :shopId"
			+ " AND shopVisitor.type = :type"
			+ " AND shopVisitor.timeline = :timeline"
			+ " AND MONTH(shopVisitor.created) = :month"
			+ " AND YEAR(shopVisitor.created) = :year")
	Optional<ShopVisitor> findAllByConditionForMonth(@Param("shopId") Integer shopId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("month") Integer month, @Param("year") Integer year);
	
	
	@Query("SELECT shopVisitor"
			+ " FROM ShopVisitor shopVisitor"
			+ " WHERE shopVisitor.shop.id = :shopId"
			+ " AND shopVisitor.type = :type"
			+ " AND shopVisitor.timeline = :timeline"
			+ " AND YEAR(shopVisitor.created) = :year")
	Optional<ShopVisitor> findAllByConditionForYear(@Param("shopId") Integer shopId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("year") Integer year);


	@Query("SELECT shopVisitor "
			+ " FROM ShopVisitor shopVisitor "
			+ " WHERE shopVisitor.shop.id = :shopId"
			+ " AND shopVisitor.type = :type"
			+ " AND shopVisitor.timeline = :timeline"
			+ " AND shopVisitor.created BETWEEN DATE(:startDate) AND DATE(:endDate)")
	List<ShopVisitor> findNewvisitorBetweenForShop(@Param("shopId") Integer shopId,
			@Param("timeline") String timeline, @Param("type") String type, 
			@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);


	@Query("SELECT shopVisitor "
			+ "FROM ShopVisitor shopVisitor "
			+ "WHERE shopVisitor.shop.id = :shopId "
			+ "AND shopVisitor.type = :type "
			+ "AND shopVisitor.timeline = :timeline "
			+ "AND DATE(created) BETWEEN DATE(:startDay) and DATE(:endDay)")
	Page<ShopVisitor> findAllByConditionsAndHour_Day(@Param("shopId") Integer shopId, @Param("type") String type,
			 @Param("timeline") String timeline, @Param("startDay") String startDay, @Param("endDay") String endDay,
			 Pageable pageable);


	@Query("SELECT shopVisitor "
			+ "FROM ShopVisitor shopVisitor "
			+ "WHERE shopVisitor.shop.id = :shopId "
			+ "AND shopVisitor.type = :type "
			+ "AND shopVisitor.timeline = :timeline "
			+ "AND YEARWEEK(shopVisitor.created) BETWEEN YEARWEEK(:startDay) AND YEARWEEK(:endDay)")
	Page<ShopVisitor> findAllByConditionsAndWeek(@Param("shopId") Integer shopId, @Param("type") String type,
			@Param("timeline") String timeline, @Param("startDay") LocalDate startDay, @Param("endDay") LocalDate endDay,
			Pageable pageable);


	@Query("SELECT shopVisitor "
			+ "FROM ShopVisitor shopVisitor "
			+ "WHERE shopVisitor.shop.id = :shopId "
			+ "AND shopVisitor.type = :type "
			+ "AND shopVisitor.timeline = :timeline "
			+ "AND (MONTH(shopVisitor.created) BETWEEN MONTH(:startDay) AND MONTH(:endDay)) "
			+ "AND (YEAR(shopVisitor.created) BETWEEN YEAR(:startDay) AND YEAR(:endDay))")
	Page<ShopVisitor> findAllByConditionsAndMonth(@Param("shopId") Integer shopId, @Param("type") String type,
			@Param("timeline") String timeline, @Param("startDay") LocalDate startDay, @Param("endDay") LocalDate endDay,
			Pageable pageable);


	@Query("SELECT shopVisitor "
			+ "FROM ShopVisitor shopVisitor "
			+ "WHERE shopVisitor.shop.id = :shopId "
			+ "AND shopVisitor.type = :type "
			+ "AND shopVisitor.timeline = :timeline "
			+ "AND YEAR(shopVisitor.created) BETWEEN YEAR(:startDay) AND YEAR(:endDay)")
	Page<ShopVisitor> findAllByConditionsAndYear(@Param("shopId") Integer shopId, @Param("type") String type,
			@Param("timeline") String timeline, @Param("startDay") LocalDate startDay, @Param("endDay") LocalDate endDay,
			Pageable pageable);
}
