package bap.jp.smartfashion.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.UserProductBasket;

/**
 * Spring Data JPA repository for the {@link UserProductBasket} entity.
 */
@Repository
public interface UserProductBasketRepository extends JpaRepository<UserProductBasket, Integer> {
}
