package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserProduct;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the {@link UserModelCollection} entity.
 */
@Repository
public interface UserModelCollectionRepository extends JpaRepository<UserModelCollection, Integer> {
}
