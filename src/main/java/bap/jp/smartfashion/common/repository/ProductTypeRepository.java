package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductType;

/**
 * Spring Data JPA repository for the {@link ProductType} entity.
 */
@Repository
public interface ProductTypeRepository extends JpaRepository<ProductType, Integer> {
	List<ProductType> findAllByProductTypeParentIdIn(List<Integer> productTypeParentIds);

	List<ProductType> findAllByProductCollection_Id(Integer collectionId);

	Optional<ProductType> findByProductTypeParentId(Integer ProductTypeParentId);
	
	List<ProductType> findAllByProductTypeParentId(Integer productTypeParentId); 
	
	Optional<ProductType> findByIdAndProductCollection_Id(Integer id, Integer collectionId);
}