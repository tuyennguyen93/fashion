package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeTypeDTO;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.api.admin.user.dto.ModelCollection;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserModelRepository extends JpaRepository<UserModel, Integer> {

	Page<UserModel> findAllByUser_IdOrUser_IdIsNull(Integer userId, Pageable pageable);
	
	@Query(value = "SELECT userModel"
			+ " FROM UserModel userModel"
			+ " LEFT JOIN User user"
			+ " ON userModel.user.id = user.id"
			+ " LEFT JOIN UserModelCollection userModelCollection"
			+ " ON userModelCollection.id = userModel.userModelCollection.id"
			+ " WHERE userModelCollection.id = :userModelCollection"
			+ " AND (user.id = :userId OR user.id IS NULL)")
	Page<UserModel> findAllByUser_IdAndUserModelCollection_IdOrUser_IdIsNull(
			@Param(value = "userId") Integer userId, 
			@Param(value = "userModelCollection") Integer userModelCollectionId, Pageable pageable);
	
	Optional<UserModel> findByIdAndUser_Id(Integer id, Integer userId);
	
	@Query(value = "SELECT userModel"
			+ " FROM UserModel userModel"
			+ " LEFT JOIN User user"
			+ " ON userModel.user.id = user.id"
			+ " WHERE userModel.id = :id"
			+ " AND (user.id = :userId OR user.id IS NULL)")
	Optional<UserModel> findByIdAndUser_IdOrUser_IdIsNull(@Param(value="id") Integer id, @Param(value="userId")  Integer userId);
	
	long countByUser_Id(int userId);
	
	long countByUser_IdIsNotNull();
	
	@Query(value = "SELECT userModel"
			+ " FROM UserModel userModel"
			+ " LEFT JOIN FETCH User user"
			+ " ON user.id = userModel.user.id"
			+ " JOIN FETCH UserModelCollection userModelCollection"
			+ " ON userModel.userModelCollection.id = userModelCollection.id"
			+ " WHERE userModelCollection.id = :collectionId AND (user.name LIKE %:name% OR user.email LIKE %:email%)")
	Page<UserModel> findAllByUserModelCollection_IdAndUser_NameContainingOrUser_EmailContaining(
			@Param(value = "collectionId") Integer collectionId, 
			@Param(value = "name") String name,
			@Param(value = "email") String email,
			Pageable pageable);
	
	Page<UserModel> findAllByUser_NameContainingOrUser_EmailContainingAndUser_IdIsNotNull(String name, String email, Pageable pageable);
	
	
	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeTypeDTO(umc.id, umc.name, COUNT(umc.id))"
			+ " FROM UserModel um"
			+ " LEFT JOIN UserModelCollection umc"
			+ " ON um.userModelCollection.id = umc.id"
			+ " WHERE um.user.id IS NOT NULL"
			+ " GROUP BY umc.id")
	List<AdminWardrobeTypeDTO> countUserModelCollection();

	@Query(value = "SELECT userModel"
			+ " FROM UserModelSetting userModelSetting"
			+ " INNER JOIN UserModel userModel"
			+ " ON (userModelSetting.userModel.id= userModel.id)"
			+ " WHERE userModelSetting.user.id=:userId")
	Optional<UserModel> findUserModelDefaultForUser(@Param(value="userId") Integer userId);

	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.user.dto.ModelCollection("
			+ " userModel.userModelCollection.id AS collectionId, COUNT(userModel.userModelCollection.id) AS amount)"
			+ " FROM UserModel userModel"
			+ " WHERE userModel.user.id = :userId"
			+ " GROUP BY userModel.userModelCollection.id")
	List<ModelCollection> findAllByUser_Id(@Param("userId") Integer userId);

	@Query(value = "SELECT userModel"
			+ " FROM UserModel userModel"
			+ " WHERE (userModel.user.id = :userId)"
			+ " AND (:collectionId = -1 OR userModel.userModelCollection.id = :collectionId)")
	Page<UserModel> findAllByUserIdAndAndUserModelCollection_Id(@Param("userId") Integer userId, @Param("collectionId") Integer collectionId, Pageable pageable);

}
