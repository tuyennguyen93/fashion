package bap.jp.smartfashion.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductRatingReply;

import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link ProductRatingReply} entity.
 */
@Repository
public interface ProductRatingReplyRepository extends JpaRepository<ProductRatingReply, Integer> {

    Optional<ProductRatingReply> findByIdAndUser_Id(Integer productRatingReply, Integer userId);
}