package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.UserModelSetting;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserModelSettingRepository extends JpaRepository<UserModelSetting, Integer> {
	
	Optional<UserModelSetting> findByUser_Id(Integer userId);
	
	Optional<UserModelSetting> findByUser_IdAndUserModel_Id(Integer userId, Integer userModelId);
}
