package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ShopProductTryOn;

/**
 * Spring Data JPA repository for the {@link ShopProductTryOn} entity.
 */
@Repository
public interface ShopProductTryOnRepository extends JpaRepository<ShopProductTryOn, Integer> {
	Optional<ShopProductTryOn> findByUser_IdAndUserModel_IdAndProductDetail_Id(Integer userId, Integer userModelId, Integer productDetailId);
	
	@Modifying
	@Query(value = "DELETE FROM ShopProductTryOn shopProductTryOn WHERE shopProductTryOn.productDetail.id in ?1")
	void deleteByProductDetail_Id(Integer productDetailId);
}
