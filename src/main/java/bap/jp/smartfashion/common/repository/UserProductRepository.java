package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeTypeDTO;
import bap.jp.smartfashion.api.admin.user.dto.ProductCollection;
import bap.jp.smartfashion.common.model.UserProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link UserProduct} entity.
 */
@Repository
public interface UserProductRepository extends JpaRepository<UserProduct, Integer> {
	
	@Query(value = "SELECT userProduct"
			+ " FROM UserProduct userProduct"
			+ " WHERE userProduct.user.id = :userId "
			+ " ORDER BY rand()")
	Page<UserProduct> findUserProductRandomByUser_Id(@Param(value = "userId") Integer userId, Pageable pageable);
	
	Page<UserProduct> findAllByUser_Id(Integer userId, Pageable pageable);

	Page<UserProduct> findAllByUser_IdAndUserProductCollection_Id(Integer userId, Integer userModelCollectionId, Pageable pageable);

	Optional<UserProduct> findByIdAndUser_Id(Integer id, Integer userId);
	
	int countByUser_IdAndUserProductCollection_Id(Integer userId, Integer userProductCollectionId);
	
	int countByUser_Id(int userId);
	
	@Query(value = "SELECT userProduct"
			+ " FROM UserProduct userProduct"
			+ " JOIN FETCH User user"
			+ " ON user.id = userProduct.user.id"
			+ " JOIN FETCH UserProductCollection userProductCollection"
			+ " ON userProduct.userProductCollection.id = userProductCollection.id"
			+ " WHERE userProductCollection.id = :collectionId AND (user.name LIKE %:name% OR user.name LIKE %:email%)")
	Page<UserProduct> findAllByUserProductCollection_IdAndUser_NameContainingOrUser_EmailContaining(
			@Param(value = "collectionId") Integer collectionId, 
			@Param(value = "name") String name,
			@Param(value = "email") String email,
			Pageable pageable);
	
	Page<UserProduct> findAllByUser_NameContainingOrUser_EmailContaining(String name, String email, Pageable pageable);
	
	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.wardrobe.dto.AdminWardrobeTypeDTO(upc.id, upc.name, COUNT(upc.id))"
		+ " FROM UserProduct up"
		+ " LEFT JOIN UserProductCollection upc"
		+ " ON up.userProductCollection.id = upc.id" 
		+ " GROUP BY upc.id")
	List<AdminWardrobeTypeDTO> countUserProductCollection();

	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.user.dto.ProductCollection(userProduct.userProductCollection.id AS collectionId, COUNT(userProduct.userProductCollection.id) AS amount)"
			+ " FROM UserProduct userProduct"
			+ " WHERE userProduct.user.id = :userId"
			+ " GROUP BY userProduct.userProductCollection.id")
	List<ProductCollection> findProductCollectionByUser_Id(@Param("userId") Integer userId);

	@Query(value = "SELECT userProduct"
			+ " FROM UserProduct userProduct"
			+ " WHERE userProduct.user.id = :userId AND (:collectionId = -1 OR userProduct.userProductCollection.id = :collectionId)")
	Page<UserProduct> findAllByUserIdAndAndUserProductCollection_Id(@Param("userId") Integer userId, @Param("collectionId") Integer collectionId, Pageable pageable);

}
