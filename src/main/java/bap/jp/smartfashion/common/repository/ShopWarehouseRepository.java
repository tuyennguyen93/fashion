package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ShopWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ShopWarehouseRepository extends JpaRepository<ShopWarehouse, Integer> {

    Optional<List<ShopWarehouse>> findAllByShop_Id(Integer shopId);
}
