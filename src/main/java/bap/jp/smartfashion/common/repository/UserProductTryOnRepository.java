package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.UserProductTryOn;

/**
 * Spring Data JPA repository for the {@link UserProductTryOn} entity.
 */
@Repository
public interface UserProductTryOnRepository extends JpaRepository<UserProductTryOn, Integer> {
	
	Optional<UserProductTryOn> findByUser_IdAndUserModel_IdAndUserProduct_Id(Integer userId, Integer userModelId, Integer userProductId);
	
	@Modifying
	@Query(value = "DELETE FROM UserProductTryOn userProductTryOn WHERE userProductTryOn.userProduct.id = :userProductId")
	void deleteByUserProduct_Id(@Param("userProductId") Integer userProductId);
	
	@Modifying
	@Query(value = "DELETE FROM UserProductTryOn userProductTryOn WHERE userProductTryOn.userModel.id = :userModelId")
	void deleteByUserModel_Id(@Param("userModelId") Integer userModelId);
}
