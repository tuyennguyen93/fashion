package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ProductMixMatchPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data JPA repository for the {@link ProductMixMatchPhoto} entity.
 */
@Repository
public interface ProductMixMatchPhotoRepository extends JpaRepository<ProductMixMatchPhoto, Integer> {
	@Modifying
	@Query(value = "DELETE FROM ProductMixMatchPhoto productMixMatchPhoto WHERE productMixMatchPhoto.productMixMatch.id in ?1")
	void deleteAllByProductMixMatch_Id(List<Integer> prodcuctMixMatchIds);
	
	@Modifying
	@Query(value = "UPDATE product_mix_match_photo productMixMatchPhoto"
			+ " INNER JOIN product_mix_match productMixMatch"
			+ " ON productMixMatchPhoto.product_mix_match_id = productMixMatch.id"
			+ " SET productMixMatchPhoto.url = :url"
			+ " WHERE productMixMatch.product_id = :productId", nativeQuery = true)
	void updateByProduct_Id(@Param("productId") Integer productId, @Param("url") String url);
}
