package bap.jp.smartfashion.common.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import bap.jp.smartfashion.common.model.ShopVisitor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ShopCollectionVisitor;

/**
 * Spring Data JPA repository for the {@link ShopCollectionVisitor} entity.
 */
@Repository
public interface ShopCollectionVisitorRepository extends JpaRepository<ShopCollectionVisitor,Integer> {
	@Query("SELECT shopCollectionVisitor"
			+ " FROM ShopCollectionVisitor shopCollectionVisitor"
			+ " WHERE shopCollectionVisitor.shop.id = :shopId"
			+ " AND shopCollectionVisitor.productCollection.id = :collectionId"
			+ " AND shopCollectionVisitor.type = :type"
			+ " AND shopCollectionVisitor.timeline = :timeline"
			+ " AND HOUR(shopCollectionVisitor.created) = :hour"
			+ " AND DATE(shopCollectionVisitor.created) = :date")
	Optional<ShopCollectionVisitor> findAllByConditionForHour(@Param("shopId") Integer shopId, @Param("collectionId") Integer collectionId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("hour") Integer hour, @Param("date") Date date);
	
	@Query("SELECT shopCollectionVisitor"
			+ " FROM ShopCollectionVisitor shopCollectionVisitor"
			+ " WHERE shopCollectionVisitor.shop.id = :shopId"
			+ " AND shopCollectionVisitor.productCollection.id = :collectionId"
			+ " AND shopCollectionVisitor.type = :type"
			+ " AND shopCollectionVisitor.timeline = :timeline"
			+ " AND DATE(shopCollectionVisitor.created) = :date")
	Optional<ShopCollectionVisitor> findAllByConditionForDate(@Param("shopId") Integer shopId, @Param("collectionId") Integer collectionId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("date") Date date);
	
	@Query("SELECT shopCollectionVisitor"
			+ " FROM ShopCollectionVisitor shopCollectionVisitor"
			+ " WHERE shopCollectionVisitor.shop.id = :shopId"
			+ " AND shopCollectionVisitor.productCollection.id = :collectionId"
			+ " AND shopCollectionVisitor.type = :type"
			+ " AND shopCollectionVisitor.timeline = :timeline"
			+ " AND YEARWEEK(shopCollectionVisitor.created) = :yearweek")
	Optional<ShopCollectionVisitor> findAllByConditionForWeek(@Param("shopId") Integer shopId, @Param("collectionId") Integer collectionId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("yearweek") Integer yearweek);
	
	@Query("SELECT shopCollectionVisitor"
			+ " FROM ShopCollectionVisitor shopCollectionVisitor"
			+ " WHERE shopCollectionVisitor.shop.id = :shopId"
			+ " AND shopCollectionVisitor.productCollection.id = :collectionId"
			+ " AND shopCollectionVisitor.type = :type"
			+ " AND shopCollectionVisitor.timeline = :timeline"
			+ " AND MONTH(shopCollectionVisitor.created) = :month"
			+ " AND YEAR(shopCollectionVisitor.created) = :year")
	Optional<ShopCollectionVisitor> findAllByConditionForMonth(@Param("shopId") Integer shopId, @Param("collectionId") Integer collectionId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("month") Integer month, @Param("year") Integer year);
	
	@Query("SELECT shopCollectionVisitor"
			+ " FROM ShopCollectionVisitor shopCollectionVisitor"
			+ " WHERE shopCollectionVisitor.shop.id = :shopId"
			+ " AND shopCollectionVisitor.productCollection.id = :collectionId"
			+ " AND shopCollectionVisitor.type = :type"
			+ " AND shopCollectionVisitor.timeline = :timeline"
			+ " AND YEAR(shopCollectionVisitor.created) = :year")
	Optional<ShopCollectionVisitor> findAllByConditionForYear(@Param("shopId") Integer shopId, @Param("collectionId") Integer collectionId, @Param("timeline") String timeline,
			@Param("type") String type, @Param("year") Integer year);

	// Query get returning visitors's info
	@Query("SELECT shopCollectionVisitor "
			+ "FROM ShopCollectionVisitor shopCollectionVisitor "
			+ "WHERE shopCollectionVisitor.shop.id = :shopId "
			+ "AND shopCollectionVisitor.type = :type "
			+ "AND shopCollectionVisitor.timeline = :timeline "
			+ "AND DATE(shopCollectionVisitor.created) = DATE(:date)")
	List<ShopCollectionVisitor> findAllByConditionsAndDay(@Param("shopId") Integer shopId,
		    @Param("type") String type,  @Param("timeline") String timeline, @Param("date") LocalDate date);


	@Query("SELECT shopCollectionVisitor "
			+ "FROM ShopCollectionVisitor shopCollectionVisitor "
			+ "WHERE shopCollectionVisitor.shop.id = :shopId "
			+ "AND shopCollectionVisitor.type = :type "
			+ "AND shopCollectionVisitor.timeline = :timeline "
			+ "AND YEARWEEK(shopCollectionVisitor.created) = :yearWeek")
	List<ShopCollectionVisitor> findAllByConditionsAndWeek(@Param("shopId") Integer shopId,
			@Param("type") String type, @Param("timeline") String timeline,
			@Param("yearWeek") Integer yearWeek);


	@Query("SELECT shopCollectionVisitor "
			+ "FROM ShopCollectionVisitor shopCollectionVisitor "
			+ "WHERE shopCollectionVisitor.shop.id = :shopId "
			+ "AND shopCollectionVisitor.type = :type "
			+ "AND shopCollectionVisitor.timeline = :timeline "
			+ "AND MONTH(shopCollectionVisitor.created) = :month "
			+ "AND YEAR(shopCollectionVisitor.created) = :year")
	List<ShopCollectionVisitor> findAllByConditionsAndMonth(@Param("shopId") Integer shopId, @Param("type") String type,
			@Param("timeline") String timeline, @Param("month") Integer month,
			@Param("year") Integer year);


	@Query("SELECT shopCollectionVisitor "
			+ "FROM ShopCollectionVisitor shopCollectionVisitor "
			+ "WHERE shopCollectionVisitor.shop.id = :shopId "
			+ "AND shopCollectionVisitor.type = :type "
			+ "AND shopCollectionVisitor.timeline = :timeline "
			+ "AND YEAR(shopCollectionVisitor.created) = :year")
	List<ShopCollectionVisitor> findAllByConditionsAndYear(@Param("shopId") Integer shopId, @Param("type") String type,
			@Param("timeline") String timeline, @Param("year") Integer year);
}
