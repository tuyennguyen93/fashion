package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.api.admin.product.dto.ProductStatusDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.Product;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link Product} entity.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

	Optional<Product> findByIdAndShop_IdAndStatus(Integer productId, Integer shopId, String status);


	@Query(value = "SELECT product"
			+ " FROM Product product"
			+ " JOIN FETCH Shop shop"
			+ " ON product.shop.id = shop.id"
			+ " JOIN FETCH ProductCollection productCollection"
			+ " ON product.productCollection.id = productCollection.id"
			+ " WHERE (product.name LIKE %:query%"
			+ " OR product.productCode LIKE %:query%)"
			+ " AND shop.id = :shopId"
			+ " AND productCollection.id = :productCollectionId"
			+ " AND product.status = :status")
	Page<Product> findProductByCondition(
			@Param(value = "query") String productCodeOrName,
			@Param(value = "shopId") Integer shopId, 
			@Param(value = "productCollectionId") Integer productCollectionId,
			@Param(value = "status") String status,
			Pageable pageable);


	Optional<Product> findByStatusAndShop_Id(String status, Integer shopId);

	@Query("SELECT product "
			+ "FROM Product product "
			+ "WHERE product.shop.id = :shopId "
			+ "AND product.status = :status "
			+ "AND UPPER(product.productCode) = UPPER(:product_code) ")
	Optional<Product> exitsProductID(@Param("shopId") Integer shopId,
			@Param("product_code") String productCode,
			@Param("status") String status);

	@Query(value = "SELECT product"
			+ " FROM Product product"
			+ " JOIN FETCH Shop shop"
			+ " ON product.shop.id = shop.id"
			+ " JOIN FETCH ProductCollection productCollection"
			+ " ON product.productCollection.id = productCollection.id"
			+ " WHERE shop.id = :shopId"
			+ " AND productCollection.id = :productCollectionId"
			+ " AND product.status = :status"
			+ " AND product.productType.id IN (:productTypes)")
	Page<Product> findByShop_IdAndProductCollection_IdAndProductType_IdAndStatus(
			@Param("shopId") Integer shopId, 
			@Param("productCollectionId") Integer productCollectionId, 
			@Param("productTypes") List<Integer> productTypes,
			@Param("status") String status,
			Pageable pageable);
	
	@Query(value = "SELECT product"
			+ " FROM Product product"
			+ " JOIN FETCH Shop shop"
			+ " ON product.shop.id = shop.id"
			+ " JOIN FETCH ProductCollection productCollection"
			+ " ON product.productCollection.id = productCollection.id"
			+ " WHERE shop.id = :shopId"
			+ " AND productCollection.id = :productCollectionId"
			+ " AND product.status = :status")
	Page<Product> findByShop_IdAndProductCollection_IdAndStatus(
			@Param("shopId") Integer shopId, 
			@Param("productCollectionId") Integer productCollectionId,
			@Param("status") String status,
			Pageable pageable);
	

	@Query("SELECT product "
			+ "FROM Product product "
			+ "WHERE product.shop.id = :shopId "
			+ "AND product.status = :status "
			+ "AND UPPER(product.name) = UPPER(:productName)")
	Optional<Product> exitsProductName(@Param("shopId") Integer shopId, @Param("productName") String productName, @Param("status") String status);

	Optional<Product> findFirstByShop_IdAndStatusOrderByPriceAsc(Integer shopId, String status);
	Optional<Product> findFirstByShop_IdAndStatusOrderByPriceDesc(Integer shopId, String status);

	@Query("SELECT product "
			+ "FROM Product product INNER JOIN Shop shop "
			+ "ON product.shop.id = shop.id "
			+ "WHERE product.shop.id = :shopId "
			+ "AND product.status = :status "
			+ "AND (product.name LIKE %:name% "
			+ "OR product.productCode LIKE %:name%) "
			+ "ORDER BY product.created DESC")
	Page<Product> findAllByNameOrProductCodeAndShop_IdAndStatus(@Param("name") String name,
			@Param("shopId") Integer shopId, @Param("status") String status, Pageable pageable);

	Optional<Product> findByIdAndStatus(Integer id, String status);
	
	Integer countByShop_Id(Integer shopId);

	Page<Product> findAllByShop_IdAndStatus(Integer shopId, String status, Pageable pageable);


	@Query(value = "SELECT product"
			+ " FROM Product product JOIN FETCH ProductCollection productCollection ON product.productCollection.id = productCollection.id"
			+ " WHERE ((:status = '' AND (product.status = 'ACTIVE' OR product.status = 'PENDING')) OR product.status = :status)"
			+ " AND (product.name LIKE %:query%"
			+ " OR product.productCode LIKE %:query%"
			+ " OR productCollection.name LIKE %:query%"
			+ " OR product.shop.name LIKE %:query%)")
	Page<Product> findAllByConditionsForAdmin(@Param("query") String query, @Param("status") String status, Pageable pageable);


	@Query(value = "SELECT new bap.jp.smartfashion.api.admin.product.dto.ProductStatusDTO(product.status, count(product.id) AS amount)"
			+ " FROM Product product"
			+ " WHERE product.status = 'ACTIVE' OR product.status = 'PENDING'"
			+ " GROUP BY product.status")
	List<ProductStatusDTO> findStatusAndAmountListForAdmin();

	@Query(value = "SELECT product"
			+ " FROM Product product"
			+ " WHERE product.id = :id AND (product.status = 'ACTIVE' OR product.status = 'PENDING')")
	Optional<Product> findByIdForAdmin(@Param("id") Integer id);
	
	@Query(value = "SELECT DISTINCT product"
			+ " FROM Product product"
			+ " INNER JOIN ProductDetail productDetail"
			+ " ON product.id = productDetail.product.id"
			+ " WHERE product.status = 'ACTIVE'"
			+ " AND productDetail.keyPoint IS NOT NULL"
			+ " AND product.productCollection.id = :productCollectionId"
			+ " ORDER BY RAND()")
	Page<Product> findRandomProduct( 
			@Param("productCollectionId") Integer productCollectionId, Pageable pageable);
	
	@Query(value = "SELECT DISTINCT product"
			+ " FROM Product product"
			+ " INNER JOIN ProductDetail productDetail"
			+ " ON product.id = productDetail.product.id"
			+ " INNER JOIN UserProductBasket userProductBasket"
			+ " ON product.id = userProductBasket.product.id"
			+ " WHERE product.status = 'ACTIVE'"
			+ " AND productDetail.keyPoint IS NOT NULL"
			+ " AND product.productCollection.id = :productCollectionId"
			+ " ORDER BY RAND()")
	Page<Product> findRandomProductBasket( 
			@Param("productCollectionId") Integer productCollectionId, Pageable pageable);
	
	@Query(value = "SELECT DISTINCT product"
			+ " FROM Product product"
			+ " INNER JOIN ProductDetail productDetail"
			+ " ON product.id = productDetail.product.id"
			+ " INNER JOIN UserProductInteraction userProductInteraction"
			+ " ON product.id = userProductInteraction.product.id"
			+ " WHERE product.status = 'ACTIVE'"
			+ " AND productDetail.keyPoint IS NOT NULL"
			+ " AND product.productCollection.id = :productCollectionId"
			+ " AND userProductInteraction.actionCode = :productInteractionType"
			+ " AND userProductInteraction.user.id = :userId"
			+ " ORDER BY RAND()")
	Page<Product> findRandomProductByInteraction(
			@Param("userId") Integer userId,
			@Param("productCollectionId") Integer productCollectionId,
			@Param("productInteractionType") String productInteractionType,
			Pageable pageable);
	
	Optional<Product> findByProductDetails_Id(Integer productDetailId);

	Optional<Product> findFirstByStatusOrderByPriceAsc(String status);

	Optional<Product> findFirstByStatusOrderByPriceDesc(String status);
	
	Optional<Product> findByIdAndShop_Id(Integer productId, Integer shopId);
}
