package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.MixMatch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link MixMatch} entity.
 */
@Repository
public interface MixMatchRepository extends JpaRepository<MixMatch, Integer> {
	Optional<MixMatch> findByIdAndAndShopId(Integer id, Integer shopId);

	@Query(value = "SELECT DISTINCT mixMatch"
			+ " FROM MixMatch mixMatch"
			+ " INNER JOIN ProductMixMatch productMixMatch"
			+ " ON mixMatch.id = productMixMatch.mixMatch.id"
			+ " INNER JOIN Product product "
			+ " ON productMixMatch.product.id = product.id"
			+ " INNER JOIN ProductCollection productCollection"
			+ " ON product.productCollection.id = productCollection.id"
			+ " INNER JOIN Shop shop"
			+ " ON product.shop.id = shop.id"
			+ " WHERE productCollection.id = :productCollectionId"
			+ " AND shop.id = :shopId"
			+ " AND (product.name LIKE %:query% OR mixMatch.name LIKE %:query%)")
	Page<MixMatch> findAllByShop_IdAndProductCollection_Id(
			@Param("shopId") Integer shopId,
			@Param("productCollectionId") Integer productCollectionId,
			@Param("query") String query,
			Pageable pageable);
	
	@Modifying
	@Query(value = "DELETE mixMatch "
			+ " FROM mix_match mixMatch"
			+ " INNER JOIN product_mix_match productMixMatch"
			+ " ON mixMatch.id = productMixMatch.mix_match_id"
			+ " WHERE productMixMatch.product_id = :id AND productMixMatch.priority = 1", nativeQuery = true)
	void deleteByProduct_IdIn(@Param("id") Integer id);
	
	@Query(value = "SELECT mixMatch"
			+ " FROM MixMatch mixMatch"
			+ " INNER JOIN ProductMixMatch productMixMatch"
			+ " ON mixMatch.id = productMixMatch.mixMatch.id"
			+ " WHERE productMixMatch.product.id IN :ids AND productMixMatch.priority = 1")
	List<MixMatch> findAllByProductId(@Param("ids") List<Integer> ids);

}
