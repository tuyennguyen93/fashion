package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductWearingPurpose;

/**
 * Spring Data JPA repository for the {@link ProductWearingPurpose} entity.
 */
@Repository
public interface ProductWearingPurposeRepository extends JpaRepository<ProductWearingPurpose, Integer> {
	Optional<ProductWearingPurpose> findOneByName(String name);
	
	List<ProductWearingPurpose> findAllByType(String type);

	Optional<ProductWearingPurpose> findByIdAndType(Integer productWearingPurposeId, String type);
}