package bap.jp.smartfashion.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.UserAuthority;

/**
 * Spring Data JPA repository for the {@link UserAuthority} entity.
 */
@Repository
public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Integer> {

}
