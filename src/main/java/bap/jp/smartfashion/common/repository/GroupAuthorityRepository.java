package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.GroupAuthority;

/**
 * Spring Data JPA repository for the {@link GroupAuthority} entity.
 */
@Repository
public interface GroupAuthorityRepository extends JpaRepository<GroupAuthority, Integer> {
	List<GroupAuthority> findByGroupType(String groupType);
	
	Optional<GroupAuthority> findOneByGroupNameAndGroupType(String groupName, String groupType);
}
