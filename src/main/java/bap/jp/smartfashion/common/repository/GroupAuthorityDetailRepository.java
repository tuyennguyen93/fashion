package bap.jp.smartfashion.common.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import bap.jp.smartfashion.common.model.GroupAuthorityDetail;

/**
 * Spring Data JPA repository for the {@link GroupAuthorityDetail} entity.
 */
@Repository
public interface GroupAuthorityDetailRepository extends JpaRepository<GroupAuthorityDetail, Integer> {
	List<GroupAuthorityDetail> findByGroupAuthority_Id(int groupAuthorityId);
	
	@Modifying
	@Query("DELETE FROM GroupAuthorityDetail gad WHERE gad.groupAuthority.id = ?1")
	void deleteByGroupAuthority_Id(Integer groupAuthorityId);
	
	@Query("SELECT gad"
			+ " FROM GroupAuthority ga"
			+ " INNER JOIN GroupAuthorityDetail gad"
			+ " ON ga.id = gad.groupAuthority.id"
			+ " INNER JOIN GroupAuthorityMember gam"
			+ " ON ga.id = gam.groupAuthority.id"
			+ " WHERE gam.objectId = :objectId AND ga.groupType = :groupType")
	List<GroupAuthorityDetail> findByObjectIdAndGroupType(@Param(value = "objectId") Integer objectId, @Param(value = "groupType") String groupType);
}
