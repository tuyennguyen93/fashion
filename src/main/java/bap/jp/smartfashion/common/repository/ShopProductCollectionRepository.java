package bap.jp.smartfashion.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ShopProductCollection;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link ShopProductCollection} entity.
 */
@Repository
public interface ShopProductCollectionRepository extends JpaRepository<ShopProductCollection, Integer> {

    Optional<ShopProductCollection> findByShop_IdAndProductCollection_Id(Integer shopId, Integer productCollectionId);

    Optional<List<ShopProductCollection>> findByShop_Id(Integer shopId);
}
