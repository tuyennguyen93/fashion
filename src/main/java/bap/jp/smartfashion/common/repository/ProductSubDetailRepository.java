package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductSubDetail;

/**
 * Spring Data JPA repository for the {@link ProductSubDetail} entity.
 */
@Repository
public interface ProductSubDetailRepository extends JpaRepository<ProductSubDetail, Integer> {	
	@Modifying
	@Query("DELETE FROM ProductSubDetail productSubDetail where productSubDetail.id in ?1")
	void deleteProductSubDetailWithIds(List<Integer> ids);
	
	Optional<ProductSubDetail> findOneByProductDetail_IdAndProductSize_id(Integer productDetailId, Integer sizeId);

	List<ProductSubDetail> findAllByProductDetail_Id(Integer productDetailId);
}