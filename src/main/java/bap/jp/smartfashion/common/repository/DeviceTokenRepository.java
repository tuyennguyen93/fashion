package bap.jp.smartfashion.common.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.DeviceToken;

/**
 * Spring Data JPA repository for the {@link DeviceToken} entity.
 */
@Repository
public interface DeviceTokenRepository extends JpaRepository<DeviceToken, Integer> {
	Optional<DeviceToken> findByUser_Id(Integer userId);
}
