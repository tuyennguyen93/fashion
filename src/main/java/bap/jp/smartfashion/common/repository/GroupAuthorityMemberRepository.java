package bap.jp.smartfashion.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import bap.jp.smartfashion.common.model.GroupAuthorityMember;

/**
 * Spring Data JPA repository for the {@link GroupAuthorityMember} entity.
 */
public interface GroupAuthorityMemberRepository extends JpaRepository<GroupAuthorityMember, Integer> {
	List<GroupAuthorityMember> findByObjectId(Integer objectId);
}
