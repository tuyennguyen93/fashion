package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ProductRatingPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRatingPhotoRepository extends JpaRepository<ProductRatingPhoto, Integer> {

}
