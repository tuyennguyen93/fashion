package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.UserRole;

/**
 * Spring Data JPA repository for the {@link UserRole} entity.
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {
	Optional<List<UserRole>> findByUserId(int userId);
	
	void deleteByUserId(int userId);

	Optional<UserRole> findByUserIdAndRoleName(Integer userId, String roleName);

	Optional<UserRole> findByRoleName(String roleName);
}
