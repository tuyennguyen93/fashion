package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import bap.jp.smartfashion.api.admin.shop.dto.AdminShopStatisticDTO;
import bap.jp.smartfashion.api.admin.shop.dto.AdminShopStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.Shop;

/**
 * Spring Data JPA repository for the {@link Shop} entity.
 */
@Repository
public interface ShopRepository extends JpaRepository<Shop, Integer> {
	
	Optional<Shop> findByIdAndUser_Id(int shopId, int userId);
	
	List<Shop> findByUser_Id(int userId);
	
	Optional<Shop> findOneByUser_Id(Integer userId);
	
	@Query(value = "SELECT COUNT(shop.id)"
			+ " FROM Shop shop "
			+ " WHERE shop.shopCode LIKE :shopCode%")
	Integer countByShopCodeLike(@Param(value = "shopCode") String shopCode);

	@Query(value = "SELECT shop"
			+ " FROM Shop shop "
			+ " WHERE shop.name LIKE :query%")
	Page<Shop> findShopList(@Param(value = "query") String query, Pageable pageable);

	@Query(value = "select COUNT(CASE WHEN shop.status = 'ACTIVE' THEN 1 END) AS active,"
			+ " COUNT(CASE WHEN shop.status = 'PENDING' THEN 1 END) AS pending,"
			+ " COUNT(CASE WHEN shop.status = 'REJECTED' THEN 1 END) AS rejected,"
			+ " COUNT(CASE WHEN shop.status = 'BLOCKED' THEN 1 END) AS blocked,"
			+ " COUNT(CASE WHEN shop.status = 'SUSPENDED' THEN 1 END) AS suspended"
			+ "	FROM shop", nativeQuery = true)
	AdminShopStatus findAllByShopStatus();

	@Query(value = "SELECT shop.id AS id, shop.name AS name, shop.status AS status, shop.created AS created,"
			+ " COUNT(DISTINCT CASE WHEN product.status = 'ACTIVE' THEN product.id END)  AS postedProducts,"
			+ " COUNT(DISTINCT CASE WHEN product.status = 'PENDING' THEN product.id END) AS pendingProducts,"
			+ " GROUP_CONCAT(DISTINCT CASE WHEN product.status = 'ACTIVE' THEN SUBSTRING(product_collection.name, 1, 1) END) AS collections,"
			+ " GROUP_CONCAT(DISTINCT shop_branch.business_address"
			+ " SEPARATOR ';') AS businessAddress"
			+ " FROM (shop LEFT JOIN product ON shop.id = product.shop_id)"
			+ " LEFT JOIN product_collection ON product_collection.id = product.product_collection_id"
			+ " LEFT JOIN shop_branch ON shop.id = shop_branch.shop_id"
			+ " WHERE ((:status = '' AND shop.status IS NOT NULL) OR shop.status = :status)"
			+ "	AND (shop.name LIKE %:query% OR (product_collection.name LIKE %:query% AND product.status = 'ACTIVE')"
			+ "		OR shop.id IN ("
			+ "		SELECT shop.id"
			+ "		FROM (shop INNER JOIN shop_branch ON shop.id = shop_branch.shop_id)"
			+ "		WHERE shop_branch.business_address LIKE %:query%))"
			+ " GROUP BY shop.id", nativeQuery = true)
	Page<AdminShopStatisticDTO> statisticAllShops(@Param("query") String query, @Param("status") String status, Pageable pageable);
}
