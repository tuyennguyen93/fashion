package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ProductMixMatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Spring Data JPA repository for the
 * {@link bap.jp.smartfashion.common.model.ProductMixMatch} entity.
 */
@Repository
public interface ProductMixMatchRepository extends JpaRepository<ProductMixMatch, Integer> {
	@Modifying
	@Query(value = "DELETE FROM ProductMixMatch productMixMatch WHERE productMixMatch.id in ?1")
	void deleteByMixMatch_IdQuery(List<Integer> ids);
	
	@Modifying
	@Query(value = "DELETE FROM ProductMixMatch productMixMatch WHERE productMixMatch.product.id in ?1")
	void deleteByProduct_IdIn(List<Integer> ids);
	
	@Modifying
	@Query(value = "DELETE FROM ProductMixMatch productMixMatch WHERE productMixMatch.mixMatch.id = :mixMatchId AND productMixMatch.product.id = :productId")
	void deleteByMixMatchAndProductId(@Param(value = "mixMatchId") Integer mixMatchId, @Param(value = "productId") Integer productId);

	List<ProductMixMatch> findAllByProduct_IdAndProduct_Status(Integer productId, String productStatus);

	@Query(value = "SELECT productMixMatch"
			+ " FROM ProductMixMatch productMixMatch"
			+ " JOIN FETCH Product product"
			+ " ON productMixMatch.product.id = product.id"
			+ " WHERE productMixMatch.mixMatch.id IN :mixMatchIds"
			+ " AND product.status = 'ACTIVE'")
	List<ProductMixMatch> findDistinctProduct_IdByMixMatch_IdIn(@Param(value = "mixMatchIds") List<Integer> mixMatchIds);
	
	@Modifying
	@Query(value = "DELETE FROM ProductMixMatch productMixMatch WHERE productMixMatch.mixMatch.id in ?1")
	void deleteByMixMatch_IdIn(List<Integer> ids);

	@Query(value = "SELECT productMixMatch"
			+ " FROM ProductMixMatch productMixMatch"
			+ " WHERE productMixMatch.product.id IN :ids AND productMixMatch.priority <> 1")
	List<ProductMixMatch> findAllByProductIdsList(@Param("ids") List<Integer> ids);
}
