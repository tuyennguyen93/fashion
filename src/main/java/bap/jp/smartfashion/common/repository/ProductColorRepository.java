package bap.jp.smartfashion.common.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductColor;

/**
 * Spring Data JPA repository for the {@link ProductColor} entity.
 */
@Repository
public interface ProductColorRepository extends JpaRepository<ProductColor, Integer> {
	Optional<ProductColor> findOneByName(String name);
}