package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ProductRatingReplyInteraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRatingReplyInteractionRepository extends JpaRepository<ProductRatingReplyInteraction, Integer> {

    Optional<ProductRatingReplyInteraction> findByProductRatingReply_IdAndUser_IdAndType(Integer id, Integer userId, String type);
}
