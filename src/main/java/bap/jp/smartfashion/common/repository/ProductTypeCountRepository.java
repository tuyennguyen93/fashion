package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductTypeCount;
import bap.jp.smartfashion.common.vo.ShopMenu;

/**
 * Spring Data JPA repository for the {@link ProductTypeCount} entity.
 */
@Repository
public interface ProductTypeCountRepository extends JpaRepository<ProductTypeCount, Integer> {

	Optional<ProductTypeCount> findByProductType_Id(int productTypeId);

	List<ProductTypeCount> findByShop_IdAndProductType_ProductCollection_IdOrderById(int shopId,
			int productCollectionId);

	List<ProductTypeCount> findByShop_IdAndProductType_ProductTypeParentIdOrderById(int shopId,
			int productTypeParentId);

	Optional<ProductTypeCount> findByProductType_IdAndShop_Id(int productTypeId, int shopId);

	
	@Query(value = "SELECT new bap.jp.smartfashion.common.vo.ShopMenu(productType.id, productType.name, SUM(productTypeCount.count))"
			+ " FROM ProductTypeCount productTypeCount"
			+ " LEFT JOIN FETCH ProductType productType"
			+ " ON productTypeCount.productType.id = productType.id"
			+ " LEFT JOIN FETCH ProductCollection productCollection"
			+ " ON productType.productCollection.id = productCollection.id"
			+ " WHERE productCollection.id = :productCollectionId "
			+ " AND productType.productTypeParentId IS NULL"
			+ " GROUP BY  productType.id")
	List<ShopMenu> findByProductType_ProductCollection_Id(int productCollectionId);

	@Query(value = "SELECT new bap.jp.smartfashion.common.vo.ShopMenu(productType.id, productType.name, SUM(productTypeCount.count)) "
			+ " FROM ProductTypeCount productTypeCount"
			+ " LEFT JOIN ProductType productType"
			+ " ON productTypeCount.productType.id=productType.id"
			+ " WHERE productType.productTypeParentId = :productTypeParentId"
			+ " GROUP BY productType.id"
			+ " ORDER BY productType.id ASC")
	List<ShopMenu> findByProductType_ProductTypeParentIdOrderById(@Param("productTypeParentId") int productTypeParentId);
}