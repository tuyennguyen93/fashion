package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.ProductRatingReplyPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRatingReplyPhotoRepository extends JpaRepository<ProductRatingReplyPhoto, Integer> {
}
