package bap.jp.smartfashion.common.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bap.jp.smartfashion.common.model.ProductInteraction;

/**
 * Spring Data JPA repository for the {@link ProductInteraction} entity.
 */
@Repository
public interface ProductInteractionRepository extends JpaRepository<ProductInteraction, Integer> {
	@Query("SELECT productInteraction"
			+ " FROM ProductInteraction productInteraction"
			+ " JOIN FETCH Product product"
			+ " ON productInteraction.product.id = product.id"
			+ " WHERE product.productCollection.id = :collectionId"
			+ " AND product.shop.id = :shopId"
			+ " AND product.status = 'ACTIVE'"
			+ " AND productInteraction.actionCode = :actionCode"
			+ " AND productInteraction.actionCount != 0"
			+ " ORDER BY productInteraction.actionCount desc")
	Page<ProductInteraction> findTopInteractionByProductCollectionAndAction(
			@Param(value = "shopId") Integer shopId,
			@Param(value = "collectionId") Integer collectionId,
			@Param(value = "actionCode") String actionCode, Pageable pageable);

	@Query("SELECT productInteraction"
			+ " FROM ProductInteraction productInteraction"
			+ " JOIN FETCH Product product"
			+ " ON productInteraction.product.id = product.id"
			+ " WHERE product.productCollection.id = :collectionId"
			+ " AND product.status = 'ACTIVE'"
			+ " AND productInteraction.actionCode = :actionCode"
			+ " AND productInteraction.actionCount != 0"
			+ " ORDER BY productInteraction.actionCount desc")
	Page<ProductInteraction> findTopInteractionByProductCollectionAndAction(@Param(value = "collectionId") Integer collectionId,
																			@Param(value = "actionCode") String actionCode, Pageable pageable);


	Optional<ProductInteraction> findByProduct_IdAndActionCode(Integer productId,String actionCode);

	List<ProductInteraction> findAllByProduct_Shop_Id(Integer shopId);

	@Query("SELECT productInteraction"
			+ " FROM ProductInteraction productInteraction"
			+ " JOIN FETCH Product product"
			+ " ON productInteraction.product.id = product.id"
			+ " AND product.shop.id = :shopId"
			+ " AND product.status = 'ACTIVE'"
			+ " AND productInteraction.actionCode = :actionCode"
			+ " AND productInteraction.actionCount != 0"
			+ " ORDER BY productInteraction.actionCount desc")
	Page<ProductInteraction> findTopInteractionByAndAction(
			@Param(value = "shopId") Integer shopId,
			@Param(value = "actionCode") String actionCode, Pageable pageable);
}