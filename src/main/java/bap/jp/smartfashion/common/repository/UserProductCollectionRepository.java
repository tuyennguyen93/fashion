package bap.jp.smartfashion.common.repository;

import bap.jp.smartfashion.common.model.UserProductCollection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the {@link UserProductCollection} entity.
 */
@Repository
public interface UserProductCollectionRepository extends JpaRepository<UserProductCollection, Integer> {
}
