package bap.jp.smartfashion.common.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ShopCollectionVisitor entity
 *
 */
@Entity
@Getter
@Setter
public class ShopCollectionVisitor extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "shop_id", referencedColumnName = "id")
	private Shop shop;

	@ManyToOne
	@JoinColumn(name = "product_collection_id", referencedColumnName = "id")
	private ProductCollection productCollection;

	@Size(max = 15, message = "Type {validation.size-max}")
	private String type;

	private Integer typeCount;

	@Size(max = 10, message = "TimeLine {validation.size-max}")
	private String timeline;
	
	public ShopCollectionVisitor() {
	}
}