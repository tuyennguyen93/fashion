package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * UserModel entity
 *
 */
@Entity
@Getter
@Setter
public class UserModelSetting extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5775862862858260184L;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@NotFound(action = NotFoundAction.IGNORE)
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "user_model_id", referencedColumnName = "id")
	private UserModel userModel;
	
	private Boolean isDefault;

	public UserModelSetting() {
	}

	public UserModelSetting(Integer id) {
		this.id = id;
	}

}
