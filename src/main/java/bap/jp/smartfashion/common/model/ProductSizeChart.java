package bap.jp.smartfashion.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductSizeChart entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class ProductSizeChart extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(unique = true)
	@Size(max = 15)
	private String name;
	
	public ProductSizeChart() {
	}
	
	public ProductSizeChart(Integer id) {
		this.id = id;
	}
}
