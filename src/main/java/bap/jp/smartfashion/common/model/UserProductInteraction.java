package bap.jp.smartfashion.common.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * UserProductInteraction entity
 *
 */
@Entity
@Getter
@Setter
public class UserProductInteraction extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;

	@Size(max = 15, message = "ActionCode {validation.size-max}")
	private String actionCode;

	private Integer actionCount;

	public UserProductInteraction(User user, Product product, @Size(max = 15) String actionCode, Integer actionCount) {
		this.user = user;
		this.product = product;
		this.actionCode = actionCode;
		this.actionCount = actionCount;
	}

	public UserProductInteraction() {
	}
}
