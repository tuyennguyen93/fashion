package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * GroupAuthorityMember entity
 *
 */
@Entity
@Getter
@Setter
@Data
public class GroupAuthorityMember extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "group_authority_id", nullable = false)
	private GroupAuthority groupAuthority;
	
	private Integer objectId;
	
	public GroupAuthorityMember() {
	}
	
}
