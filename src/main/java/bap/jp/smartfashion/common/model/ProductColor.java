package bap.jp.smartfashion.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * ProductColor entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class ProductColor extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(unique = true)
	@Size(max = 10, message = "ProductColor's name {validation.size-max}")
	private String name;
	
	@Size(max = 50, message = "Color Code {validation.size-max}")
	private String colorCode;
	
	public ProductColor() {}

	public ProductColor(Integer productColorId) {
		this.id = productColorId;
	}
}
