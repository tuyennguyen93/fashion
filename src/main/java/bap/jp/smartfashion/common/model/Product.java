package bap.jp.smartfashion.common.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Product entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class Product extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "shop_id")
	private Shop shop;
	
	@Size(max = 50, message = "{info.product.name} {validation.size-max}")
	@NotEmpty(message = "{info.product.name} {validation.not-empty}")
	private String name;
	
	@Size(max = 15, message = "{info.product.code} {validation.size-max}")
	private String productCode;
	
	@ManyToOne
	@JoinColumn(name = "product_collection_id")
	private ProductCollection productCollection;
	
	@Size(max = 5, message = "Currency {validation.size-max}")
	private String currency;
	
	@Digits(integer = 8, fraction = 2, message = "Price, {javax.validation.constraints.Digits.message}")
	@PositiveOrZero(message = "Price {validation.unsigned-number-or-zero}")
	private BigDecimal price;
	
	@Digits(integer = 8, fraction = 2, message = "OldPrice, {javax.validation.constraints.Digits.message}")
	@PositiveOrZero(message = "OldPrice {validation.unsigned-number-or-zero}")
	private BigDecimal oldPrice;

	@Digits(integer = 2, fraction = 2, message = "Discount, {javax.validation.constraints.Digits.message}")
	@PositiveOrZero(message = "Discount {validation.unsigned-number-or-zero}")
	private BigDecimal discount;
	
	@Size(max = 100, message = "Guarantee {validation.size-max}")
	private String guarantee;
	
	@Size(max = 10, message = "Status {validation.size-max}")
	@NotNull(message = "Status {validation.not-null}")
	private String status;
	
	@ManyToOne
	@JoinColumn(name = "product_wearing_purpose_id")
	private ProductWearingPurpose productWearingPurpose;
	
	@ManyToOne
	@JoinColumn(name = "product_type_id")
	private ProductType productType;
	
	@Size(max = 1000, message = "Description {validation.size-max}")
	private String description;
	
	private boolean highlight;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	@OrderBy(value = "id")
	private List<ProductDetail> productDetails;

	
	@JsonManagedReference
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	private List<ProductInteraction> productInteractions;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	private List<UserProductInteraction> userProductInteractions;
	
	@OneToOne
	@JoinColumn(name = "product_size_chart_id")
	private ProductSizeChart productSizeChart;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	private List<UserProductBasket> userProductBaskets;
	
	private String url;
	
	public Product() {}

	public Product(Shop shop, String status) {
		this.shop = shop;
		this.status = status;
	}

	public Product(Integer productId) {
		this.id = productId;
	}
}
