package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * GroupAuthorityDetail entity
 *
 */
@Entity
@Getter
@Setter
@Data
@AllArgsConstructor
public class GroupAuthorityDetail extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "group_authority_id", nullable = false)
	private GroupAuthority groupAuthority;
	
	@Size(max = 50)
	private String actionName;
	
	@Size(max = 50)
	private String functionName;

	public GroupAuthorityDetail() {
	}
}