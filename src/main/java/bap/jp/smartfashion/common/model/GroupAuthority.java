package bap.jp.smartfashion.common.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * GroupAuthority entity
 *
 */
@Entity
@Getter
@Setter
@Data
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"groupName", "groupType"})})
public class GroupAuthority extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Size(max = 50)
	private String groupName;
	
	@Size(max = 50)
	private String groupType;

	@OneToMany(mappedBy = "groupAuthority", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<GroupAuthorityDetail> groupAuthorityDetails;
	
	@OneToMany(mappedBy = "groupAuthority", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<GroupAuthorityMember> groupAuthorityMembers;
	
	public GroupAuthority() {
	}
}