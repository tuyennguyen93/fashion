package bap.jp.smartfashion.common.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * User entity
 *
 */
@Entity
@Getter
@Setter
@Data
public class User extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Size(max = 50, message = "Name {validation.size-max}")
	private String name;
	
	@Size(max = 150, message = "Email {validation.size-max}")
	@Email(message = "Email {validation.email}")
	private String email;
	
	@Size(max = 15, message = "Phone {validation.size-max}")
	private String phone;
	
	@Size(max = 255, message = "Password {validation.size-max}")
	@JsonIgnore
	private String password;
	
	private boolean registerStatus;
	private boolean blocked;
	private boolean deleted;
	
	@Size(max = 255, message = "AccessToken {validation.size-max}")
	private String accessToken;
	
	@Size(max = 15, message = "UserType {validation.size-max}")
	private String userType;
	
	@Size(max = 50, message = "UID {validation.size-max}")
	private String uid;
	
	@Size(max = 255, message = "Avatar {validation.size-max}")
	private String avatar;

	@Size(max = 13, message = "Tax Number {validation.size-max}")
	private String taxNumber;

	private Integer gender;

	@Size(max = 11, message = "Passport Number {validation.size-max}")
	private String passportNumber;
	
	@Size(max = 255, message = "PassportBackSide's ImageUrl {validation.size-max}")
	private String passportBackSide;
	
	@Size(max = 255, message = "PassportFrontSide's ImageUrl {validation.size-max}")
	private String passportFrontSide;
	
	@Size(max = 15, message = "Platform {validation.size-max}")
	private String platform;

	@Column(name = "lang_key")
	private String langKey;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "UserRole", 
		joinColumns = {@JoinColumn(name = "userId", referencedColumnName = "id")},
		inverseJoinColumns = {@JoinColumn(name = "roleName", referencedColumnName = "name")})
	private Set<Role> roles;

	@JsonIgnore
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private List<UserAuthority> userAuthorities;

	public User() {}

	public User(Integer id) {
		this.id = id;
	}
}