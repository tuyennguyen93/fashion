package bap.jp.smartfashion.common.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * Role entity
 *
 */
@Entity
@Getter
@Setter
public class Role implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Size(max = 50, message = "Name {validation.size-max}")
	private String name;

	public Role() {
	}
}
