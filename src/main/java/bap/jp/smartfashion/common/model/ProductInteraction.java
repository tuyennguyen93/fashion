package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductInteraction entity
 *
 */
@Entity
@Getter
@Setter
public class ProductInteraction extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;

	@Size(max = 15, message = "Action code {validation.size-max}")
	private String actionCode;

	private Integer actionCount;

	public ProductInteraction(Product product, @Size(max = 15) String actionCode, Integer actionCount) {
		this.product = product;
		this.actionCode = actionCode;
		this.actionCount = actionCount;
	}

	public ProductInteraction() {
	}
}
