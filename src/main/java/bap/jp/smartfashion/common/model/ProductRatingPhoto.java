package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ProductRatingPhoto Entity
 *
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProductRatingPhoto extends BaseModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Size(max = 255, message = "Image's url {validation.size-max}")
    @NotNull(message = "Image's url {validation.not-null}")
    private String url;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "product_rating_id", referencedColumnName = "id")
    private ProductRating productRating;

    public ProductRatingPhoto(ProductRating productRating, String image) {
        this.productRating = productRating;
        this.url = image;
    }
}
