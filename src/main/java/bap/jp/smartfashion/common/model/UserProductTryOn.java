package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * UserProductTryOn entity
 *
 */
@Entity
@Getter
@Setter
public class UserProductTryOn extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "user_model_id", referencedColumnName = "id")
	private UserModel userModel;
	
	@ManyToOne
	@JoinColumn(name = "user_product_id", referencedColumnName = "id")
	private UserProduct userProduct;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	@NotNull(message = "Image's url {validation.not-null}")
	private String url;

}
