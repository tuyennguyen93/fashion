package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductRatingCount entity
 *
 */
@Entity
@Getter
@Setter
public class ProductRatingCount extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;
	private Integer ratingStar;
	private Integer ratingCount;
	
}