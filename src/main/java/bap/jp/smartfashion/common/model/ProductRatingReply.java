package bap.jp.smartfashion.common.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * ProductRatingReply entity
 *
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProductRatingReply extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "product_rating_id", referencedColumnName = "id")
	private ProductRating productRating;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "shop_id", referencedColumnName = "id")
	private Shop shop;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@Size(max = 1000, message = "Comment {validation.size-max}")
	private String comment;

	@Column(name = "like_count")
	@NotNull(message = "Like Count {validation.not-null}")
	private Integer likeCount;

	@JsonManagedReference
	@OneToMany(mappedBy = "productRatingReply", cascade = CascadeType.ALL)
	private List<ProductRatingReplyPhoto> imageUrls;

	public ProductRatingReply(Integer id) {
		this.id = id;
	}
}