package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * UserBankAccount entity
 *
 */
@Entity
@Getter
@Setter
@Data
public class UserBankAccount extends BaseModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Size(max = 14, message = "Account Number {validation.size-max}")
    private String accountNumber;

    @Size(max = 30, message = "Owner Name {validation.size-max}")
    private String ownerName;

    @Size(max = 30, message = "Bank's name {validation.size-max}")
    private String bankName;

    @Size(max = 30, message = "Bank's branch {validation.size-max}")
    private String bankBranch;
}

