package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductPhoto entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class ProductPhoto extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "product_detail_id", referencedColumnName = "id")
	private ProductDetail productDetail;
	
	@Size(max = 255, message = "Image's url {validation.size-max}")
	private String urlOriginal;

	public ProductPhoto() {}
}
