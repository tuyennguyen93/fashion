package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductType entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "product_collection_id"})})
public class ProductType extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Size(max = 50, message = "ProductType's name {validation.size-max}")
	@NotNull(message = "ProductType's name {validation.not-null}")
	private String name;
	
	@JsonIgnore
	private Integer productTypeParentId;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "product_collection_id")
	private ProductCollection productCollection;

	public ProductType() {}

	public ProductType(Integer productTypeId) {
		this.id = productTypeId;
	}
}