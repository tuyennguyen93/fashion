package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import bap.jp.smartfashion.common.dto.ProductSizeDTO;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductSubDetail entity
 *
 */
@Entity
@Getter
@Setter
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"product_detail_id", "product_size_id"})})
public class ProductSubDetail extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "product_detail_id", referencedColumnName = "id")
	private ProductDetail productDetail;

	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "product_size_id")
	private ProductSize productSize;
	
	private Integer total;

	public ProductSubDetail() {}
}
