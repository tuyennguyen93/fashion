package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.List;

/**
 * MixMatch entity
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class MixMatch extends BaseModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Size(max = 50, message = "MixMatch's name {validation.size-max}")
	private String name;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "shop_id", referencedColumnName = "id")
	private Shop shop;

	@ManyToOne
	@JoinColumn(name = "product_collection_id", referencedColumnName = "id")
	private ProductCollection productCollection;

	@NotFound(action = NotFoundAction.IGNORE)
	@OneToMany(mappedBy = "mixMatch", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<ProductMixMatch> productMixMatches;

	public MixMatch() {
	}

}
