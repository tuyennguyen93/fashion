package bap.jp.smartfashion.common.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductRating entity
 *
 */
@Entity
@Getter
@Setter
public class ProductRating extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;
	
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	private int rating;
	
	@Size(max = 1000, message = "Comment {validation.size-max}")
	@NotNull(message = "Comment {validation.not-null}")
	private String comment;

	@Column(name = "like_count")
	@NotNull(message = "Like Count {validation.not-null}")
	private Integer likeCount;

	@Column(name = "comment_count")
	@NotNull(message = "Comment Count {validation.not-null}")
	private Integer commentCount;
	
	@NotFound(action = NotFoundAction.IGNORE)
	@JsonManagedReference
	@OneToMany(mappedBy = "productRating" , cascade = CascadeType.ALL)
	private List<ProductRatingReply> productRatingReplies;

	@JsonManagedReference
	@OneToMany(mappedBy = "productRating" , cascade = CascadeType.ALL)
	private List<ProductRatingPhoto> productRatingPhotos;

	public ProductRating(Integer id) {
		this.id = id;
	}

	public ProductRating() {}
}