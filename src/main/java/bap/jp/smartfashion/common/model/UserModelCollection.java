package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

/**
 * UserModelCollection entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class UserModelCollection extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -862822539164622203L;
	
	@Size(max = 50, message = "User model collection {validation.size-max}")
	private String name;

	public UserModelCollection() {
	}
}
