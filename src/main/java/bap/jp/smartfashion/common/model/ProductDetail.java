package bap.jp.smartfashion.common.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductDetail entity
 *
 */
@Entity
@Getter
@Setter
public class ProductDetail extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name ="product_id")
	private Product product; 
	
	@ManyToOne
	@JoinColumn(name ="product_color_id")
	private ProductColor productColor;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "productDetail" , cascade = CascadeType.ALL)
	@OrderBy(value = "productSize.id")
	private List<ProductSubDetail> productSubDetails = new ArrayList<>();
	
	@NotFound(action = NotFoundAction.IGNORE)
	@JsonManagedReference
	@OneToMany(mappedBy = "productDetail" , cascade = CascadeType.ALL)
	@OrderBy(value = "id")
	private List<ProductPhoto> productPhotos = new ArrayList<>();
	
	@JsonIgnore
	private String keyPoint;
	
	@JsonIgnore
	private String keyPointMessage;

	public ProductDetail() {}
	
	public ProductDetail(Integer id) {
		this.id = id;
	}

}
