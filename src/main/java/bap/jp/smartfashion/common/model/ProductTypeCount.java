package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductTypeCount entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"product_type_id", "shop_id"})})
public class ProductTypeCount extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	@JoinColumn(name = "product_type_id", updatable = false)
	private ProductType productType;
	
	@ManyToOne
	@JoinColumn(name = "shop_id", updatable = false)
	private Shop shop;
	
	private Integer count;
	
	public ProductTypeCount() {}

}
