package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * ShopBranch entity
 *
 */
@Entity
@Getter
@Setter
@Data
@AllArgsConstructor
public class ShopBranch extends BaseModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "shop_id", nullable = false)
    private Shop shop;

    @Size(max = 100, message = "BusinessAddress {validation.size-max}")
    private String businessAddress;

    public ShopBranch() {
    }
}
