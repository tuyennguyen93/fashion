package bap.jp.smartfashion.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductCollection entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class ProductCollection extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(unique = true)
	@Size(max = 50, message = "ProductCollection's name {validation.size-max}")
	private String name;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	private String image;
	
	@Size(max = 1000, message = "Description {validation.size-max}")
	private String description;
	
	@Size(max = 10, message = "Type {validation.size-max}")
	private String type;
	
	
	public ProductCollection(Integer id) {
		this.id = id;
	}

	public ProductCollection() {
	}
	
}
