package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * UserProduct entity
 *
 */
@Entity
@Getter
@Setter
public class UserProduct extends BaseModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	@NotNull(message = "Image's url {validation.not-null}")
	private String url;

	@JsonIgnore
	private String keyPoint;
	
	@JsonIgnore
	private String keyPointMessage;
	
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "user_product_collection_id", referencedColumnName = "id")
	private UserProductCollection userProductCollection;

	public UserProduct() {
	}
	
	public UserProduct(Integer id) {
		this.id = id;
	}

}