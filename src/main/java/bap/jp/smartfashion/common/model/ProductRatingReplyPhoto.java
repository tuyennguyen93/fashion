package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ProductRatingReplyPhoto Entity
 *
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class ProductRatingReplyPhoto extends BaseModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "product_rating_reply_id", referencedColumnName = "id")
    private ProductRatingReply productRatingReply;

    @Size(max = 255, message = "Image's url {validation.size-max}")
    @NotNull(message = "Image's url {validation.not-null}")
    private String url;

    public ProductRatingReplyPhoto(ProductRatingReply productRatingReply, String imageUrl) {
        this.productRatingReply = productRatingReply;
        this.url = imageUrl;
    }
}
