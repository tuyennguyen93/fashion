package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import bap.jp.smartfashion.common.dto.ProductMixMatchPhotoDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

/**
 * ProductMixMatchPhoto entity
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class ProductMixMatchPhoto extends BaseModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	private String url;

	@JsonBackReference
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "product_mix_match_id", referencedColumnName = "id")
	private ProductMixMatch productMixMatch;

	public ProductMixMatchPhoto() {
	}
	
	public ProductMixMatchPhoto(Integer id) {
		this.id = id;
	}
}
