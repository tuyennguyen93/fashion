package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductWearingPurpose entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "type"})})
public class ProductWearingPurpose extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Size(max = 100, message = "Name {validation.size-max}")
	@NotNull(message = "Name {validation.not-null}")
	private String name;
	
	@Size(max = 10, message = "Type {validation.size-max}")
	private String type;

	public ProductWearingPurpose() {
	}
}
