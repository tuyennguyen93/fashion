package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * UserProductBasket entity
 *
 */
@Entity
@Getter
@Setter
public class UserProductBasket extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;


	public UserProductBasket(User user, Product product) {
		this.user = user;
		this.product = product;
	}

	public UserProductBasket() {
	}
}
