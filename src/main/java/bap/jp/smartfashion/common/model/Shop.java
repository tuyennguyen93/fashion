package bap.jp.smartfashion.common.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.List;

/**
 * Shop entity
 *
 */
@Entity
@Getter
@Setter
public class Shop extends BaseModel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Size(min = 2, max = 50, message = "Shop's name  {validation.size-min-max}")
	private String name;

	@Email(message = "Email: {validation.email}")
	@Size(max = 150, message = "Email {validation.size-max}")
	private String email;

	@Size(max = 15, message = "Phone {validation.size-max}")
	private String phone;

	@Size(max = 255, message = "Avatar {validation.size-max}")
	private String avatar;

	@JsonFormat(pattern="HH:mm:ss")
	private LocalTime openTime;

	@JsonFormat(pattern="HH:mm:ss")
	private LocalTime closeTime;

	@JsonIgnore
	private String shopCode;

	@Size(max = 1000, message = "Description {validation.size-max}")
	private String description;
	
	@Size(max = 10)
	private String status;
	
	@JsonIgnore
	@OneToMany(mappedBy = "shop", cascade = CascadeType.ALL)
	private List<ShopBranch> shopBranches;

	@JsonIgnore
	@OneToMany(mappedBy = "shop", cascade = CascadeType.ALL)
	private List<ShopWarehouse> shopWareHouses;

	public Shop(Integer shopId) {
		this.id = shopId;
	}
	
	public Shop(){}
}
