package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.JoinFormula;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

/**
 * Product mix match
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class ProductMixMatch extends BaseModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@JsonBackReference
//	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "mix_match_id", referencedColumnName = "id")
	private MixMatch mixMatch;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "productMixMatch", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<ProductMixMatchPhoto> productMixMatchPhotos;

	private Integer priority;

	public ProductMixMatch() {
	}

}
