package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * UserRole entity
 *
 */
@Entity
@Getter
@Setter
@Data
@AllArgsConstructor
public class UserRole extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer userId;
	
	@Size(max = 50, message = "RoleName {validation.size-max}")
	private String roleName;

	public UserRole() {
	}
}