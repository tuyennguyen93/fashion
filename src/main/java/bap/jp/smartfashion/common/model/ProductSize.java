package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductSize entity
 *
 */
@Entity
@Getter
@Setter
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"size", "type"})})
public class ProductSize extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Size(max = 10, message = "Size {validation.size-max}")
	private String size;
	
	@JsonIgnore
	@Size(max = 10, message = "Type {validation.size-max}")
	private String type;

	@ManyToOne
	@JoinColumn(name = "product_size_chart_id")
	private ProductSizeChart productSizeChart;
	
	public ProductSize() {}

	public ProductSize(Integer productSizeId) {
		this.id = productSizeId;
	}

	public ProductSize(String size, String type, ProductSizeChart productSizeChart) {
		this.size = size;
		this.type = type;
		this.productSizeChart = productSizeChart;
	}
}
