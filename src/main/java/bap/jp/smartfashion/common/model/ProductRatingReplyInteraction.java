package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * ProductRatingReplyInteraction Entity
 *
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProductRatingReplyInteraction extends BaseModel {
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "product_rating_reply_id", referencedColumnName = "id")
    private ProductRatingReply productRatingReply;

    @NotNull(message = "{validation.not-null}")
    private String type;
}
