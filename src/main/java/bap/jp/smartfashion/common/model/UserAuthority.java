package bap.jp.smartfashion.common.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * UserAuthority entity
 *
 */
@Entity
@Getter
@Setter
public class UserAuthority extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Size(max = 50, message = "ActionName {validation.size-max}")
	private String actionName;

	@Size(max = 50, message = "FunctionName {validation.size-max}")
	private String functionName;
}
