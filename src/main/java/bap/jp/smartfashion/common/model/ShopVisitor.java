package bap.jp.smartfashion.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import bap.jp.smartfashion.common.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

/**
 * ShopVisitor entity
 *
 */
@Entity
@Getter
@Setter
public class ShopVisitor extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "shop_id", referencedColumnName = "id")
	private Shop shop;
	@Size(max = 15, message = "Type {validation.size-max}")
	private String type;
	private Integer typeCount;
	@Size(max = 10, message = "TimeLine {validation.size-max}")
	private String timeline;
	
	public ShopVisitor() {
	}
}