package bap.jp.smartfashion.common.model;

import bap.jp.smartfashion.common.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * UserModel entity
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
public class UserModel extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5775862862858260184L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	private String url;

	@JsonIgnore
	private String keyPoint;
	
	@JsonIgnore
	private String keyPointMessage;
	
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne
	@JoinColumn(name = "user_model_collection_id", referencedColumnName = "id")
	private UserModelCollection userModelCollection;
	
	@NotFound(action = NotFoundAction.IGNORE)
	@OneToOne(mappedBy = "userModel", cascade = CascadeType.ALL)
	private UserModelSetting userModelSetting;

	public UserModel() {
	}

	public UserModel(Integer id) {
		this.id = id;
	}

	public UserModel(User user, String url, String keyPoint, UserModelCollection userModelCollection) {
		super();
		this.user = user;
		this.url = url;
		this.keyPoint = keyPoint;
		this.userModelCollection = userModelCollection;
	}
	

}
