package bap.jp.smartfashion.common.specification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductWearingPurpose;
import bap.jp.smartfashion.common.model.ProductColor;
import bap.jp.smartfashion.common.model.ProductSize;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductSubDetail;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.vo.ProductFilter;
import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.model.ProductSizeChart;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserProductInteraction;
import bap.jp.smartfashion.common.model.UserProductBasket;


/**
 * Specifications Builder for {@link Product}
 *
 */
public class ProductSpecificationsBuilder {
	private static final String ALL = "ALL";

	/**
	 * Filter product
	 * @param productFilter {@link ProductFilter}
	 * @return
	 */
	public static Specification<Product> filterProduct(String name, ProductFilter productFilter) {
		return (root, query, cb) -> {
			
			List<Predicate> predicateList = new ArrayList<Predicate>();

			// Don't get values deleted
			predicateList.add(cb.equal(root.get("status"), ProductEnum.ProductStatus.ACTIVE.getCode()));

			// search shop
			if (productFilter.getShopId() != null) {
				Join<Product, Shop> shopJoin;
				shopJoin = root.join("shop", JoinType.INNER);
				predicateList.add(cb.equal(shopJoin.get("id"), productFilter.getShopId()));
			}
			// search product type
			if (productFilter.getProductTypeId() != null) {
				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = root.join("productType", JoinType.INNER);
				List<Integer> productTypes = (List<Integer>) productFilter.getProductTypeId();
				if (!productTypes.isEmpty()) {
					predicateList.add(productTypeJoin.get("id").in(productTypes));
				}
			}

			// search collection
			if (productFilter.getCollectionId() != null) {
				Join<Product, ProductCollection> productCollectionJoin;
				productCollectionJoin = root.join("productCollection", JoinType.INNER);
				predicateList.add(cb.equal(productCollectionJoin.get("id"), productFilter.getCollectionId()));
			}
			
			
			// search product name
			if (!StringUtils.isEmpty(name)) {
				Predicate searchProductName = cb.like(root.get("name"), "%" + name + "%");
				Predicate searchProductCode = cb.like(root.get("productCode"), "%" + name + "%");

				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = root.join("productType");
				Predicate searchProductType = cb.like(productTypeJoin.get("name"),"%" + name + "%");

				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = root.join("productDetails");
				Predicate searchProductDetail = cb.like(productDetailJoin.join("productColor").get("name"),"%" + name + "%");

				Predicate rs = cb.or(searchProductName, searchProductCode, searchProductType, searchProductDetail);

				predicateList.add(rs);

			}
			
			// search wearing purposes
			if (productFilter.getWearingPurposeIds() != null) {
				Join<Product, ProductWearingPurpose> productWearingPurposeJoin;
				productWearingPurposeJoin = root.join("productWearingPurpose");
				predicateList.add(productWearingPurposeJoin.get("id").in(productFilter.getWearingPurposeIds()));
			}

			// search range price
			if (productFilter.getMinPrice() != null && productFilter.getMaxPrice() != null) {
				predicateList.add(cb.between(root.get("price"), productFilter.getMinPrice(), productFilter.getMaxPrice()));
			}
			
			// search color
			if ((productFilter.getColorIds() != null && !productFilter.getColorIds().isEmpty())
					&& (productFilter.getSizes() == null || productFilter.getSizes().contains(ALL))) {

				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = root.join("productDetails");

				Join<ProductDetail, ProductColor> productColorJoin;
				productColorJoin = productDetailJoin.join("productColor");
				predicateList.add(productColorJoin.get("id").in(productFilter.getColorIds()));
			}

			// search size chart
			if (!StringUtils.isEmpty(productFilter.getSizeChart()) && !productFilter.getSizeChart().equalsIgnoreCase(ALL)) {
				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = root.join("productDetails");
				
				Join<ProductDetail, ProductSubDetail> productSubDetailJoin;
				productSubDetailJoin = productDetailJoin.join("productSubDetails");

				Join<ProductSubDetail, ProductSize> productSizeJoin;
				productSizeJoin = productSubDetailJoin.join("productSize");

				Join<Product, ProductSizeChart> productSizeChartJoin;
				productSizeChartJoin = root.join("productSizeChart");
				predicateList.add(cb.equal(productSizeChartJoin.get("name"), productFilter.getSizeChart()));

				if (productFilter.getSizes() != null && !productFilter.getSizes().contains(ALL)) {
					if(productFilter.getColorIds() != null && !productFilter.getColorIds().isEmpty()) {
						Join<ProductDetail, ProductColor> productColorJoin;
						productColorJoin = productDetailJoin.join("productColor");

						List<Predicate> predicates = new ArrayList<>();
						for (String size : productFilter.getSizes()) {
							predicates.add(cb.and(productColorJoin.get("id").in(productFilter.getColorIds()),
									cb.equal(productSizeJoin.get("size"), size)));
						}
						predicateList.add(cb.or(predicates.toArray(new Predicate[predicates.size()])));
					} else {
						predicateList.add(productSizeJoin.get("size").in(productFilter.getSizes()));
					}
				}
			} else if (productFilter.getSizes() != null && !productFilter.getSizes().isEmpty()){
				if (!productFilter.getSizes().contains(ALL)) {

					Join<Product, ProductDetail> productDetailJoin;
					productDetailJoin = root.join("productDetails");

					Join<ProductDetail, ProductSubDetail> productSubDetailJoin;
					productSubDetailJoin = productDetailJoin.join("productSubDetails");

					Join<ProductSubDetail, ProductSize> productSizeJoin;
					productSizeJoin = productSubDetailJoin.join("productSize");

					if(productFilter.getColorIds() != null && !productFilter.getColorIds().isEmpty()) {
						Join<ProductDetail, ProductColor> productColorJoin;
						productColorJoin = productDetailJoin.join("productColor");

						List<Predicate> predicates = new ArrayList<>();
						for (String size : productFilter.getSizes()) {
							predicates.add(cb.and(productColorJoin.get("id").in(productFilter.getColorIds()),
									cb.equal(productSizeJoin.get("size"), size)));
						}
						predicateList.add(cb.or(predicates.toArray(new Predicate[predicates.size()])));
					} else {
						predicateList.add(productSizeJoin.get("size").in(productFilter.getSizes()));
					}
				}
			}

			if(productFilter.isSale()) {
				predicateList.add(cb.greaterThan(root.get("discount"), 0));
			}
			query.distinct(true);
			return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
		};
	}
	
	/**
	 * search {@link Product}
	 * @param name {@link Product} name
	 * @param productFilter {@link ProductFilter}
	 * @return
	 */
	public static Specification<Product> searchProducts(String name, ProductFilter productFilter) {
		return (root, query, cb) -> {
			
			List<Predicate> predicateList = new ArrayList<Predicate>();

			// Don't get values deleted
			predicateList.add(cb.equal(root.get("status"), ProductEnum.ProductStatus.ACTIVE.getCode()));

			// search shop
			if (productFilter.getShopId() != null) {
				Join<Product, Shop> shopJoin;
				shopJoin = root.join("shop");
				predicateList.add(cb.equal(shopJoin.get("id"), productFilter.getShopId()));
			}
			// search product type
			if (productFilter.getProductTypeId() != null) {
				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = root.join("productType");
				List<Integer> productTypes = (List<Integer>) productFilter.getProductTypeId();
				predicateList.add(productTypeJoin.get("id").in(productTypes));
			}
			// search collection
			if (productFilter.getCollectionId() != null) {
				Join<Product, ProductCollection> productCollectionJoin;
				productCollectionJoin = root.join("productCollection");
				predicateList.add(cb.equal(productCollectionJoin.get("id"), productFilter.getCollectionId()));
			}
			
			// search product name
			if (!StringUtils.isEmpty(name)) {
				Predicate searchProductName = cb.like(root.get("name"), "%" + name + "%");
				Predicate searchProductCode = cb.like(root.get("productCode"), "%" + name + "%");


				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = root.join("productType");
				Predicate searchProductType = cb.like(productTypeJoin.get("name"),"%" + name + "%");

				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = root.join("productDetails");
				Predicate searchProductDetail = cb.like(productDetailJoin.join("productColor").get("name"),"%" + name + "%");

				Predicate rs = cb.or(searchProductName, searchProductCode, searchProductType, searchProductDetail);

				predicateList.add(rs);

			}

			// search product that have discount
			if(productFilter.isSale()) {
				predicateList.add(cb.greaterThan(root.get("discount"), 0));
			}
			query.distinct(true);

			return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
		};
	}
	/**
	 * Check is search all size 
	 * 
	 * @param productFilter {@link ProductFilter}
	 * @return
	 */
	private static boolean isAllSize(ProductFilter productFilter) {
		return productFilter.getSizes() != null && productFilter.getSizes().contains(ALL);
	}
	
	/**
	 * Check is search by sizes 
	 * 
	 * @param productFilter {@link ProductFilter}
	 * @return
	 */
	private static boolean isFilterBySizes(ProductFilter productFilter) {
		return (productFilter.getSizes() != null && !productFilter.getSizes().contains(ALL)) && !productFilter.getSizes().isEmpty();
	}
	
	private static void buildProductInteraction(ProductFilter productFilter, Root root, CriteriaBuilder cb) {
		if(productFilter.getSortBy() != null) {
			if (productFilter.getSortBy().equals(ProductEnum.ProductSort.TRIED_PRODUCTS.toString())) {
				Join<Product, ProductInteraction> productInteractionJoin;
				productInteractionJoin = root.join("productInteractions", JoinType.LEFT);
				productInteractionJoin.on(cb.equal(productInteractionJoin.get("actionCode"), ProductEnum.ProductInteraction.TRY_PRODUCT.getCode()));
			} else if (productFilter.getSortBy().equals(ProductEnum.ProductSort.FAVORITED_PRODUCTS.toString())) {
				Join<Product, ProductInteraction> productInteractionJoin;
				productInteractionJoin = root.join("productInteractions", JoinType.LEFT);
				productInteractionJoin.on(cb.equal(productInteractionJoin.get("actionCode"), ProductEnum.ProductInteraction.FAVORITE_PRODUCT.getCode()));
			}
		}
	}

	/**
	 * filter product with interaction
	 *
	 * @param productFilter {@link ProductFilter}
	 * @return {@link Specification} {@link Product}
	 */
	public static Specification<Product> filterProductInteraction(String name, ProductFilter productFilter) {
		return (root, query, cb) -> {

			List<Predicate> predicateList = new ArrayList<Predicate>();

			Subquery<Product> sq = query.subquery(Product.class);
			Root<Product> productRoot = sq.from(Product.class);

			// Don't get values deleted
			predicateList.add(cb.equal(productRoot.get("status"), ProductEnum.ProductStatus.ACTIVE.getCode()));

			// search shop
			if (productFilter.getShopId() != null) {
				Join<Product, Shop> shopJoin;
				shopJoin = productRoot.join("shop", JoinType.INNER);
				predicateList.add(cb.equal(shopJoin.get("id"), productFilter.getShopId()));
			}
			// search product type
			if (productFilter.getProductTypeId() != null) {
				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = productRoot.join("productType", JoinType.INNER);
				List<Integer> productTypes = (List<Integer>) productFilter.getProductTypeId();
				if (!productTypes.isEmpty()) {
					predicateList.add(productTypeJoin.get("id").in(productTypes));
				}
			}
			// search collection
			if (productFilter.getCollectionId() != null) {
				Join<Product, ProductCollection> productCollectionJoin;
				productCollectionJoin = productRoot.join("productCollection", JoinType.INNER);
				predicateList.add(cb.equal(productCollectionJoin.get("id"), productFilter.getCollectionId()));
			}
			
			// search product name
			if (!StringUtils.isEmpty(name)) {
				Predicate searchProductName = cb.like(productRoot.get("name"), "%" + name + "%");
				Predicate searchProductCode = cb.like(productRoot.get("productCode"), "%" + name + "%");


				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = productRoot.join("productType");
				Predicate searchProductType = cb.like(productTypeJoin.get("name"),"%" + name + "%");

				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = productRoot.join("productDetails");
				Predicate searchProductDetail = cb.like(productDetailJoin.join("productColor").get("name"),"%" + name + "%");

				Predicate rs = cb.or(searchProductName, searchProductCode, searchProductType, searchProductDetail);

				predicateList.add(rs);

			}

			// search color
			if ((productFilter.getColorIds() != null && !productFilter.getColorIds().isEmpty())
					&& (productFilter.getSizes() == null || productFilter.getSizes().contains(ALL))) {
				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = productRoot.join("productDetails");
				Join<ProductDetail, ProductColor> productColorJoin;
				productColorJoin = productDetailJoin.join("productColor");
				predicateList.add(productColorJoin.get("id").in(productFilter.getColorIds()));
			}

			// search size chart
			if (!StringUtils.isEmpty(productFilter.getSizeChart()) && !productFilter.getSizeChart().equalsIgnoreCase(ALL)) {
				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = productRoot.join("productDetails");
				
				Join<ProductDetail, ProductSubDetail> productSubDetailJoin;
				productSubDetailJoin = productDetailJoin.join("productSubDetails");
				
				Join<ProductSubDetail, ProductSize> productSizeJoin;
				productSizeJoin = productSubDetailJoin.join("productSize");

				Join<Product, ProductSizeChart> productSizeChartJoin;
				productSizeChartJoin = productRoot.join("productSizeChart");
				predicateList.add(cb.equal(productSizeChartJoin.get("name"), productFilter.getSizeChart()));

				if (productFilter.getSizes() != null && !productFilter.getSizes().contains(ALL)) {
					if(productFilter.getColorIds() != null && !productFilter.getColorIds().isEmpty()) {
						Join<ProductDetail, ProductColor> productColorJoin;
						productColorJoin = productDetailJoin.join("productColor");

						List<Predicate> predicates = new ArrayList<>();
						for (String size : productFilter.getSizes()) {
							predicates.add(cb.and(productColorJoin.get("id").in(productFilter.getColorIds()),
									cb.equal(productSizeJoin.get("size"), size)));
						}
						predicateList.add(cb.or(predicates.toArray(new Predicate[predicates.size()])));
					} else {
						predicateList.add(productSizeJoin.get("size").in(productFilter.getSizes()));
					}
				}
			} else if (productFilter.getSizes() != null && !productFilter.getSizes().isEmpty()) {
				if (!productFilter.getSizes().contains(ALL)) {

					Join<Product, ProductDetail> productDetailJoin;
					productDetailJoin = productRoot.join("productDetails");

					Join<ProductDetail, ProductSubDetail> productSubDetailJoin;
					productSubDetailJoin = productDetailJoin.join("productSubDetails");

					Join<ProductSubDetail, ProductSize> productSizeJoin;
					productSizeJoin = productSubDetailJoin.join("productSize");

					if(productFilter.getColorIds() != null && !productFilter.getColorIds().isEmpty()) {
						Join<ProductDetail, ProductColor> productColorJoin;
						productColorJoin = productDetailJoin.join("productColor");

						List<Predicate> predicates = new ArrayList<>();
						for (String size : productFilter.getSizes()) {
							predicates.add(cb.and(productColorJoin.get("id").in(productFilter.getColorIds()),
									cb.equal(productSizeJoin.get("size"), size)));
						}
						predicateList.add(cb.or(predicates.toArray(new Predicate[predicates.size()])));
					} else {
						predicateList.add(productSizeJoin.get("size").in(productFilter.getSizes()));
					}
				}
			}

			if(productFilter.isSale()) {
				predicateList.add(cb.greaterThan(productRoot.get("discount"), 0));
			}

			buildProductInteraction(productFilter, productRoot, cb);
			sq.select(productRoot).where(cb.and(predicateList.toArray(new Predicate[predicateList.size()])));
			buildProductInteraction(productFilter, root, cb);
			return cb.in(root).value(sq);
		};
	}

	/**
	 * search Product with interaction
	 *
	 * @param name {@link Product} productName
	 * @param productFilter {@link ProductFilter}
	 * @return {@link Specification} {@link Product}
	 */
	public static Specification<Product> searchProductsWithInteraction(String name, ProductFilter productFilter) {
		return (root, query, cb) -> {

			List<Predicate> predicateList = new ArrayList<Predicate>();

			Subquery<Product> sq = query.subquery(Product.class);
			Root<Product> productRoot = sq.from(Product.class);

			// Don't get values deleted
			predicateList.add(cb.equal(productRoot.get("status"), ProductEnum.ProductStatus.ACTIVE.getCode()));

			// search shop
			if (productFilter.getShopId() != null) {
				Join<Product, Shop> shopJoin;
				shopJoin = productRoot.join("shop");
				predicateList.add(cb.equal(shopJoin.get("id"), productFilter.getShopId()));
			}
			// search product type
			if (productFilter.getProductTypeId() != null) {
				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = productRoot.join("productType");
				List<Integer> productTypes = (List<Integer>) productFilter.getProductTypeId();
				predicateList.add(productTypeJoin.get("id").in(productTypes));
			}
			// search collection
			if (productFilter.getCollectionId() != null) {
				Join<Product, ProductCollection> productCollectionJoin;
				productCollectionJoin = productRoot.join("productCollection");
				predicateList.add(cb.equal(productCollectionJoin.get("id"), productFilter.getCollectionId()));
			}

			// search product name
			if (!StringUtils.isEmpty(name)) {
				Predicate searchProductName = cb.like(productRoot.get("name"), "%" + name + "%");
				Predicate searchProductCode = cb.like(productRoot.get("productCode"), "%" + name + "%");


				Join<Product, ProductType> productTypeJoin;
				productTypeJoin = productRoot.join("productType");
				Predicate searchProductType = cb.like(productTypeJoin.get("name"), "%" + name + "%");

				Join<Product, ProductDetail> productDetailJoin;
				productDetailJoin = productRoot.join("productDetails");
				Predicate searchProductDetail = cb.like(productDetailJoin.join("productColor").get("name"), "%" + name + "%");

				Predicate rs = cb.or(searchProductName, searchProductCode, searchProductType, searchProductDetail);

				predicateList.add(rs);

			}

			// search product that have discount
			if(productFilter.isSale()) {
				predicateList.add(cb.greaterThan(productRoot.get("discount"), 0));
			}

			buildProductInteraction(productFilter, productRoot, cb);
			sq.select(productRoot).where(cb.and(predicateList.toArray(new Predicate[predicateList.size()])));
			buildProductInteraction(productFilter, root, cb);
			return cb.in(root).value(sq);
		};
	}
}