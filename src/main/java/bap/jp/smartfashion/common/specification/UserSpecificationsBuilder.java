package bap.jp.smartfashion.common.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.vo.UserFilter;

/**
 * Specifications Builder for {@link User}
 *
 */
public class UserSpecificationsBuilder {

	public static Specification<User> filterUser(UserFilter userFilter) {
		return (root, query, cb) -> {
			
			List<Predicate> predicateList = new ArrayList<Predicate>();
			predicateList.add(cb.equal(root.get("deleted"), 0));
			
			if (userFilter.getBlocked() != null) {
				predicateList.add(cb.equal(root.get("blocked"), userFilter.getBlocked()));
			}
			
			if (userFilter.getUserType() != null) {
				predicateList.add(cb.equal(root.get("userType"), userFilter.getUserType()));
			}

			if (userFilter.getRegisterStatus() != null) {
				predicateList.add(cb.equal(root.get("registerStatus"), userFilter.getRegisterStatus()));
			}
			
			return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
		};
	}
}