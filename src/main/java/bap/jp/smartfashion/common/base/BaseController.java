package bap.jp.smartfashion.common.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import bap.jp.smartfashion.common.vo.JwtToken;
import bap.jp.smartfashion.config.jwt.AccessToken;
import bap.jp.smartfashion.config.jwt.JwtProvider;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import bap.jp.smartfashion.util.constants.Constants;

/**
 * Controller to managing base function in application
 */
@Controller
public class BaseController {
	

	@Autowired
	private JwtProvider tokenProvider;

	/**
	 * Generate jwt for api
	 * 
	 * @param userName userName
	 * @param isRemeberMe remember me
	 * 
	 * @return {@link APIResponse}
	 */
	protected JwtToken jwtForAPIResponse(String userName, boolean isRemeberMe) {
		AccessToken accessToken = this.tokenProvider.createAccessToken(userName, isRemeberMe);
		JwtToken token = new JwtToken(accessToken.getToken(), Constants.JWT_TOKEN_TYPE);
		
		return token;
	}
}
