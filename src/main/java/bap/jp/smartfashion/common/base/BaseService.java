package bap.jp.smartfashion.common.base;


import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.dto.ProductDetailDTO;
import bap.jp.smartfashion.common.dto.ProductRatingDTO;
import bap.jp.smartfashion.common.dto.ProductSizeDTO;
import bap.jp.smartfashion.common.dto.ProfileDTO;
import bap.jp.smartfashion.common.dto.ShopCollectionVisitorDTO;
import bap.jp.smartfashion.common.dto.ShopVisitorInfo;
import bap.jp.smartfashion.common.mapper.ProductMapper;
import bap.jp.smartfashion.common.model.*;
import bap.jp.smartfashion.common.repository.*;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.common.vo.ReturningVisitor;
import bap.jp.smartfashion.enums.ProductEnum;

import bap.jp.smartfashion.enums.ShopEnum;
import bap.jp.smartfashion.enums.ShopEnum.ShopStatus;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;

import bap.jp.smartfashion.util.CodeEnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import bap.jp.smartfashion.common.dto.ImageUploadDTO;
import bap.jp.smartfashion.common.service.AWSService;
import bap.jp.smartfashion.common.service.EmailService;
import bap.jp.smartfashion.common.vo.PreSignedAWS;
import bap.jp.smartfashion.config.UserPrinciple;
import bap.jp.smartfashion.config.jwt.TokenStore;
import bap.jp.smartfashion.enums.TokenEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.util.constants.Constants;

/**
 * Base Service
 *
 */
@Service
public class BaseService {

	@Value("${email.smartfashion}")
	protected String smartFashionEmail;

	@Value("${website.smartfashion}")
	protected String smartFashionWebsite;

	@Value("${base.url}")
	protected String baseUrl;

	@Value("${app.paging.default.limit}")
	protected int pagingDefaultLimit;

	@Value("${app.paging.default.pageInit}")
	protected int pagingDefaultPageInit;

	@Value("${app.paging.default.offset}")
	protected int pagingDefaultOffset;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ShopRepository shopRepository;

	@Autowired
	private AWSService awsService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Autowired
	private ProductCollectionRepository productCollectionRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private UserBankAccountRepository userBankAccountRepository;

	@Autowired
	private ShopBranchRepository shopBranchRepository;

	@Autowired
	private MixMatchRepository mixMatchRepository;

	@Autowired
	private ProductTypeCountRepository productTypeCountRepository;

	@Autowired
	private ProductRatingRepository productRatingRepository;

	@Autowired
	private ProductRatingCountRepository productRatingCountRepository;

	@Autowired
	private ProductRatingReplyRepository productRatingReplyRepository;

	@Autowired
	private ProductMapper productMapper;

	@Autowired
	private ProductColorRepository productColorRepository;

	@Autowired
    private UserModelRepository userModelRepository;

	@Autowired
	private ShopVisitorRepository shopVisitorRepository;

	@Autowired
    private ShopCollectionVisitorRepository shopCollectionVisitorRepository;

	@Autowired
	private ProductRatingInteractionRepository productRatingInteractionRepository;

	@Autowired
	private ProductRatingReplyInteractionRepository productRatingReplyInteractionRepository;

	@Autowired
	private ProductTypeRepository productTypeRepository;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ProductSizeChartRepository productSizeChartRepository;

	@Autowired
	private ShopWarehouseRepository shopWarehouseRepository;
	
	/**
	 * Build {@link PageRequest} with page and limit
	 *
	 * @param page
	 * @param limit
	 * @return
	 */
	protected PageRequest buildPageRequest(Integer page, Integer limit) {
		page = page == null ? 0 : page - this.pagingDefaultPageInit;
		limit = limit == null ? this.pagingDefaultLimit : limit;
		return PageRequest.of(page, limit);
	}

	/**
	 * Build {@link PageRequest} with page, limit and sort
	 *
	 * @param page
	 * @param limit
	 * @param sort
	 * @return
	 */
	protected PageRequest buildPageRequest(Integer page, Integer limit, Sort sort) {
		page = page == null ? 0 : page - this.pagingDefaultPageInit;
		limit = limit == null ? this.pagingDefaultLimit : limit;
		return PageRequest.of(page, limit, sort);
	}


	/**
	 * Find {@link User} by User Id
	 *
	 * @param userId
	 * @return
	 * @throws NotFoundException
	 */
	protected User findUserByUserId(int userId) throws NotFoundException {
		return this.userRepository.findByIdAndDeletedIsFalse(userId).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("info.user"))));
	}

	/**
	 * Find {@link ProductCollection} by Product collection Id
	 *
	 * @param collectionId
	 * @return
	 * @throws NotFoundException
	 */
	protected ProductCollection findProductCollectionById(int collectionId) throws NotFoundException {
		return this.productCollectionRepository.findById(collectionId).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_PRODUCT_COLLECTION_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("product-collection"))));
	}

	/**
	 * Get current userid has logged.
	 *
	 * @return
	 */
	public Integer getCurrentUserId() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer userId = null;
		if (principal instanceof UserDetails) {
			userId = ((UserPrinciple) principal).getId();
		} else if (!principal.equals("anonymousUser")){
			userId = Integer.parseInt((String) principal);
		}
		return userId;
	}

	/**
	 * Get current username has logged.
	 *
	 * @return username
	 */
	public User getCurrentUserLogged() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = new User();
		if (principal instanceof UserDetails) {
			user.setId(((UserPrinciple) principal).getId());
			user.setName(((UserPrinciple) principal).getName());
			user.setLangKey(((UserPrinciple) principal).getLangKey());
		}
		return user;
	}

	public List<PreSignedAWS> uploadImages(List<ImageUploadDTO> imageUploadDTOs) {
		List<PreSignedAWS> preSignedAWSResponse = new ArrayList<PreSignedAWS>();
		imageUploadDTOs.forEach(imageUploadDTO -> {
			String fileName = imageUploadDTO.getFileName() + "_" + new Date().getTime() + "." + imageUploadDTO.getFileType();
			PreSignedAWS preSignedAWS = this.awsService.uploadPresignedUrl(fileName);
			preSignedAWSResponse.add(preSignedAWS);
		});
		
		return preSignedAWSResponse;
	}

	/**
	 * Find {@link Shop} belong User logged
	 *
	 * @return
	 * @throws NotFoundException
	 */
	protected boolean isShopBelongUserLogged() throws NotFoundException {
		this.shopRepository.findOneByUser_Id(this.getCurrentUserId()).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("info.shop"))));
		return true;
	}

	/**
	 * Find {@link Shop} belong user logged
	 *
	 * @return
	 * @throws NotFoundException
	 */
	public Shop checkShopBelongUserLogged(int shopId) throws NotFoundException {
		int userId = this.getCurrentUserId();
		return this.shopRepository.findByIdAndUser_Id(shopId,userId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
					this.messageService.getMessage("info.shop"))));
	}
	/**
	 * Send email to verify user
	 *
	 * @param user  {@link User} information
	 * @param token Token used to verify
	 */
	protected void sendEmailToVerifyUser(User user, String token) {
		// set context email
		Context context = new Context();
		Map<String, Object> model = new HashMap<>();
		model.put("smartFashionWebsite", this.smartFashionWebsite); // website url
		model.put("user", user); //user info
		String url = this.baseUrl + "/activate" + Constants.SLASH + token; //create base url
		model.put("url", url);
		model.put("tokenTime", TokenEnum.TOKEN_CREATE_USER.getValue());
		model.put("token", token);
		context.setVariables(model);

		String content = this.templateEngine.process("/email/email_create_user", context);
		this.emailService.sendEmail(Constants.EMAIL_VERIFY_USER_TITLE, this.smartFashionEmail, user.getEmail(), content);
	}

	/**
	 * Get value from redis
	 *
	 * @param key key to get value
	 * @return value of key
	 */
	protected Object redisGetValue(final String key) {
		return this.redisTemplate.opsForValue().get(key);
	}

	/**
	 * Get value from redis
	 *
	 * @param key key to get value
	 * @return value of key
	 */
	protected void redisSetValue(String key, String value) {
		this.redisTemplate.opsForValue().set(key, value);
	}

	/**
	 * Set value to redis
	 *
	 * @param key  key to set into redis
	 * @param date Expire date of key
	 * @return value of key
	 */
	protected void redisSetExpire(String key, Date date) {
		this.redisTemplate.expireAt(key, date);
	}

	/**
	 * Get expire time of key
	 *
	 * @param key key to get expire time
	 * @return value of key
	 */
	protected long redisGetExpire(String key) {
		return this.redisTemplate.getExpire(key);
	}

	/**
	 * Delete key in redis
	 *
	 * @param key key to delete
	 */
	protected void redisDeleteKey(String key) {
		this.redisTemplate.delete(key);
	}

	/**
	 * check shop be long user logged
	 *
	 * @param shopId
	 * @throws NotFoundException
	 */
	public Shop checkShopUserLogged(int shopId) throws NotFoundException {
		int userId = this.getCurrentUserId();
		return this.shopRepository.findByIdAndUser_Id(shopId, userId).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("info.shop")))
		);

	}


	/**
	 * get product with shop
	 *
	 * @param productId {@link Product} id
	 * @param shopId    {@link Shop}
	 * @return product {@link Product}
	 * @throws NotFoundException
	 */
	public Product getProductWithShop(Integer productId, Integer shopId) throws NotFoundException {
		return this.productRepository.findByIdAndShop_IdAndStatus(productId, shopId,
				ProductEnum.ProductStatus.ACTIVE.getCode()).orElseThrow(
				()-> new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("info.product"))));
	}

	/**
	 * get information user by user Id
	 *
	 * @return User
	 * @throws NotFoundException
	 */
	public User getUserByUserId(int userId) throws NotFoundException {
		return this.userRepository.findById(userId).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_USER_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("info.user"))));
	}


	/**
	 * Get infomation shop by user id
	 *
	 * @return Shop
	 * @throws NotFoundException
	 */
	public Shop getShopByUserId(int userId) throws NotFoundException {
		return this.shopRepository.findOneByUser_Id(userId).orElse(null);
	}

	/**
	 * Get shop branch by shop id
	 *
	 * @return ShopBranch
	 */
	public List<ShopBranch> getShopBranchByShopId(Integer shopId) {
		return this.shopBranchRepository.findByShopId(shopId);
	}

	/**
	 * Get bank account by user logged
	 *
	 * @return UserBankAccount
	 * @throws NotFoundException
	 */
	public UserBankAccount getUserBankByUserId(Integer userId) throws NotFoundException {
		return this.userBankAccountRepository.findOneByUser_Id(userId).orElse(null);
	}


	/**
	 * Get profile
	 *
	 * @return {@link ProfileDTO}
	 */
	public ProfileDTO getProfileCurrentUserLogged() throws NotFoundException {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ProfileDTO profile = new ProfileDTO();

		if (principal instanceof UserDetails) {
			int userId = getCurrentUserId();

			// find user by user id
			User userProfile = this.getUserByUserId(userId);
			profile.setId(userId);
			profile.setEmail(userProfile.getEmail());
			profile.setTaxNumber(userProfile.getTaxNumber());
			profile.setGender(userProfile.getGender());
			profile.setPassportNumber(userProfile.getPassportNumber());
			profile.setPassportBackSide(userProfile.getPassportBackSide());
			profile.setPassportFrontSide(userProfile.getPassportFrontSide());
			profile.setOwnerName(userProfile.getName());
			profile.setPhone(userProfile.getPhone());

			// find shop by userId
			Shop shopProfile = this.getShopByUserId(userId);
			if (shopProfile != null) {
				profile.setShopName(shopProfile.getName());
				if(shopProfile.getOpenTime()!=null){
					profile.setOpenTime(shopProfile.getOpenTime().toString());
				}
				if(shopProfile.getCloseTime()!=null) {
					profile.setCloseTime(shopProfile.getCloseTime().toString());
				}
				profile.setShopPhone(shopProfile.getPhone());
				profile.setDescription(shopProfile.getDescription());
				profile.setAvatar(shopProfile.getAvatar());

				// get shop branch
				List<ShopBranch> shopBranch = this.getShopBranchByShopId(shopProfile.getId());
				profile.setShopBranchList(shopBranch);

				// Get list of warehouses's addresses
				Optional<List<ShopWarehouse>> shopWareHouses = this.shopWarehouseRepository.findAllByShop_Id(shopProfile.getId());
				if(shopWareHouses.isPresent()) {
					profile.setWarehouseAddressList(shopWareHouses.get());
				}
			}
			// find bank account
			UserBankAccount userBankAccountProfile = this.getUserBankByUserId(userId);
			if (userBankAccountProfile != null) {
				profile.setBankName(userBankAccountProfile.getBankName());
				profile.setBankBranch(userBankAccountProfile.getBankBranch());
				profile.setOwnerBankName(userBankAccountProfile.getOwnerName());
				profile.setAccountNumber(userBankAccountProfile.getAccountNumber());
			}

		}
		return profile;
	}

	/**
	 * check draft product
	 *
	 * @param shopId {@link Shop}
	 * @return productDTO {@link ProductDTO}
	 */
	public Product checkDraftProduct(Integer shopId) {
		return this.productRepository.findByStatusAndShop_Id(ProductEnum.ProductStatus.DRAFT.toString(), shopId).orElse(null);
	}

	/**
	 * Check mail unique
	 *
	 * @param userId {@link User}
	 * @param email {@link User}
	 * @param userType {@link UserRole}
	 * @return {true, false}
	 * @throws ConflictException
	 */
	public Boolean checkEmailUnique(Integer userId, String email, String userType) throws ConflictException {
		Optional<User> existingUserOptional = this.userRepository.findOneByEmailAndUserType(email, userType); //check email of user other
		if (existingUserOptional.isPresent() && (!existingUserOptional.get().getId().equals(userId))) {
			throw new ConflictException(ConflictException.ERROR_EMAIL_EXISTED, this.messageService.getMessage("error.email-exited-msg")); //throw exception if email existed
		}
		return true;
	}

	/**
	 * Check taxNumber unique
	 *
	 * @param userId {@link User}
	 * @param taxNumber {@link User}
	 * @param userType {@link UserRole}
	 * @return {true, false}
	 * @throws ConflictException
	 */
	public Boolean checkTaxNumberUnique(Integer userId, String taxNumber, String userType) throws ConflictException {
		Optional<User> existingUserOptional = this.userRepository.findOneByTaxNumberAndUserType(taxNumber, userType);
		if (existingUserOptional.isPresent() && (!existingUserOptional.get().getId().equals(userId))) {
			throw new ConflictException(ConflictException.ERROR_TAX_NUMBER_EXISTED, this.messageService.getMessage("error.tax-number-exited-msg"));
		}
		return true;
	}

	/**
	 * Check passpostNumber
	 *
	 * @param userId {@link User}
	 * @param passpostNumber {@link User}
	 * @param userType {@link User}
	 * @return {true, false}
	 * @throws ConflictException
	 */
	public Boolean checkPasspostNumberUnique(Integer userId, String passpostNumber, String userType) throws ConflictException {
		Optional<User> existingUserOptional = this.userRepository.findOneByPassportNumberAndUserType(passpostNumber, userType);
		if (existingUserOptional.isPresent() && (!existingUserOptional.get().getId().equals(userId))) {
			throw new ConflictException(ConflictException.ERROR_PASSPORT_NUMBER_EXISTED, this.messageService.getMessage("error.passport-number-exited-msg"));
		}
		return true;
	}

	/**
	 * check product with shop
	 *
	 * @param productId {@link Product} id
	 * @param shopId    {@link Shop}
	 * @return product {@link Product}
	 * @throws NotFoundException
	 */
	public Product checkProductWithShop(Integer productId, Integer shopId)  {
		return this.productRepository.findByIdAndShop_IdAndStatus(productId, shopId,
				ProductEnum.ProductStatus.ACTIVE.getCode()).orElse(null);
	}

	/**
	 * find mix match by mix match id
	 *
	 * @param mixMatchId {@link MixMatch}
	 * @return {@link MixMatch}
	 */
	public MixMatch findMixMatchById(Integer mixMatchId) throws NotFoundException {
		return this.mixMatchRepository.findById(mixMatchId).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_MIX_MATCH_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("mix-match"))));
	}

	/**
	 * Check list of size in productDetail, duplicate ot not
	 *
	 * @param productSizeDTOs list of {@link ProductSizeDTO}
	 * @throws ConflictException
	 */
	public void checkDuplicateSizeInProductSubDetail(List<ProductSizeDTO> productSizeDTOs) throws ConflictException {
		Set<Integer> sizeIds = new HashSet<>();
		for(ProductSizeDTO productSizeDTO : productSizeDTOs) {
			if(!sizeIds.add(productSizeDTO.getId())) {
				throw new ConflictException(ConflictException.ERROR_PRODUCT_SIZE_CONFLICT, String.format(this.messageService.getMessage("error.conflict-msg"),
						this.messageService.getMessage("size")));
			}
		}
	}

	/**
	 * check Color Duplicate
	 *
	 * @param productDetailDtos {@link ProductDetailDTO}
	 * @throws NotFoundException
	 */
	public void checkColorDuplicate(List<ProductDetailDTO>  productDetailDTOs) throws ConflictException {
		Set<Integer> colorIds = new HashSet<>();
		for(ProductDetailDTO productDetailDTO : productDetailDTOs) {
			if(!colorIds.add(productDetailDTO.getProductColor().getId())) {
				throw new ConflictException(ConflictException.ERROR_PRODUCT_COLOR_CONFLICT, String.format(this.messageService.getMessage("error.conflict-msg"),
						this.messageService.getMessage("product-detail-color")));
			}
		}
	}

	/**
	 * Get ProductType with productTypeId and shopId
	 *
	 * @param productTypeId {@link ProductType}
	 * @param shopId {@link Shop}
	 * @return {@link ProductTypeCount}
	 */
	public ProductTypeCount getProductTypeCount(Integer productTypeId, Integer shopId) {
		return this.productTypeCountRepository.findByProductType_IdAndShop_Id(productTypeId, shopId).orElse(null);
	}

	/**
	 * Check ProductRating with userId and productId, exits or not
	 *
	 * @param userId {@link User}
	 * @param productId {@link Product}
	 * @return {@link ProductRating}
	 */
	public ProductRating checkProductRatingByUser(Integer userId, Integer productId) {
		return this.productRatingRepository.findByProduct_IdAndUser_IdAndStatus(productId, userId,
				ProductEnum.ProductStatus.ACTIVE.getCode()).orElse(null);
	}

	/**
	 * Check Product with Id, exits or not
	 *
	 * @param productId {@link Product}
	 * @return {@link Product}
	 * @throws NotFoundException
	 */
	public Product checkExitedProduct(Integer productId) throws NotFoundException {
		return this.productRepository.findByIdAndStatus(productId, ProductEnum.ProductStatus.ACTIVE.getCode()).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("info.product"))));
	}

	/**
	 * Check ProductRatingCount With productId and Rating, exits or not
	 *
	 * @param productId {@link Product}
	 * @param ratingStar {@link ProductRating}
	 * @return {@link ProductRatingCount}
	 */
	public ProductRatingCount checkProductRatingCount(Integer productId, Integer ratingStar) {
		return this.productRatingCountRepository.findByProduct_IdAndRatingStar(productId, ratingStar).orElse(null);
	}

	/**
	 * Increase ProductRatingCount
	 * Increase count of every different rating
	 *
	 * @param productRatingCount {@link ProductRatingCount}
	 * @param productRating {@link ProductRating}
	 * @return
	 */
	public ProductRatingCount increaseProductRatingCount(ProductRatingCount productRatingCount, ProductRating productRating) {
		if(productRatingCount == null) {
			productRatingCount = new ProductRatingCount();
			productRatingCount.setProduct(productRating.getProduct());
			productRatingCount.setRatingStar(productRating.getRating());
			productRatingCount.setRatingCount(1);
		} else {
			productRatingCount.setRatingCount(productRatingCount.getRatingCount() + 1);
		}
		return productRatingCount;
	}

	/**
	 * Check ProductRating, exits ot not
	 *
	 * @param productRatingId {@link ProductRating}
	 * @return {@link ProductRating}
	 * @throws NotFoundException
	 */
	public ProductRating checkExitedProductRating(Integer productRatingId) throws NotFoundException {
		return this.productRatingRepository.findById(productRatingId).orElseThrow(
				() ->	new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("product-rating"))));
	}

	/**
	 * Check current user's ProductRatingReply
	 *
	 * @param userId {@link User}
	 * @param productRatingReplyId {@link ProductRatingReply}
	 * @return {@link ProductRatingReply}
	 * @throws NotFoundException
	 */
	public ProductRatingReply checkProductRatingReplyWithUser(Integer userId, Integer productRatingReplyId) throws NotFoundException {
		return this.productRatingReplyRepository.findByIdAndUser_Id(productRatingReplyId, userId).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_RATING_REPLY_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("product-rating-reply"))));
	}

	/**
	 * Check ProductRatingCount for Current ProductRating
	 * ProductRating is going to add or update
	 * increase productRatingCount
	 *
	 * @param productId {@link Product}
	 * @param productRating {@link ProductRating}
	 * @param productRatingDTO {@link ProductRatingDTO}
	 * @param isUpdated boolean
	 * @return List {@link ProductRatingCount}
	 * @throws ConflictException
	 */
	public List<ProductRatingCount> checkProductRatingCountForCurrentProductRating(Integer productId, ProductRating productRating, ProductRatingDTO productRatingDTO, boolean isUpdated) throws BadRequestException {
		List<ProductRatingCount> productRatingCounts = new ArrayList<>();

		// Set flag in case: update productRating and rating has change
		boolean updatedRating = true;

		ProductRatingCount totalRating = this.getTotalRating(productId);

		this.productMapper.checkValidRating(productRatingDTO.getRating());
		if(isUpdated) {
			ProductRatingCount productRatingCount = this.checkProductRatingCount(productId, productRating.getRating());
			if(productRating.getRating() != productRatingDTO.getRating()) {
				productRatingCount.setRatingCount(productRatingCount.getRatingCount() - 1);
				totalRating.setRatingCount(totalRating.getRatingCount() - 1);
				productRatingCounts.add(productRatingCount);
			} else {
				// set updateRating to false in case: rating don't change
				updatedRating = false;
			}
		}

		// Default updateRating is true for both update ProductRating and create new ProductRating
		// case: update ProductRating and rating don't change, don't action
		if(updatedRating == true) {
			ProductRatingCount productRatingCount = this.checkProductRatingCount(productId, productRatingDTO.getRating());
			if(productRatingCount != null) {
				productRatingCount.setRatingCount(productRatingCount.getRatingCount() + 1);
				productRatingCounts.add(productRatingCount);
			} else {
				ProductRatingCount newProductRatingCount = new ProductRatingCount();
				newProductRatingCount.setRatingStar(productRatingDTO.getRating());
				newProductRatingCount.setRatingCount(1);
				newProductRatingCount.setProduct(new Product(productId));
				productRatingCounts.add(newProductRatingCount);
			}

			// Check count of rating for product
			// If information of totalRating didn't exit, create it
			if(totalRating == null) {
				totalRating  = new ProductRatingCount();
				totalRating.setRatingStar(0);
				totalRating.setProduct(new Product(productId));
				totalRating.setRatingCount(1);
				productRatingCounts.add(totalRating);
			} else {
				totalRating.setRatingCount(totalRating.getRatingCount() + 1);
				productRatingCounts.add(totalRating);
			}
		}
		return productRatingCounts;
	}

	/**
	 * Get List of Product's Colors
	 *
	 * @return {@link ProductColor}
	 */
	public List<ProductColor> getProductColors() {
		return this.productColorRepository.findAll();
	}
	
	/**
	 * Get product size chart list
	 *
	 * @return {@link ProductSizeChart}
	 */
	public List<ProductSizeChart> getProductSizeCharts() {
		return this.productSizeChartRepository.findAll();
	}

    /**
     * Check userModel by id, exits or not
     *
     * @param userModelId {@link UserModel}
     * @return {@link UserModel}
     * @throws NotFoundException
     */
	public UserModel checkExitedUserModel(Integer userModelId) throws NotFoundException {
	    return this.userModelRepository.findById(userModelId).orElseThrow(
                ()-> new NotFoundException(NotFoundException.ERROR_USER_MODEL_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("user-model"))));
    }

	/**
	 * Get count of Rating for product
	 *
	 * @param productId {@link Product}
	 * @return {@link ProductRatingCount}
	 */
	public ProductRatingCount getTotalRating(Integer productId) {

		// Get total of ratings in every product
		// default rating for totalRating is 0
		return this.productRatingCountRepository.findByProduct_IdAndRatingStar(productId, 0).orElse(null);
	}

	/**
	 * check MixMatch be long ShopId
	 *
	 * @param MixmatchId {@link MixMatch}
	 * @throws NotFoundException
	 */
	public MixMatch checkMixMatchByShopId(Integer MixmatchId , Integer ShopId) throws NotFoundException {
		return this.mixMatchRepository.findByIdAndAndShopId(MixmatchId, ShopId).orElseThrow(() ->
				new NotFoundException(NotFoundException.ERROR_MIX_MATCH_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("mix-match")))
		);

	}

	/**
	 * Get visitorInfo By Conditions
	 *
	 * @param shopId {@link Shop}
	 * @param type role of visitor {@link ShopEnum.VisitorType}
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @param startDay day starts
	 * @param endDay day ends
	 * @param pageRequest {@link PageRequest}
	 * @return List {@link ShopVisitor} by page
	 * @throws NotFoundException
	 * @throws ConflictException
	 */
	public List<ShopVisitorInfo> getVisitorInfoByConditions(Integer shopId, String type, String timeline,
			LocalDate startDay, LocalDate endDay, PageRequest pageRequest)
			throws NotFoundException, BadRequestException {
		Page<ShopVisitor> shopVisitors = null;
		List<ShopVisitorInfo> shopVisitorInfos = new ArrayList<>();
		// Check visitor's type, exits or not
		this.checkVisitorType(type);

		// Check timeline, exits or not
		boolean result =this.checkTimeLine(timeline);
		if(!result) {
			throw new NotFoundException(NotFoundException.ERROR_STATISTIC_TIMELINE_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("timeline")));
		}

		// Check valid range between startDay and endDay
		this.checkDayRange(startDay, endDay);

		if(timeline.equals(ShopEnum.Timeline.HOUR.toString())) {
			shopVisitors = this.shopVisitorRepository.findAllByConditionsAndHour_Day(shopId, type,timeline,
					startDay.toString(), endDay.toString(), pageRequest);

			int index = 0;
			LocalDateTime currentDay = startDay.atStartOfDay();
			for(int i = 0;i < pageRequest.getPageSize(); i++) {
				if(index < shopVisitors.getContent().size() && currentDay.getHour() == shopVisitors.getContent().get(index).created.getHour()
					&& currentDay.toLocalDate().isEqual(shopVisitors.getContent().get(index).created.toLocalDate())) {
					shopVisitorInfos.add(new ShopVisitorInfo(shopVisitors.getContent().get(index)));
					index = index + 1;
				} else {
					ShopVisitorInfo shopVisitorInfo = new ShopVisitorInfo();
					shopVisitorInfo.setTimeline(ShopEnum.Timeline.HOUR.toString());
					shopVisitorInfo.setType(type);
					shopVisitorInfo.setTotal(0);
					shopVisitorInfo.setDate(Date.from( currentDay.atZone( ZoneId.systemDefault()).toInstant()));
					shopVisitorInfos.add(shopVisitorInfo);
				}
				currentDay = currentDay.plusHours(1);
			}
		} else if (timeline.equals(ShopEnum.Timeline.DAY.toString())) {
			shopVisitors = this.shopVisitorRepository.findAllByConditionsAndHour_Day(shopId, type,timeline,
					startDay.toString(), endDay.toString(), pageRequest);

			long daysBetween = ChronoUnit.DAYS.between(startDay, endDay)+1;
			List<LocalDate> totalDates = LongStream.iterate(0, i -> i+1)
					.limit(daysBetween).mapToObj(i->startDay.plusDays(i))
					.collect(Collectors.toList());

			int index = 0, limit = 1;
			for(LocalDate localDate : totalDates) {
				if(limit > pageRequest.getPageSize()) {
					break;
				}
				if(index < shopVisitors.getContent().size() && localDate.isEqual(shopVisitors.getContent().get(index).created.toLocalDate())) {
					shopVisitorInfos.add(new ShopVisitorInfo(shopVisitors.getContent().get(index)));
					index = index + 1;
				} else {
					ShopVisitorInfo shopVisitorInfo = new ShopVisitorInfo();
					shopVisitorInfo.setTimeline(ShopEnum.Timeline.DAY.toString());
					shopVisitorInfo.setType(type);
					shopVisitorInfo.setTotal(0);
					shopVisitorInfo.setDate(Date.from( localDate.atStartOfDay().atZone( ZoneId.systemDefault()).toInstant()));
					shopVisitorInfos.add(shopVisitorInfo);
				}
				limit++;
			}
		} else if (timeline.equals(ShopEnum.Timeline.WEEK.toString())) {
			shopVisitors = this.shopVisitorRepository.findAllByConditionsAndWeek(shopId, type,timeline,
					startDay, endDay, pageRequest);
			long weeksBetween = ChronoUnit.WEEKS.between(startDay, endDay)+1;

			int index = 0, dbWeek, limit = 1;
			LocalDate indexDay = startDay;
			WeekFields weekFields = WeekFields.ISO;
			Integer currentWeek = indexDay.get(weekFields.weekOfWeekBasedYear()) - 1;
			boolean flag = false;
			for(int i = 0;i < weeksBetween; i++) {
				if(limit > pageRequest.getPageSize()) {
					break;
				}
				if(index < shopVisitors.getContent().size()) {
					dbWeek = shopVisitors.getContent().get(index).getCreated().get(weekFields.weekOfWeekBasedYear()) - 1;
					if(index < shopVisitors.getContent().size() && currentWeek.equals(dbWeek)
							&& indexDay.getYear() == shopVisitors.getContent().get(index).getCreated().getYear()) {
						shopVisitorInfos.add(new ShopVisitorInfo(shopVisitors.getContent().get(index)));
						index = index + 1;
						flag = true;
					} else {
						flag = false;
					}
				} else {
					flag = false;
				}
				if (!flag) {
					ShopVisitorInfo shopVisitorInfo = new ShopVisitorInfo();
					shopVisitorInfo.setTimeline(ShopEnum.Timeline.WEEK.toString());
					shopVisitorInfo.setType(type);
					shopVisitorInfo.setTotal(0);
					shopVisitorInfo.setDate(Date.from( indexDay.atStartOfDay().atZone( ZoneId.systemDefault()).toInstant()));
					shopVisitorInfos.add(shopVisitorInfo);
				}
				indexDay = indexDay.plusDays(7);
				currentWeek = indexDay.get(weekFields.weekOfWeekBasedYear()) - 1;
				limit++;
			}
		} else if (timeline.equals(ShopEnum.Timeline.MONTH.toString())) {
			shopVisitors = this.shopVisitorRepository.findAllByConditionsAndMonth(shopId, type,timeline,
					startDay, endDay, pageRequest);
			long monthsBetween = ChronoUnit.MONTHS.between(startDay, endDay)+1;
			LocalDate indexDay = startDay;
			int index = 0, limit = 1;
			int currentMonth = indexDay.getMonthValue();
			for(int i = 0;i < monthsBetween; i++) {
				if (limit > pageRequest.getPageSize()) {
					break;
				}
				if(index < shopVisitors.getContent().size()
					&& currentMonth == shopVisitors.getContent().get(index).created.getMonthValue()
					&& indexDay.getYear() == shopVisitors.getContent().get(index).getCreated().getYear()) {
					shopVisitorInfos.add(new ShopVisitorInfo(shopVisitors.getContent().get(index)));
					index = index + 1;
				} else {
					ShopVisitorInfo shopVisitorInfo = new ShopVisitorInfo();
					shopVisitorInfo.setTimeline(ShopEnum.Timeline.MONTH.toString());
					shopVisitorInfo.setType(type);
					shopVisitorInfo.setTotal(0);
					shopVisitorInfo.setDate(Date.from( indexDay.atStartOfDay().atZone( ZoneId.systemDefault()).toInstant()));
					shopVisitorInfos.add(shopVisitorInfo);
				}
				indexDay = indexDay.plusMonths(1);
				currentMonth = indexDay.getMonthValue();
				limit++;
			}
		} else if (timeline.equals(ShopEnum.Timeline.YEAR.toString())) {
			shopVisitors = this.shopVisitorRepository.findAllByConditionsAndYear(shopId, type,timeline,
					startDay, endDay, pageRequest);

			long yearsBetween = endDay.getYear() - startDay.getYear() + 1;
			LocalDate indexDay = startDay;
			int index = 0, limit = 1;
			int currentYear = startDay.getYear();
			for (int i = 0;i < yearsBetween; i++) {
				if(limit > pageRequest.getPageSize()) {
					break;
				}
				if(index < shopVisitors.getContent().size() && currentYear == shopVisitors.getContent().get(index).created.getYear()) {
					shopVisitorInfos.add(new ShopVisitorInfo(shopVisitors.getContent().get(index)));
					index = index + 1;
				} else {
					ShopVisitorInfo shopVisitorInfo = new ShopVisitorInfo();
					shopVisitorInfo.setTimeline(ShopEnum.Timeline.YEAR.toString());
					shopVisitorInfo.setType(type);
					shopVisitorInfo.setTotal(0);
					shopVisitorInfo.setDate(Date.from( indexDay.atStartOfDay().atZone( ZoneId.systemDefault()).toInstant()));
					shopVisitorInfos.add(shopVisitorInfo);
				}
				indexDay = indexDay.plusYears(1);
				currentYear = indexDay.getYear();
			}
			limit++;
		}
		return shopVisitorInfos;
	}

	/**
	 * Check timeline, valid or not
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @return {true, false}
	 */
	public boolean checkTimeLine(String timeline) {
		for (ShopEnum.Timeline c : ShopEnum.Timeline.values()) {
			if (c.name().equals(timeline)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check visitor type
	 *
	 * @param type role of visitor {@link ShopEnum.VisitorType}
	 * @throws NotFoundException
	 */
	public void checkVisitorType(String type) throws NotFoundException {
		ShopEnum.VisitorType visitorType = CodeEnumUtils.fromCodeQuietly(ShopEnum.VisitorType.class, type);
		if(visitorType == null) {
			throw new NotFoundException(NotFoundException.ERROR_VISITOR_TYPE_NOT_FOUND, String.format(this.messageService.getMessage("error.not-found-msg"),
					this.messageService.getMessage("visitor-type")));
		}
	}

	/**
	 * Check Day's range, valid or not
	 *
	 * @param startDay day starts
	 * @param endDay day ends
	 * @throws BadRequestException
	 */
	public void checkDayRange(LocalDate startDay, LocalDate endDay) throws BadRequestException {
		if(startDay.isAfter(endDay)) {
			throw new BadRequestException(BadRequestException.ERROR_INVALID_DAY_RANGE, String.format(this.messageService.getMessage("error.bad-value-msg"),
					this.messageService.getMessage("start-day-end-day")), false);
		}
	}

	/**
	 * Get ShopCollection by ReturningVisitor
	 *
	 * @param shopId {@link Shop}
	 * @param timeline Milestones of time {@link ShopEnum.Timeline}
	 * @return List {@link ReturningVisitor}
	 * @throws NotFoundException
	 */
	public List<ReturningVisitor> getShopCollectionReturningVisitors(Integer shopId, String timeline) throws NotFoundException {
		List<ReturningVisitor> returningVisitors = new ArrayList<>();
	    List<ShopCollectionVisitor> currentReturningVisitors = new ArrayList<>();
		List<ShopCollectionVisitor> lastReturningVisitors = new ArrayList<>();

		String type = "RETURN_VISITOR";
	    Integer currentYear;
	    Integer lastYear;
        boolean result =this.checkTimeLine(timeline);
        if(!result) {
            throw new NotFoundException(NotFoundException.ERROR_STATISTIC_TIMELINE_NOT_FOUND,
                    String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("timeline")));
        }

        LocalDate today = LocalDateTime.now().toLocalDate();
        if(timeline.equals(ShopEnum.Timeline.DAY.toString())) {
			lastReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndDay(shopId, type,
					timeline, today.minusDays(1));
			currentReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndDay(shopId, type,
					timeline, today);
        } else if (timeline.equals(ShopEnum.Timeline.WEEK.toString())) {
			WeekFields weekFields = WeekFields.ISO;
            currentYear = today.getYear();
            Integer currentWeek = today.get(weekFields.weekOfWeekBasedYear()) - 1;
            Integer lastWeek;
            if(currentWeek == 1) {
                lastWeek = 52;
                lastYear = currentYear - 1;
            } else {
                lastWeek = currentWeek - 1;
				lastYear = currentYear;
            }
            String firstZero_lastWeek = "";
            String firstZero_currentWeek = "";
            if(currentWeek < 10) {
            	firstZero_currentWeek = "0";
			}
            if (lastWeek < 10) {
            	firstZero_lastWeek = "0";
			}
			String lastYearWeek = lastYear.toString() + firstZero_lastWeek + lastWeek.toString();
            String currentYearWeek = currentYear.toString() + firstZero_currentWeek + currentWeek.toString();
            lastReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndWeek(shopId, type, timeline,
                    Integer.parseInt(lastYearWeek));
            currentReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndWeek(shopId, type, timeline,
					Integer.parseInt(currentYearWeek));
        } else if (timeline.equals(ShopEnum.Timeline.MONTH.toString())) {
        	Integer currentMonth = today.getMonthValue();
        	currentYear = today.getYear();
        	Integer lastMonth;
        	if (currentMonth == 1) {
        		lastMonth = 12;
        		lastYear = currentYear - 1;
			} else {
        		lastMonth = currentMonth - 1;
        		lastYear = currentYear;
			}
			lastReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndMonth(shopId, type, timeline,
					lastMonth, lastYear);
			currentReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndMonth(shopId, type, timeline,
					currentMonth, currentYear);
        } else if (timeline.equals(ShopEnum.Timeline.YEAR.toString())) {
			currentYear = today.getYear();
			lastYear = currentYear - 1;
			lastReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndYear(shopId, type, timeline,
					lastYear);
			currentReturningVisitors = this.shopCollectionVisitorRepository.findAllByConditionsAndYear(shopId, type, timeline,
					currentYear);
        }

		ReturningVisitor lastReturningVisitor = new ReturningVisitor();
        lastReturningVisitor.setMilestone("Past");
		lastReturningVisitor.setShopCollectionVisitors(lastReturningVisitors.stream().
				map(ShopCollectionVisitorDTO::new).collect(Collectors.toCollection(ArrayList::new)));
		returningVisitors.add(lastReturningVisitor);

		ReturningVisitor currentReturningVisitor = new ReturningVisitor();
		currentReturningVisitor.setMilestone("Current");
		currentReturningVisitor.setShopCollectionVisitors(currentReturningVisitors.stream().
				map(ShopCollectionVisitorDTO::new).collect(Collectors.toCollection(ArrayList::new)));
		returningVisitors.add(currentReturningVisitor);

		return returningVisitors;
    }

	/**
	 * Check comment like for user
	 *
	 * @param userId {@link User}
	 * @param type {@link ProductRatingInteraction}
	 * @return {@link ProductRatingInteraction}
	 */
    public ProductRatingInteraction checkCommentLikeForUser(Integer productRatingId, Integer userId, String type) {
		return this.productRatingInteractionRepository.findByProductRating_IdAndUser_IdAndType(productRatingId, userId, type).orElse(null);
	}

	/**
	 * Check user liked comment reply, exits or not
	 *
	 * @param productRatingReplyId {@link ProductRatingReply}
	 * @param userId {@link User}
	 * @param type user's emotion for product, this is "LIKE"
	 * @return {@link ProductRatingReplyInteraction}
	 */
	public ProductRatingReplyInteraction checkCommentReply_LikeForUser(Integer productRatingReplyId, Integer userId, String type) {
		return this.productRatingReplyInteractionRepository.findByProductRatingReply_IdAndUser_IdAndType(productRatingReplyId, userId, type).orElse(null);
	}

	/**
	 * Check Product Rating Reply, exits or not
	 *
	 * @param productRatingReplyId {@link ProductRatingReply}
	 * @return {@link ProductRatingReply}
	 * @throws NotFoundException
	 */
	public ProductRatingReply checkExitedProductRatingReply(Integer productRatingReplyId) throws ConflictException {
    	return this.productRatingReplyRepository.findById(productRatingReplyId).orElseThrow(
				()-> new ConflictException(ConflictException.ERROR_PRODUCT_RATING_REPLY_EXISTED,
						String.format(this.messageService.getMessage("error.not-found-msg"),
								this.messageService.getMessage("product-rating-reply"))));
	}

	/**
	 * Check Exited ProductType, return data exits or not
	 *
	 * @param productTypeId {@link ProductType}
	 * @return @{link ProductType}
	 */
	public ProductType getExitedProductType(Integer productTypeId) {
		return this.productTypeRepository.findById(productTypeId).orElse(null);
	}

	/**
	 * Check Exited Shop, return data if it exits or throws error if did't exit
	 *
	 * @param shopId {@link Shop}
	 * @return {@link Shop}
	 * @throws NotFoundException
	 */
	public Shop checkExitedShop(Integer shopId) throws NotFoundException {
		return this.shopRepository.findById(shopId).orElseThrow(
				()-> new NotFoundException(NotFoundException.ERROR_SHOP_NOT_FOUND,
					String.format(this.messageService.getMessage("error.not-found-msg"),
						this.messageService.getMessage("shop-with-ID") + ": " + shopId)));
	}
	
	public UserModel getUserModelUserLogged() throws NotFoundException {
		return this.userModelRepository.findUserModelDefaultForUser(this.getCurrentUserId()).orElseThrow(
				()-> new NotFoundException(NotFoundException.ERROR_USER_MODEL_NOT_FOUND, this.messageService.getMessage("error.user-model-default")));
	}
	
	public Shop checkShopCanPostProduct(Integer shopId) throws SmartFashionException, NotFoundException {
		Shop shop = this.checkShopBelongUserLogged(shopId);
		if (shop.getStatus().equals(ShopStatus.SUSPENDED.name())) {
			throw new SmartFashionException(HttpStatus.INTERNAL_SERVER_ERROR.value(), SmartFashionException.ERROR_SHOP_STATUS_SUSPENDED, this.messageService.getMessage("error.shop-status-suspended"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	return shop;
	}
	
	public Shop checkShopBlockingLogin(int userId) throws AuthenticationException, NotFoundException {
		Shop shop = this.getShopByUserId(userId);
		if (shop != null) {
			if (ShopStatus.REJECTED.name().equals(shop.getStatus())) {
				throw new AuthenticationException(SmartFashionException.ERROR_SHOP_STATUS_REJECT, this.messageService.getMessage("error.shop-status-rejected"));
			} else if (ShopStatus.BLOCKED.name().equals(shop.getStatus())) {
				throw new AuthenticationException(SmartFashionException.ERROR_SHOP_STATUS_BLOCKED, this.messageService.getMessage("error.shop-status-blocked"));
			} else if (shop.getStatus().equals(ShopStatus.PENDING.name())) {
				throw new AuthenticationException(SmartFashionException.ERROR_SHOP_STATUS_PENDING, this.messageService.getMessage("error.shop-status-pending"));
			} 
		}
	return shop;
	}
}
