package bap.jp.smartfashion.common.dto;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a password
 *
 */
@Getter
@Setter
@Data
public class PasswordResetDTO {
	
	@NotEmpty(message = "Password {validation.not-empty}")
	@ApiModelProperty(notes = "Password", required = true)
	private String password;
	
	@NotEmpty(message = "ConfirmPassword {validation.not-empty}")
	@ApiModelProperty(notes = "Confirm password", required = true)
	private String confirmPassword;
}
