package bap.jp.smartfashion.common.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.enums.UserEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * A DTO representing a {@link User}
 *
 */
@Getter
@Setter
public class UserDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	private Integer id;

	
	@ApiModelProperty(notes = "User name", required = true)
	@Size(min = 1, max = 50, message = "Name {validation.size-min-max}")
	@NotEmpty(message = "Name {validation.not-empty}")
	private String name;

	@ApiModelProperty(notes = "Gender", required = true)
    private Integer gender;

	@NotEmpty(message = "Email {validation.not-empty}")
	@Email(message = "{info.email} {validation.invalid-format}")
	@ApiModelProperty(notes = "Email", required = true)
	private String email;
	
	@Size(min = 1, max = 11, message = "Phone {validation.size-min-max}")
	@ApiModelProperty(notes = "User phone")
	private String phone;

    @Size(max = 13, message = "Tax Number {validation.size-max}")
    @ApiModelProperty(notes = "Tax Number")
    private String taxNumber;

    @Size(max = 11, message = "Passport Number {validation.size-max}")
    @ApiModelProperty(notes = "Passport Number")
    private String passportNumber;

    @Size(max = 255, message = "PassportBackSide's ImageUrl {validation.size-max}")
    @ApiModelProperty(notes = "PassportBackSide")
    private String passportBackSide;

    @Size(max = 255, message = "PassportFrontSide's ImageUrl {validation.size-max}")
    @ApiModelProperty(notes = "PassportFrontSide")
    private String passportFrontSide;

	@ApiModelProperty(notes = "Only for update")
	private boolean registerStatus;

	@ApiModelProperty(notes = "Only for update")
	private boolean blocked;
	
	@ApiModelProperty(hidden = true)
	private String userType;
	
	@ApiModelProperty(hidden = true)
	private String uid;

	@ApiModelProperty(notes = "User avatar")
	private String avatar;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(notes = "Shop belong to user", hidden = true)
	private Shop shop;
	
	@ApiModelProperty(notes = "Platform used application", allowableValues = "WEB, MOBILE_ANDROID, MOBILE_IOS")
	private String platform;

	@ApiModelProperty(notes = "Role of user, check if role_admin or role_super_admin", hidden = true)
	@JsonInclude(value = Include.NON_NULL)
	private String[] userRoles;

	@ApiModelProperty(notes = "language that user is using app")
	private String langKey;
	
	public UserDTO() {
		// Empty constructor needed for Jackson.
	}
	
	public UserDTO(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.email = user.getEmail();
		this.registerStatus = user.isRegisterStatus();
		this.blocked = user.isBlocked();
		this.userType = user.getUserType();
		this.uid = user.getUid();
		this.avatar = user.getAvatar();
		this.gender = user.getGender();
		this.passportNumber = user.getPassportNumber();
		this.passportFrontSide = user.getPassportFrontSide();
		this.passportBackSide = user.getPassportBackSide();
		this.taxNumber = user.getTaxNumber();
		this.langKey = this.setLangKey(user.getLangKey());
		this.userRoles = user.getRoles().stream().map(role -> role.getName()).toArray(String[]::new);
	}

	public String setLangKey(String langKey) {
		return StringUtils.isEmpty(langKey) ? UserEnum.LanguageEnum.ENGLISH.getCode() : langKey;
	}
}