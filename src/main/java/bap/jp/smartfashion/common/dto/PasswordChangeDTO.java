package bap.jp.smartfashion.common.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a password
 *
 */
@Getter
@Setter
@Data
public class PasswordChangeDTO {

    @NotEmpty(message = "Old Password {validation.not-empty}")
    @ApiModelProperty(notes = "Old Password", required = true)
    private String oldPassword;

    @NotEmpty(message = "Confirmed Password {validation.not-empty}")
    @ApiModelProperty(notes = "Confirmed password", required = true)
    @Size(min = 8, max = 25, message = "New password {validation.size-min-max}")
    private String confirmedPassword;
}
