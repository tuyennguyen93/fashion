package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.UserModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.ObjectUtils;

@Getter
@Setter
@NoArgsConstructor
public class TryOnDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9180900974981711877L;

	@ApiModelProperty(notes = "Model Id")
	@NotNull(message = "Model ID {validation.not-null}")
	private Integer modelId;
	
	@ApiModelProperty(notes = "Product ID with User Product\n"
			+ "Product Detail ID with Shop Product")
	@NotNull(message = "Product ID {validation.not-null}")
	private Integer productId;
	

	@ApiModelProperty(notes = "Is user product uploaded by user")
	private Boolean isUserProduct = false;
	
	@ApiModelProperty(notes = "Tried on result", readOnly = true)
	private String triedOnURL;
	

	public TryOnDTO(UserModel userModel) {
	}
}
