package bap.jp.smartfashion.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.vo.ProductRatingVO;
import bap.jp.smartfashion.enums.ProductEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a {@link Product}
 *
 */
@Getter
@Setter
@Data
public class ProductDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	private Integer id;

	@NotEmpty(message = "Product's name {validation.not-empty}")
	@ApiModelProperty(notes = "Product name", required = true)
	@Size(max = 50, message = "{info.product.name} {validation.size-max}")
	private String name;

	@ApiModelProperty(notes = "Product code", hidden = true)
	@Size(max = 15, message = "Product's code {validation.size-max}")
	private String productCode;

	@ApiModelProperty(notes = "Product Collection", hidden = true)
	private ProductCollectionDTO productCollection;
	
	@ApiModelProperty(notes = "Product currency", required = true, allowableValues = "$,¥,VND")
	@Size(max = 5, message = "Product's currency {validation.size-max}")
	private String currency;

	@NotNull(message = "Product's price {validation.not-null}")
	@ApiModelProperty(notes = "Price (set for price if product is not discounting)", required = true)
	@Digits(integer = 8, fraction = 2, message = "Price, {javax.validation.constraints.Digits.message}")
	@PositiveOrZero(message = "Price {validation.unsigned-number-or-zero}")
	private BigDecimal price;
	
	@ApiModelProperty(notes = "Old Price - Price before discount(set for price if product is discounting)", readOnly = true)
	@Digits(integer = 8, fraction = 2, message = "oldPrice, {javax.validation.constraints.Digits.message}")
	private BigDecimal oldPrice;


	@ApiModelProperty(notes = "Discount")
	@Digits(integer = 2, fraction = 2, message = "Discount, {javax.validation.constraints.Digits.message}")
	@PositiveOrZero(message = "Discount {validation.unsigned-number-or-zero}")
	private BigDecimal discount;

	@ApiModelProperty(notes = "Guarantee")
	@Size(max = 100, message = "Guarantee {validation.size-max}")
	private String guarantee;

	@ApiModelProperty(hidden = true)
	private String status;

	@JsonInclude(value = Include.NON_NULL)
	@NotNull(message = "Product's WearingPurposeID {validation.not-null}")
	@ApiModelProperty(notes = "Wearing purpose id", required = true)
	@Positive(message = "Product's WearingPurposeID {validation.unsigned-number}")
	private Integer productWearingPurposeId;

	@ApiModelProperty(hidden = true)
	private ProductWearingPurposeDTO productWearingPurpose;

	@JsonInclude(value = Include.NON_NULL)
	@NotNull(message = "Product's collectionID {validation.not-null}")
	@ApiModelProperty(notes = "Product collection id", required = true)
	@Positive(message = "Product's collectionID {validation.unsigned-number}")
	private Integer productCollectionId;

	@JsonInclude(value = Include.NON_NULL)
	@NotNull(message = "Product's typeID {validation.not-null}")
	@ApiModelProperty(notes = "Type of clothes", required = true)
	@Positive(message = "Product's typeID {validation.unsigned-number}")
	private Integer productTypeId;

	@ApiModelProperty(hidden = true)
	private ProductTypeDTO productType;

	@ApiModelProperty(notes = "Description")
	@Size(max = 1000, message = "Description {validation.size-max}")
	private String description;

	@ApiModelProperty(notes = "Highlight")
	private boolean highlight;

	@ApiModelProperty(readOnly = true)
	private Boolean isNewProduct;

	@ApiModelProperty(readOnly = true)
	private Date postedDate;

	@ApiModelProperty(readOnly = true)
	private List<ProductDetailDTO> productDetails;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(hidden = true)
	private ProductRatingVO productRating;
	
	@ApiModelProperty(readOnly = true)
	private int triedCount = 0;
	
	@ApiModelProperty(readOnly = true)
	private int favoriteCount = 0;
	
	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(readOnly = true)
	private List<ProductMixMatchDTO> productMixMatchs;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(readOnly = true)
	private Boolean isFavorite;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(readOnly = true)
	private Boolean isTry;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(readOnly = true)
	private List<ProductRatingDTO> productRatings;

	@ApiModelProperty(readOnly = false)
	private Integer productSizeChartId;
	
	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(hidden = true)
	private ProductSizeChartDTO productSizeChart;
	
	@ApiModelProperty(notes = "Product image", readOnly = true)
	private String url;

	
	public ProductDTO() {
	}

	public ProductDTO(Product product) {
		this.id = product.getId();
		this.name = product.getName();
		this.productCode = product.getProductCode();
		this.productCollection = new ProductCollectionDTO(product.getProductCollection());
		this.productCollectionId = product.getProductCollection().getId();
		this.currency = product.getCurrency();
		this.price = product.getPrice();
		this.oldPrice = product.getOldPrice();
		this.discount = product.getDiscount();
		this.guarantee = product.getGuarantee();
		this.status = product.getStatus();
		this.productWearingPurpose = new ProductWearingPurposeDTO(product.getProductWearingPurpose());
		this.productWearingPurposeId = product.getProductWearingPurpose().getId();
		this.productType = new ProductTypeDTO(product.getProductType());
		this.productTypeId = product.getProductType().getId();
		this.url = product.getUrl();
		this.description = product.getDescription();
		this.highlight = product.isHighlight();
		this.isNewProduct = isNewProduct(product.getCreated());
		if (product.getProductDetails() != null) {
			this.productDetails = product.getProductDetails().stream().map(ProductDetailDTO::new).collect(Collectors.toList()); 
		}
		this.postedDate = Date.from( product.getCreated().atZone( ZoneId.systemDefault()).toInstant());
		if (product.getProductInteractions() != null) {
			for (ProductInteraction productInteraction : product.getProductInteractions()) {
				if (productInteraction.getActionCode().equals(ProductEnum.ProductInteraction.TRY_PRODUCT.getCode())) {
					this.triedCount = productInteraction.getActionCount();
				}
				if (productInteraction.getActionCode().equals(ProductEnum.ProductInteraction.FAVORITE_PRODUCT.getCode())) {
					this.favoriteCount = productInteraction.getActionCount();
				}
			}
		}
		if (product.getProductSizeChart() != null) {
			this.productSizeChart = new ProductSizeChartDTO(product.getProductSizeChart());
			this.productSizeChartId = product.getProductSizeChart().getId();
		}
	}

	private boolean isNewProduct(LocalDateTime createdDate) {
		LocalDateTime expiredDate = createdDate.plusDays(15); // set expiredDate
		LocalDateTime localDateTime = LocalDateTime.now();
		return localDateTime.isBefore(expiredDate);
	}


}
