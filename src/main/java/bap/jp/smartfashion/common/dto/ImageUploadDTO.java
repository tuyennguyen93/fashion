package bap.jp.smartfashion.common.dto;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a image information
 *
 */
@Getter
@Setter
@Data
public class ImageUploadDTO {
	@NotEmpty(message = "File name is required")
	@ApiModelProperty(notes = "File name", required = true)
	private String fileName;
	
	@NotEmpty(message = "File type is required")
	@ApiModelProperty(notes = "File type", required = true)
	private String fileType;
}