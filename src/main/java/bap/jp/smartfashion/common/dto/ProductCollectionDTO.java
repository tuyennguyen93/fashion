package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ShopProductCollection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * A DTO representing a {@link ProductCollection}
 *
 */
@Getter
@Setter
public class ProductCollectionDTO {
	@ApiModelProperty(hidden = true)
	private Integer id;

	@ApiModelProperty(notes = "Collection name", required = true)
	@Size(max = 50, message = "Collection's name {validation.size-max}")
	private String name;

	@ApiModelProperty(notes = "Collection image", required = true)
	@Size(max = 255, message = "Collection's image {validation.size-max}")
	private String image;

	@ApiModelProperty(notes = "Collection description", required = true)
	@Size(max = 1000, message = "Collection's description {validation.size-max}")
	private String description;

	@ApiModelProperty(notes = "Collection type", required = true)
	@Size(max = 10, message = "Collection's type {validation.size-max}")
	private String type;

	public ProductCollectionDTO(ProductCollection productCollection) {
		this.id = productCollection.getId();
		this.name = productCollection.getName();
		this.image = productCollection.getImage();
		this.description = productCollection.getDescription();
		this.type = productCollection.getType();
	}

	public ProductCollectionDTO(ShopProductCollection shopProductCollection) {
		this.id = shopProductCollection.getProductCollection().getId();
		this.name = shopProductCollection.getProductCollection().getName();
		this.image = shopProductCollection.getUrl();
		this.description = shopProductCollection.getProductCollection().getDescription();
		this.type = shopProductCollection.getProductCollection().getType();
	}

	public ProductCollectionDTO() {
	}
}
