package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.UserProduct;
import bap.jp.smartfashion.common.model.UserProductCollection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 * A DTO representing a {@link UserProductCollection}
 *
 */
@Getter
@Setter
@Data
public class UserProductCollectionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2772788557385418554L;

	@ApiModelProperty(notes = "User product collection id")
	private Integer id;

	@ApiModelProperty(notes = "User product collection name")
	@Size(max = 50)
	private String name;
	
	@ApiModelProperty(notes = "User product belong to product collection")
	private Integer count;

	public UserProductCollectionDTO() {
	}

	public UserProductCollectionDTO(UserProductCollection userProductCollection) {
		this.id = userProductCollection.getId();
		this.name = userProductCollection.getName();
	}
}