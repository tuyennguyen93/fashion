package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 *  A DTO representing a {@link Product} with update action
 *
 */
@Getter
@Setter
public class ProductUpdateDTO extends ProductDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "Set true if is quick update, otherwise")
	private boolean quickUpdate;

	@ApiModelProperty(notes = "Hidden in case quick Update")
	private List<ProductDetailDTO> productDetailDTOs;
}
