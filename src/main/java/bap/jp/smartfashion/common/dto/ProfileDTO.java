package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ShopBranch;
import bap.jp.smartfashion.common.model.ShopWarehouse;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Time;
import java.util.List;

/**
 * A DTO representing
 * {@link bap.jp.smartfashion.common.model.User},
 * {@link bap.jp.smartfashion.common.model.Shop}
 * {@link bap.jp.smartfashion.common.model.UserBankAccount}
 */
@Getter
@Setter
@Data
public class ProfileDTO implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    private Integer id;

    @NotEmpty(message = "OwnerName {validation.not-empty}")
    @Size(max = 50, message = "Owner's name {validation.size-max}")
    @ApiModelProperty(notes = "User name", required = true)
    private String ownerName;

    @Email(message = "Email: {validation.email}")
    @ApiModelProperty(notes = "Email", required = true)
    private String email;

    @Size(max = 15, message = "Phone {validation.size-max}")
    @ApiModelProperty(notes = "User phone")
    private String phone;

    @Size(max = 13, message = "Tax Number {validation.size-max}")
    @ApiModelProperty(notes = "Tax number")
    private String taxNumber;


    @ApiModelProperty(notes = "gender")
    @NotNull(message = "Gender {validation.not-null}")
    @Min(value = 1, message = "Gender {javax.validation.constraints.Min.message}")
    @Max(value = 3, message = "Gender {javax.validation.constraints.Max.message}")
    private Integer gender;

    @Size(max = 11, message = "Passport Number {validation.size-max}")
    @NotEmpty(message = "Passport Number {validation.not-empty}")
    @ApiModelProperty(notes = "passport Number")
    private String passportNumber;

    @Size(max = 255, message = "PassportFrontSide's ImageUrl {validation.size-max}")
    @ApiModelProperty(notes = "passport Front Side")
    private String passportFrontSide;

    @Size(max = 255, message = "PassportBackSide's ImageURl {validation.size-max}")
    @ApiModelProperty(notes = "passport Back Side")
    private String passportBackSide;

    @ApiModelProperty(notes = "list of warehouse's addresses ", required = true)
    @Valid
    private List<ShopWarehouse> warehouseAddressList;

    @ApiModelProperty(notes = "Shop name", required = true)
    @Size(max = 50, message = "Shop's name {validation.size-max}")
    @NotEmpty(message = "Shop's name {validation.not-empty}")
    private String shopName;

    @ApiModelProperty(notes = "Shop open time", required = true)
    @NotEmpty(message = "OpenTime {validation.not-empty}")
    private String openTime;

    @ApiModelProperty(notes = "Shop close time", required = true)
    @NotEmpty(message = "CloseTime {validation.not-empty}")
    private String closeTime;

    @ApiModelProperty(notes = "Shop phone", required = true)
    @NotEmpty(message = "Shop's phone {validation.not-empty}")
    @Size(max = 15, message = "Shop's phone {validation.size-max}")
    private String shopPhone;

    @ApiModelProperty(notes = "Shop Branches's Address list", required = true)
    @Valid
    private List<ShopBranch> shopBranchList;

    @ApiModelProperty(notes = "Shop description", required = true)
    @Size(max = 1000, message = "Description {validation.size-max}")
    private String description;

    @ApiModelProperty(notes = "account Number", required = true)
    @NotEmpty(message = "Account Number {validation.not-empty}")
    @Size(max = 14, message = "Account Number {validation.size-max}")
    private String accountNumber;

    @ApiModelProperty(notes = "owner Bank Name", required = true)
    @NotEmpty(message = "Owner Bank Name {validation.not-empty}")
    @Size(max = 30, message = "Owner Bank Name {validation.size-max}")
    private String ownerBankName;

    @ApiModelProperty(notes = "Bank Name", required = true)
    @NotEmpty(message = "Bank's name {validation.not-empty}")
    @Size(max = 30, message = "Bank's name {validation.size-max}")
    private String bankName;

    @ApiModelProperty(notes = "bank Branch", required = true)
    @NotEmpty(message = "Bank's branch {validation.not-empty}")
    @Size(max = 30, message = "Bank's branch {validation.size-max}")
    private String bankBranch;

    @ApiModelProperty(notes = "User avatar")
    private String avatar;

    public ProfileDTO() {
        // Empty constructor needed for Jackson.
    }

}
