package bap.jp.smartfashion.common.dto;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductColor;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductSubDetail;
import bap.jp.smartfashion.common.vo.ProductColorVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * A DTO representing a {@link ProductColor}
 *
 */
@Getter
@Setter
public class ProductColorDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "Product color Id")
	private Integer id;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(hidden = true)
	private Integer totalItems;
	
	@ApiModelProperty(notes = "Color name", required = true)
	@Size(max = 10, message = "{validation.size-max}")
	private String name;
	
	@ApiModelProperty(notes = "Color code", required = true, example = "#FFFFFF")
	@Size(max = 50, message = "{validation.size-max}")
	private String colorCode;
	
	public ProductColorDTO(ProductColor productColor) {
		this.id = productColor.getId();
		this.name = productColor.getName();
		this.colorCode = productColor.getColorCode();
	}
	
	public ProductColorDTO(ProductDetail productDetail) {
		ProductColor productColor = productDetail.getProductColor();
		this.id = productColor.getId();
		this.name = productColor.getName();
		this.colorCode = productColor.getColorCode();
		int totalItems = productDetail.getProductSubDetails().stream().mapToInt(ProductSubDetail::getTotal).sum();
		this.totalItems = totalItems;
		
	}

	public ProductColorDTO() {}
	
}
