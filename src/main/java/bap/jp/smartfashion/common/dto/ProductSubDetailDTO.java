package bap.jp.smartfashion.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductSubDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a {@link ProductSubDetail}
 *
 */
@Getter
@Setter
public class ProductSubDetailDTO {
	private Integer id;
	private ProductSizeDTO productSize;
	private Integer total;
	
	public ProductSubDetailDTO() {
		
	}
	
	public ProductSubDetailDTO(ProductSubDetail productSubDetail) {
		this.id = productSubDetail.getId();
		this.total = productSubDetail.getTotal();
		this.productSize = new ProductSizeDTO(productSubDetail.getProductSize());
	}
}
