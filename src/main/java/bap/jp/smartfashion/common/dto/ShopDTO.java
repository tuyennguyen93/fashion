package bap.jp.smartfashion.common.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a {@link Shop}
 *
 */
@Getter
@Setter
@Data
public class ShopDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	private int id;
	
	@ApiModelProperty(hidden = true)
	private User user;
	
	@ApiModelProperty(notes = "Shop name", required = true)
	@NotEmpty(message = "Shop's name {validation.not-empty}")
	@Size(min = 2, message = "{info.shop.name} {validation.size-max}")
	private String name;
	
	@Email(message = "Email {validation.email}")
	@ApiModelProperty(notes = "Email address")
	private String email;
	
	@ApiModelProperty(notes = "Shop phone")
	private String phone;
	
	@ApiModelProperty(notes = "Avatar URL")
	private String avatar;
	
	@ApiModelProperty(notes = "Shop Description")
	private String description;

	
	@ApiModelProperty(notes = "Shop Description", hidden = true)
	@JsonIgnore
	private String shopCode;

	public ShopDTO() {
		// Empty constructor needed for Jackson.
	}
	
	public ShopDTO(Shop shop) {
		this.id = shop.getId();
	//	this.user = shop.getUser();
		this.name = shop.getName();
		this.email = shop.getEmail();
		this.phone = shop.getPhone();
		this.avatar = shop.getAvatar();
		this.description = shop.getDescription();
		this.shopCode = shop.getShopCode();
	}
}
