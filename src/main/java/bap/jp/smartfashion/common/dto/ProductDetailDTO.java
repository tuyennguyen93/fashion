package bap.jp.smartfashion.common.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * A DTO representing a {@link ProductDetail}
 *
 */
@Getter
@Setter
public class ProductDetailDTO {
	private Integer id;
	private ProductColorDTO productColor;
	private List<ProductSubDetailDTO> productSubDetails;
	private List<ProductPhotoDTO> productPhotos;
	
	@ApiModelProperty(hidden = true)
	private String sizeChart;
	
	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(allowableValues = "NEW, EDIT", notes = "Action on product detail. If EDIT then id is required")
	private String action;
	
	public ProductDetailDTO() {}
	
	public ProductDetailDTO(ProductDetail productDetail) {
		this.id = productDetail.getId();
		this.productColor = new ProductColorDTO(productDetail);
		this.productSubDetails = productDetail.getProductSubDetails().stream().map(ProductSubDetailDTO::new).collect(Collectors.toList());
		this.productPhotos = productDetail.getProductPhotos().stream().map(ProductPhotoDTO::new).collect(Collectors.toList());
	}
}
