package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ShopVisitor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ShopVisitorInfo extends ShopNewVistorDTO {
    private String timeline;
    public ShopVisitorInfo(ShopVisitor shopVisitor) {
        this.timeline = shopVisitor.getTimeline();
        this.setTotal(shopVisitor.getTypeCount());
        this.setDate(Date.from( shopVisitor.getCreated().atZone( ZoneId.systemDefault()).toInstant()));
        this.setType(shopVisitor.getType());
    }
}
