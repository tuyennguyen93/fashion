package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ShopProductCollection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * A DTO to get data from {@link ShopProductCollection}
 *
 */

@Setter
@Getter
@NoArgsConstructor
public class ShopProductCollectionDTO {
    private Integer shopId;
    private Integer productCollectionId;
    private String url;

    public ShopProductCollectionDTO(ShopProductCollection shopProductCollection) {
        this.shopId = shopProductCollection.getShop().getId();
        this.productCollectionId = shopProductCollection.getProductCollection().getId();
        this.url = shopProductCollection.getUrl();
    }
}
