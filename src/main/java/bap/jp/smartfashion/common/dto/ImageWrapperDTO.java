package bap.jp.smartfashion.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * A DTO representing a imageUrl of {@link Shop}
 *
 */
@Getter
@Setter
@Data
public class ImageWrapperDTO {
    @NotEmpty(message = "photo name is required")
    @ApiModelProperty(notes = "Photo name", required = true)
    private String photo;
}
