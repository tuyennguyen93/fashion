package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.MixMatch;
import bap.jp.smartfashion.common.model.ProductMixMatch;
import bap.jp.smartfashion.common.model.ProductMixMatchPhoto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A DTO representing a {@link MixMatch}
 *
 */
@Getter
@Setter
@Data
public class MixMatchDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	private Integer id;

	@Size(max = 50)
	private String name;

	@NotEmpty(message = "Product Id is required")
	@ApiModelProperty(notes = "List Product Id", required = true)
	private List<ProductMixMatchDTO> productMixMatches;

	public MixMatchDTO() {
	}

	public MixMatchDTO(MixMatch mixMatch) {
		this.id = mixMatch.getId();
		this.name = mixMatch.getName();
		this.productMixMatches = mixMatch.getProductMixMatches().stream().map(ProductMixMatchDTO::new).collect(Collectors.toList());
	}

}
