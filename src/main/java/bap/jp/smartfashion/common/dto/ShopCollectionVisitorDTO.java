package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ShopCollectionVisitor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ShopCollectionVisitorDTO {
    private Integer collectionId;
    private String collectionName;
    private String type;
    private Integer typeCount;
    private String timeline;

    public ShopCollectionVisitorDTO(ShopCollectionVisitor shopCollectionVisitor) {
        this.collectionId = shopCollectionVisitor.getProductCollection().getId();
        this.collectionName = shopCollectionVisitor.getProductCollection().getName();
        this.type = shopCollectionVisitor.getType();
        this.typeCount = shopCollectionVisitor.getTypeCount();
        this.timeline = shopCollectionVisitor.getTimeline();
    }
}
