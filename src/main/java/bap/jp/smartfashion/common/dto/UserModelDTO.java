package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.UserModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.ObjectUtils;

@Getter
@Setter
@NoArgsConstructor
public class UserModelDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9180900974981711877L;

	@ApiModelProperty(notes = "User model Id (set id = 0 if it's create new)")
	private Integer id;
	
	@ApiModelProperty(notes = "Image url")
	@Size(max = 255, message = "Image url {validation.size-max}")
	private String url;
	
	@ApiModelProperty(notes = "User model collection id")
	@NotNull(message = "User model collection id {validation.not-null}")
	private Integer userModelCollectionId;
	
	@ApiModelProperty(notes = "Is user model default",readOnly = true)
	private Boolean isDefault = false;
	
	@ApiModelProperty(notes = "Is user model system", readOnly = true)
	private Boolean isSystem;

	public UserModelDTO(UserModel userModel) {
		this.id = userModel.getId();
		this.url = userModel.getUrl();
		this.userModelCollectionId = userModel.getUserModelCollection().getId();
//		if (ObjectUtils.allNotNull(userModel.getUserModelSetting())) {
//			this.isDefault = userModel.getUserModelSetting().getIsDefault();
//		}
		this.isSystem = userModel.getUser() == null;
	}
}
