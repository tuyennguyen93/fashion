package bap.jp.smartfashion.common.dto;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductInteraction;
import bap.jp.smartfashion.common.model.ProductMixMatch;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a {@link ProductMixMatch}
 *
 */
@Getter
@Setter
public class ProductMixMatchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	@ApiModelProperty(hidden = true)
	private Integer mixMatchId;
	
	private Integer productId;

	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(allowableValues = "NEW, EDIT, DELETE", notes = "Action on product detail. If EDIT and DELETE then id is required")
	private String action;
	
	private List<ProductMixMatchPhotoDTO> productMixMatchPhotos;
	
	private Integer priority;
	
	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(hidden = true)
	private ProductDTO product;

	public ProductMixMatchDTO() {}

	public ProductMixMatchDTO(ProductMixMatch productMixMatch) {
		this.id = productMixMatch.getId();
		this.priority = productMixMatch.getPriority();
		this.productId = productMixMatch.getProduct().getId();
		this.mixMatchId = productMixMatch.getMixMatch().getId();
		this.productMixMatchPhotos = productMixMatch.getProductMixMatchPhotos().stream().map(ProductMixMatchPhotoDTO::new).collect(Collectors.toList());
		this.product = new ProductDTO(productMixMatch.getProduct());
	}

}
