package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ProductSizeChart;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * A DTO representing a {@link ProductSizeChart}
 *
 */
@Getter
@Setter
public class ProductSizeChartDTO {

	private Integer id;
	private String name;
	
	
	public ProductSizeChartDTO() {}
	
	public ProductSizeChartDTO(ProductSizeChart productSizeChart) {
		if (productSizeChart != null) {
			this.id = productSizeChart.getId();
			this.name = productSizeChart.getName();
		}
	}
}
