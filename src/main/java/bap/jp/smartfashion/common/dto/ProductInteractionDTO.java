package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductPhoto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductInteraction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A DTO representing a {@link ProductInteraction}
 *
 */
@Getter
@Setter
public class ProductInteractionDTO {
	
	private Integer id;
	@JsonInclude(value = Include.NON_NULL)
	private Product product;

	@Size(max = 15, message = "ActionCode {validation.size-max}")
	private String actionCode;

	private Integer actionCount;

	@JsonInclude(value = Include.NON_NULL)
	private String productDetails;
	
	public ProductInteractionDTO(ProductInteraction productInteraction) {
		this.id = productInteraction.getId();
		this.product = new Product(productInteraction.getProduct().getId(), productInteraction.getProduct().getName(),
				productInteraction.getProduct().getProductDetails().get(0));
		this.actionCode = productInteraction.getActionCode();
		this.actionCount = productInteraction.getActionCount();
	}

	@Getter
	@Setter
	@AllArgsConstructor
	class Product {
		private Integer id;
		private String name;
		private ProductDetailDTO productDetail;

		public Product (Integer id, String name, ProductDetail productDetail) {
			this.id = id;
			this.name = name;
			this.productDetail = new ProductDetailDTO(productDetail);
		}
	}

	@Getter
	@Setter
	class ProductDetailDTO {
		private Integer id;
		private List<ProductPhotoDTO> photos;

		public ProductDetailDTO(ProductDetail productDetail) {
			this.id = productDetail.getId();
			this.photos = productDetail.getProductPhotos().stream().map(ProductPhotoDTO::new).collect(Collectors.toList());
		}
	}

}

