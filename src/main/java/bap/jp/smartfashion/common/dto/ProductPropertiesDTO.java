package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.vo.Currency;
import bap.jp.smartfashion.common.vo.ProductSizeWithCollection;
import bap.jp.smartfashion.common.vo.ProductTypeWithCollection;
import bap.jp.smartfashion.common.vo.ProductWearingPurposeWithType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * A DTO to get Data about properties relate to product
 *
 */
@Getter
@Setter
public class ProductPropertiesDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "List of product collection", required = true)
    private List<ProductCollectionDTO> productCollections;

    @ApiModelProperty(notes = "List of ProductWearingPurposes", required = true)
    private List<ProductWearingPurposeWithType> productWearingPurposes;

    @ApiModelProperty(notes = "List of ProductTypes", required = true)
    private List<ProductTypeWithCollection> productTypes;

    @ApiModelProperty(notes = "List of ProductSizes", required = true)
    private List<ProductSizeWithCollection> productSizes;

    @ApiModelProperty(notes = "List of Product's Currencies", required = true)
    private List<Currency> currencies;

    public ProductPropertiesDTO(List<ProductCollectionDTO> productCollections, List<ProductTypeWithCollection> productTypeWithCollection,
                                List<ProductWearingPurposeWithType> productWearingPurposeWithType,
                                List<ProductSizeWithCollection> productSizeWithCollections, List<Currency> currencies) {
        this.productCollections = productCollections;
        this.productTypes = productTypeWithCollection;
        this.productWearingPurposes = productWearingPurposeWithType;
        this.productSizes = productSizeWithCollections;
        this.currencies = currencies;
    }
}
