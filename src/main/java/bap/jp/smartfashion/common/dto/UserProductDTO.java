package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 * A DTO representing a {@link UserProduct}
 *
 */
@Getter
@Setter
@Data
public class UserProductDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2772788557385418554L;

	@ApiModelProperty(notes = "User product Id (set id = 0 if it's create new)")
	private Integer id;

	@ApiModelProperty(notes = "Image URL")
	@Size(max = 255, message = "Image url {validation.size-max}")
	private String url;

	@ApiModelProperty(notes = "User product collection id")
	@NotNull(message = "User product collection id {validation.not-null}")
	private Integer userProductCollectionId;

	public UserProductDTO() {
	}

	public UserProductDTO(UserProduct userProduct) {
		this.id = userProduct.getId();
		this.url = userProduct.getUrl();
		this.userProductCollectionId = userProduct.getUserProductCollection().getId();
	}
}