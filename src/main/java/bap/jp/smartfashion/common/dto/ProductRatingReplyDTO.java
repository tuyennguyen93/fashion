package bap.jp.smartfashion.common.dto;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import bap.jp.smartfashion.common.model.ProductRatingReply;
import bap.jp.smartfashion.common.model.ProductRatingReplyPhoto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

/**
 * A DTO representing a {@link ProductRatingReply}
 *
 */
@Getter
@Setter
public class ProductRatingReplyDTO {
	
	@ApiModelProperty(hidden = true)
	private int id;
	
	@ApiModelProperty(notes = "Product rating id")
	@Positive(message = "ProductRating ID {validation.unsigned-number}")
	@NotNull(message = "ProductRating ID {validation.not-null}")
	private Integer productRatingId;
	
	@ApiModelProperty(notes = "User information", hidden = true)
	private User user;

	@ApiModelProperty(hidden = true)
	private Integer shopId;

	@ApiModelProperty(notes = "Comment reply for review comment")
	@NotNull(message = "Comment {validation.not-null}")
	@Size(max = 1000, message = "Comment {validation.size-max}")
	private String comment;

	@ApiModelProperty(hidden = true)
	private Date postedDate;

	private Integer likeCount;

	private List<String> imageUrls;
	
	public ProductRatingReplyDTO() {}
	
	public ProductRatingReplyDTO(ProductRatingReply productRatingReply) {
		this.id = productRatingReply.getId();
		this.productRatingId = productRatingReply.getUser().getId();
		if (productRatingReply.getShop() != null) {
			this.shopId = productRatingReply.getShop().getId(); 
		}
		this.user = new User(productRatingReply.getUser());
		this.comment = productRatingReply.getComment();
		this.likeCount = productRatingReply.getLikeCount();
		this.postedDate = Date.from( productRatingReply.getCreated().atZone( ZoneId.systemDefault()).toInstant());
		if(productRatingReply.getImageUrls() != null) {
			this.imageUrls = productRatingReply.getImageUrls().stream()
					.map(ProductRatingReplyPhoto::getUrl).collect(Collectors.toList());
		}
	}
	
	@Getter
	@Setter
	class User {
		private Integer id;
		private String name;
		private String avatar;
		public User(bap.jp.smartfashion.common.model.User user) {
			this.id = user.getId();
			this.name = user.getName();
			this.avatar = user.getAvatar();
		}
	}
}	
