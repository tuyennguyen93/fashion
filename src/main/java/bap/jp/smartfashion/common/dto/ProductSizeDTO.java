package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ProductSize;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a {@link ProductSize}
 *
 */
@Getter
@Setter
public class ProductSizeDTO extends ProductSizeGroup {

	public ProductSizeDTO(ProductSize productSize) {
		if (productSize != null) {
			this.setId(productSize.getId());
			this.setSize(productSize.getSize());
		}
	}

	public ProductSizeDTO() {
	}
}

