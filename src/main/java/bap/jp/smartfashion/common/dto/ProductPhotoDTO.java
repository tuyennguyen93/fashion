package bap.jp.smartfashion.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductPhoto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * A DTO representing a {@link ProductPhoto}
 *
 */
@Getter
@Setter
public class ProductPhotoDTO {
	private Integer id;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	@NotEmpty(message = "Image's url {validation.not-empty}")
	private String urlOriginal;
	
	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(allowableValues = "NEW, EDIT, DELETE", notes = "Action on product photo. If EDIT and DELETE then id is required")
	private String action;
	
	public ProductPhotoDTO() {}
	
	public ProductPhotoDTO(ProductPhoto productPhoto) {
		this.id = productPhoto.getId();
		this.urlOriginal = productPhoto.getUrlOriginal();
	}
}
