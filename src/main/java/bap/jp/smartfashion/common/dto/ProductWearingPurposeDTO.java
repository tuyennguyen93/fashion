package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.ProductWearingPurpose;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  A DTO representing a {@link ProductWearingPurpose}
 *
 */
@Getter
@Setter
public class ProductWearingPurposeDTO {
	@ApiModelProperty(hidden = true)
	private Integer id;

	@ApiModelProperty(notes = "Wearing purpose", required = true)
	@Size(max = 100, message = "ProductWearingPurpose's name {validation.size-max}")
	@NotNull(message = "ProductWearingPurpose's name {validation.not-null}")
	private String name;

	public ProductWearingPurposeDTO(ProductWearingPurpose productWearingPurpose) {
		this.id = productWearingPurpose.getId();
		this.name = productWearingPurpose.getName();
	}

	public ProductWearingPurposeDTO() {
	}
}
