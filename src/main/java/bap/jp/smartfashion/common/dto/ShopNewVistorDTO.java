package bap.jp.smartfashion.common.dto;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;

import bap.jp.smartfashion.common.model.ShopVisitor;
import lombok.Getter;
import lombok.Setter;
/**
 * A DTO representing a {@link ShopVisitor}
 *
 */
@Getter
@Setter
public class ShopNewVistorDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer total;
	private Date date;
	private String type;
	
	public ShopNewVistorDTO(ShopVisitor shopVisitor) {
		this.total = shopVisitor.getTypeCount();
		this.date = Date.from( shopVisitor.getCreated().atZone( ZoneId.systemDefault()).toInstant());
		this.type = shopVisitor.getType();
	}

	public ShopNewVistorDTO() {}

}
