package bap.jp.smartfashion.common.dto;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import bap.jp.smartfashion.common.dto.ProductRatingReplyDTO.User;
import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingPhoto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

/**
 * A DTO representing a {@link ProductRating}
 *
 */
@Getter
@Setter
public class ProductRatingDTO {

	@ApiModelProperty(hidden = true)
	private int id;

	@ApiModelProperty(hidden = true)
	private User user;

	@Size(max = 5, message = "Rating {validation.size-max}")
	@Positive(message = "Rating {validation.unsigned-number}")
	private int rating;

	@Size(max = 1000, message = "Comment {validation.size-max}")
	@NotNull(message = "Comment {validation.not-null}")
	private String comment;

	@ApiModelProperty(hidden = true, notes = " display with case data response")
	private Integer likeCount;

	@ApiModelProperty(hidden = true, notes = " display with case data response")
	private Integer commentCount;

	@ApiModelProperty(hidden = true, notes = " display with case data response")
	private Date postedDate;

	@ApiModelProperty(hidden = true, notes = " display with case data response")
	private List<ProductRatingReplyDTO> productRatingReplies;

	private List<String> imageUrls = new ArrayList<>();

	public ProductRatingDTO() {}
	
	public ProductRatingDTO(ProductRating productRating) {
		this.id = productRating.getId();
		if(productRating.getUser() != null) {
			this.user = new User(productRating.getUser());
		}
		this.rating = productRating.getRating();
		this.comment = productRating.getComment();
		this.likeCount = productRating.getLikeCount();
		this.commentCount = productRating.getCommentCount();
		if(productRating.getProductRatingReplies() != null) {
			this.productRatingReplies = productRating.getProductRatingReplies().stream().map(ProductRatingReplyDTO::new).collect(Collectors.toList());
		}
		this.postedDate = Date.from( productRating.getCreated().atZone( ZoneId.systemDefault()).toInstant());
		if(productRating.getProductRatingPhotos() != null) {
			this.imageUrls = productRating.getProductRatingPhotos().stream().map(ProductRatingPhoto::getUrl).collect(Collectors.toList());

		}
	}
	
	@Getter
	@Setter
	class User {
		private Integer id;
		private String name;
		private String avatar;
		public User(bap.jp.smartfashion.common.model.User user) {
			this.id = user.getId();
			this.name = user.getName();
			this.avatar = user.getAvatar();
		}
	}
}	
