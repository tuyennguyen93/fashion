package bap.jp.smartfashion.common.dto;

import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserProduct;
import bap.jp.smartfashion.common.model.UserProductCollection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 * A DTO representing a {@link UserModelCollection}
 *
 */
@Getter
@Setter
@Data
public class UserModelCollectionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2772788557385418554L;

	@ApiModelProperty(notes = "User model collection id")
	private Integer id;

	@ApiModelProperty(notes = "User model collection name")
	@Size(max = 50)
	private String name;

	public UserModelCollectionDTO() {
	}

	public UserModelCollectionDTO(UserModelCollection userModelCollection) {
		this.id = userModelCollection.getId();
		this.name = userModelCollection.getName();
	}
}