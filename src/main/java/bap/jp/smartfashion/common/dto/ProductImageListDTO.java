package bap.jp.smartfashion.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import bap.jp.smartfashion.common.model.Product;

/**
 * A DTO representing a {@link bap.jp.smartfashion.common.model.ProductPhoto}
 *
 */
@Getter
@Setter
public class ProductImageListDTO {
	private Integer productId;
	private String productName;
	private String productCode;
	private List<ProductPhotoDTO> productPhoto;

	public ProductImageListDTO() {
	}

	public ProductImageListDTO(Product product) {
		this.productId = product.getId();
		this.productName = product.getName();
		this.productCode = product.getProductCode();
		this.productPhoto = new ArrayList<ProductPhotoDTO>();
		product.getProductDetails().forEach(productDetail -> {
			this.productPhoto.addAll(productDetail.getProductPhotos().stream().map(ProductPhotoDTO::new).collect(Collectors.toList()));
		});
	}

}
