package bap.jp.smartfashion.common.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import bap.jp.smartfashion.common.model.ProductType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

/**
 * A DTO representing a {@link ProductType}
 *
 */
@Getter
@Setter
public class ProductTypeDTO {
	
	@ApiModelProperty(hidden = true)
	private Integer id;
	
	@ApiModelProperty(notes = "Product type name", required = true)
	@Size(max = 50, message = "ProductType's name {validation.size-max}")
	private String name;
	
	@JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(notes = "Product type child list", required = true)
	private List<ProductTypeDTO> childList;

	@ApiModelProperty(notes = "Product type parent", required = true)
	private Integer productTypeParentId;

	@ApiModelProperty(notes = "Product type parent name", hidden = true)
	private String productTypeParentName;

	public ProductTypeDTO(ProductType productType) {
		this.id = productType.getId();
		this.name = productType.getName();
		this.productTypeParentId = productType.getProductTypeParentId();
	}

	public ProductTypeDTO() {
	}
}
