package bap.jp.smartfashion.common.dto;

import lombok.Getter;
import lombok.Setter;
import bap.jp.smartfashion.common.model.ProductMixMatchPhoto;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 * A DTO representing {@link ProductMixMatchPhoto}
 *
 */
@Getter
@Setter
public class ProductMixMatchPhotoDTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	@Size(max = 255, message = "Image's url {validation.size-max}")
	private String url;

	public ProductMixMatchPhotoDTO() {
	}

	public ProductMixMatchPhotoDTO(ProductMixMatchPhoto productMixMatchPhoto) {
		this.id = productMixMatchPhoto.getId();
		this.url = productMixMatchPhoto.getUrl();
	}
}
