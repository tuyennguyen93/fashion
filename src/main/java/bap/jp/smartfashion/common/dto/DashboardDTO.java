package bap.jp.smartfashion.common.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * A DTO representing a Dashboard
 *
 */
@Getter
@Setter
public class DashboardDTO {
	private List<ProductInteractionDTO> productsInteractions;
	private List<ProductCollectionDTO> productCollections;
	private List<ShopNewVistorDTO> shopVisitors;
}
