package bap.jp.smartfashion.common.mapper;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.dto.*;
import bap.jp.smartfashion.common.model.*;
import bap.jp.smartfashion.common.repository.ProductRatingPhotoRepository;
import bap.jp.smartfashion.common.service.MessageService;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.ConflictException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ProductColorRepository;
import bap.jp.smartfashion.common.repository.ProductDetailRepository;
import bap.jp.smartfashion.common.repository.ProductPhotoRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ProductSizeChartRepository;
import bap.jp.smartfashion.common.repository.ProductSizeRepository;
import bap.jp.smartfashion.common.repository.ProductSubDetailRepository;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;
import bap.jp.smartfashion.common.repository.ProductWearingPurposeRepository;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.enums.ProductEnum;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.CodeEnumUtils;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Mapper for the entity {@link Product} and its DTO called {@link ProductDTO}.
 *
 */
@Service
public class ProductMapper {
	
	@Autowired
	private ProductTypeRepository productTypeRepository;
	
	@Autowired
	private ProductWearingPurposeRepository productWearingPurposeRepository;
	
	@Autowired
	private ProductCollectionRepository productCollectionRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductPhotoRepository productPhotoRepository;
	
	@Autowired
	private ProductSizeRepository productSizeRepository;
	
	@Autowired
	private ProductColorRepository productColorRepository;

	@Autowired
	private ProductDetailRepository productDetailRepository;
	
	@Autowired
	private ProductSubDetailRepository productSubDetailRepository;

	@Autowired
	private BaseService baseService;

	@Autowired
	private ProductRatingPhotoRepository productRatingPhotoRepository;

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductSizeChartRepository productSizeChartRepository;

	@Autowired
	private MessageService messageService;

	/**
	 * Convert {@link ProductDTO} to {@link Product}
	 * 
	 * @param productUpdateDTO {@link ProductUpdateDTO}
	 * @param shopId {@link Shop} Id
	 * @param productId {@link Product}
	 * @return
	 * @throws NotFoundException
	 * @throws ConflictException 
	 * @throws BadRequestException 
	 */
	public Product productUpdateDTOToProduct(ProductUpdateDTO productUpdateDTO, Integer shopId, Integer productId) throws NotFoundException, ConflictException, BadRequestException {
		if (productUpdateDTO == null) {
			return null;
		} else {
			Product product = this.checkProduct(productId, shopId);
			if (product.getShop().getId() != shopId) {
				throw new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product"));
			}

			if (productUpdateDTO.isQuickUpdate()) {
				product.setName(productUpdateDTO.getName());
				product.setDescription(productUpdateDTO.getDescription());
				product.setHighlight(productUpdateDTO.isHighlight());
			} else {
				product.setName(productUpdateDTO.getName());
				this.checkCurrency(productUpdateDTO.getCurrency());
				product.setCurrency(productUpdateDTO.getCurrency());
				product.setGuarantee(productUpdateDTO.getGuarantee());
				product.setDescription(productUpdateDTO.getDescription());
				product.setHighlight(productUpdateDTO.isHighlight());
				
				ProductWearingPurpose productWearingPurpose = this.checkProductWearingPurpose(productUpdateDTO.getProductWearingPurposeId());
				product.setProductWearingPurpose(productWearingPurpose);

				ProductCollection productCollection = this.checkProductCollection(productUpdateDTO.getProductCollectionId());
				product.setProductCollection(productCollection);
				product.setProductCode(SmartFashionUtils.replaceNewCollectionShopCode(
						product.getProductCode(), 
						product.getShop().getShopCode(),
						productCollection.getName()));

				ProductType productType = this.checkValidProductType(productCollection.getId(), productUpdateDTO.getProductTypeId());
				this.productService.checkProductTypeCountForProductUpdate(product, productType, shopId);
				product.setProductType(productType);
				ProductSizeChart productSizeChart = this.checkProductSizeChart(productUpdateDTO.getProductSizeChartId());
				product.setProductSizeChart(productSizeChart);
			}
			// Check Product's price
			// this.checkProductPrice(productUpdateDTO.getPrice());
			//update price when discount
			if (productUpdateDTO.getDiscount() == null || productUpdateDTO.getDiscount().compareTo(BigDecimal.ZERO) <= 0) {
				product.setPrice(productUpdateDTO.getPrice());
				product.setOldPrice(null);
				product.setDiscount(new BigDecimal(0));
			} else {
				product.setOldPrice(productUpdateDTO.getPrice());
				product.setPrice(ProductService.calPriceDiscount(productUpdateDTO.getPrice(), productUpdateDTO.getDiscount()));
				product.setDiscount(productUpdateDTO.getDiscount());
			}
			
			return product;
		}
	}

	/**
	 * @throws BadRequestException 
	 *
	 */
	public Product setProductData(Shop shop, ProductDTO productDTO, Product product) throws NotFoundException, ConflictException, BadRequestException {
		product.setName(productDTO.getName());
		// Check Product's currency
		this.checkCurrency(productDTO.getCurrency());
		product.setCurrency(productDTO.getCurrency());

		product.setGuarantee(productDTO.getGuarantee());
		product.setDescription(productDTO.getDescription());
		product.setHighlight(productDTO.isHighlight());

		// Check Product's price
		// this.checkProductPrice(productDTO.getPrice());
		//update price when discount
		if (productDTO.getDiscount() == null || productDTO.getDiscount().compareTo(BigDecimal.ZERO) <= 0) {
			product.setPrice(productDTO.getPrice());
			product.setOldPrice(null);
			product.setDiscount(new BigDecimal(0));
		} else {
			product.setOldPrice(productDTO.getPrice());
			product.setPrice(ProductService.calPriceDiscount(productDTO.getPrice(), productDTO.getDiscount()));
			product.setDiscount(productDTO.getDiscount());
		}
		
		ProductCollection productCollection = this.checkProductCollection(productDTO.getProductCollectionId());
		product.setProductCollection(productCollection);
		
		ProductWearingPurpose productWearingPurpose = this.checkProductWearingPurposeIdWithCollectionType(
				productCollection.getType(), productDTO.getProductWearingPurposeId());
		product.setProductWearingPurpose(productWearingPurpose);

		// Check product type id valid
        ProductType productType = this.checkValidProductType(productCollection.getId(), productDTO.getProductTypeId());
		product.setProductType(productType);
		product.setCreated(LocalDateTime.now());
		//build product code
		if(product.getId() == null || product.getId() == 0) {
			int productCount = this.productRepository.countByShop_Id(shop.getId()) + 1;
			product.setProductCode(String.valueOf(productCount));
			product.setProductCode(SmartFashionUtils.generateProductCode(shop, product));
		} else {
			product.setProductCode(SmartFashionUtils.replaceNewCollectionShopCode(
					product.getProductCode(), 
					product.getShop().getShopCode(),
					productCollection.getName()));
		}
		
		ProductSizeChart productSizeChart = this.checkProductSizeChart(productDTO.getProductSizeChartId());
		product.setProductSizeChart(productSizeChart);
		
		return product;
	}

	/**
	 * Convert {@link ProductDetailDTO} to {@link ProductDetail}
	 * @param productDetailDTO {@link ProductDetailDTO}
	 * @param isNew Set true if set for new {@link ProductDetail}, otherwise
	 * 
	 * @return {@link ProductDetail}
	 * @throws NotFoundException
	 */
	public ProductDetail findProductDetailById(ProductDetailDTO productDetailDTO, boolean isNew) throws NotFoundException {
		if (productDetailDTO == null) {
			return null;
		} else {
			ProductDetail productDetail  = new ProductDetail();
			if (!isNew) {
				productDetail = this.findProductDetailById(productDetailDTO.getId());
			}
			return productDetail; 
		}
	}
	
	/**
	 * Convert {@link ProductColorDTO} to {@link ProductColor}
	 * @param productColorDTO {@link ProductColorDTO}
	 * 
	 * @return {@link ProductColor}
	 * @throws NotFoundException
	 */
	public ProductColor productColorDTOToProductColor(ProductColorDTO productColorDTO) throws NotFoundException {
		if (productColorDTO == null) {
			return null;
		} else {
			ProductColor productColor = this.findProductColorById(productColorDTO.getId());
			return productColor; 
		}
	}
	
	/**
	 * Convert {@link ProductPhotoDTO} to {@link ProductPhoto}
	 * @param productDetail {@link ProductDetail}
	 * @param productPhotoDTO {@link ProductPhotoDTO}
	 * @param isNew Set true if set for new {@link ProductPhoto}, otherwise
	 * 
	 * @return {@link ProductPhoto}
	 * @throws NotFoundException
	 */
	public ProductPhoto productPhotoDTOToProductPhoto(ProductDetail productDetail, ProductPhotoDTO productPhotoDTO, boolean isNew) throws NotFoundException {
		if (productPhotoDTO == null) {
			return null;
		} else {
			ProductPhoto productPhoto = null;
			if (isNew) {
				productPhoto = new ProductPhoto();
				productPhoto.setProductDetail(productDetail);
				productPhoto.setUrlOriginal(productPhotoDTO.getUrlOriginal());
			} else {
				productPhoto = this.findProductPhotoById(productPhotoDTO.getId());
				productPhoto.setProductDetail(productDetail);
				productPhoto.setUrlOriginal(productPhotoDTO.getUrlOriginal());
			}
			
			return productPhoto; 
		}
	}
	
	/**
	 * Convert {@link ProductSubDetailDTO} to {@link ProductSubDetail}
	 * @param productDetail {@link ProductDetail}
	 * @param productSubDetailDTO {@link ProductSubDetailDTO}
	 * @param isNew Set true if set for new {@link ProductSubDetail}, otherwise
	 * 
	 * @return {@link ProductSubDetail}
	 * @throws NotFoundException
	 * @throws ConflictException 
	 */
	public ProductSubDetail productSubDetailDTOToProductSubDetail(ProductDetail productDetail, ProductSubDetailDTO productSubDetailDTO, boolean isNew) throws NotFoundException, ConflictException {
		if (productSubDetailDTO == null) {
			return null;
		} else {
			ProductSubDetail productSubDetail = null;
			if (isNew) {
				productSubDetail = new ProductSubDetail();
				productSubDetail.setProductDetail(productDetail);
				ProductSize productSize = this.findProductSizeById(productSubDetailDTO.getProductSize().getId());
				this.checkSizeExistInProductDetail(productDetail.getId(), productSize.getId());
				productSubDetail.setProductSize(productSize);
				productSubDetail.setTotal(productSubDetailDTO.getTotal());
				
			} else {
				productSubDetail = this.findProductSubDetailById(productSubDetailDTO.getId());
				ProductSize productSize = this.findProductSizeById(productSubDetailDTO.getProductSize().getId());
			//	this.checkSizeExistInProductDetail(productDetail.getId(), productSize.getId());
				productSubDetail.setProductDetail(productDetail);
				productSubDetail.setProductSize(productSize);
				productSubDetail.setTotal(productSubDetailDTO.getTotal());
			}
			return productSubDetail; 
		}
	}
	
	/**
	 * Find {@link ProductDetail} by Id 
	 * @param productDetailId {@link ProductDetail} Id
	 * 
	 * @return {@link ProductDetail}
	 * @throws NotFoundException
	 */
	private ProductDetail findProductDetailById(Integer productDetailId) throws NotFoundException {
		return this.productDetailRepository.findById(productDetailId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_DETAIL_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product detail")));
	}
	
	/**
	 * Find {@link ProductColor} by Id 
	 * @param colorId {@link ProductColor} Id
	 * 
	 * @return {@link ProductColor}
	 * @throws NotFoundException
	 */
	private ProductColor findProductColorById(Integer colorId) throws NotFoundException {
		return this.productColorRepository.findById(colorId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_COLOR_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Color")));
	}
	
	/**
	 * Find {@link ProductSubDetail} by Id 
	 * @param productSubDetailId {@link ProductSubDetail} Id
	 * 
	 * @return {@link ProductSubDetail}
	 * @throws NotFoundException
	 */
	private ProductSubDetail findProductSubDetailById(Integer productSubDetailId) throws NotFoundException {
		return this.productSubDetailRepository.findById(productSubDetailId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_SUB_DETAIL_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product sub detail"), HttpStatus.NOT_FOUND));
	}
	
	/**
	 * Find {@link ProductSize} by Id 
	 * @param sizeId {@link ProductSize} Id
	 * 
	 * @return {@link ProductSize}
	 * @throws NotFoundException
	 */
	public ProductSize findProductSizeById(Integer sizeId) throws NotFoundException {
		return this.productSizeRepository.findById(sizeId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_SIZE_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Size"), HttpStatus.NOT_FOUND));
	}
	
	/**
	 * Check {@link ProductSize} existed in {@link ProductDetail}
	 * @param productDetailId {@link ProductDetail} Id
	 * @param productSizeId {@link ProductSize} Id
	 * @throws NotFoundException
	 */
	private void checkSizeExistInProductDetail(Integer productDetailId, Integer productSizeId) throws ConflictException {
		Optional<ProductSubDetail> optional = this.productSubDetailRepository.findOneByProductDetail_IdAndProductSize_id(productDetailId, productSizeId);
		if (optional.isPresent()) {
			throw new ConflictException(ConflictException.ERROR_SIZE_EXITED,
				String.format(APIConstants.ERROR_EXISTED_MSG, "Size"));
		}
	}
	
	/**
	 * Find {@link ProductPhoto} by Id
	 * @param productPhotoId {@link ProductPhoto} Id
	 * @return  {@link ProductPhoto}
	 * @throws NotFoundException
	 */
	private ProductPhoto findProductPhotoById(Integer productPhotoId) throws NotFoundException {
		return this.productPhotoRepository.findById(productPhotoId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_PHOTO_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product photo")));
	}
	
	/**
	 * Check currency of {@link Product} price
	 * @param currency currency
	 * @throws NotFoundException
	 */
	private void checkCurrency(String currency) throws NotFoundException {
		ProductEnum.Currency codeEnum = CodeEnumUtils.fromCodeQuietly(ProductEnum.Currency.class, currency);
		if(codeEnum == null) {
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_CURRENCY_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Currency"));
		}
	}
	
	/**
	 * Check {@link Product}
	 * @param productId {@link Product} Id
	 * @return  {@link Product}
	 * @throws NotFoundException
	 */
	public Product checkProduct(Integer productId, Integer shopId) throws NotFoundException {
		Product product = this.productRepository.findByIdAndStatus(productId, ProductEnum.ProductStatus.ACTIVE.getCode()).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product")));
		
		if (product.getShop().getId() != shopId) {
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product"));
		}
		return product;
	}
	
	/**
	 * Check {@link Product}
	 * @param productId {@link Product} Id
	 * @return  {@link Product}
	 * @throws NotFoundException
	 * @throws BadRequestException 
	 */
	public ProductSizeChart checkProductSizeChart(Integer productSizeChartId) throws NotFoundException, BadRequestException {
		if (productSizeChartId == null) {
			throw new BadRequestException(BadRequestException.ERROR_INVALID_SIZE_CHART, "Product size chart id is not null", false);
		}
		ProductSizeChart productSizeChart = this.productSizeChartRepository.findById(productSizeChartId).orElseThrow(() ->
			new NotFoundException(NotFoundException.ERROR_PRODUCT_SIZE_CHART_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product size chart")));
		return productSizeChart;
	}
	/**
	 * Check {@link ProductWearingPurpose}
	 * @param productWearingPurposeId {@link ProductWearingPurpose} Id
	 * @return  {@link ProductWearingPurpose}
	 * @throws NotFoundException
	 */
	private ProductWearingPurpose checkProductWearingPurpose(Integer productWearingPurposeId) throws NotFoundException {
		ProductWearingPurpose productWearingPurpose = this.productWearingPurposeRepository.findById(productWearingPurposeId).orElseThrow(() -> 
		new NotFoundException(NotFoundException.ERROR_PRODUCT_WEARING_PURPOSE_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Wearing purpose")));
		
		return productWearingPurpose;
	}
	
	/**
	 * Check {@link ProductCollection}
	 * @param productCollectionId {@link ProductCollection} Id
	 * @return  {@link ProductType}
	 * @throws NotFoundException
	 */
	private ProductCollection checkProductCollection(Integer productCollectionId) throws NotFoundException {
		ProductCollection productCollection = this.productCollectionRepository.findById(productCollectionId).orElseThrow(() -> 
		new NotFoundException(NotFoundException.ERROR_PRODUCT_COLLECTION_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product Collection")));
		
		return productCollection;
	}

	public Product exitsProductID(Integer shopId, String productCode) {
		return this.productRepository.exitsProductID(shopId, productCode, ProductEnum.ProductStatus.ACTIVE.getCode()).orElse(null);
	}

	/**
	 * Check ProductWearingPurpose with collection, exits or not
	 *
	 * @param productType {@link ProductType}
	 * @param productWearingPurposeId {@link ProductWearingPurpose}
	 * @return {@link ProductWearingPurpose}
	 * @throws NotFoundException
	 */
	private ProductWearingPurpose checkProductWearingPurposeIdWithCollectionType(String productType, Integer productWearingPurposeId) throws NotFoundException{
		return this.productWearingPurposeRepository.findByIdAndType(productWearingPurposeId, productType).orElseThrow(
				() -> new NotFoundException(NotFoundException.ERROR_PRODUCT_WEARING_PURPOSE_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "Product Wearing Purpose with that Collection")));
	}

	/**
	 * Check ProductType, exits or not
	 *
	 * @param collectionId {@link ProductCollection}
	 * @param productTypeId {@link ProductType}
	 * @return {@link ProductType}
	 * @throws NotFoundException
	 */
	public ProductType checkValidProductType(Integer collectionId, Integer productTypeId) throws NotFoundException, ConflictException {
        ProductType productType = this.baseService.getExitedProductType(productTypeId);
        if(productType == null) {
            throw new NotFoundException(NotFoundException.ERROR_PRODUCT_TYPE_NOT_FOUND,
                    String.format(this.messageService.getMessage("error.not-found-msg"),
							this.messageService.getMessage("product-type")), HttpStatus.NOT_FOUND);
        } else if (productType.getProductTypeParentId() == null){
            List<ProductType> productTypes = this.productTypeRepository.findAllByProductTypeParentId(productType.getId());
            if(!productTypes.isEmpty()) {
                throw new ConflictException(String.format(this.messageService.getMessage("error.bad-value-msg"),
						this.messageService.getMessage("product-type")));
            }
			if (!productType.getProductCollection().getId().equals(collectionId)) {
				throw new ConflictException(String.format(this.messageService.getMessage("error.bad-value-msg"),
						this.messageService.getMessage("product-type-with-collection")));
			}
        } else if (productType.getProductTypeParentId() != null) {
            ProductType productTypeParent = this.productTypeRepository.findById(productType.getProductTypeParentId()).orElse(null);
            if(!productTypeParent.getProductCollection().getId().equals(collectionId)) {
                throw new ConflictException(String.format(this.messageService.getMessage("error.bad-value-msg"),
						this.messageService.getMessage("product-type-with-collection")));
            }
        }
        return productType;
	}

	/**
	 * Convert productRatingDTO to ProductRating
	 *
	 * @param productRating {@link ProductRating}
	 * @param productRatingDTO {@link ProductRatingDTO}
	 * @return {@link ProductRating}
	 * @throws BadRequestException
	 */
	public ProductRating convertProductRatingDTOToProductRating(ProductRating productRating, ProductRatingDTO productRatingDTO, boolean isUpdated) throws BadRequestException {
		List<ProductRatingPhoto> photos = new ArrayList<>();
		if(productRatingDTO == null) {
			return null;
		} else {
			if(isUpdated) {
				if(!productRating.getProductRatingPhotos().isEmpty()) {
					this.productRatingPhotoRepository.deleteAll(productRating.getProductRatingPhotos());
				}
			} else {
				productRating.setLikeCount(0);
				productRating.setCommentCount(0);
			}
			this.checkValidRating(productRatingDTO.getRating());
			productRating.setRating(productRatingDTO.getRating());
			productRating.setComment(productRatingDTO.getComment());

			if(!productRatingDTO.getImageUrls().isEmpty()) {
				for(String image : productRatingDTO.getImageUrls()) {
					photos.add(new ProductRatingPhoto(productRating, image));
				}
				productRating.setProductRatingPhotos(photos);
			}
		}
		return productRating;
	}

	/**
	 * Check valid Rating
	 * rating from 1 to 5
	 *
	 * @param rating {@link ProductRating}
	 * @throws BadRequestException
	 */
	public void checkValidRating(Integer rating) throws BadRequestException {
		if (rating <= 0 || rating > 5) {
			throw new BadRequestException(BadRequestException.ERROR_INVALID_RATING,
				String.format(APIConstants.ERROR_BAD_VALUE_MSG, "Rating"), false);
		}
	}

	/**
	 * check product interaction
	 * @param actionCode
	 * @throws NotFoundException
	 */
	public void checkProductInteraction(String actionCode) throws NotFoundException {
		ProductEnum.ProductInteraction codeEnum = CodeEnumUtils.fromCodeQuietly(ProductEnum.ProductInteraction.class,actionCode);
		if(codeEnum == null) {
			throw new NotFoundException(NotFoundException.ERROR_PRODUCT_INTERACTION_NOT_FOUND, String.format(APIConstants.ERROR_NOT_FOUND_MSG, "action code"));
		}
	}

	/**
	 * check product's price, valid or not
	 *
	 * @param price {@link Product}
	 * @throws BadRequestException
	 */
	public void checkProductPrice(BigDecimal price) throws BadRequestException {
		if(price == null || (price.compareTo(BigDecimal.ZERO) < 0 || price.compareTo(BigDecimal.ZERO) == 0)) {
			throw new BadRequestException(String.format(APIConstants.ERROR_BAD_VALUE_MSG, "price"));
		}
	}

	/**
	 * check Product's name, exits or not
	 *
	 * @param shopId {@link Shop}
	 * @param name {@link Product} product's name
	 */
	public Product checkProductName(Integer shopId, String name) {
		return this.productRepository.exitsProductName(shopId, name, ProductEnum.ProductStatus.ACTIVE.getCode()).orElse(null);
	}

}
