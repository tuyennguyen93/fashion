package bap.jp.smartfashion.common.mapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.common.dto.ShopDTO;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.repository.ShopRepository;
import bap.jp.smartfashion.enums.ShopEnum.ShopStatus;
import bap.jp.smartfashion.util.SmartFashionUtils;

/**
 * Mapper for the entity {@link Shop} and its DTO called {@link ShopDTO}.
 *
 */
@Service
public class ShopMapper {

	@Autowired
	private ShopRepository shopRepository;
	
	/**
	 * Mapping {@link ShopDTO} to new {@link Shop}
	 * @param shopDTO {@link ShopDTO}
	 * @return {@link Shop}
	 */
	public Shop shopDTOToShop(ShopDTO shopDTO) {
		if (shopDTO == null) {
			return null;
		} else {
			Shop shop = new Shop();
			shop.setUser(shopDTO.getUser());
			shop.setName(shopDTO.getName());
			shop.setEmail(shopDTO.getEmail());
			shop.setPhone(shopDTO.getPhone());
			shop.setAvatar(shopDTO.getAvatar());
			shop.setDescription(shopDTO.getDescription());
			shop.setShopCode(this.generateShopCode(shopDTO.getName()));
			shop.setStatus(ShopStatus.PENDING.getCode());
			
			return shop;
		}
	}
	/**
	 * Mapping {@link ShopDTO} to {@link Shop}
	 * @param shopDTO {@link ShopDTO}
	 * @param shop {@link Shop}
	 * @return {@link Shop}
	 */
	public Shop shopDTOToShop(ShopDTO shopDTO, Shop shop) {
		if (shopDTO == null) {
			return null;
		} else {
			shop.setName(shopDTO.getName());
			shop.setEmail(shopDTO.getEmail());
			shop.setPhone(shopDTO.getPhone());
			shop.setAvatar(shopDTO.getAvatar());
			shop.setDescription(shopDTO.getDescription());
			return shop;
		}
	}
	
	/***
	 * Generate shopCode from shopName
	 * 
	 * @param shopName {@link Shop} name
	 * @return {@link Shop} code
	 */
	private String generateShopCode(String shopName) {
		String shopCode = SmartFashionUtils.generateShopCode(shopName, StringUtils.EMPTY);
		Integer shopCodeCount = this.shopRepository.countByShopCodeLike(shopCode);
		if (shopCodeCount > 0) {
			shopCode =  SmartFashionUtils.generateShopCode(shopName, String.valueOf(shopCodeCount));
		}
		
		return shopCode;
	}
}
