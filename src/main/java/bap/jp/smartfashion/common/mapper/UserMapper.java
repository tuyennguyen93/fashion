package bap.jp.smartfashion.common.mapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.api.authentication.dto.AuthenticationSNSDTO;
import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.vo.UserManagement;
import bap.jp.smartfashion.enums.UserEnum;
import bap.jp.smartfashion.enums.UserEnum.LanguageEnum;

/**
 * Mapper for the entity {@link User} and its DTO called {@link UserDTO}.
 *
 */
@Service
public class UserMapper {
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public User userManagementToUser(UserManagement userManagement) {
		if (userManagement == null) {
			return null;
		} else {
			User user = new User();
			user.setName(userManagement.getName());
			user.setEmail(userManagement.getEmail());
			user.setPhone(userManagement.getPhone());
			user.setPassword(this.bCryptPasswordEncoder.encode(userManagement.getPassword()));
			user.setRegisterStatus(userManagement.isRegisterStatus());
			user.setUserType(UserEnum.UserTypeEnum.LOCAL.toString());
			String avatar = StringUtils.isEmpty(userManagement.getAvatar()) ? "https://bap-fashion-shop.s3.amazonaws.com/default/images/avatar.jpg" : userManagement.getAvatar();
			user.setAvatar(avatar);
			user.setPlatform(userManagement.getPlatform());
			user.setLangKey(LanguageEnum.ENGLISH.getCode());
			return user;
		}
	}
	
	public User userSNSToUser(AuthenticationSNSDTO authenticationSNSDto) {
		if (authenticationSNSDto == null) {
			return null;
		} else {
			User user = new User();
			user.setUid(authenticationSNSDto.getUid());
			user.setName(authenticationSNSDto.getUserName());
			user.setEmail(authenticationSNSDto.getEmail());
			user.setPhone(authenticationSNSDto.getPhone());
			user.setUserType(authenticationSNSDto.getUserType());
			user.setRegisterStatus(authenticationSNSDto.isRegisterStatus());
			user.setAccessToken(authenticationSNSDto.getAccessToken());
			String avatar = StringUtils.isEmpty(authenticationSNSDto.getAvatar()) ? "https://bap-fashion-shop.s3.amazonaws.com/default/images/avatar.jpg" : authenticationSNSDto.getAvatar();
			user.setAvatar(avatar);
			user.setLangKey(LanguageEnum.ENGLISH.getCode());
			return user;
		}
	}
	
	public User userDTOToUser(UserDTO userDTO, User user) {
		if (userDTO == null || user == null) {
			return null;
		} else {
			user.setEmail(userDTO.getEmail());
			user.setName(userDTO.getName());
			user.setPhone(userDTO.getPhone());
			user.setRegisterStatus(userDTO.isRegisterStatus());
			user.setBlocked(userDTO.isBlocked());
			return user;
		}
	}
}
