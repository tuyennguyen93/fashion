package bap.jp.smartfashion.common.vo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a token
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class Token implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String key;
	private String value;
	@JsonIgnore
	private Date expired;


	public Token(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public Token() {
	}
}
