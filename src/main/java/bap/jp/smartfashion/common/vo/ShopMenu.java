package bap.jp.smartfashion.common.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a shop menu
 *
 */
@Getter
@Setter
public class ShopMenu implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private long count;
	private List<ShopMenu> listChild;
	
	public ShopMenu(Integer id, String name, long count) {
		super();
		this.id = id;
		this.name = name;
		this.count = count;
	}

	public ShopMenu() {
	}
	
}
