package bap.jp.smartfashion.common.vo;

import java.util.List;

import bap.jp.smartfashion.common.model.Product;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a {@link Product} to filter
 *
 */
@Getter
@Setter
public class ProductForUserFilter {
	private String query;
	private Object productTypeId;
	private Integer collectionId;
	private boolean isProductTriedOn;
	private boolean isProductFavorited;
	private boolean isProductBasket;
	private List<Integer> productIds;
	private Integer page;
	private Integer limit;
	private List<Integer> colorIds;
	private String sizeChart;
	private List<String> sizes;
	private List<Integer> wearingPurposeIds;
	private Integer minPrice;
	private Integer maxPrice;
	private String sortBy;
}
