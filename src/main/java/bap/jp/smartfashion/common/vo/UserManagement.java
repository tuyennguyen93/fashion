package bap.jp.smartfashion.common.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import bap.jp.smartfashion.common.dto.UserDTO;
import bap.jp.smartfashion.common.model.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a {@link User} to login via {@link UserDTO}
 *
 */
@Getter
@Setter
public class UserManagement extends UserDTO {

	public UserManagement() {
	}

	@NotEmpty(message = "{info.password} {validation.not-empty}")
	@Size(min = 8, max = 25, message = "{info.password} {validation.size-min-max}")
	@ApiModelProperty(notes = "Password")
	private String password;

	@ApiModelProperty(notes = "Is create a new shop")
	private Boolean isShop = false;
	
	@Override
	public String toString() {
		return "UserManagement [password=" + password + ", getId()=" + getId() + ", getName()=" + getName()
				+ ", getEmail()=" + getEmail() + ", isRegisterStatus()=" + isRegisterStatus() + ", isBlocked()="
				+ isBlocked() + ", getUserType()=" + getUserType() + ", getUid()=" + getUid() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
}
