package bap.jp.smartfashion.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * A DTO to get List product's sizes with collection
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class ProductSizeWithCollection {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "size type", required = true)
    private String sizeType;

    @ApiModelProperty(notes = "List of Product's sizes", required = true)
    private List<ProductProperty> productSizes;
}
