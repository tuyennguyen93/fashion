package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.model.Shop;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing {@link Shop} to handle filter menu
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class ShopFilterMenu {
	private String name;
	private Object data;
	public ShopFilterMenu() {
	}
}
