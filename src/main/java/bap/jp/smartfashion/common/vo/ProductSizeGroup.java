package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.model.ProductSize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO to get product's sizes with group collection
 * Using to get properties relate to product
 *
 */
@Getter
@Setter
public class ProductSizeGroup {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "Product size Id")
    private Integer id;

    @ApiModelProperty(notes = "Size", required = true)
    private String size;

    public ProductSizeGroup(ProductSize productSize) {
        this.id = productSize.getId();
        this.size = productSize.getSize();
    }

    public ProductSizeGroup() {
    }
}
