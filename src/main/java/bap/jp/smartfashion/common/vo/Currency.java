package bap.jp.smartfashion.common.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A Value Object to get product's Currency
 * {@link ProductEnum.Currency}
 *
 */

@Getter
@Setter
@AllArgsConstructor
public class Currency {
    private String id;
    private String name;
}
