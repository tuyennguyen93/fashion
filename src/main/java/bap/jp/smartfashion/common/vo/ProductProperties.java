package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.dto.ProductCollectionDTO;
import bap.jp.smartfashion.common.model.ProductColor;
import bap.jp.smartfashion.common.model.ProductSizeChart;
import bap.jp.smartfashion.common.vo.Currency;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * A DTO to get Data about properties relate to product
 *
 */
@Getter
@Setter
public class ProductProperties {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "List of Collections", required = true)

	private List<ProductCollectionDTO> productCollections;

	@ApiModelProperty(notes = "List of ProductWearingPurposes", required = true)
	private List<ProductWearingPurposeWithType> productWearingPurposes;

	@ApiModelProperty(notes = "List of ProductTypes", required = true)
	private List<ProductTypeWithCollection> productTypes;

	@ApiModelProperty(notes = "List of ProductSizes", required = true)
	private List<ProductSizeWithCollection> productSizes;

	@ApiModelProperty(notes = "List of Product's Currencies", required = true)
	private List<Currency> currencies;

	@ApiModelProperty(notes = "List of Product's Colors", required = true)
	private List<ProductColor> productColors;

	@ApiModelProperty(notes = "List of Product's size chart", required = true)
	private List<ProductSizeChart> productSizeCharts;

	public ProductProperties(
			List<ProductCollectionDTO> productCollections,
			List<ProductTypeWithCollection> productTypeWithCollection,
			List<ProductWearingPurposeWithType> productWearingPurposeWithType,
			List<ProductSizeWithCollection> productSizeWithCollections,
			List<Currency> currencies,
			List<ProductColor> productColors, 
			List<ProductSizeChart> productSizeCharts) {
		this.productCollections = productCollections;
		this.productTypes = productTypeWithCollection;
		this.productWearingPurposes = productWearingPurposeWithType;
		this.productSizes = productSizeWithCollections;
		this.currencies = currencies;
		this.productColors = productColors;
		this.productSizeCharts = productSizeCharts;
	}
}
