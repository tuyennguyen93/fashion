package bap.jp.smartfashion.common.vo;

import java.net.URL;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a pre-signed url
 *
 */
@Getter
@Setter
public class PreSignedAWS {
	private URL preSignedURL;
	private String url;
}
