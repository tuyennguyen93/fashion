package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.dto.ProductWearingPurposeDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * A DTO to get List Product's wearing purposes with type( collection)
 *
 */
@Setter
@Getter
@AllArgsConstructor
public class ProductWearingPurposeWithType {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "Type (Collection)", required = true)
    private String type;

    @ApiModelProperty(notes = "List of Product wearing purposes", required = true)
    private List<ProductWearingPurposeDTO> productWearingPurposes;
}

