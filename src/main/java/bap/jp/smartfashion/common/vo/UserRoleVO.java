package bap.jp.smartfashion.common.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UserRoleVO {
    private String userNameOrEmail;
    private String role;
}
