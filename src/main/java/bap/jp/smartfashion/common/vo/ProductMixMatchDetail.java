package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.dto.ProductDTO;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductMixMatch;
import bap.jp.smartfashion.common.model.ProductMixMatchPhoto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * A value object representing a {@link ProductMixMatch} to set in {@link ProductDTO}
 *
 */
@Getter
@Setter
public class ProductMixMatchDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String currency;
	private BigDecimal price;
	private BigDecimal discount;
}
