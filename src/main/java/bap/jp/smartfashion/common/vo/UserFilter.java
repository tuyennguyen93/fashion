package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.model.User;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a {@link User} to filter
 *
 */
@Getter
@Setter
public class UserFilter {
	private Boolean registerStatus;
	private Boolean blocked;
	private String userType;
}
