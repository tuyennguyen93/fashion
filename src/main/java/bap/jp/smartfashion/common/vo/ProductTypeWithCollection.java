package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.dto.ProductTypeDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * A DTO to get List of Product's types
 *
 */
@Setter
@Getter
@AllArgsConstructor
public class ProductTypeWithCollection {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "Collection Id", required = true)
    private Integer collectionId;

    @ApiModelProperty(notes = "Product's types", required = true)
    private List<ProductTypeDTO> productTypes;

}

