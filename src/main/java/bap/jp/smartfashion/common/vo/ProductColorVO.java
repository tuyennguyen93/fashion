package bap.jp.smartfashion.common.vo;

import java.io.Serializable;

import bap.jp.smartfashion.common.model.ProductColor;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * A value object representing a {@link ProductColor}
 *
 */
@Getter
@Setter
public class ProductColorVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Color id", required = true)
	private int id;
	
	@ApiModelProperty(notes = "Color name", required = true)
	@Size(max = 10, message = "{validation.size-max}")
	private String name;
	
	@ApiModelProperty(notes = "Color code", required = true, example = "#FFFFFF")
	@Size(max = 50, message = "{validation.size-max}")
	private String colorCode;
	
	public ProductColorVO(ProductColor productColor) {
		this.id = productColor.getId();
		this.name = productColor.getName();
		this.colorCode = productColor.getColorCode();
	}

	public ProductColorVO() {}
	
}
