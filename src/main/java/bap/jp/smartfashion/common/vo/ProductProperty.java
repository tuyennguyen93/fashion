package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a {@link Product} with properties
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class ProductProperty {
	private String name;
	private Object data;
}
