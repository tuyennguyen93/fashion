package bap.jp.smartfashion.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a JWT
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Data
public class JwtToken {
	
	private String accessToken;
	private String tokenType;

	public JwtToken() {
	}
	
}
