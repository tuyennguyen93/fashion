package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.dto.ShopCollectionVisitorDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ReturningVisitor {
    private String milestone;
    private List<ShopCollectionVisitorDTO> shopCollectionVisitors;
}
