package bap.jp.smartfashion.common.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 *
 * DTO to get list of IDS
 *
 */
@Setter
@Getter
@NoArgsConstructor
public class ObjectID {
    private List<Integer> idsList;
}
