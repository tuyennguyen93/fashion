package bap.jp.smartfashion.common.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * A value object representing a message
 */
@Getter
@Setter
@AllArgsConstructor
public class Message {
	private String key;
	private String value;
	
	public Message() {
	}
}
