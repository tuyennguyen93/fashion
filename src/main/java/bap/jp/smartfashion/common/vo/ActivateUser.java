package bap.jp.smartfashion.common.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a api response data
 *
 */
@Getter
@Setter
@AllArgsConstructor 
public class ActivateUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Email")
	private String email;
	
	@ApiModelProperty(notes = "Platform used application", allowableValues = "WEB, MOBILE_ANDROID, MOBILE_IOS")
	private String platform;

	public ActivateUser() {
	}
	
}
