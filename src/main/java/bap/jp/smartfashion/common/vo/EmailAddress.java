package bap.jp.smartfashion.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * A value object representing a email
 *
 */
@Getter
@Setter
@Data
@AllArgsConstructor
public class EmailAddress {
	private String email;

	public EmailAddress() {
	}
	
}
