package bap.jp.smartfashion.common.vo;

import bap.jp.smartfashion.common.model.Product;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSearchInfo {
    private Integer productId;
    private String productName;
    private String productCode;
    private String detailInformation;

    public ProductSearchInfo(Product product) {
        this.productId = product.getId();
        this.productName = product.getName();
        this.productCode = product.getProductCode();
        this.detailInformation = this.createDetailInformation(product);
    }

    public String createDetailInformation(Product product) {
        return String.format("(%s/%s/%s)",product.getProductCollection().getName(),
                product.getProductType().getName(), product.getProductWearingPurpose().getName()).toLowerCase();
    }
}
