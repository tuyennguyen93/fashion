package bap.jp.smartfashion.common.vo;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRatingVO {
	private int[] productRatingCount;
	private double ratingStar;
}
