package bap.jp.smartfashion.command;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import bap.jp.smartfashion.common.model.Role;
import bap.jp.smartfashion.common.model.UserRole;
import bap.jp.smartfashion.common.repository.RoleRepository;
import bap.jp.smartfashion.common.repository.UserRoleRepository;
import bap.jp.smartfashion.enums.RoleEnum;
import bap.jp.smartfashion.enums.UserEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Admin;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bap.jp.smartfashion.api.admin.user.AdminUserService;
import bap.jp.smartfashion.api.user.UserService;
import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.ProductCollection;
import bap.jp.smartfashion.common.model.ProductColor;
import bap.jp.smartfashion.common.model.ProductDetail;
import bap.jp.smartfashion.common.model.ProductPhoto;
import bap.jp.smartfashion.common.model.ProductRating;
import bap.jp.smartfashion.common.model.ProductRatingCount;
import bap.jp.smartfashion.common.model.ProductSize;
import bap.jp.smartfashion.common.model.ProductSizeChart;
import bap.jp.smartfashion.common.model.ProductSubDetail;
import bap.jp.smartfashion.common.model.ProductType;
import bap.jp.smartfashion.common.model.ProductTypeCount;
import bap.jp.smartfashion.common.model.ProductWearingPurpose;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserModel;
import bap.jp.smartfashion.common.model.UserModelCollection;
import bap.jp.smartfashion.common.model.UserProductCollection;
import bap.jp.smartfashion.common.repository.ProductCollectionRepository;
import bap.jp.smartfashion.common.repository.ProductColorRepository;
import bap.jp.smartfashion.common.repository.ProductDetailRepository;
import bap.jp.smartfashion.common.repository.ProductRatingCountRepository;
import bap.jp.smartfashion.common.repository.ProductRatingReplyRepository;
import bap.jp.smartfashion.common.repository.ProductRatingRepository;
import bap.jp.smartfashion.common.repository.ProductRepository;
import bap.jp.smartfashion.common.repository.ProductSizeRepository;
import bap.jp.smartfashion.common.repository.ProductSubDetailRepository;
import bap.jp.smartfashion.common.repository.ProductTypeCountRepository;
import bap.jp.smartfashion.common.repository.ProductTypeRepository;
import bap.jp.smartfashion.common.repository.ProductWearingPurposeRepository;
import bap.jp.smartfashion.common.repository.ShopRepository;
import bap.jp.smartfashion.common.repository.UserModelCollectionRepository;
import bap.jp.smartfashion.common.repository.UserModelRepository;
import bap.jp.smartfashion.common.repository.UserProductCollectionRepository;
import bap.jp.smartfashion.common.repository.UserRepository;
import bap.jp.smartfashion.common.service.ProductService;
import bap.jp.smartfashion.common.vo.UserManagement;

/**
 * Initialize Data Command Line
 *
 */
@Component
public class InitializeDataCommandLine implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(InitializeDataCommandLine.class);
	private static final boolean RUN = true;

	@Autowired
	private ProductCollectionRepository productCollectionRepository;

	@Autowired
	private ProductSizeRepository productSizeRepository;
	
	@Autowired
	private ProductColorRepository productColorRepository;

	@Autowired
	private ProductTypeRepository productTypeRepository;
	
	@Autowired
	private ProductWearingPurposeRepository productWearingPurposeRepository;
	
	@Autowired
	private ShopRepository shopRepository;
	
	@Autowired
	private ProductTypeCountRepository productTypeCountRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserProductCollectionRepository userProductCollectionRepository;

	@Autowired
	private UserModelCollectionRepository userModelCollectionRepository;
	
	@Autowired
	private UserModelRepository userModelRepository;
	
	@Value("${email.smartfashion}")
	protected String super_admin_email;

	@Value("${super_admin.password}")
	protected String super_admin_password;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void run(String... args) throws Exception {
		if (RUN) {
			/** Init data for product collection */
			if (this.productCollectionRepository.count() == 0) {
				LOG.info("~~~ Init data for product collection ~~~");
				List<ProductCollection> productCollections = new ArrayList<>(Arrays.asList(
						new ProductCollection("Women", "https://bap-fashion-shop.s3.amazonaws.com/women_1569465706650.jpg", "New Arrivals","ADULT"),
						new ProductCollection("Men", "https://bap-fashion-shop.s3.amazonaws.com/men_1569465684175.jpg", "New Shirts","ADULT"),
						new ProductCollection("Girls", "https://bap-fashion-shop.s3.amazonaws.com/girl_1569465579078.jpg", "New Skirts","KID"),
						new ProductCollection("Boys", "https://bap-fashion-shop.s3.amazonaws.com/boy_1569465640477.jpg", "New Shirts","KID")));
				this.productCollectionRepository.saveAll(productCollections);
				LOG.info("~~~ Init data for product collection - DONE ~~~");
			}
			ProductCollection productCollectionWomen = this.productCollectionRepository.findById(1).get();
			ProductCollection productCollectionMen = this.productCollectionRepository.findById(2).get();
			ProductCollection productCollectionGirls = this.productCollectionRepository.findById(3).get();
			ProductCollection productCollectionBoys = this.productCollectionRepository.findById(4).get();

			
			/** Init data for product size */
			if (this.productSizeRepository.count() == 0) {
				LOG.info("~~~ Init data for product size ~~~");
				List<ProductSize> productSizes = new ArrayList<>(Arrays.asList(
						// adult
						new ProductSize("XS", "ADULT", new ProductSizeChart(1)),
						new ProductSize("S", "ADULT", new ProductSizeChart(1)),
						new ProductSize("M", "ADULT", new ProductSizeChart(1)),
						new ProductSize("L", "ADULT", new ProductSizeChart(1)),
						new ProductSize("XL", "ADULT", new ProductSizeChart(1)),
						new ProductSize("XXL", "ADULT", new ProductSizeChart(1)),
						
						new ProductSize("XS", "ADULT", new ProductSizeChart(2)),
						new ProductSize("S", "ADULT", new ProductSizeChart(2)),
						new ProductSize("M", "ADULT", new ProductSizeChart(2)),
						new ProductSize("L", "ADULT", new ProductSizeChart(2)),
						new ProductSize("XL", "ADULT", new ProductSizeChart(2)),
						new ProductSize("XXL", "ADULT", new ProductSizeChart(2)),
						
						new ProductSize("XS", "ADULT", new ProductSizeChart(3)),
						new ProductSize("S", "ADULT", new ProductSizeChart(3)),
						new ProductSize("M", "ADULT", new ProductSizeChart(3)),
						new ProductSize("L", "ADULT", new ProductSizeChart(3)),
						new ProductSize("XL", "ADULT", new ProductSizeChart(3)),
						new ProductSize("XXL", "ADULT", new ProductSizeChart(3)),
						
						new ProductSize("XS", "ADULT", new ProductSizeChart(4)),
						new ProductSize("S", "ADULT", new ProductSizeChart(4)),
						new ProductSize("M", "ADULT", new ProductSizeChart(4)),
						new ProductSize("L", "ADULT", new ProductSizeChart(4)),
						new ProductSize("XL", "ADULT", new ProductSizeChart(4)),
						new ProductSize("XXL", "ADULT", new ProductSizeChart(4)),
						// kid
						new ProductSize("3-4", "KID", new ProductSizeChart(1)),
						new ProductSize("4-5", "KID", new ProductSizeChart(1)),
						new ProductSize("5-6", "KID", new ProductSizeChart(1)),
						new ProductSize("6-7", "KID", new ProductSizeChart(1)),
						new ProductSize("7-8", "KID", new ProductSizeChart(1)),
						new ProductSize("8-9", "KID", new ProductSizeChart(1)),
						new ProductSize("9-10", "KID", new ProductSizeChart(1)),
						new ProductSize("10-11", "KID", new ProductSizeChart(1)),
						new ProductSize("11-12", "KID", new ProductSizeChart(1)),
						new ProductSize("12-13", "KID", new ProductSizeChart(1)),
						new ProductSize("13-14", "KID", new ProductSizeChart(1)),
						
						new ProductSize("3-4", "KID", new ProductSizeChart(2)),
						new ProductSize("4-5", "KID", new ProductSizeChart(2)),
						new ProductSize("5-6", "KID", new ProductSizeChart(2)),
						new ProductSize("6-7", "KID", new ProductSizeChart(2)),
						new ProductSize("7-8", "KID", new ProductSizeChart(2)),
						new ProductSize("8-9", "KID", new ProductSizeChart(2)),
						new ProductSize("9-10", "KID", new ProductSizeChart(2)),
						new ProductSize("10-11", "KID", new ProductSizeChart(2)),
						new ProductSize("11-12", "KID", new ProductSizeChart(2)),
						new ProductSize("12-13", "KID", new ProductSizeChart(2)),
						new ProductSize("13-14", "KID", new ProductSizeChart(2)),
						
						new ProductSize("3-4", "KID", new ProductSizeChart(3)),
						new ProductSize("4-5", "KID", new ProductSizeChart(3)),
						new ProductSize("5-6", "KID", new ProductSizeChart(3)),
						new ProductSize("6-7", "KID", new ProductSizeChart(3)),
						new ProductSize("7-8", "KID", new ProductSizeChart(3)),
						new ProductSize("8-9", "KID", new ProductSizeChart(3)),
						new ProductSize("9-10", "KID", new ProductSizeChart(3)),
						new ProductSize("10-11", "KID", new ProductSizeChart(3)),
						new ProductSize("11-12", "KID", new ProductSizeChart(3)),
						new ProductSize("12-13", "KID", new ProductSizeChart(3)),
						new ProductSize("13-14", "KID", new ProductSizeChart(3)),
						
						new ProductSize("3-4", "KID", new ProductSizeChart(4)),
						new ProductSize("4-5", "KID", new ProductSizeChart(4)),
						new ProductSize("5-6", "KID", new ProductSizeChart(4)),
						new ProductSize("6-7", "KID", new ProductSizeChart(4)),
						new ProductSize("7-8", "KID", new ProductSizeChart(4)),
						new ProductSize("8-9", "KID", new ProductSizeChart(4)),
						new ProductSize("9-10", "KID", new ProductSizeChart(4)),
						new ProductSize("10-11", "KID", new ProductSizeChart(4)),
						new ProductSize("11-12", "KID", new ProductSizeChart(4)),
						new ProductSize("12-13", "KID", new ProductSizeChart(4)),
						new ProductSize("13-14", "KID", new ProductSizeChart(4))));
				this.productSizeRepository.saveAll(productSizes); 
				LOG.info("~~~ Init data for product size - DONE ~~~");
			}
			
			/** Init data for product color */
			if (this.productColorRepository.count() == 0) {
				LOG.info("~~~ Init data for product color ~~~");
				List<ProductColor> productColors = new ArrayList<>(Arrays.asList(
						new ProductColor("Beige","#f6d2a0"),
						new ProductColor("Orange","#feaa1e"),
						new ProductColor("Black","#000"),
						new ProductColor("Pink","#fe9dd8"),
						new ProductColor("Blue","#4790e2"),
						new ProductColor("Purple","#a74ff9"),
						new ProductColor("Brown","#8f5529"),
						new ProductColor("Red","#dc3434"),
						new ProductColor("Green","#7ed320"),
						new ProductColor("White","#efefef"),
						new ProductColor("Grey","#999"),
						new ProductColor("Yellow","#ffed1e")));
				this.productColorRepository.saveAll(productColors);
				LOG.info("~~~ Init data for product color - DONE ~~~");
			}
			
			/** Init data for product type */
			if (this.productTypeRepository.count() == 0) {
				LOG.info("~~~ Init data for product type ~~~");
				List<ProductType> productTypes = new ArrayList<>(Arrays.asList(
						
						new ProductType("Tops", null, productCollectionMen),
						new ProductType("Bottoms", null, productCollectionMen),
						new ProductType("Outerwears", null, productCollectionMen),
						new ProductType("Suits", null, productCollectionMen),
						
						new ProductType("Tops", null, productCollectionWomen),
						new ProductType("Bottoms", null, productCollectionWomen),
						new ProductType("Dresses", null, productCollectionWomen),
						new ProductType("Outerwears", null, productCollectionWomen),
						new ProductType("Jumpsuits", null, productCollectionWomen),
						new ProductType("Suits", null, productCollectionWomen),

						new ProductType("Tops", null, productCollectionGirls),
						new ProductType("Bottoms", null, productCollectionGirls),
						new ProductType("Dresses", null, productCollectionGirls),
						new ProductType("Outerwears", null, productCollectionGirls),
						new ProductType("Jumpsuits", null, productCollectionGirls),
						
						new ProductType("Tops", null, productCollectionBoys),
						new ProductType("Bottoms", null, productCollectionBoys),
						new ProductType("Outerwears", null, productCollectionBoys)
					));
				
				List<ProductType> productTypesChildMen = new ArrayList<>(Arrays.asList(
						new ProductType("T-Shirts", 1, null),
						new ProductType("Shirts", 1, null),
						new ProductType("Hoodies", 1, null),
						new ProductType("Sweaters", 1, null),
						new ProductType("Shorts", 2, null),
						new ProductType("Pants", 2, null),
						new ProductType("Jeans", 2, null),
						new ProductType("Jackets", 3, null),
						new ProductType("Coats", 3, null),
						new ProductType("Cardigans", 3, null)
						));
				
				List<ProductType> productTypesChildWomen = new ArrayList<>(Arrays.asList(
						new ProductType("T-Shirts", 5, null),
						new ProductType("Shirts", 5, null),
						new ProductType("Blouses", 5, null),
						new ProductType("Sweaters", 5, null),
						new ProductType("Shorts", 6, null),
						new ProductType("Pants", 6, null),
						new ProductType("Jeans", 6, null),
						new ProductType("Skirts", 6, null),
						new ProductType("Sleeve Dresses", 7, null),
						new ProductType("Sleeveless Dresses", 7, null),
						new ProductType("Jackets", 8, null),
						new ProductType("Coats", 8, null),
						new ProductType("Cardigans", 8, null),
						new ProductType("Shorts", 9, null),
						new ProductType("Long", 9, null),
						new ProductType("Shorts", 10, null),
						new ProductType("Long", 10, null)
						));
				
				List<ProductType> productTypesChildGirls = new ArrayList<>(Arrays.asList(
						new ProductType("T-Shirts", 11, null),
						new ProductType("Shirts", 11, null),
						new ProductType("Blueses", 11, null),
						new ProductType("Sweaters", 11, null),
						new ProductType("Shorts", 12, null),
						new ProductType("Pants", 12, null),
						new ProductType("Jeans", 12, null),
						new ProductType("Skirts", 12, null),
						new ProductType("Sleeve Dresses", 13, null),
						new ProductType("Sleeveless Dresses", 13, null),
						new ProductType("Jackets", 14, null),
						new ProductType("Coats", 14, null),
						new ProductType("Cardigans", 14, null),
						new ProductType("Shorts", 15, null),
						new ProductType("Long", 15, null)
						));
				
				List<ProductType> productTypesChildBoys = new ArrayList<>(Arrays.asList(
						new ProductType("T-Shirts", 16, null),
						new ProductType("Shirts", 16, null),
						new ProductType("Hoodies", 16, null),
						new ProductType("Sweaters", 16, null),
						new ProductType("Shorts", 17, null),
						new ProductType("Pants", 17, null),
						new ProductType("Jeans", 17, null),
						new ProductType("Jackets", 18, null),
						new ProductType("Coats", 18, null),
						new ProductType("Cardigans", 18, null)
						));

				productTypes.addAll(productTypesChildWomen);
				productTypes.addAll(productTypesChildMen);
				productTypes.addAll(productTypesChildGirls);
				productTypes.addAll(productTypesChildBoys);
				this.productTypeRepository.saveAll(productTypes);
				
				LOG.info("~~~ Init data for product type - DONE ~~~");
			}
			
			/** Init data for product wearing purpose */
			if (this.productWearingPurposeRepository.count() == 0) {
				LOG.info("~~~ Init data for product wearing purpose ~~~");
				List<ProductWearingPurpose> productWearingPurposes = new ArrayList<>(Arrays.asList(
						new ProductWearingPurpose("Go to school", "ADULT"),
						new ProductWearingPurpose("Go to work", "ADULT"),
						new ProductWearingPurpose("Go to party", "ADULT"),
						new ProductWearingPurpose("Go traveling", "ADULT"),
						new ProductWearingPurpose("Exercise", "ADULT"),
						new ProductWearingPurpose("Casual wear", "ADULT"),
						new ProductWearingPurpose("Home wear", "ADULT"),
						
						new ProductWearingPurpose("Go to school", "KID"),
						new ProductWearingPurpose("Casual wear", "KID"),
						new ProductWearingPurpose("Exercise", "KID"),
						new ProductWearingPurpose("Homewear", "KID")));
				this.productWearingPurposeRepository.saveAll(productWearingPurposes);
				LOG.info("~~~ Init data for product wearing purpose - DONE ~~~");
			}
			
			if (this.userRepository.count() == 0) {
			/** Init data for product */
			User user = new User();
			user.setName("Super Admin");
			user.setEmail(super_admin_email);
			user.setPassword(this.bCryptPasswordEncoder.encode(super_admin_password));
			user.setPhone("0345811801");
			user.setAvatar("https://bap-fashion-shop.s3.amazonaws.com/women_1569465706650.jpg");
			user.setPlatform("WEB");
			user.setRegisterStatus(true);
			user.setUserType(UserEnum.UserTypeEnum.LOCAL.toString());

			/** Set default role **/
			Set<Role> roles = new HashSet<Role>();
			List<String> roleList = new ArrayList<>();
			roleList.add(RoleEnum.ApplicationRole.ROLE_SUPER_ADMIN.name());
			roleList.add(RoleEnum.ApplicationRole.ROLE_ADMIN.name());
			roleList.add(RoleEnum.ApplicationRole.ROLE_USER.name());
			roleList.add(RoleEnum.ApplicationRole.ROLE_SHOP.name());
			this.roleRepository.findByNameIn(roleList).forEach(e -> {
				roles.add((Role) e);
			});
			user.setRoles(roles);
			this.userRepository.save(user);
			Shop shop = new Shop();
			shop.setUser(user);
			shop.setName("Super Admin");
			shop.setEmail("smartfashion@bap.jp");
			shop.setDescription("Admin Shop");
			shop.setPhone("0345811801");
			shop.setAvatar("https://bap-fashion-shop.s3.amazonaws.com/women_1569465706650.jpg");
			shop.setShopCode("Ad1");
			shop = this.shopRepository.save(shop);
			
			List<ProductTypeCount> productTypeCountsShop = new ArrayList<>();
			List<ProductType> productTypes = this.productTypeRepository.findAll();
			for (ProductType productType : productTypes) {
				ProductTypeCount productTypeCount = new ProductTypeCount();
				productTypeCount.setProductType(productType);
				productTypeCount.setShop(shop);
				productTypeCount.setCount(0);
				productTypeCountsShop.add(productTypeCount);
			}
			this.productTypeCountRepository.saveAll(productTypeCountsShop);
			}
			
			/** Init user product collection **/
			if (this.userProductCollectionRepository.count() == 0) {
			
				List<UserProductCollection> userProductCollections = new ArrayList<>(Arrays.asList(
					new UserProductCollection("Tops"),
					new UserProductCollection("Bottoms"),
					new UserProductCollection("Outerwears"),
					new UserProductCollection("Other")));
				this.userProductCollectionRepository.saveAll(userProductCollections);
			}
		
			/** Init user model collection **/
			if (this.userModelCollectionRepository.count() == 0) {
				List<UserModelCollection> userModelCollections = new ArrayList<>();
				UserModelCollection userModelCollectionWomen = new UserModelCollection("Women");
				userModelCollections.add(userModelCollectionWomen);
				UserModelCollection userModelCollectionMen = new UserModelCollection("Men");
				userModelCollections.add(userModelCollectionMen);
				UserModelCollection userModelCollectionGirls = 	new UserModelCollection("Girls");
				userModelCollections.add(userModelCollectionGirls);
				UserModelCollection	userModelCollectionBoys = new UserModelCollection("Boys");
				userModelCollections.add(userModelCollectionBoys);
				this.userModelCollectionRepository.saveAll(userModelCollections);
				
				List<UserModel> userModels = new ArrayList<>();
				UserModel userModelWomen = new UserModel(
					null,
					"https://bap-fashion-shop.s3.amazonaws.com/default/models/model_women.jpg",
					"[(753, 171), (750, 362), (590, 342), (553, 621), (573, 866), (893, 382), (920, 665), (970, 895), (657, 812), (653, 1228), (653, 1580), (843, 822), (817, 1184), (767, 1570), (723, 137), (783, 137), (690, 166), (823, 166)]", 
					userModelCollectionWomen);
				userModels.add(userModelWomen);
				UserModel userModelMen = new UserModel(
					null,
					"https://bap-fashion-shop.s3.amazonaws.com/default/models/model_men.jpg",
					"[(700, 157), (717, 377), (557, 386), (490, 651), (513, 905), (873, 377), (923, 626), (927, 880), (623, 920), (650, 1228), (657, 1624), (817, 924), (813, 1257), (813, 1639), (673, 132), (730, 132), (633, 166), (773, 161)]",
					userModelCollectionMen);
				userModels.add(userModelMen);
				UserModel userModelGirls = new UserModel(
					null,
					"https://bap-fashion-shop.s3.amazonaws.com/default/models/model_girls.jpg",
					"[(677, 176), (700, 377), (557, 386), (510, 655), (463, 885), (850, 382), (880, 655), (860, 885), (587, 871), (613, 1247), (633, 1624), (777, 905), (807, 1277), (827, 1648), (640, 147), (710, 137), (607, 191), (753, 157)]",
					userModelCollectionGirls);
				userModels.add(userModelGirls);
				UserModel userModelBoys = new UserModel(
					null,
					"https://bap-fashion-shop.s3.amazonaws.com/default/models/model_boys.jpg",
					"[(723, 201), (713, 426), (553, 435), (500, 680), (480, 924), (883, 416), (950, 670), (980, 910), (623, 910), (607, 1272), (607, 1624), (827, 934), (823, 1257), (850, 1609), (683, 166), (760, 161), (623, 201), (810, 201)]",
					userModelCollectionBoys);
				userModels.add(userModelBoys);
				this.userModelRepository.saveAll(userModels);
			}
		}
	}
}
