package bap.jp.smartfashion.aop;

import java.io.IOException;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import bap.jp.smartfashion.common.response.APIResponseError;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.exception.SmartFashionException;
import bap.jp.smartfashion.exception.ConflictException;
import bap.jp.smartfashion.util.SmartFashionUtils;
import bap.jp.smartfashion.util.constants.APIConstants;
import bap.jp.smartfashion.util.constants.Constants;

/**
 * SmartFashion ExceptionHandler
 *
 */
@ControllerAdvice
public class SmartFashionExceptionHandler {

	private final Logger log = LoggerFactory.getLogger(LoggingAspect.class);
	
	private static final String ERROR_UNKOWN = "ERROR_UNKOWN";

	/**
	 * Handle {@link AuthenticationException}
	 * 
	 * @param e {@link AuthenticationException}
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {AuthenticationException.class})
	protected ResponseEntity<APIResponseError> handleAuthenticationException(AuthenticationException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.UNAUTHORIZED.value())
			.error(e.getError())
			.message(e.getMessage())
			.build();
		log.warn(Constants.LOG_LEVEL_WARN + ":" + e.getMessage());
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.UNAUTHORIZED);
	}
	
	/**
	 * Handle {@link ConflictException}
	 * 
	 * @param e {@link ConflictException}
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {ConflictException.class})
	protected ResponseEntity<APIResponseError> handleConflictException(ConflictException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.CONFLICT.value())
			.error(e.getError())
			.message(e.getMessage())
			.build();
		log.warn(Constants.LOG_LEVEL_WARN + ":" + e.getMessage());
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.CONFLICT);
	}
	
	/**
	 * Handle {@link NotFoundException}
	 * 
	 * @param e {@link NotFoundException}
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {NotFoundException.class})
	protected ResponseEntity<APIResponseError> handleNotFoundException(NotFoundException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
				.code(HttpStatus.NOT_FOUND.value())
				.error(e.getError())
				.message(e.getMessage())
				.build();
			log.warn(Constants.LOG_LEVEL_WARN + ":" + e.getMessage());
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.NOT_FOUND);
	}
	
	/**
	 * Handle {@link EntityNotFoundException}
	 * 
	 * @param e {@link EntityNotFoundException}
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {EntityNotFoundException.class})
	protected ResponseEntity<APIResponseError> handleEntityNotFoundException(EntityNotFoundException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.NOT_FOUND.value())
			.error("ERROR_ENTITY_NOT_FOUND")
			.message(e.getMessage())
			.build();
		log.warn(Constants.LOG_LEVEL_WARN + ":" + e.getMessage());
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.NOT_FOUND);
	}
	

	/**
	 * Handle {@link JsonMappingException}
	 * 
	 * @param e {@link JsonMappingException}
	 * @throws BadRequestException 
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {JsonMappingException.class})
	protected  ResponseEntity<APIResponseError> handleJsonMappingException(JsonMappingException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.BAD_REQUEST.value())
			.error("ERROR_JSON_MAPPING")
			.message(e.getMessage())
			.build();
		
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handle {@link JsonMappingException}
	 * 
	 * @param e {@link JsonMappingException}
	 * @throws BadRequestException 
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {InvalidFormatException.class})
	protected  ResponseEntity<APIResponseError> handleInvalidFormatException(InvalidFormatException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.BAD_REQUEST.value())
			.error("ERROR_INVALID_FORMAT")
			.message(e.getMessage())
			.build();
		
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Handle {@link MethodArgumentTypeMismatchException}
	 * 
	 * @param e {@link MethodArgumentTypeMismatchException}
	 * @throws BadRequestException 
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
	protected  ResponseEntity<APIResponseError> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.BAD_REQUEST.value())
			.error(HttpStatus.BAD_REQUEST.name())
			.message(e.getMessage())
			.build();
		
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
	}
	/**
	 * Handler unknown exception.
	 *
	 * @param e exception
	 * @return error response
	 */
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<APIResponseError> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
		log.error(e.getMessage(), e);
		Throwable nextException = e.getCause();
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.BAD_REQUEST.value())
			.error(HttpStatus.BAD_REQUEST.name())
			.message(!StringUtils.isEmpty(nextException.getCause().getMessage()) ? nextException.getCause().getMessage() : "Unknown error")
			.build();
		
		
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Handle {@link BadRequestException}
	 * 
	 * @param e {@link BadRequestException}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws JSONException
	 * @returm {@link ResponseEntity} of {@link APIResponse}
	 */
	@ExceptionHandler(value = BadRequestException.class)
	protected ResponseEntity<APIResponseError> handleBadRequestException(BadRequestException e)
			throws JsonParseException, JsonMappingException, IOException, JSONException {
		log.error(e.getMessage(), e);
		Object message = e.getMessage();
		if (e.isJson()) {
			ObjectMapper mapper = new ObjectMapper();
			message = mapper.readValue(e.getMessage(), Object.class);
		}
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.BAD_REQUEST.value())
			.error(e.getError())
			.message(message)
			.build();
		log.warn(Constants.LOG_LEVEL_WARN + ":" + e.getMessage());
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handler unknown exception.
	 *
	 * @param e exception
	 * @return error response
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<APIResponseError> handleException(Exception e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.BAD_REQUEST.value())
			.error(ERROR_UNKOWN)
			.message(!StringUtils.isEmpty(e.getMessage()) ? e.getMessage() : "Unknown error")
			.build();
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
	}
	

	@ExceptionHandler(SmartFashionException.class)
	public ResponseEntity<APIResponseError> handleSmartFashionException(SmartFashionException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(e.getCode())
			.error(e.getError())
			.message(!StringUtils.isEmpty(e.getMessage()) ? e.getMessage() : "Unknown error")
			.build();
		return new ResponseEntity<APIResponseError>(apiResponseError, e.getHttpStatus());
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<APIResponseError> handleAccessDeniedException(AccessDeniedException e) {
		log.error(e.getMessage(), e);
		APIResponseError apiResponseError = APIResponseError.builder()
			.code(HttpStatus.UNAUTHORIZED.value())
			.error(HttpStatus.UNAUTHORIZED.name())
			.message(!StringUtils.isEmpty(e.getMessage()) ? e.getMessage() : "Unknown error")
			.build();
		return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.UNAUTHORIZED);
	}
}
