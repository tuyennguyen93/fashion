package bap.jp.smartfashion.config;

import org.springframework.cloud.aws.mail.simplemail.SimpleEmailServiceJavaMailSender;
import org.springframework.cloud.aws.mail.simplemail.SimpleEmailServiceMailSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;

/**
 * The class for email configuration.
 * 
 */
@Configuration
public class EmailServiceConfig {
	
	@Bean
	public AmazonSimpleEmailService amazonSimpleEmailService(AWSCredentialsProvider credentialsProvider) {

		return AmazonSimpleEmailServiceClientBuilder.standard().withCredentials(credentialsProvider).withRegion(Regions.US_WEST_2).build();
	}

	@Bean
	public MailSender mailSender(AmazonSimpleEmailService ses) {
		return new SimpleEmailServiceMailSender(ses);
	}

	@Bean
	public JavaMailSender javaMailSender(AmazonSimpleEmailService amazonSimpleEmailService) {
		return new SimpleEmailServiceJavaMailSender(amazonSimpleEmailService);
	}
}
