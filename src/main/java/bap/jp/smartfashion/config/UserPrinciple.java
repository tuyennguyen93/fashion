package bap.jp.smartfashion.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;

import bap.jp.smartfashion.common.model.User;

/**
 *  The class for user principle
 *
 */
public class UserPrinciple implements UserDetails {
	
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;

	private String email;

	@JsonIgnore
	private String password;
	
	private String langKey;

	private Set<GrantedAuthority> authorities = new HashSet<>();

	public UserPrinciple(User user, Set<GrantedAuthority> authorities) {
		this.id = user.getId();
		this.name = user.getName();
		this.email = user.getEmail();
		this.password = user.getPassword();
		this.authorities = authorities;
		this.langKey = user.getLangKey();
	}

	public static UserPrinciple build(User user, Set<GrantedAuthority> grantedAuthorities) {
		System.out.println("User ID:" + user.getId() + " has roles: "+ grantedAuthorities.toString());
		return new UserPrinciple(user, grantedAuthorities);
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getLangKey() {
		return langKey;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Set<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}
}
