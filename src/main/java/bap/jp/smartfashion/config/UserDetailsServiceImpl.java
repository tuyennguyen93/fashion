package bap.jp.smartfashion.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import bap.jp.smartfashion.api.authentication.AuthenticationService;
import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.model.GroupAuthorityDetail;
import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.common.model.UserAuthority;
import bap.jp.smartfashion.common.repository.GroupAuthorityDetailRepository;
import bap.jp.smartfashion.config.jwt.TokenStore;
import bap.jp.smartfashion.enums.RoleEnum;
import bap.jp.smartfashion.exception.AuthenticationException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.Constants;
import bap.jp.smartfashion.util.constants.GroupAuthorityConstants;

/**
 *  The class for user detail service implement
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private GroupAuthorityDetailRepository groupAuthorityDetailRepository;

	@Autowired
	private TokenStore tokenStore;
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	@Autowired
	private BaseService baseService;
	
	
	@Override
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		
		User user = this.authenticationService.findUserByUserId(Integer.parseInt(userId)).orElseThrow(() -> 
				new UsernameNotFoundException("User not found with id : " + userId));
		
		Set<GrantedAuthority> grantedAuthority = new HashSet<>();
		List<GroupAuthorityDetail> groupAuthorityDetailList = this.groupAuthorityDetailRepository.findByObjectIdAndGroupType(user.getId(), GroupAuthorityConstants.GROUP_TYPE_USER);
		for (GroupAuthorityDetail groupAuthorityDetail : groupAuthorityDetailList) {
			String groupAuthorityName = Constants.ROLE_PREFIX + groupAuthorityDetail.getFunctionName() + Constants.CHAR_UNDERSCORE + groupAuthorityDetail.getActionName();
			grantedAuthority.add(new SimpleGrantedAuthority(groupAuthorityName));
		}

		for (UserAuthority userAuthority : user.getUserAuthorities()) {
			String groupAuthorityName = Constants.ROLE_PREFIX + userAuthority.getFunctionName() + Constants.CHAR_UNDERSCORE + userAuthority.getActionName();
			grantedAuthority.add(new SimpleGrantedAuthority(groupAuthorityName));
		}

		// set application role
		grantedAuthority.add(new SimpleGrantedAuthority(RoleEnum.ApplicationRole.ROLE_ANONYMOUS.name()));
		user.getRoles().forEach(role -> {
			grantedAuthority.add(new SimpleGrantedAuthority(role.getName()));
		});
		if (user.isDeleted()) {
			String unameKey = TokenStore.UNAME_ACCESS_KEY + user.getId();
			String accessKey_old = this.redisTemplate.opsForValue().get(unameKey);
			this.tokenStore.revokingJwtToken(accessKey_old, unameKey);
			throw new UsernameNotFoundException("User not found");
		}
		try {
			this.baseService.checkShopBlockingLogin(user.getId());
		} catch (AuthenticationException | NotFoundException e) {
			throw new UsernameNotFoundException(e.getMessage(), e);
		}
		return UserPrinciple.build(user, grantedAuthority);
	}

}
