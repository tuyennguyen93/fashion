package bap.jp.smartfashion.config.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import bap.jp.smartfashion.common.base.BaseService;
import bap.jp.smartfashion.common.model.User;
/**
 * TokenStore Handle JWT
 *
 */
@Component
public class TokenStore {
	public static final String UNAME_ACCESS_KEY = "uname_access_key:";
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	/**
	 * Store JWT Token 
	 * @param accessToken JWT Token
	 * @param userName Info of {@link User}
	 */
	public void storeJwtToken(AccessToken accessToken, String userName) {
		// delete jwt old
		String unameKey = UNAME_ACCESS_KEY + userName;
		String accessKey_old = this.redisTemplate.opsForValue().get(unameKey);
	//	this.revokingJwtToken(accessKey_old, unameKey);
		
		this.redisTemplate.opsForValue().set(accessToken.getToken(), accessToken.getToken());
		this.redisTemplate.expireAt(accessToken.getToken(), accessToken.getExpried());
		//tracking jwt each user
		this.redisTemplate.opsForValue().set(unameKey, accessToken.getToken());
		this.redisTemplate.expireAt(unameKey, accessToken.getExpried());
	}
	
	/**
	 * Revoking JWT Token 
	 * @param accessToken JWT Token
	 * @param unameKey JWT key used
	 */
	public void revokingJwtToken(String accessToken, String unameKey) {
		
		this.redisTemplate.delete(accessToken);
		this.redisTemplate.delete(unameKey); // delete token
	}
	
	/**
	 * Read JWT Token 
	 * @param accessToken JWT Token
	 * 
	 * @return JWT Token
	 */
	public String readJwtToken(String accessToken) {
		return this.redisTemplate.opsForValue().get(accessToken);
	}
}
