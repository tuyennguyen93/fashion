package bap.jp.smartfashion.config.jwt;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import bap.jp.smartfashion.common.model.User;
import bap.jp.smartfashion.enums.TokenEnum;

import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.ServletException;

@Component
public class JwtProvider {

	private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);
	
	@Value("${app.jwt.secret}")
	private String jwtSecret;
	
	@Autowired
	private TokenStore tokenStore;
/*
	public String generateToken(String userName, boolean rememberMe) {

		long now = (new Date()).getTime();
		long dateToMilliseconds = 24*60*60*1000;
		Date validity;
		if (rememberMe) {
			validity = new Date(now + TokenEnum.TOKEN_REMEMBER_ME_EXPIRED.getValue() * dateToMilliseconds);
		} else {
			validity = new Date(now + TokenEnum.TOKEN_JWT_EXPIRED.getValue() * dateToMilliseconds);
		}
		// create jwt from username(email and phone)
		return Jwts.builder().setSubject(userName).setIssuedAt(new Date(now)).setExpiration(validity)
				.signWith(SignatureAlgorithm.HS512, this.jwtSecret).compact();
	}
*/
	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(this.jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) throws ServletException {
		try {
			authToken = this.tokenStore.readJwtToken(authToken);
			if (authToken == null) {
				throw new ServletException("Expired JWT token");
			}
			Jwts.parser().setSigningKey(this.jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature -> message: {} ", e);
			e.printStackTrace();
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token -> message: {}", e);
			e.printStackTrace();
		} catch (ExpiredJwtException e) {
			logger.error("Expired JWT token -> message: {}", e);
			e.printStackTrace();
		} catch (UnsupportedJwtException e) {
			logger.error("Unsupported JWT token -> message: {}", e);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty -> message: {}", e);
			e.printStackTrace();
		}

		return false;
	}
	
	/**
	 * Creates access token.
	 *
	 * @param userName {@link User}
	 * @param rememberMe if application is remember me
	 * @return {@link AccessToken}
	 */
	
	public AccessToken createAccessToken(String userName, boolean rememberMe) {
		long now = (new Date()).getTime();
		long dateToMilliseconds = 24*60*60*1000;
		Date validity;
		if (rememberMe) {
			validity = new Date(now + TokenEnum.TOKEN_REMEMBER_ME_EXPIRED.getValue() * dateToMilliseconds);
		} else {
			validity = new Date(now + TokenEnum.TOKEN_JWT_EXPIRED.getValue() * dateToMilliseconds);
		}
		
		String jwt = Jwts.builder().setSubject(userName).setExpiration(validity).signWith(SignatureAlgorithm.HS512, this.jwtSecret).compact();
		AccessToken accessToken = new AccessToken();
		accessToken.setToken(jwt);
		accessToken.setExpried(validity);
		
		this.tokenStore.storeJwtToken(accessToken, userName);
		return accessToken;
	}
}
