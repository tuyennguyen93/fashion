package bap.jp.smartfashion.config.jwt;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessToken {
	private String token;
	private Date expried;
}
