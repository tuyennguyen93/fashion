package bap.jp.smartfashion.util;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.util.constants.Constants;
/**
 * Random Utils
 *
 */
public class RandomUtils {

	public static final int DEFAULT_PASSWORD_LENGTH = 8;
	public static final int DEFAULT_VERIFY_CODE_LENGTH = 6;
	private static final RandomStringGenerator RandomAlphaNumericGenerator = new RandomStringGenerator.Builder()
			.withinRange('0', 'Z').filteredBy(CharacterPredicates.DIGITS, CharacterPredicates.LETTERS).build();

	/**
	 * Generate random password
	 * @return Password random from 0-Z with 8 characters
	 */
	public static String generatePassword() {
		return RandomAlphaNumericGenerator.generate(DEFAULT_PASSWORD_LENGTH);
	}
	
	/**
	 * Genereate token type {@link UUID}
	 * @return Toke type {@link UUID}
	 */
	public static String generateToken() {
		return UUID.randomUUID().toString();
	}
	
	/**
	 * Generate verify code on mobile
	 * @return Verify code for mobile
	 */
	public static String generateVerifycode() {
		return RandomAlphaNumericGenerator.generate(DEFAULT_VERIFY_CODE_LENGTH);
	}
}
