package bap.jp.smartfashion.util;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.Shop;
import bap.jp.smartfashion.common.response.APIResponseError;
import bap.jp.smartfashion.common.vo.Message;
import bap.jp.smartfashion.common.vo.PageInfo;
import bap.jp.smartfashion.exception.BadRequestException;
import bap.jp.smartfashion.exception.NotFoundException;
import bap.jp.smartfashion.util.constants.APIConstants;
import bap.jp.smartfashion.util.constants.Constants;

/**
 * SmartFashion Utils
 *
 */
public class SmartFashionUtils {
	@Value("${app.paging.default.limit}")
	protected int pagingDefaultLimit;

	@Value("${app.paging.default.pageInit}")
	protected int pagingDefaultPageInit;

	@Value("${app.paging.default.offset}")
	protected int pagingDefaultOffset;

	/**
	 * Get JWT Token
	 * 
	 * @param jwtToken JWT Token has token type
	 * @return jwt token HS512
	 */
	public static String getJwtToken(String jwtToken) {
		if (jwtToken != null && jwtToken.startsWith(Constants.JWT_TOKEN_TYPE)) {
			return jwtToken.replace(Constants.JWT_TOKEN_TYPE, "");
		}
		return null;
	}

	public static boolean isEmail(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}

	public static JSONObject getMessageListFromErrorsValidation(Errors errors) {
		JSONObject jsonObject = new JSONObject();
		try {
			for (ObjectError error : errors.getAllErrors()) {
				String fieldName = ((FieldError) error).getField();
				JSONArray jsonArray = new JSONArray();
				jsonArray.put(error.getDefaultMessage());
				jsonObject.put(fieldName, jsonArray);
			
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	/**
	 * Create paging with basic information
	 * 
	 * @param <T>
	 * @param page {@link Page} of T
	 * @return {@link PageInfo}
	 */
	public static <T> PageInfo<T> pagingResponse(Page<T> page) {
		// page info
		PageInfo<T> pageInfo = new PageInfo<T>();
		pageInfo.setTotal(page.getTotalElements());
		pageInfo.setLimit(page.getSize());
		pageInfo.setPages(page.getTotalPages());
		pageInfo.setPage(page.getNumber() + 1);
		pageInfo.setResult(page.getContent());

		return pageInfo;
	}

	/**
	 * Check string is uuid format
	 * 
	 * @param uuid
	 * @return true if string is uuid format, otherwise
	 */
	public static boolean isUUIDFormat(String uuid) {
		return Pattern.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}", uuid);
	}

	/**
	 * Check format of token
	 * @param token
	 * @throws BadRequestException
	 */
	public static void checkFormatToken(String token) throws BadRequestException {
		if (token.length() != 6) {
			if (!isUUIDFormat(token)) {
				throw new BadRequestException(APIConstants.TOKEN_INVALID_MSG);
			}
		}
	}

	/**
	 * Build {@link APIResponse} Error
	 * 
	 * @param <T>
	 * @param data
	 * @param statusCode
	 * @return {@link APIResponse}
	 */
	public static APIResponseError buildAPIResponseError(int statusCode, Object messages) {

		APIResponseError apiResponse = new APIResponseError();
		apiResponse.setTimestamp(new Date().getTime());
		apiResponse.setCode(statusCode);
		apiResponse.setMessage(messages);
		apiResponse.setError(StringUtils.upperCase(APIConstants.ERROR_MSG));

		return apiResponse;
	}

	/**
	 * Distinc by key function
	 * 
	 * @param <T>
	 * @param keyExtractor
	 * @return
	 */
	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {

		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
	
	/**
	 * Generate {@link Shop} code
	 * @param shopName Shop Name
	 * @param shopCount Number product belong to shop
	 * @return Shop code
	 */
	public static String generateShopCode(String shopName, String shopCount) {
		String shopCode = StringUtils.upperCase(shopName.substring(0, 2)) + shopCount;
		shopCode = unAccentCharacters(shopCode);
		return shopCode;
	}
	
	/**
	 * Unaccent Characters (Use convert Vietnamse to Unicode)
	 * @param s String to convert
	 * @return new String converted
	 */
	public static String unAccentCharacters(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		temp = temp.replaceAll("đ", "D");
		temp = temp.replaceAll("Đ", "D");
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("");
	}
	
	/**
	 * Generate {@link Product} code 
	 * @param shopCode {@link Shop} code
	 * @param productIndex Index of {@link Product} in database by {@link Shop}
	 * @return
	 */
	public static String generateProductCode(String shopCode, int productIndex) {
		String productCode = String.valueOf(productIndex);
		productCode = StringUtils.leftPad(productCode, 5, "0");
		String newCode = shopCode + Constants.CHAR_HYPHEN + productCode;
		
		return newCode;
	}
	
	/**
	 * Generate product code
	 * 
	 * @param product {@link Product}
	 * @return product code
	 */
	public static String generateProductCode(Shop shop, Product product) {
		String productCode = StringUtils.EMPTY;
		int productIndex = Integer.valueOf(product.getProductCode());
		String productCollectionCode = StringUtils.substring(product.getProductCollection().getName(), 0, 1);
		String newShopCode = shop.getShopCode() + StringUtils.upperCase(productCollectionCode);
		productCode = generateProductCode(newShopCode, productIndex);
		return productCode;
	}
	
	/**
	 * Replace new collection shop code
	 * @param productCode Product code
	 * @param shopCode Shop code
	 * @param collectionName product collection mae
	 * @return new product code
	 */
	public static String replaceNewCollectionShopCode(String productCode, String shopCode, String collectionName) {
		String[] productCodeArr = StringUtils.split(productCode, Constants.CHAR_HYPHEN);
		String productCollectionCode = StringUtils.substring(collectionName, 0, 1);
		return shopCode + StringUtils.upperCase(productCollectionCode) + Constants.CHAR_HYPHEN + productCodeArr[1];
	}
}
