package bap.jp.smartfashion.util.constants;
/**
 * Constants
 */
public class Constants {

	/**
	 * Common
	 */
	public static final String ROLE_PREFIX = "ROLE_";
	public static final String JWT_TOKEN_TYPE = "Bearer ";
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String SLASH = "/";
	public static final char CHAR_COLON = ':';
	public static final char CHAR_COMMA = ',';
	public static final char CHAR_UNDERSCORE = '_';
	public static final char CHAR_HYPHEN = '-';
	
	/**
	 *  Email
	 */
	public static final String EMAIL_VERIFY_USER_TITLE = "Xác thực đăng ký tài khoản";
	public static final String EMAIL_RESET_PASSWORD_TITLE = "Khôi phục mật khẩu";
	public static final String EMAIL_VERIFY_SHOP_USER_TITLE = "Xác thực cộng tác viên";

	public static final String EMAIL_NOTICE_PASSWORD_CHANGED_TITLE = "Mật khẩu đã được thay đổi";

	/**
	 *  Token
	 */
	public static final String TOKEN_IS_READY = "Token is ready";

	/**
	 * LOG LEVEL
	 */
	public static final String LOG_LEVEL_DEBUG = "DEBUG";
	public static final String LOG_LEVEL_INFO = "INFO";
	public static final String LOG_LEVEL_WARN = "WARN";
	public static final String LOG_LEVEL_ERROR = "ERROR";
	public static final String LOG_LEVEL_TRACE = "TRACE";
}
