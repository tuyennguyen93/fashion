package bap.jp.smartfashion.util.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bap.jp.smartfashion.api.authorization.vo.AccessControlListVO;
import bap.jp.smartfashion.api.authorization.vo.AuthorityActionVO;
import bap.jp.smartfashion.enums.RoleEnum;

/**
 * The class to create access control list
 *
 */
public class AccessControlListConstants {

	public static AccessControlListVO actionForUserGroup() {
		AccessControlListVO accessControlListVO = new AccessControlListVO();
		List<AuthorityActionVO> actionVOs = new ArrayList<AuthorityActionVO>();
		actionVOs.add(new AuthorityActionVO(
				RoleEnum.FunctionName.FUNCTION_USER.getCode(),
			Arrays.asList(new String[] {
				RoleEnum.AuthorityAction.ACTION_VIEW.getCode()
			})));

		actionVOs.add(new AuthorityActionVO(
			RoleEnum.FunctionName.FUNCTION_SHOP.getCode(),
			Arrays.asList(
				new String[] {
					RoleEnum.AuthorityAction.ACTION_VIEW.getCode(),
					RoleEnum.AuthorityAction.ACTION_CREATE.getCode(),
					RoleEnum.AuthorityAction.ACTION_EDIT.getCode(),
					RoleEnum.AuthorityAction.ACTION_DELETE.getCode(),
					RoleEnum.AuthorityAction.ACTION_ALL.getCode(),
				})));
		
		actionVOs.add(new AuthorityActionVO(
				RoleEnum.FunctionName.FUNCTION_PRODUCT.getCode(),
				Arrays.asList(
					new String[] {
						RoleEnum.AuthorityAction.ACTION_VIEW.getCode()
					})));
			
		actionVOs.add(new AuthorityActionVO(
			RoleEnum.FunctionName.FUNCTION_CUSTOMER_CARE.getCode(),
			Arrays.asList(
				new String[] {
					RoleEnum.AuthorityAction.ACTION_VIEW.getCode(),
					RoleEnum.AuthorityAction.ACTION_CREATE.getCode(),
					RoleEnum.AuthorityAction.ACTION_EDIT.getCode(),
					RoleEnum.AuthorityAction.ACTION_DELETE.getCode(),
					RoleEnum.AuthorityAction.ACTION_ALL.getCode(),
			})));
		
		accessControlListVO.setGroupType(GroupAuthorityConstants.GROUP_TYPE_USER);
		accessControlListVO.setAuthorityActionVOs(actionVOs);
		
		return accessControlListVO;
	}
	
	public static AccessControlListVO actionForShopGroup() {
		AccessControlListVO accessControlListVO = new AccessControlListVO();
		List<AuthorityActionVO> actionVOs = new ArrayList<AuthorityActionVO>();
		
		actionVOs.add(new AuthorityActionVO(
			RoleEnum.FunctionName.FUNCTION_USER.getCode(),
			Arrays.asList(new String[] {
				RoleEnum.AuthorityAction.ACTION_VIEW.getCode()
			})));
			
		
		actionVOs.add(new AuthorityActionVO(
			RoleEnum.FunctionName.FUNCTION_SHOP.getCode(),
			Arrays.asList(new String[] {
				RoleEnum.AuthorityAction.ACTION_VIEW.getCode()
			})));
		
		actionVOs.add(new AuthorityActionVO(
			RoleEnum.FunctionName.FUNCTION_PRODUCT.getCode(),
			Arrays.asList(
				new String[] {
					RoleEnum.AuthorityAction.ACTION_VIEW.getCode(),
					RoleEnum.AuthorityAction.ACTION_CREATE.getCode(),
					RoleEnum.AuthorityAction.ACTION_EDIT.getCode(),
					RoleEnum.AuthorityAction.ACTION_DELETE.getCode(),
					RoleEnum.AuthorityAction.ACTION_ALL.getCode(),
				})));
		
		actionVOs.add(new AuthorityActionVO(
				RoleEnum.FunctionName.FUNCTION_CUSTOMER_CARE.getCode(),
			Arrays.asList(
				new String[] {
					RoleEnum.AuthorityAction.ACTION_VIEW.getCode(),
					RoleEnum.AuthorityAction.ACTION_CREATE.getCode(),
					RoleEnum.AuthorityAction.ACTION_EDIT.getCode(),
					RoleEnum.AuthorityAction.ACTION_DELETE.getCode(),
					RoleEnum.AuthorityAction.ACTION_ALL.getCode(),
			})));
		
		accessControlListVO.setGroupType(GroupAuthorityConstants.GROUP_TYPE_USER);
		accessControlListVO.setAuthorityActionVOs(actionVOs);
		
		return accessControlListVO;
	}

}
