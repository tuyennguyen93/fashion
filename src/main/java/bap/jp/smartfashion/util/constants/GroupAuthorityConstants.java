package bap.jp.smartfashion.util.constants;

import bap.jp.smartfashion.enums.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class GroupAuthorityConstants {

	public static final String GROUP_TYPE_USER = "USER";
	public static final String GROUP_TYPE_SHOP = "SHOP";
	
	public static final String GET_AUTHOR_GROUP_USER 		= "GET_AUTHOR_GROUP_USER"; 
	public static final String GET_AUTHOR_GROUP_SHOP 		= "GET_AUTHOR_GROUP_SHOP";
	public static final String LIST_AUTHOR_GROUP_USER	 	= "LIST_AUTHOR_GROUP_USER"; 
	public static final String LIST_AUTHOR_GROUP_SHOP		= "LIST_AUTHOR_GROUP_SHOP";
	
	@Getter
	@AllArgsConstructor
	public enum AuthorityGroupAction implements CodeEnum {
		GROUP_TYPE_USER			(GroupAuthorityConstants.GROUP_TYPE_USER, "GROUP_TYPE_USER"),
		GROUP_TYPE_SHOP			(GroupAuthorityConstants.GROUP_TYPE_SHOP, "GROUP_TYPE_SHOP"),
		GET_AUTHOR_GROUP_USER	(GroupAuthorityConstants.GET_AUTHOR_GROUP_USER, "GET_AUTHOR_GROUP_USER"),
		GET_AUTHOR_GROUP_SHOP	(GroupAuthorityConstants.GET_AUTHOR_GROUP_SHOP, "GET_AUTHOR_GROUP_SHOP"),
		LIST_AUTHOR_GROUP_USER	(GroupAuthorityConstants.LIST_AUTHOR_GROUP_USER, "LIST_AUTHOR_GROUP_USER"),
		LIST_AUTHOR_GROUP_SHOP	(GroupAuthorityConstants.LIST_AUTHOR_GROUP_SHOP, "LIST_AUTHOR_GROUP_SHOP");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum AuthorityGroupType implements CodeEnum {
		USER	(GroupAuthorityConstants.GROUP_TYPE_USER, "GROUP_TYPE_USER"),
		SHOP	(GroupAuthorityConstants.GROUP_TYPE_SHOP, "GROUP_TYPE_SHOP");
		
		private final String code;
		private final String display;
	}
}
