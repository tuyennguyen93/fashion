package bap.jp.smartfashion.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import bap.jp.smartfashion.enums.CodeEnum;

/**
 * Enum Utils
 *
 */
public final class CodeEnumUtils {

	private CodeEnumUtils() {
    }

	public static <T extends CodeEnum> List<T> toList(Class<T> clazz) {
		T[] enums = clazz.getEnumConstants();
		List<T> result = new ArrayList<T>(enums.length);

		for (T t : enums) {
			result.add(t);
		}

		return result;
	}

	public static <T extends CodeEnum> T fromCode(Class<T> clazz, String code) {

		if (StringUtils.isEmpty(code)) {
			return null;
		}

		T[] values = clazz.getEnumConstants();
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (StringUtils.equals(t.getCode(), code)) {
				return t;
			}
		}
		throw new IllegalArgumentException("IllegalArgumentException:" + code + " " + clazz.getCanonicalName());
	}

	public static <T extends CodeEnum> boolean hasCode(Class<T> clazz, String code) {
		T[] values = clazz.getEnumConstants();
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (StringUtils.equals(t.getCode(), code)) {
				return true;
			}
		}
		return false;
	}

	public static <T extends CodeEnum> T fromCodeQuietly(Class<T> clazz, String code) {

		if (StringUtils.isEmpty(code)) {
			return null;
		}

		T[] values = clazz.getEnumConstants();
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (StringUtils.equals(t.getCode(), code)) {
				return t;
			}
		}
		return null;
	}

	public static <T extends CodeEnum> T fromDisplay(Class<T> clazz, String display) {

		if (StringUtils.isEmpty(display)) {
			return null;
		}

		T[] values = clazz.getEnumConstants();
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (StringUtils.equals(t.getDisplay(), display)) {
				return t;
			}
		}
		throw new IllegalArgumentException("IllegalArgumentException:" + display + " " + clazz.getCanonicalName());
	}

	public static <T extends CodeEnum> boolean hasDisplay(Class<T> clazz, String display) {
		T[] values = clazz.getEnumConstants();
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (StringUtils.equals(t.getDisplay(), display)) {
				return true;
			}
		}
		return false;
	}

	public static <T extends CodeEnum> T fromDisplayQuietly(Class<T> clazz, String display) {

		if (StringUtils.isEmpty(display)) {
			return null;
		}

		T[] values = clazz.getEnumConstants();
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (StringUtils.equals(t.getDisplay(), display)) {
				return t;
			}
		}
		return null;
	}

	public static <T extends CodeEnum> boolean isCodeEnum(Class<T> clazz, String code) {
		if (StringUtils.isEmpty(code)) {
			return false;
		}

		boolean ret = false;
		T[] codeEnums = clazz.getEnumConstants();
		for (CodeEnum codeEnum : codeEnums) {
			if (code.equals(codeEnum.getCode())) {
				ret = true;
				break;
			}
		}
		return ret;
	}

	public static <T extends CodeEnum> boolean isValidEnum(Class<T> clazz, String code) {
		if (StringUtils.isEmpty(code)) {
			return true;
		}
		return isCodeEnum(clazz, code);
	}

	public static <T> List<T> valueList(Class<T> clazz) {
		T[] codeEnums = clazz.getEnumConstants();
		List<T> resultList = new ArrayList<T>();
		for (T codeEnum : codeEnums) {
			resultList.add(codeEnum);
		}
		return resultList;
	}

	public static <T extends CodeEnum> boolean equals(T t, String code) {
		if (t == null || StringUtils.isEmpty(code)) {
			return false;
		}
		return StringUtils.equals(t.getCode(), code);
	}

	public static <T extends CodeEnum> boolean contains(List<T> list, String code) {
		if (list == null || StringUtils.isEmpty(code)) {
			return false;
		}
		for (T t : list) {
			if (StringUtils.equals(t.getCode(), code)) {
				return true;
			}
		}
		return false;
	}
}
