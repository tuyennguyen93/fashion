package bap.jp.smartfashion.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
/**
 * Badrequest Exception
 *
 */
@Getter
@Setter
public class BadRequestException extends Exception {

	public static final String ERROR_CREATE_USER_BAD_REQUEST = "ERROR_CREATE_USER_BAD_REQUEST";
	public static final String ERROR_UPDATE_USER_BAD_REQUEST = "ERROR_UPDATE_USER_BAD_REQUEST";
	public static final String ERROR_USER_PLATFORM_BAD_REQUEST = "ERROR_USER_PLATFORM_BAD_REQUEST";
	public static final String ERROR_PRODUCT_DETAIL_BAD_REQUEST = "ERROR_PRODUCT_DETAIL_BAD_REQUEST";
	public static final String ERROR_PRODUCT_TYPE_BAD_REQUEST = "ERROR_PRODUCT_TYPE_BAD_REQUEST";
	public static final String ERROR_PRODUCT_SIZE_BAD_REQUEST = "ERROR_PRODUCT_SIZE_BAD_REQUEST";
	public static final String ERROR_PRODUCT_COLOR_BAD_REQUEST = "ERROR_PRODUCT_COLOR_BAD_REQUEST";
	public static final String ERROR_PRODUCT_SUB_DETAIL_BAD_REQUEST = "ERROR_PRODUCT_SUB_DETAIL_BAD_REQUEST";
	public static final String ERROR_SIGN_IN_BAD_REQUEST = "ERROR_SIGN_IN_BAD_REQUEST";
	public static final String ERROR_SIGN_UP_BAD_REQUEST = "ERROR_SIGN_UP_BAD_REQUEST";
	public static final String ERROR_SIGN_IN_SNS_BAD_REQUEST = "ERROR_SIGN_IN_SNS_BAD_REQUEST";
	public static final String ERROR_SHOP_UPDATE_BAD_REQUEST = "ERROR_SHOP_UPDATE_BAD_REQUEST";
	public static final String ERROR_SHOP_CREATE_BAD_REQUEST = "ERROR_SHOP_CREATE_BAD_REQUEST";
	public static final String ERROR_SHOP_STATUS_BAD_REQUEST = "ERROR_SHOP_STATUS_BAD_REQUEST";
	public static final String ERROR_USER_STATUS_BAD_REQUEST = "ERROR_USER_STATUS_BAD_REQUEST";
	public static final String ERROR_USER_CHANGE_PASSWORD_BAD_REQUEST = "ERROR_USER_CHANGE_PASSWORD_BAD_REQUEST";
	public static final String ERROR_USER_RESET_PASSWORD_BAD_REQUEST = "ERROR_USER_RESET_PASSWORD_BAD_REQUEST";
	public static final String ERROR_USER_REACTIVE_BAD_REQUEST = "ERROR_USER_REACTIVE_BAD_REQUEST";
	public static final String ERROR_SORT_RANDOM_DATA_BAD_REQUEST = "ERROR_SORT_RANDOM_DATA_BAD_REQUEST";
	public static final String ERROR_INVALID_LANG_KEY = "ERROR_INVALID_LANG_KEY";
	public static final String ERROR_EMPTY_PRODUCT_ID_LIST = "ERROR_EMPTY_PRODUCT_ID_LIST";
	public static final String ERROR_EMPTY_USER_ID_LIST = "ERROR_EMPTY_USER_ID_LIST";
	public static final String ERROR_EMAIL_IS_SENT = "ERROR_EMAIL_IS_SENT";
	public static final String ERROR_INVALID_USER_COLLECTION_TYPE = "ERROR_INVALID_USER_COLLECTION_TYPE";
	public static final String ERROR_INVALID_DAY_RANGE = "ERROR_INVALID_DAY_RANGE";
	public static final String ERROR_INVALID_SIZE_CHART = "ERROR_INVALID_SIZE_CHART";
	public static final String ERROR_USER_UNACTIVED = "ERROR_USER_UNACTIVED";
	public static final String ERROR_USER_IS_BLOCKED = "ERROR_USER_IS_BLOCKED";
	public static final String ERROR_INVALID_RATING = "ERROR_INVALID_RATING";
	public static final String USER_IS_ACTIVED = "USER_IS_ACTIVED";
	public static final String ERROR_TOKEN_IS_EXPIRED = "ERROR_TOKEN_IS_EXPIRED";

	private static final long serialVersionUID = 1L;
	private String error;
	private String message;
	private boolean isJson;
	
	public BadRequestException() {
		super();
	}
	
	public BadRequestException(String message, boolean isJson) {
		super(message);
		this.message = message;
		this.isJson = isJson;
	}
	
	
	public BadRequestException(String error, String message, boolean isJson) {
		super(message);
		this.error = error;
		this.message = message;
		this.isJson = isJson;
	}

	public BadRequestException(String message) {
		super();
		this.message = message;
	}
}
