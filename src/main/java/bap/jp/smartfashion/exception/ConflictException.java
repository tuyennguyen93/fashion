package bap.jp.smartfashion.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
/**
 * User Exception
 *
 */
@Getter
@Setter
public class ConflictException extends Exception {

	public static final String ERROR_PRODUCT_RATING_REPLY_EXISTED = "ERROR_PRODUCT_RATING_REPLY_EXISTED";
	public static final String ERROR_EMAIL_EXISTED = "ERROR_EMAIL_EXISTED";
	public static final String ERROR_TAX_NUMBER_EXISTED = "ERROR_TAX_NUMBER_EXISTED";
	public static final String ERROR_PASSPORT_NUMBER_EXISTED = "ERROR_PASSPORT_NUMBER_EXISTED";
	public static final String ERROR_PRODUCT_SIZE_CONFLICT = "ERROR_PRODUCT_SIZE_CONFLICT";
	public static final String ERROR_PRODUCT_COLOR_CONFLICT = "ERROR_PRODUCT_COLOR_CONFLICT";
	public static final String ERROR_USER_TYPE_DONT_CHANGE_EMAIL  = "ERROR_USER_TYPE_DONT_CHANGE_EMAIL";
	public static final String ERROR_PASSWORD_DOESNT_MATCH = "ERROR_PASSWORD_DOESNT_MATCH";
	public static final String ERROR_EMAIL_ACTIVE_USER_IS_SENT = "ERROR_EMAIL_ACTIVE_USER_IS_SENT";
	public static final String ERROR_INVALID_OLD_PASSWORD = "ERROR_INVALID_OLD_PASSWORD";
	public static final String ERROR_PRODUCT_WEARING_PURPOSE_EXITED = "ERROR_PRODUCT_WEARING_PURPOSE_EXITED";
	public static final String ERROR_PRODUCT_COLLECTION_EXITED = "ERROR_PRODUCT_COLLECTION_EXITED";
	public static final String ERROR_PRODUCT_COLOR_EXITED = "ERROR_PRODUCT_COLOR_EXITED";
	public static final String ERROR_SIZE_EXITED = "ERROR_SIZE_EXITED";
	public static final String ERROR_PRODUCT_MIXMATCH_EXITED = "ERROR_PRODUCT_MIXMATCH_EXITED";
	
	private static final long serialVersionUID = 1L;
	private String error;
	private String message;
	private HttpStatus httpStatus;
	
	public ConflictException() {
		super();
	}
	
	public ConflictException(String error, String message) {
		super(message);
		this.error = error;
		this.message = message;
	}

	public ConflictException(String message) {
		super(message);
		this.message = message;
	}
}
