package bap.jp.smartfashion.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
/**
 * NotFound Exception
 *
 */
@Getter
@Setter
public class NotFoundException extends Exception {

	public static final String ERROR_NOT_FOUND = "ERROR_NOT_FOUND";
	public static final String ERROR_SHOP_NOT_FOUND = "ERROR_SHOP_NOT_FOUND";
	public static final String ERROR_USER_NOT_FOUND = "ERROR_USER_NOT_FOUND";
	public static final String ERROR_MIX_MATCH_NOT_FOUND = "ERROR_MIX_MATCH_NOT_FOUND";
	public static final String ERROR_USER_MODEL_NOT_FOUND = "ERROR_USER_MODEL_NOT_FOUND";
	public static final String ERROR_USER_PRODUCT_NOT_FOUND = "ERROR_USER_PRODUCT_NOT_FOUND";
	public static final String ERROR_PRODUCT_NOT_FOUND = "ERROR_PRODUCT_NOT_FOUND";
	public static final String ERROR_PRODUCT_DETAIL_NOT_FOUND = "ERROR_PRODUCT_DETAIL_NOT_FOUND";
	public static final String ERROR_PRODUCT_SUB_DETAIL_NOT_FOUND = "ERROR_PRODUCT_SUB_DETAIL_NOT_FOUND";
	public static final String ERROR_PRODUCT_COLOR_NOT_FOUND = "ERROR_PRODUCT_COLOR_NOT_FOUND";
	public static final String ERROR_PRODUCT_SIZE_NOT_FOUND = "ERROR_PRODUCT_SIZE_NOT_FOUND";
	public static final String ERROR_PRODUCT_SIZE_CHART_NOT_FOUND = "ERROR_PRODUCT_SIZE_CHART_NOT_FOUND";
	public static final String ERROR_PRODUCT_TYPE_NOT_FOUND = "ERROR_PRODUCT_TYPE_NOT_FOUND";
	public static final String ERROR_PRODUCT_PHOTO_NOT_FOUND = "ERROR_PRODUCT_PHOTO_NOT_FOUND";
	public static final String ERROR_PRODUCT_CURRENCY_NOT_FOUND = "ERROR_PRODUCT_CURRENCY_NOT_FOUND";
	public static final String ERROR_STATISTIC_TIMELINE_NOT_FOUND = "ERROR_STATISTIC_TIMELINE_NOT_FOUND";
	public static final String ERROR_PRODUCT_WEARING_PURPOSE_NOT_FOUND = "ERROR_PRODUCT_WEARING_PURPOSE_NOT_FOUND";
	public static final String ERROR_PRODUCT_COLLECTION_NOT_FOUND = "ERROR_PRODUCT_COLLECTION_NOT_FOUND";
	public static final String ERROR_PRODUCT_INTERACTION_NOT_FOUND = "ERROR_PRODUCT_INTERACTION_NOT_FOUND";
	public static final String ERROR_PRODUCT_RATING_NOT_FOUND = "ERROR_PRODUCT_RATING_NOT_FOUND";
	public static final String ERROR_PRODUCT_RATING_REPLY_NOT_FOUND = "ERROR_PRODUCT_RATING_REPLY_NOT_FOUND";
	public static final String ERROR_PRODUCT_COLOR_ACTION_TYPE_NOT_FOUND = "ERROR_PRODUCT_COLOR_ACTION_TYPE_NOT_FOUND";
	public static final String ERROR_USER_MODEL_DEFAULT_NOT_FOUND = "ERROR_USER_MODEL_DEFAULT_NOT_FOUND";
	public static final String ERROR_VISITOR_TYPE_NOT_FOUND = "ERROR_VISITOR_TYPE_NOT_FOUND";
	public static final String ERROR_SHOP_PRODUCT_COLLECTION_NOT_FOUND = "ERROR_SHOP_PRODUCT_COLLECTION_NOT_FOUND";
	public static final String ERROR_EMPTY_LANG_KEY = "ERROR_EMPTY_LANG_KEY";
	
	private static final long serialVersionUID = 1L;
	private String error;
	private String message;
	private HttpStatus httpStatus;
	
	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String error, String message) {
		super(message);
		this.error = error;
		this.message = message;
	}

	public NotFoundException(String error, String message, HttpStatus httpStatus) {
		super(message);
		this.error = error;
		this.message = message;
		this.httpStatus = httpStatus;
	}
	
	public NotFoundException(String message) {
		super(message);
		this.message = message;
	}
}
