package bap.jp.smartfashion.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
/**
 * SmartFashion Exception
 *
 */
@Getter
@Setter
public class SmartFashionException extends Exception {

	public static final String ERROR_AI_KEYPOINT = "ERROR_AI_KEYPOINT";
	public static final String ERROR_AI_CONNECTION = "ERROR_AI_CONNECTION";
	public static final String ERROR_AI_CANT_TRY = "ERROR_AI_CANT_TRY";
	public static final String ERROR_SHOP_STATUS_SUSPENDED = "ERROR_SHOP_STATUS_SUSPENDED";
	public static final String ERROR_SHOP_STATUS_PENDING = "ERROR_SHOP_STATUS_PENDING";
	public static final String ERROR_SHOP_STATUS_REJECT = "ERROR_SHOP_STATUS_REJECT";
	public static final String ERROR_SHOP_STATUS_BLOCKED = "ERROR_SHOP_STATUS_BLOCKED";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int code;
	private String error;
	private String message;
	private HttpStatus httpStatus;
	
	public SmartFashionException() {
		
	}

	public SmartFashionException(int code, String error, String message, HttpStatus httpStatus) {
		super(message);
		this.code = code;
		this.error = error;
		this.message = message;
		this.httpStatus = httpStatus;
	}
	
}
