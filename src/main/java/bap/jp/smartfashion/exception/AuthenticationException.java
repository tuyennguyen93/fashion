package bap.jp.smartfashion.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
/**
 * Authentication Exception
 *
 */
@Getter
@Setter
public class AuthenticationException extends Exception {

	public static final String UNAUTHORIZED_USER_NOT_FOUND = "UNAUTHORIZED_USER_NOT_FOUND";
	public static final String UNAUTHORIZED_SHOP_NOT_FOUND = "UNAUTHORIZED_SHOP_NOT_FOUND";
	public static final String UNAUTHORIZED_USER_INACTIVE = "UNAUTHORIZED_USER_INACTIVE";
	public static final String UNAUTHORIZED_USER_BLOCKED = "UNAUTHORIZED_USER_BLOCKED";
	public static final String UNAUTHORIZED_INVALID_EMAIL_OR_PASSWORD = "UNAUTHORIZED_INVALID_EMAIL_OR_PASSWORD";
	public static final String UNAUTHORIZED_INVALID_USER_TYPE = "UNAUTHORIZED_INVALID_USER_TYPE";
	
	private static final long serialVersionUID = 1L;
	private int code;
	private String error;
	private String message;
	private HttpStatus httpStatus;
	
	public AuthenticationException() {
	}
	
	public AuthenticationException(String error, String message) {
		super(message);
		this.error = error;
		this.message = message;
	}

	public AuthenticationException(int code, String message, HttpStatus httpStatus) {
		super(message);
		this.code = code;
		this.message = message;
		this.httpStatus = httpStatus;
	}
}
