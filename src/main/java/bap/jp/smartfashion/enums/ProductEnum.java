package bap.jp.smartfashion.enums;

import bap.jp.smartfashion.common.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The class for {@link Product} with enum related to it.
 *
 */
public class ProductEnum {
	
	@Getter
	@AllArgsConstructor
	public enum ProductStatus implements CodeEnum {
		DRAFT		("DRAFT", "DRAFT"),
		ACTIVE		("ACTIVE", "ACTIVE"),
		DELETED		("DELETED", "DELETED"),
		PENDING		("PENDING", "PENDING");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum Currency implements CodeEnum {
		DOLLAR	("$", "$"),
		JPY		("¥", "¥"),
		VND		("VND", "VND");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum SizeType implements CodeEnum {
		ADULT	("ADULT", "ADULT"),
		KID		("KID", "KID");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum ProductStep implements CodeEnum {
		PRODUCT			("PRODUCT", "PRODUCT"),
		PRODUCT_DETAIL	("PRODUCT_DETAIL", "PRODUCT_DETAIL");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum ProductAction implements CodeEnum {
		NEW		("NEW", "NEW"),
		EDIT	("EDIT", "EDIT"),
		DELETE	("DELETE", "DELETE");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum ProductSort {
		NEW_ARRIVAL,
		HIGHLIGHT_PRODUCTS,
		TRIED_PRODUCTS,
		FAVORITED_PRODUCTS,
		LOWEST_TO_HIGHEST_PRICE,
		HIGHEST_TO_LOWEST_PRICE,
		SALE_PRODUCTS
	}
	
	@Getter
	@AllArgsConstructor
	public enum ProductInteraction implements CodeEnum {
		TRY_PRODUCT			("TRY", "Try product"),
		FAVORITE_PRODUCT	("FAVORITE", "Favorite product");
		
		private final String code;
		private final String display;
	}

	@Getter
	@AllArgsConstructor
	public enum Product_CommentEmotion implements CodeEnum {
		LIKE		("LIKE", "Like user's comment");

		private final String code;
		private final String display;
	}
}
