package bap.jp.smartfashion.enums;

import bap.jp.smartfashion.common.model.Product;
import bap.jp.smartfashion.common.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The class for {@link User} with enum related to it.
 *
 */
public class UserEnum {
	@Getter
	@AllArgsConstructor
	public enum Platform implements CodeEnum {
		WEB		("WEB", "Web"),
		MOBILE_IOS	("MOBILE_IOS", "MOBILE IOS"),
		MOBILE_ANDROID	("MOBILE_ANDROID", "MOBILE ANDROID");
		
		private final String code;
		private final String display;
	}
	
	/**
	 * The enum class for {@link User} type
	 *
	 */
	public enum UserTypeEnum {
		LOCAL,
		FACEBOOK,
		GOOGLE
	}
	
	/**
	 * The enum class for {@link User} type
	 *
	 */
	public enum UserStatus {
		ALL,
		ACTIVE,
		DELETED,
		PENDING
	}

	@Getter
	@AllArgsConstructor
	public enum LanguageEnum implements CodeEnum {
		ENGLISH		("en", "English"),
		VIETNAMESE	("vi", "vietnamese");

		private final String code;
		private final String display;
	}
}
