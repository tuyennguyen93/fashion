package bap.jp.smartfashion.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The class for {@link Shop} with enum related to it.
 *
 */
public class ShopEnum {
	
	@Getter
	@AllArgsConstructor
	public enum VisitorType implements CodeEnum {
		NEW_VISITOR		("NEW_VISITOR", "New visitor"),
		RETURN_VISITOR	("RETURN_VISITOR", "Return visitor");
		
		private final String code;
		private final String display;
	}
	
	@Getter
	@AllArgsConstructor
	public enum Timeline {
		HOUR, DAY, WEEK, MONTH, YEAR
	}
	
	@Getter
	@AllArgsConstructor
	public enum ShopStatus implements CodeEnum {
		ACTIVE		("ACTIVE", "Active"),
		PENDING		("PENDING", "Pending"),
		REJECTED	("REJECTED", "Rejected"),
		BLOCKED		("BLOCKED", "Blocked"),
		SUSPENDED	("SUSPENDED", "Suspended");
		
		private final String code;
		private final String display;
	}
}
