package bap.jp.smartfashion.enums;
/**
 * Code Enum
 *
 */
public interface CodeEnum {
	String getCode();
	String getDisplay();
}
