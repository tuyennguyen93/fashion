package bap.jp.smartfashion.enums;
import bap.jp.smartfashion.common.model.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The class for {@link Role} with enum related to it.
 *
 */
public class RoleEnum {
	
	/**
	 * Application role
	 *
	 */
	public enum ApplicationRole {
		ROLE_ADMIN,
		ROLE_SHOP,
		ROLE_USER,
		ROLE_ANONYMOUS,
		ROLE_SUPER_ADMIN;
	}
	
	/**
	 * Action role
	 *
	 */
	@Getter
	@AllArgsConstructor
	public enum AuthorityAction implements CodeEnum {
		
		ACTION_VIEW		("VIEW", "View"),
		ACTION_CREATE	("CREATE", "Create"),
		ACTION_EDIT		("EDIT", "Edit"),
		ACTION_DELETE	("DELETE", "Delete"),
		ACTION_ALL		("ALL", "All");

		private final String code;
		private final String display;
	}
	/**
	 * Function Name
	 *
	 */
	@Getter
	@AllArgsConstructor
	public enum FunctionName implements CodeEnum {
		
		FUNCTION_USER			("USER", "User function"),
		FUNCTION_ROLE			("ROLE", "Role function"),
		FUNCTION_SHOP			("SHOP", "Shop function"),
		FUNCTION_PRODUCT		("PRODUCT", "Product function"),
		FUNCTION_CUSTOMER_CARE	("CUSTOMER_CARE", "Customer care function");
	
		private final String code;
		private final String display;
	}
	
	/**
	 * Role type
	 *
	 */
	@Getter
	@AllArgsConstructor
	public enum RoleType implements CodeEnum {
		
		APP_ROLE	("APP_ROLE", "App Role"),
		FUNC_ROLE	("FUNC_ROLE", "Function Role");

		private final String code;
		private final String display;
	}
	
}
