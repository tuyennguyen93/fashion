package bap.jp.smartfashion.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * The class for token with enum related to it.
 *
 */
@Getter
@AllArgsConstructor
public enum TokenEnum {
	/**
	 * <p>Token use for create user</p>
	 * code: Token type</br>
	 * value: Token time(<b>Minutes</b>)
	 */
	TOKEN_CREATE_USER	(0, 10),
	/**
	 * <p>Token use for reset password</p>
	 * code: Token type</br>
	 * value: Token time(<b>Minutes</b>)
	 */
	TOKEN_RESET_PASSWORD	(1, 5),
	
	/**
	 * <p>Remember me Token expired</p>
	 * code: Token type</br>
	 * value: Token time(<b>Days</b>)
	 */
	
	TOKEN_REMEMBER_ME_EXPIRED	(2, 30),
	
	/**
	 * <p>JWT Token expired</p>
	 * code: Token type</br>
	 * value: Token time(<b>Days</b>)
	 */
	TOKEN_JWT_EXPIRED	(3, 7),
	
	/**
	 * <p>Token create user shop</p>
	 * code: Token type</br>
	 * value: Token time(<b>Minutes</b>)
	 */
	TOKEN_CREATE_USER_SHOP (4, 10);
	private final int code;
	private final int value;
}