#
# Update product_photo table
#
#
ALTER TABLE product_photo
DROP COLUMN key_point;


#
# Update product_detail table
#
#
ALTER TABLE product_detail
ADD COLUMN key_point text null,
ADD COLUMN key_point_message text null;


#
# Update user_product table
#
#
ALTER TABLE user_product
MODIFY COLUMN key_point text null,
ADD COLUMN key_point_message text null;

#
# Update user_model table
#
#
ALTER TABLE user_model
MODIFY COLUMN key_point text null,
ADD COLUMN key_point_message text null;
