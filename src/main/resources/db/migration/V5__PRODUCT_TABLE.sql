#
#product_wearing_purpose TABLE
#
create table if not exists product_wearing_purpose
(
	id				int auto_increment  primary key,
	name				varchar(100) not null,
	type 			varchar(10) not null,
	product_collection_id 		int null,
  	created   			timestamp null,
    	modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;

#
#product_collection TABLE
#
create table if not exists product_collection
(
	id			int auto_increment  primary key,
	name			varchar(50) not null,
	image			varchar(255) null,
	description		text null,
  	created   		timestamp null,
    	modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_collection  unique (name)
)charset=utf8;

#
#product_type TABLE
#
create table if not exists product_type
(
	id			int auto_increment  primary key,
	name			varchar(50) not null,
	product_collection_id 	int null,
	product_type_parent_id	int null,
  	created   		timestamp null,
    	modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_type  unique (name, product_collection_id, product_type_parent_id)
)charset=utf8;

#
#product_type_count TABLE
#
create table if not exists product_type_count
(
	id			int auto_increment  primary key,
	shop_id				int(11) not null,
	product_type_id		int null,
	count			int default 0 not null,
	created   		timestamp null,
    	modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_type_count  unique (product_type_id, shop_id)
)charset=utf8;

#
#product TABLE
#
create table if not exists product
(
	id				int auto_increment  primary key,
	shop_id				int(11) not null,
	name				varchar(100) not null,
	product_code			varchar(15) null,
	product_collection_id		int(11) not null,
	currency			varchar(5)  null,
	price				decimal(10,2) not null,
	discount			decimal(4,2) null,
	guarantee			varchar(100)  null,
	status				varchar(10) not null,
	product_wearing_purpose_id 	int(11) not null,
	product_type_id			int(11) not null,
	image				varchar(255) null,
	highlight			tinyint(1) default 0 null,
	description			text null,
  	created   			timestamp null,
    	modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product on product (shop_id, product_collection_id, product_wearing_purpose_id, product_type_id);

#
#product_category TABLE
#
create table if not exists product_category
(
	id				int auto_increment  primary key,
	name				varchar(100)  not null,
	product_name			varchar(100) not null,
	shop_id				int(11)  not null,
	product_code			varchar(15) null,
	product_collection_id		int(11)  not null,
	price				decimal(10,2) not null,
	discount			decimal(4,2) null,
	guarantee			varchar(100) null,
	product_wearing_purpose_id	int(11) not null,
	product_type_id			int(11) not null,
	description			text null,
	highlight			tinyint(1) default 0 null,
  	created   			timestamp null,
    	modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_category unique (name, shop_id)
)charset=utf8;
create index index_product_category on product_category (shop_id, product_collection_id, product_wearing_purpose_id, product_type_id);

#
#product_size TABLE
#
create table if not exists product_size
(
	id		int auto_increment  primary key,
	area		varchar(10) null,
	size		varchar(10) not null,
	type 		varchar(10) not null,
  	created   	timestamp null,
    	modified  	timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_size unique (area, size)
)charset=utf8;


#
#product_sub_detail TABLE
#
create table if not exists product_sub_detail
(
	id			int auto_increment  primary key,
	product_detail_id	varchar(50) not null,
	product_size_id		varchar(10) not null,
	total			int not null,
  	created   		timestamp null,
    	modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_sub_detail unique (product_detail_id, product_size_id)
)charset=utf8;
create index index_product_sub_detail on product_sub_detail (product_detail_id, product_size_id);

#
#product_photo TABLE
#
create table if not exists product_photo
(
	id			int auto_increment  primary key,
	product_detail_id	int(11) not null,
	url_thumbnail		varchar(255) null,
	url_original		varchar(255) null,
  	created   		timestamp null,
    	modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_photo on product_photo (product_detail_id);

#
#product_color TABLE
#
create table if not exists product_color
(
	id		int auto_increment  primary key,
	name		varchar(10) not null,
	color_code	varchar(50) not null,
  	created   	timestamp null,
    	modified  	timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
	constraint unique_product_color unique (name)
)charset=utf8;

#
#product_detail TABLE
#
create table if not exists product_detail
(
	id			int auto_increment  primary key,
	product_id		int(11) not null,
	product_color_id	int(11) not null,
  	created   		timestamp null,
    	modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_detail on product_detail (product_id, product_color_id);



