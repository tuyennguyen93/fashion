#
#shop TABLE
#
create table if not exists shop
(
    id			int auto_increment primary key,
    user_id 		int not null,
    name        	varchar(50) not null,
    address     	varchar(150) null,
    email       	varchar(150) null,
    phone       	varchar(11) null,
    avatar 		varchar(255),
    background_photo 	varchar(255),
    deleted 		tinyint(1) default 0 null comment '0: Undeleted shop
					       	           1: Deleted shop',
    description    	varchar(255) null,
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop on shop (user_id);

