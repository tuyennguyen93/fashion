#
# Update product_photo table
#
#
ALTER TABLE product_photo
ADD key_point varchar(255) null;


#
# Update user_product table
#
#
ALTER TABLE user_product
ADD key_point varchar(255) null;

#
# Update user_model table
#
#
ALTER TABLE user_model
ADD key_point varchar(255) null
