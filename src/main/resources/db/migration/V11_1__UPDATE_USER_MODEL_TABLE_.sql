#
# Update user_product table
#
#
ALTER TABLE user_product
ADD name varchar(50) not null;

#
# Update user_model table
#
#
ALTER TABLE user_model
ADD name varchar(50) not null;


#
# Update product_rating_reply table
#
#
ALTER TABLE product_rating_reply
MODIFY COLUMN shop_id int null;
