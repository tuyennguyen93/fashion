#
#
# Create product_rating_photo table
#
#
create table if not exists product_rating_photo
(
    id              		int auto_increment  primary key,
    product_rating_id           int not null,
    url   	    		varchar(255),
    created         		timestamp null,
    modified        		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating_photo on product_rating_photo (product_rating_id);

#
#
# Create product_rating_reply_photo table
#
#
create table if not exists product_rating_reply_photo
(
    id              		int auto_increment  primary key,
    product_rating_reply_id     int not null,
    url   	    		varchar(255),
    created         		timestamp null,
    modified        		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating_reply_photo on product_rating_reply_photo (product_rating_reply_id);

#
#
# Create product_rating_interaction table
#
#
create table if not exists product_rating_interaction
(
    id              		int auto_increment primary key,
    user_id   	    		int not null,
    product_rating_id   	int not null,
    type	 		varchar(15),
    created         		timestamp null,
    modified        		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating_interaction on product_rating_interaction (user_id, product_rating_id);

#
#
# Create product_rating_reply_interaction table
#
#
create table if not exists product_rating_reply_interaction
(
    id              		int auto_increment primary key,
    user_id   	    		int not null,
    product_rating_reply_id   	int not null,
    type	 		varchar(15),
    created         		timestamp null,
    modified        		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating_reply_interaction on product_rating_reply_interaction (user_id, product_rating_reply_id);


#
# Update product_rating table
#
#
ALTER TABLE product_rating
ADD COLUMN like_count int not null default 0,
ADD COLUMN comment_count int not null default 0;

#
# Update product_rating_reply table
#
#
ALTER TABLE product_rating_reply
ADD COLUMN like_count int not null default 0;


