#
# create shop_product_collection TABLE
#
create table if not exists shop_product_collection
(
    id              		int auto_increment primary key,
    shop_id        		int not null,
    product_collection_id	int not null, 
    url				varchar(255) null, 
    created   			timestamp null,
    modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop_product_collection on shop_product_collection (shop_id, product_collection_id);
