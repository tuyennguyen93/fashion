#
#
# Update product_category table
#
#
ALTER TABLE product_category
ADD currency   varchar(5) null;


#
#
# Update product_mix_match_photo table
#
#
ALTER TABLE product_mix_match_photo CHANGE url url varchar(255);
