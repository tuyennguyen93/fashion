#
#
# Add column to tracking user type if signin with social(sns)
# Remove not null for password
#
#
ALTER TABLE user
MODIFY password     varchar(255) null,
ADD user_type 	    varchar(15)	default 'LOCAL'  null  comment 'LOCAL: Normal user
							    	   FACEBOOK: Facebook user
								   GOOGLE: Google user',
ADD access_token    varchar(255) null
