#
# create user_product_basket table
#
#
create table if not exists user_product_basket
(
    id              		int auto_increment primary key,
    user_id        		int not null,
    product_id			int not null,
    created   			timestamp null,
    modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_product_basket on user_product_basket (user_id, product_id);
