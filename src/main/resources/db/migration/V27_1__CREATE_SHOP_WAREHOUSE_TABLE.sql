#
# create shop_warehouse TABLE
#
create table if not exists shop_warehouse
(
    id              		int auto_increment primary key,
    shop_id        		int not null,
    warehouse_address		varchar(255) null,
    created   			timestamp null,
    modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop_warehouse on shop_warehouse (shop_id);
