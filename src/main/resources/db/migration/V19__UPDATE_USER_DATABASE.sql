#
#
# Create user_model_collection table
#
#
create table if not exists user_model_collection
(
    id              int auto_increment  primary key,
    name   	    varchar(50),
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;


#
#
# Create user_product_collection table
#
#
create table if not exists user_product_collection
(
    id              int auto_increment  primary key,
    name   	    varchar(50),
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;


#
#
# Create user_model_setting table
#
#
create table if not exists user_model_setting
(
    id              int auto_increment  primary key,
    user_id   	    int not null,
    user_model_id   int not null,
    is_default      tinyint(1) default 0,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_model_setting on user_model_setting (user_id, user_model_id);


#
#
# Update user_model table
#
#
ALTER TABLE user_model
MODIFY COLUMN user_id int null,
ADD user_model_collection_id int not null;


#
#
# Update user_product table
#
#
ALTER TABLE user_product
ADD user_product_collection_id int not null;



