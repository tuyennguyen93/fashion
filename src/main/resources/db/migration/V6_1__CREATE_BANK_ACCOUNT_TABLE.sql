
#
#
# Create user_bank_account table
#
#
create table if not exists user_bank_account
(
	id					int auto_increment primary key,
	user_id				int not null,
	account_number		varchar(14) null,
	owner_name			varchar(50) null,
	bank_name			varchar(50) null,
	bank_branch			varchar(100) null,
	created				timestamp null,
	modified 			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_bank_account on user_bank_account (user_id);