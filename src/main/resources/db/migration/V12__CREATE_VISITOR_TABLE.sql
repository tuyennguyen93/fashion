#
#
# Create shop_visitor table
#
#
create table if not exists shop_visitor
(
    id              		int auto_increment  primary key,
    shop_id   	    		int not null,
    type	 		varchar(15),
    type_count 	    		int not null default 0,
    timeline         		varchar(10) not null,
    created         		timestamp null,
    modified        		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop_visitor on shop_visitor (shop_id);


#
#
# Create shop_collection_visitor table
#
#
create table if not exists shop_collection_visitor
(
    id              		int auto_increment  primary key,
    shop_id   	    		int not null,
    product_collection_id   	int not null,
    type	 		varchar(15),
    type_count 	    		int not null default 0,
    timeline         		varchar(10) not null,
    created         		timestamp null,
    modified        		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop_collection_visitor on shop_collection_visitor (shop_id, product_collection_id);


#
# Update user_model table
#
#
ALTER TABLE user_model
MODIFY COLUMN url varchar(255) null;

#
# Update user_product table
#
#
ALTER TABLE user_product
MODIFY COLUMN url varchar(255) null;

