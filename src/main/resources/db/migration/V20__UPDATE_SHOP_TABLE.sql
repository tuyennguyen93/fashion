#
#insert value into role TABLE
#
INSERT INTO `role` 
VALUES  ('ROLE_SUPER_ADMIN');


#
#
# Update user_model table
#
#
ALTER TABLE shop
ADD status varchar(10) null,
DROP COLUMN deleted,
DROP COLUMN background_photo;


#
#
# Update user_model table
#
#
ALTER TABLE user_model
DROP COLUMN name;


#
#
# Update user_product table
#
#
ALTER TABLE user_product	
DROP COLUMN name;
