#
#
# Update user table
#
#
ALTER TABLE user
ADD gender 					tinyint(1) null,
ADD passport_number 		varchar(11) null,
ADD passport_front_side 	varchar(255) null,
ADD passport_back_side 		varchar(255) null,
ADD tax_number 				varchar(13) null;


#
#
# Update shop table
#
#
ALTER TABLE shop
DROP COLUMN address,
ADD open_time 				time null,
ADD close_time 				time null,
ADD warehouse_address 		varchar(255) null;

#
#
# Create shop_branch table
#
#
create table if not exists shop_branch
(
	id					int auto_increment primary key,
	shop_id				int not null,
	bussiness_address	varchar(255),
	created				timestamp null,
	modified 			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop_branch on shop_branch (shop_id);
