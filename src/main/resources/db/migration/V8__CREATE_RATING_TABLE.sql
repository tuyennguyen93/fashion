#
#product_rating TABLE
#

create table if not exists product_rating
(
    id              	int auto_increment primary key,
    product_id        	int not null,
    user_id		int not null, 
    rating		int null, 
    comment		text null,
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating on product_rating (product_id, user_id);

#
#product_rating_reply TABLE
#

create table if not exists product_rating_reply
(
    id              		int auto_increment primary key,
    product_rating_id       	int not null,
    shop_id			int not null,
    user_id			int not null, 
    comment			text null,
    created   			timestamp null,
    modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating_reply on product_rating_reply (product_rating_id, shop_id, user_id);

#
#product_rating_count TABLE
#

create table if not exists product_rating_count
(
    id              	int auto_increment primary key,
    product_id       	int not null,
    rating_star		int null, 
    rating_count	int default 0, 
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_rating_count on product_rating_count (product_id);

