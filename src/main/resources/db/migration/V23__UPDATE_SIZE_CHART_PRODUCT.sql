

#
# Create product_size_chart table
#
#
create table if not exists product_size_chart
(
    id              int auto_increment primary key,
    name            varchar(15) not null,
    created   timestamp null,
    modified  timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;


#
# Update product_size table
#
#
ALTER TABLE product_size
ADD product_size_chart_id int null;



#
# Update product_size_chart table
#
#
INSERT INTO product_size_chart (name) 
VALUES  ('US'),
	('UK'),
	('EURO'),
	('ASIA');


UPDATE product_size
SET product_size_chart_id = 1 where size_chart = 'US';

UPDATE product_size
SET product_size_chart_id = 2 where size_chart = 'UK';

UPDATE product_size
SET product_size_chart_id = 3 where size_chart = 'EURO';

UPDATE product_size
SET product_size_chart_id = 4 where size_chart = 'ASIA';




#
# Update product table
#
#
ALTER TABLE product
ADD product_size_chart_id int null;


