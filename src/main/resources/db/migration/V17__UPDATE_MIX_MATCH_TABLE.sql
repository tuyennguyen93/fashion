#
# Update product_mix_match table
#
#
ALTER TABLE product_mix_match
ADD priority int null;


#
# Update product_mix_match_photo table
#
#
ALTER TABLE product_mix_match_photo
DROP COLUMN priority;

ALTER TABLE product_mix_match_photo
CHANGE COLUMN `mix_match_id` `product_mix_match_id` int not null;

