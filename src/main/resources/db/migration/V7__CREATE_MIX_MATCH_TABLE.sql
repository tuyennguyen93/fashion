#
#mix_match TABLE
#
create table if not exists mix_match
(
    id             	int auto_increment primary key,
    name           	varchar(50) not null,
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;

#
#product_mix_match TABLE
#
create table if not exists product_mix_match
(
    id              	int auto_increment primary key,
    mix_match_id        int not null,
    product_id		int not null,
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_mix_match on product_mix_match (mix_match_id, product_id);

#
#product_mix_match_photo TABLE
#
create table if not exists product_mix_match_photo
(
    id              	int auto_increment primary key,
    mix_match_id        int not null,
    url			int not null, 
    priority		int null, 
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_mix_match_photo on product_mix_match_photo (mix_match_id);

#
#shop_branch TABLE - change column name
#
ALTER TABLE shop_branch CHANGE bussiness_address business_address varchar(255);

