#
# Update user table
#
#
ALTER TABLE user
ADD platform varchar(15) null;

#
#
# Create user_product table
#
#
create table if not exists user_product
(
    id              int auto_increment  primary key,
    user_id   	    int not null,
    url 	    int not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_product on user_product (user_id);


#
#
# Create user_model table
#
#
create table if not exists user_model
(
    id              int auto_increment  primary key,
    user_id   	    int not null,
    url 	    int not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_model on user_model (user_id);


#
#
# Create user_product_try_on table
#
#
create table if not exists user_product_try_on
(
    id              int auto_increment  primary key,
    user_id   	    int not null,
    user_model_id   int not null,
    user_product_id int not null,
    url 	    int not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_product_try_on on user_product_try_on (user_id, user_model_id, user_product_id);


#
#
# Create shop_product_try_on table
#
#
create table if not exists shop_product_try_on
(
    id              int auto_increment  primary key,
    user_id   	    int not null,
    user_model_id   int not null,
    product_id      int not null,
    url 	    int not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_shop_product_try_on on shop_product_try_on (user_id, user_model_id, product_id);


#
#
# Create device_token table
#
#
create table if not exists device_token
(
    id              int auto_increment  primary key,
    user_id   	    int not null,
    device_token	    varchar(50) not null, 
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_device_token on device_token (user_id);
