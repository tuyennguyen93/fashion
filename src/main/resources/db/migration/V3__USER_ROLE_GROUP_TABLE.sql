
#
#group_authority TABLE
#
create table if not exists group_authority
(
    id           int auto_increment  primary key,
    group_name   varchar(50) not null,
    group_type   varchar(50) not null,
    created      timestamp null,
    modified     timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    CONSTRAINT unique_group_authority UNIQUE (group_name, group_type)
)charset=utf8;

#
#group_authority_member TABLE
#
create table if not exists group_authority_member
(
    id              int auto_increment  primary key,
    group_authority_id   int not null,
    object_id 	    int not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    CONSTRAINT unique_group_authority_member UNIQUE (group_authority_id, object_id)
)charset=utf8;
create index index_group_authority_member on group_authority_member (group_authority_id);

#
#group_authority_detail TABLE
#
create table if not exists group_authority_detail
(
    id              int auto_increment  primary key,
    group_authority_id   int not null,
    action_name     varchar(50) not null,
    function_name   varchar(50) not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    CONSTRAINT unique_group_authority_detail UNIQUE (group_authority_id, action_name, function_name)
)charset=utf8;
create index index_group_authority_detail on group_authority_detail (group_authority_id);

#
#user_authority TABLE
#
create table if not exists user_authority
(
    id              int auto_increment  primary key,
    user_id   int not null,
    action_name     varchar(50) not null,
    function_name   varchar(50) not null,
    created         timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    CONSTRAINT unique_group_authority_detail UNIQUE (user_id, action_name, function_name)
)charset=utf8;
create index index_user_authority on user_authority (user_id);

