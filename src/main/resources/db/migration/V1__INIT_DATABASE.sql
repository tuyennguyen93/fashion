create table if not exists role
(
    name      varchar(50) not null primary key,
    constraint name_UNIQUE unique (name)
)charset=utf8;

#
#insert value into role TABLE
#
INSERT INTO `role` 
VALUES  ('ROLE_USER'),
	('ROLE_ADMIN'),
	('ROLE_SHOP');


create table if not exists user
(
    id              int auto_increment primary key,
    email           varchar(150) null,
    name            varchar(50) null,
    password        varchar(255) not null,
    phone           varchar(11) null,
    register_status tinyint(1) default 0      null comment '0: Inactive user
							    1: Actived user',
    avatar    varchar(255) null, 
    created   timestamp null,
    modified  timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;

create table if not exists user_role
(
    id        int auto_increment  primary key,
    user_id   int         not null,
    role_name varchar(50) not null,
    created   timestamp null,
    modified  timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;

