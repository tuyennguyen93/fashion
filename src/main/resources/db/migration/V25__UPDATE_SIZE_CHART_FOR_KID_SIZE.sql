#
# Update product_size table for kid
#
#

ALTER TABLE product_size
DROP INDEX unique_product_size,
DROP COLUMN size_chart;

UPDATE product_size
SET product_size_chart_id = 1 where product_size_chart_id is null and type = 'KID';
