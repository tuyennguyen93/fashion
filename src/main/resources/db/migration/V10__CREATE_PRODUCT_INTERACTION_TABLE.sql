#
# create product_interaction table
#
#
create table if not exists product_interaction
(
    id              int auto_increment primary key,
    product_id      int not null,
    action_code	    varchar(15) not null,
    action_count    int not null default 0,
    created   	    timestamp null,
    modified        timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_product_interaction on product_interaction (product_id);


#
# create user_product_interaction table
#
#
create table if not exists user_product_interaction
(
    id              		int auto_increment primary key,
    user_id        		int not null,
    product_id			int not null,
    action_code	    		varchar(15) not null,
    action_count    		int not null default 0,
    created   			timestamp null,
    modified  			timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;
create index index_user_product_interaction on user_product_interaction (user_id, product_id);
