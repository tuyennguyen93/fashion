#
# Update user table
#
#
ALTER TABLE user
MODIFY COLUMN phone varchar(15) null,
MODIFY COLUMN passport_number varchar(14) null;


#
# Update shop table
#
#
ALTER TABLE shop
MODIFY COLUMN phone varchar(15) null,
MODIFY COLUMN warehouse_address varchar(100) null,
MODIFY COLUMN description text null;


#
# Update product table
#
#
ALTER TABLE product
MODIFY COLUMN name varchar(50) not null;

#
# Update shop_branch table
#
#
ALTER TABLE shop_branch
MODIFY COLUMN business_address varchar(100) not null;



#
# Update user_bank_account table
#
#
ALTER TABLE user_bank_account
MODIFY COLUMN owner_name varchar(30) not null,
MODIFY COLUMN bank_name varchar(30) not null,
MODIFY COLUMN bank_branch varchar(30) not null;




